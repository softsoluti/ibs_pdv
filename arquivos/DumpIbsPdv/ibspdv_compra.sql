CREATE DATABASE  IF NOT EXISTS `ibspdv` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ibspdv`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: ibspdv
-- ------------------------------------------------------
-- Server version	5.6.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `compra`
--

DROP TABLE IF EXISTS `compra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compra` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tipo` int(11) NOT NULL COMMENT 'Tipo da nota 1 para entrada normal 2 para devolução',
  `numero` varchar(20) NOT NULL,
  `serie` varchar(20) NOT NULL,
  `modelo` varchar(10) NOT NULL,
  `data_nota` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data_recebimento` timestamp NULL DEFAULT NULL,
  `chave_nf` varchar(255) DEFAULT NULL,
  `numero_documento` int(11) DEFAULT NULL,
  `base_icms` double DEFAULT NULL,
  `valor_icms` double DEFAULT NULL,
  `base_icms_subs` double DEFAULT NULL,
  `valor_icms_subs` double DEFAULT NULL,
  `total_desconto` double DEFAULT NULL,
  `total_itens` double DEFAULT NULL,
  `outras_despesas` double DEFAULT NULL,
  `valor_ipi` double DEFAULT NULL,
  `frete` double DEFAULT NULL,
  `seguro` double DEFAULT NULL,
  `total_nota` double DEFAULT NULL,
  `situacao` int(11) NOT NULL,
  `fornecedor_id` int(11) unsigned DEFAULT NULL,
  `transportadora_id` int(11) unsigned DEFAULT NULL,
  `nota_fiscal_id` int(11) unsigned DEFAULT NULL,
  `natureza_operacao_id` int(11) NOT NULL,
  `data_inclusao` timestamp NULL DEFAULT NULL,
  `data_alteracao` timestamp NULL DEFAULT NULL,
  `empresa_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `numero` (`numero`,`fornecedor_id`),
  KEY `fk_compra_fornecedor1_idx` (`fornecedor_id`),
  KEY `fk_compra_transportadora1_idx` (`transportadora_id`),
  KEY `fk_compra_nota_fiscal1_idx` (`nota_fiscal_id`),
  KEY `fk_compra_natureza_operacao1_idx` (`natureza_operacao_id`),
  KEY `fk_compra_empresa1_idx` (`empresa_id`),
  CONSTRAINT `fk_compra_empresa1` FOREIGN KEY (`empresa_id`) REFERENCES `empresa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_compra_fornecedor1` FOREIGN KEY (`fornecedor_id`) REFERENCES `fornecedor` (`id`),
  CONSTRAINT `fk_compra_natureza_operacao1` FOREIGN KEY (`natureza_operacao_id`) REFERENCES `natureza_operacao` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_compra_nota_fiscal1` FOREIGN KEY (`nota_fiscal_id`) REFERENCES `nota_fiscal` (`id`),
  CONSTRAINT `fk_compra_transportadora1` FOREIGN KEY (`transportadora_id`) REFERENCES `transportadora` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 PACK_KEYS=0;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compra`
--

LOCK TABLES `compra` WRITE;
/*!40000 ALTER TABLE `compra` DISABLE KEYS */;
INSERT INTO `compra` VALUES (1,1,'1','1','1','2014-07-24 13:45:24','2014-07-09 03:00:00','111111111111111111111111111111111111111111111111',0,5000,600,0,0,15000,20000,0,0,0,0,5000,1,1,1,NULL,42,'2014-07-09 14:39:06','2014-07-09 14:39:54',1),(2,1,'2','1','1','2014-07-24 13:45:25','2014-07-16 03:00:00','',0,1000,120,0,0,4000,5000,0,0,0,0,1000,1,1,1,NULL,42,'2014-07-16 15:31:44','2014-07-16 15:33:38',1),(3,2,'1','1','1','2014-07-28 03:00:00','2014-07-28 03:00:00','',NULL,NULL,NULL,NULL,NULL,0,0.01,NULL,NULL,NULL,NULL,0.01,0,NULL,NULL,NULL,19,NULL,'2014-07-28 12:34:43',1),(4,2,'1','1','1','2014-07-28 03:00:00','2014-07-28 03:00:00','',NULL,NULL,NULL,NULL,NULL,0,0.01,NULL,NULL,NULL,NULL,0.01,0,NULL,NULL,NULL,29,NULL,'2014-07-28 13:04:10',1),(5,2,'1','1','1','2014-07-28 03:00:00','2014-07-28 03:00:00','',NULL,NULL,NULL,NULL,NULL,0,0.01,NULL,NULL,NULL,NULL,0.01,0,NULL,NULL,NULL,28,NULL,'2014-07-28 13:08:55',1),(6,2,'1','1','1','2014-07-28 03:00:00','2014-07-28 03:00:00','',NULL,NULL,NULL,NULL,NULL,0,10,NULL,NULL,NULL,NULL,10,0,NULL,NULL,NULL,28,NULL,'2014-07-28 13:10:02',1),(7,2,'2','1','1','2014-07-28 03:00:00','2014-07-28 03:00:00','',NULL,NULL,NULL,NULL,NULL,0,10,NULL,NULL,NULL,NULL,10,0,NULL,NULL,NULL,19,NULL,'2014-07-28 13:13:22',1);
/*!40000 ALTER TABLE `compra` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-03 23:35:24
