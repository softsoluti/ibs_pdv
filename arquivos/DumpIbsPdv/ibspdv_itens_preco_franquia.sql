CREATE DATABASE  IF NOT EXISTS `ibspdv` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ibspdv`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: ibspdv
-- ------------------------------------------------------
-- Server version	5.6.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `itens_preco_franquia`
--

DROP TABLE IF EXISTS `itens_preco_franquia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itens_preco_franquia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_preco_franquia_id` int(11) NOT NULL,
  `produto_id` int(10) unsigned NOT NULL,
  `plano_id` int(11) NOT NULL,
  `valor` varchar(20) DEFAULT NULL,
  `codigo_sap` varchar(45) DEFAULT NULL,
  `tipo_pagamento_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_itens_preco_franquia_tipo_preco_franquia1_idx` (`tipo_preco_franquia_id`),
  KEY `fk_itens_preco_franquia_produto1_idx` (`produto_id`),
  KEY `fk_itens_preco_franquia_plano1_idx` (`plano_id`),
  KEY `fk_itens_preco_franquia_tipo_pagamento1_idx` (`tipo_pagamento_id`),
  CONSTRAINT `fk_itens_preco_franquia_plano1` FOREIGN KEY (`plano_id`) REFERENCES `plano` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_itens_preco_franquia_produto1` FOREIGN KEY (`produto_id`) REFERENCES `produto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_itens_preco_franquia_tipo_pagamento1` FOREIGN KEY (`tipo_pagamento_id`) REFERENCES `tipo_pagamento` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_itens_preco_franquia_tipo_preco_franquia1` FOREIGN KEY (`tipo_preco_franquia_id`) REFERENCES `tipo_preco_franquia` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=267 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itens_preco_franquia`
--

LOCK TABLES `itens_preco_franquia` WRITE;
/*!40000 ALTER TABLE `itens_preco_franquia` DISABLE KEYS */;
INSERT INTO `itens_preco_franquia` VALUES (1,17,3,9,'INEXISTENTE','',1),(2,17,3,9,'INEXISTENTE','',2),(3,17,3,6,'INEXISTENTE','',1),(4,17,3,14,'238','',2),(5,17,3,8,'INEXISTENTE','',1),(6,17,3,17,'INEXISTENTE','',2),(7,17,3,41,'399','',1),(8,17,3,20,'INEXISTENTE','',2),(9,17,3,41,'399','',2),(10,17,3,43,'399','',1),(11,17,3,3,'399','',1),(12,17,3,42,'399','',2),(13,17,3,25,'499','',1),(14,17,3,16,'0','',1),(15,17,3,13,'INEXISTENTE','',2),(16,17,3,10,'0','',1),(17,17,3,20,'INEXISTENTE','',1),(18,17,3,15,'INEXISTENTE','',1),(19,17,3,18,'Só à VISTA','',2),(20,17,3,43,'399','',2),(21,17,3,8,'INEXISTENTE','',2),(22,17,3,11,'0','',1),(23,17,3,21,'399','',2),(24,17,3,21,'399','',1),(25,17,3,2,'INEXISTENTE','',1),(26,17,3,2,'INEXISTENTE','',2),(27,17,3,13,'INEXISTENTE','',1),(28,17,3,1,'399','',1),(29,17,3,4,'INEXISTENTE','',1),(30,17,3,5,'INEXISTENTE','',2),(31,17,3,12,'INEXISTENTE','',1),(32,17,3,19,'INEXISTENTE','',1),(33,17,3,6,'INEXISTENTE','',2),(34,17,3,22,'399','',2),(35,17,3,10,'Só à VISTA','',2),(36,17,3,12,'INEXISTENTE','',2),(37,17,3,24,'399','',1),(38,17,3,42,'399','',1),(39,17,3,4,'INEXISTENTE','',2),(40,17,3,19,'INEXISTENTE','',2),(41,17,3,5,'INEXISTENTE','',1),(42,17,3,11,'Só à VISTA','',2),(43,17,3,7,'0','',1),(44,17,3,7,'Só à VISTA','',2),(45,17,3,1,'399','',2),(46,17,3,16,'Só à VISTA','',2),(47,17,3,15,'INEXISTENTE','',2),(48,17,3,18,'0','',1),(49,17,3,17,'INEXISTENTE','',1),(50,17,3,3,'399','',2),(51,17,3,14,'199','',1),(52,17,3,24,'399','',2),(53,17,3,25,'598','',2),(54,17,3,22,'399','',1),(55,19,3,8,'INEXISTENTE','',2),(56,19,3,21,'399','',2),(57,19,3,6,'INEXISTENTE','',2),(58,19,3,15,'INEXISTENTE','',2),(59,19,3,41,'399','',2),(60,19,3,5,'INEXISTENTE','',1),(61,19,3,25,'499','',2),(62,19,3,24,'399','',2),(63,19,3,15,'INEXISTENTE','',1),(64,19,3,17,'INEXISTENTE','',2),(65,19,3,42,'399','',1),(66,19,3,7,'49','',1),(67,19,3,22,'399','',2),(68,19,3,19,'INEXISTENTE','',1),(69,19,3,16,'Só à VISTA','',2),(70,19,3,9,'49','',1),(71,19,3,8,'INEXISTENTE','',1),(72,19,3,43,'399','',1),(73,19,3,3,'399','',2),(74,19,3,11,'269','',2),(75,19,3,3,'399','',1),(76,19,3,20,'INEXISTENTE','',2),(77,19,3,41,'399','',1),(78,19,3,4,'INEXISTENTE','',2),(79,19,3,2,'INEXISTENTE','',1),(80,19,3,2,'INEXISTENTE','',2),(81,19,3,20,'INEXISTENTE','',1),(82,19,3,10,'INEXISTENTE','',1),(83,19,3,16,'49','',1),(84,19,3,12,'INEXISTENTE','',1),(85,19,3,19,'INEXISTENTE','',2),(86,19,3,9,'Só à VISTA','',2),(87,19,3,24,'399','',1),(88,19,3,13,'INEXISTENTE','',1),(89,19,3,1,'399','',2),(90,19,3,18,'Só à VISTA','',2),(91,19,3,22,'399','',1),(92,19,3,14,'359','',1),(93,19,3,14,'359','',2),(94,19,3,18,'49','',1),(95,19,3,25,'499','',1),(96,19,3,43,'399','',2),(97,19,3,11,'269','',1),(98,19,3,5,'INEXISTENTE','',2),(99,19,3,42,'399','',2),(100,19,3,1,'399','',1),(101,19,3,7,'Só à VISTA','',2),(102,19,3,21,'399','',1),(103,19,3,13,'INEXISTENTE','',2),(104,19,3,12,'INEXISTENTE','',2),(105,19,3,17,'INEXISTENTE','',1),(106,19,3,6,'INEXISTENTE','',1),(107,19,3,4,'INEXISTENTE','',1),(108,19,3,10,'INEXISTENTE','',2),(109,21,3,3,'399','',1),(110,21,3,13,'Só à VISTA','',2),(111,21,3,8,'0','',1),(112,21,3,6,'Só à VISTA','',2),(113,21,3,12,'0','',1),(114,21,3,17,'Só à VISTA','',2),(115,21,3,1,'399','',1),(116,21,3,42,'399','',1),(117,21,3,8,'Só à VISTA','',2),(118,21,3,18,'Só à VISTA','',2),(119,21,3,13,'0','',1),(120,21,3,1,'399','',2),(121,21,3,9,'0','',1),(122,21,3,15,'Só à VISTA','',2),(123,21,3,22,'399','',2),(124,21,3,25,'598','',2),(125,21,3,41,'399','',2),(126,21,3,11,'Só à VISTA','',2),(127,21,3,17,'0','',1),(128,21,3,2,'399','',1),(129,21,3,16,'Só à VISTA','',2),(130,21,3,7,'0','',1),(131,21,3,3,'399','',2),(132,21,3,41,'399','',1),(133,21,3,15,'0','',1),(134,21,3,11,'0','',1),(135,21,3,20,'Só à VISTA','',2),(136,21,3,4,'399','',2),(137,21,3,24,'399','',2),(138,21,3,20,'0','',1),(139,21,3,2,'399','',2),(140,21,3,21,'399','',1),(141,21,3,12,'Só à VISTA','',2),(142,21,3,4,'399','',1),(143,21,3,9,'Só à VISTA','',2),(144,21,3,21,'399','',2),(145,21,3,14,'238','',2),(146,21,3,7,'Só à VISTA','',2),(147,21,3,5,'Só à VISTA','',2),(148,21,3,43,'399','',2),(149,21,3,10,'0','',1),(150,21,3,18,'0','',1),(151,21,3,25,'499','',1),(152,21,3,22,'399','',1),(153,21,3,19,'Só à VISTA','',2),(154,21,3,24,'399','',1),(155,21,3,5,'0','',1),(156,21,3,6,'0','',1),(157,21,3,16,'0','',1),(158,21,3,43,'399','',1),(159,21,3,19,'0','',1),(160,21,3,14,'199','',1),(161,21,3,42,'399','',2),(162,21,3,10,'Só à VISTA','',2),(163,22,3,11,'Só à VISTA','',2),(164,22,3,8,'INEXISTENTE','',1),(165,22,3,6,'INEXISTENTE','',1),(166,22,3,43,'399','',1),(167,22,3,16,'10','',1),(168,22,3,41,'399','',2),(169,22,3,22,'399','',2),(170,22,3,18,'10','',1),(171,22,3,1,'399','',2),(172,22,3,17,'INEXISTENTE','',1),(173,22,3,4,'INEXISTENTE','',1),(174,22,3,19,'INEXISTENTE','',2),(175,22,3,5,'INEXISTENTE','',1),(176,22,3,14,'199','',1),(177,22,3,21,'399','',1),(178,22,3,12,'INEXISTENTE','',2),(179,22,3,20,'INEXISTENTE','',2),(180,22,3,1,'399','',1),(181,22,3,15,'INEXISTENTE','',1),(182,22,3,2,'INEXISTENTE','',2),(183,22,3,41,'399','',1),(184,22,3,13,'INEXISTENTE','',1),(185,22,3,42,'399','',1),(186,22,3,24,'399','',2),(187,22,3,10,'INEXISTENTE','',1),(188,22,3,2,'INEXISTENTE','',1),(189,22,3,25,'499','',1),(190,22,3,9,'Só à VISTA','',2),(191,22,3,42,'399','',2),(192,22,3,20,'INEXISTENTE','',1),(193,22,3,24,'399','',1),(194,22,3,3,'399','',2),(195,22,3,25,'499','',2),(196,22,3,43,'399','',2),(197,22,3,7,'Só à VISTA','',2),(198,22,3,3,'399','',1),(199,22,3,14,'199','',2),(200,22,3,5,'INEXISTENTE','',2),(201,22,3,11,'10','',1),(202,22,3,7,'10','',1),(203,22,3,8,'INEXISTENTE','',2),(204,22,3,4,'INEXISTENTE','',2),(205,22,3,10,'INEXISTENTE','',2),(206,22,3,16,'Só à VISTA','',2),(207,22,3,12,'INEXISTENTE','',1),(208,22,3,6,'INEXISTENTE','',2),(209,22,3,9,'10','',1),(210,22,3,13,'INEXISTENTE','',2),(211,22,3,19,'INEXISTENTE','',1),(212,22,3,21,'399','',2),(213,22,3,17,'INEXISTENTE','',2),(214,22,3,18,'Só à VISTA','',2),(215,22,3,22,'399','',1),(216,22,3,15,'INEXISTENTE','',2),(217,23,3,24,'454','',2),(218,23,3,17,'Só à VISTA','',2),(219,23,3,18,'Só à VISTA','',2),(220,23,3,10,'Só à VISTA','',2),(221,23,3,14,'238','',2),(222,23,3,6,'0','',1),(223,23,3,7,'0','',1),(224,23,3,19,'0','',1),(225,23,3,12,'0','',1),(226,23,3,11,'Só à VISTA','',2),(227,23,3,18,'0','',1),(228,23,3,20,'Só à VISTA','',2),(229,23,3,5,'Só à VISTA','',2),(230,23,3,2,'319','',1),(231,23,3,7,'Só à VISTA','',2),(232,23,3,4,'289','',1),(233,23,3,9,'Só à VISTA','',2),(234,23,3,25,'598','',2),(235,23,3,20,'0','',1),(236,23,3,24,'379','',1),(237,23,3,21,'399','',2),(238,23,3,13,'Só à VISTA','',2),(239,23,3,5,'0','',1),(240,23,3,9,'0','',1),(241,23,3,8,'Só à VISTA','',2),(242,23,3,2,'382','',2),(243,23,3,17,'0','',1),(244,23,3,22,'399','',2),(245,23,3,1,'399','',1),(246,23,3,1,'399','',2),(247,23,3,12,'Só à VISTA','',2),(248,23,3,6,'Só à VISTA','',2),(249,23,3,8,'0','',1),(250,23,3,13,'0','',1),(251,23,3,19,'Só à VISTA','',2),(252,23,3,21,'399','',1),(253,23,3,22,'399','',1),(254,23,3,11,'0','',1),(255,23,3,15,'0','',1),(256,23,3,16,'Só à VISTA','',2),(257,23,3,25,'499','',1),(258,23,3,3,'442','',2),(259,23,3,15,'Só à VISTA','',2),(260,23,3,3,'369','',1),(261,23,3,10,'0','',1),(262,23,3,4,'346','',2),(263,23,3,14,'199','',1),(264,23,3,16,'0','',1),(265,28,3,25,'549','',2),(266,28,3,25,'549','',1);
/*!40000 ALTER TABLE `itens_preco_franquia` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-03 23:35:19
