CREATE DATABASE  IF NOT EXISTS `ibspdv` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ibspdv`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: ibspdv
-- ------------------------------------------------------
-- Server version	5.6.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `caixa`
--

DROP TABLE IF EXISTS `caixa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `caixa` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `aberto` bit(1) NOT NULL,
  `data_abertura` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data_fechamento` timestamp NULL DEFAULT NULL,
  `valor_abertura` double NOT NULL,
  `ecf_id` int(11) unsigned DEFAULT NULL,
  `operador_id` int(11) unsigned NOT NULL,
  `numero_caixa` int(11) NOT NULL,
  `empresa_id` int(11) unsigned NOT NULL,
  `operador_id1` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_caixa_ecf1_idx` (`ecf_id`),
  KEY `fk_caixa_operador1_idx` (`operador_id`),
  KEY `fk_caixa_empresa1_idx` (`empresa_id`),
  KEY `fk_caixa_operador2_idx` (`operador_id1`),
  CONSTRAINT `fk_caixa_ecf1` FOREIGN KEY (`ecf_id`) REFERENCES `ecf` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_caixa_empresa1` FOREIGN KEY (`empresa_id`) REFERENCES `empresa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_caixa_operador1` FOREIGN KEY (`operador_id`) REFERENCES `operador` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_caixa_operador2` FOREIGN KEY (`operador_id1`) REFERENCES `operador` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 PACK_KEYS=0;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `caixa`
--

LOCK TABLES `caixa` WRITE;
/*!40000 ALTER TABLE `caixa` DISABLE KEYS */;
INSERT INTO `caixa` VALUES (4,'\0','2014-07-09 14:33:49','2014-07-11 16:21:34',0,NULL,2,1,1,NULL),(10,'\0','2014-07-11 16:30:40','2014-07-15 18:41:10',10,1,6,1,1,NULL),(11,'\0','2014-07-15 18:43:10','2014-07-16 14:53:47',10,1,7,1,1,NULL),(13,'\0','2014-07-16 14:54:17','2014-07-22 15:48:28',10,1,8,1,1,NULL),(14,'','2014-07-22 15:48:33',NULL,0,NULL,9,1,1,NULL);
/*!40000 ALTER TABLE `caixa` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-03 23:35:25
