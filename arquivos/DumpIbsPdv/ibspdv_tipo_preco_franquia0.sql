CREATE DATABASE  IF NOT EXISTS `ibspdv` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ibspdv`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: ibspdv
-- ------------------------------------------------------
-- Server version	5.6.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tipo_preco_franquia`
--

DROP TABLE IF EXISTS `tipo_preco_franquia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_preco_franquia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `preco_franquia_id` int(11) unsigned DEFAULT NULL,
  `descricao` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tipo_preco_franquia_preco_franquia1_idx` (`preco_franquia_id`),
  CONSTRAINT `fk_tipo_preco_franquia_preco_franquia1` FOREIGN KEY (`preco_franquia_id`) REFERENCES `preco_franquia` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_preco_franquia`
--

LOCK TABLES `tipo_preco_franquia` WRITE;
/*!40000 ALTER TABLE `tipo_preco_franquia` DISABLE KEYS */;
INSERT INTO `tipo_preco_franquia` VALUES (15,2,'Aparelhos Consumo'),(16,2,'Banda Larga Consumo'),(17,2,'TVEN Aparelhos Consumo'),(18,2,'TVEN Banda Larga Consumo'),(19,2,'LOL Aparelhos Consumo'),(20,2,'LOL Banda Larga Consumo'),(21,2,'Aparelhos Portabilidade'),(22,2,'LOL Aparelhos Portabilidade'),(23,2,'Aparelhos Clientes da Base'),(24,2,'Banda Larga Clientes da Base'),(25,2,'Delivery Aparelhos'),(26,2,'Delivery Banda Larga'),(27,2,'Fora de Linha'),(28,2,'Faturamento');
/*!40000 ALTER TABLE `tipo_preco_franquia` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-05 15:57:27
