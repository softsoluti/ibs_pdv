CREATE DATABASE  IF NOT EXISTS `ibspdv` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ibspdv`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: ibspdv
-- ------------------------------------------------------
-- Server version	5.6.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ativo` bit(1) NOT NULL,
  `regional` varchar(2) DEFAULT NULL,
  `codigo_aa` varchar(4) DEFAULT NULL,
  `pessoa_juridica_id` int(11) unsigned DEFAULT NULL,
  `franquia_id` int(11) unsigned DEFAULT NULL,
  `data_inclusao` timestamp NULL DEFAULT NULL,
  `data_alteracao` timestamp NULL DEFAULT NULL,
  `observacao` varchar(100) DEFAULT NULL,
  `classificacao_id` int(11) unsigned DEFAULT NULL,
  `crt_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_empresa_pessoa_juridica1_idx` (`pessoa_juridica_id`),
  KEY `fk_empresa_franquia1_idx` (`franquia_id`),
  KEY `fk_empresa_classificacao1_idx` (`classificacao_id`),
  KEY `fk_empresa_crt1_idx` (`crt_id`),
  CONSTRAINT `fk_empresa_classificacao1` FOREIGN KEY (`classificacao_id`) REFERENCES `classificacao` (`id`),
  CONSTRAINT `fk_empresa_crt1` FOREIGN KEY (`crt_id`) REFERENCES `crt` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_empresa_franquia1` FOREIGN KEY (`franquia_id`) REFERENCES `franquia` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_empresa_pessoa_juridica1` FOREIGN KEY (`pessoa_juridica_id`) REFERENCES `pessoa_juridica` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 PACK_KEYS=0;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa`
--

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
INSERT INTO `empresa` VALUES (1,'','CO','AAAA',1,NULL,'2014-07-07 17:53:10','2014-07-29 14:07:28','',1,2);
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-05 15:57:25
