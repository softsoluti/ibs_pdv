CREATE DATABASE  IF NOT EXISTS `ibspdv` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ibspdv`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: ibspdv
-- ------------------------------------------------------
-- Server version	5.6.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `reducao_z`
--

DROP TABLE IF EXISTS `reducao_z`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reducao_z` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `crz` int(11) DEFAULT NULL,
  `coo_ini` int(11) DEFAULT NULL,
  `coo_fin` int(11) DEFAULT NULL,
  `cro` int(11) DEFAULT NULL,
  `movimento` timestamp NULL DEFAULT NULL,
  `emissao` timestamp NULL DEFAULT NULL,
  `bruto` double DEFAULT NULL,
  `gt` double DEFAULT NULL,
  `issqn` bit(1) DEFAULT NULL,
  `gnf` int(11) DEFAULT NULL,
  `ccf` int(11) DEFAULT NULL,
  `cfd` int(11) DEFAULT NULL,
  `cdc` int(11) DEFAULT NULL,
  `grg` int(11) DEFAULT NULL,
  `nfc` int(11) DEFAULT NULL,
  `ccdc` int(11) DEFAULT NULL,
  `ecf_id` int(11) unsigned NOT NULL,
  `caixa_id` int(11) unsigned NOT NULL,
  `usuario_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_reducao_z_ecf1_idx` (`ecf_id`),
  KEY `fk_reducao_z_caixa1_idx` (`caixa_id`),
  KEY `fk_reducao_z_usuario1_idx` (`usuario_id`),
  CONSTRAINT `fk_reducao_z_caixa1` FOREIGN KEY (`caixa_id`) REFERENCES `caixa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_reducao_z_ecf1` FOREIGN KEY (`ecf_id`) REFERENCES `ecf` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_reducao_z_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reducao_z`
--

LOCK TABLES `reducao_z` WRITE;
/*!40000 ALTER TABLE `reducao_z` DISABLE KEYS */;
INSERT INTO `reducao_z` VALUES (1,22,1,970,2,'2014-07-11 03:00:00','2014-07-15 18:41:10',0,3045692.86,'\0',329,546,0,0,63,0,0,1,10,2),(2,23,971,973,2,'2014-07-15 03:00:00','2014-07-16 14:53:47',0,3045692.86,'\0',330,546,0,0,63,0,0,1,11,2);
/*!40000 ALTER TABLE `reducao_z` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-03 23:35:23
