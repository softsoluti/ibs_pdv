CREATE DATABASE  IF NOT EXISTS `ibspdv` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ibspdv`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: ibspdv
-- ------------------------------------------------------
-- Server version	5.6.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `imposto`
--

DROP TABLE IF EXISTS `imposto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imposto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ativo` bit(1) NOT NULL,
  `descricao` varchar(45) NOT NULL,
  `cst_icms` int(11) DEFAULT NULL,
  `cst_pis` int(11) DEFAULT NULL,
  `cst_cofins` int(11) DEFAULT NULL,
  `aliquota_icms` double DEFAULT NULL,
  `aliquota_icms_st` double DEFAULT NULL,
  `iva` double DEFAULT NULL,
  `aliquota_pis` double DEFAULT NULL,
  `aliquota_cofins` double DEFAULT NULL,
  `data_inclusao` timestamp NULL DEFAULT NULL,
  `data_alteracao` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_icms_cst1_idx` (`cst_icms`),
  KEY `fk_imposto_cst1_idx` (`cst_pis`),
  KEY `fk_imposto_cst2_idx` (`cst_cofins`),
  CONSTRAINT `fk_icms_cst1` FOREIGN KEY (`cst_icms`) REFERENCES `cst` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_imposto_cst1` FOREIGN KEY (`cst_pis`) REFERENCES `cst` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_imposto_cst2` FOREIGN KEY (`cst_cofins`) REFERENCES `cst` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imposto`
--

LOCK TABLES `imposto` WRITE;
/*!40000 ALTER TABLE `imposto` DISABLE KEYS */;
INSERT INTO `imposto` VALUES (1,'','ICMS 12%',1,1,1,12,0,0,0,0,'2014-07-09 14:20:41','2014-08-04 20:52:56');
/*!40000 ALTER TABLE `imposto` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-05 15:57:30
