package acbr.acbrnfemonitor;

import acbr.acbrmonitor.*;

/**
 * Enumerador que define os comando do ECF no ACBrMonitor.
 * <code>Link para as definicoes http://acbr.sourceforge.net/drupal/?q=node/24</code>
 *
 * @author Pedro H. Lira
 */
public enum EComandoNFE {

    NFE_StatusServico,
    /*Verifica o Status do Serviço dos WebServices da Receita.
     Exemplo de Resposta:
     OK: Serviço em Operação
     [STATUS]
     Versao=1.07
     TpAmb=2
     VerAplic=SP_NFE_PL_005c
     CStat=107
     XMotivo=Serviço em Operação
     CUF=35
     DhRecbto=2009-03-25T08:44:20
     TMed=1
     DhRetorno=
     XObs=

     ERRO: WebService Consulta Status serviço:
     - Inativo ou Inoperante tente novamente.
     - Synapse TCP/IP Socket error 11001: Host not found*/
    NFE_AssinarNFe,
    /*
     Assina uma NFe. Arquivo assinado será salvo na pasta configurada na aba WebService na opção "Salvar Arquivos de Envio e Resposta".

     NFE.AssinarNFe( cArquivo )

     Parâmetros
     cArquivo - Caminho do arquivo a ser assinado.

     Exemplo:
     NFE.ASSINARNFE("c:\35XXXXXXXXXXXXXXXX550010000000050000000058-nfe.xml")

     Exemplo de Resposta:
     OK:
     */
    NFE_ValidarNFe,
    /*
     Valida arquivo da NFe. Arquivo deve estar assinado.

     NFE.ValidaNFe( cArquivo )

     Parâmetros
     cArquivo - Caminho do arquivo a ser validado.

     Exemplo: 
     NFE.VALIDARNFE("c:\35XXXXXXXXXXXXXXXX550010000000050000000058-nfe.xml")

     Exemplo de Resposta:
     OK: 
     ERRO: 1871 - Element '{http://www.portalfiscal.inf.br/nfe}NFe': Missing child element(s). Expected is ( {http://www.w3.org/2000/09/xmldsig#}Signature ).
     */
    NFE_ConsultarNFe,
    /*	
     Consulta uma NFe.

     NFE.ConsultarNFe( cChaveNFe )

     Parâmetros
     cChaveNFe - Chave da NFe a ser consultada.

     Exemplo:
     NFE.CONSULTARNFE("35XXXXXXXXXXXXXXXX550010000000050000000058")

     Exemplo de Resposta:
     OK: Autorizado o uso da NF-e
     [CONSULTA]
     Versao=1.07
     Id=
     TpAmb=2
     VerAplic=SP_NFE_PL_005c
     CStat=100
     XMotivo=Autorizado o uso da NF-e
     CUF=35
     ChNFe=350XXXXXXXXXXXXXXXX550010000000220000000229
     DhRecbto=2009-03-24T20:19:38
     NProt=1350900073XXXXX
     DigVal=OZl9uzQ+JVFPxNuqBJ/ex7TTxhc=
     */
    NFE_CancelarNFe,
    /*
     Cancela um NFe já autorizada.

     NFE.CancelarNFe( cChaveNFe, cJustificativa )

     Exemplo:
     NFE.CANCELARNFE("35XXXXXXXXXXXXXXXX550010000000050000000058","Teste de Cancelamento")

     Exemplo de Resposta:
     OK: Cancelamento de NF-e homologado
     [CANCELAMENTO]
     Versao=1.07
     Id=
     TpAmb=2
     VerAplic=SP_NFE_PL_005c
     CStat=101
     XMotivo=Cancelamento de NF-e homologado
     CUF=35
     ChNFe=350XXXXXXXXXXXXXXXXX550010000000220000000229
     DhRecbto=2009-03-25T08:50:50
     NProt=2009-03-25T08:50:50

     ERRO: Informar uma Justificativa para cancelar a Nota Fiscal Eletronica
     */
    NFE_ImprimirDanfe,
    /*	
     Imprime o DANFe baseado num arquivo XML de NFe.

     NFE.ImprimirDanfe( cArquivo )

     Parâmetros
     cArquivo - Caminho do arquivo a ser validado.

     Exemplo:
     NFE.IMPRIMIRDANFE("c:\35XXXXXXXXXXXXXXXX550010000000050000000058-nfe.xml")

     Exemplo de Resposta:
     OK:
     */
    NFE_InutilizarNFe,
    /*
     Inutiliza uma faixa de numeração de NFe.

     NFE.InutilizarNFe( cCNPJ, cJustificativa, nAno, nModelo, nSerie, nNumInicial, nNumFinal)

     Parâmetros
     cCNPJ - CNPJ do contribuinte
     cJustificativa - Justificativa para inutilização
     nAno - Ano que foi inutilizado a numeração
     nModelo - Modelo da Nota Fiscal
     nSerie - Série da Nota Fiscal
     nNumInicial - Número Inicial a ser inutilizado
     nNumFinal - Número Final a ser inutilizado

     Exemplo:
     NFE.INUTILIZARNFE( "XXXXXXXXXXXXX", "Teste de inutilizacao", 08, 55, 1, 1, 4)

     Exemplo de Resposta:
     OK: Inutilização de número homologado
     [INUTILIZACAO]
     Versao=1.07
     Id=
     TpAmb=2
     VerAplic=SP_NFE_PL_005c
     CStat=102
     XMotivo=Inutilização de número homologado
     CUF=35
     DhRecbto=2009-03-25T08:59:31
     NProt=13508000XXXXXXX

     ERRO: Rejeição: Uma NF-e da faixa já está inutilizada na Base de dados da SEFAZ
     */
    NFE_EnviarNFe,
    /*	
     Envia NFe.

     NFE.EnviarNFe( cArquivo, nLote,[ nAssina, nImprime ] )

     Parâmetros
     cArquivo -Caminho do arquivo a ser enviado.
     nLote - Número do Lote
     nAssina - Coloque 0 se não quiser que o componente assine o arquivo. - Parâmetro Opcional
     nImprime - Coloque 1 se quiser que o DANFe seja impresso logo após a autorização - Parâmetro Opcional

     Exemplo:
     NFE.ENVIARNFE("c:\35XXXXXXXXXXXXXXXX550010000000050000000058-nfe.xml",1,1,1)

     Exemplo de Resposta:
     OK: Lote recebido com sucesso
     [ENVIO]
     Versao=1.10
     TpAmb=2
     VerAplic=SP_NFE_PL_005c
     CStat=103
     XMotivo=Lote recebido com sucesso
     CUF=35
     NRec=35000000XXXXXXX
     DhRecbto=2009-03-25T09:25:04
     TMed=1
     Lote processado
     [RETORNO]
     Versao=1.10
     TpAmb=2
     VerAplic=SP_NFE_PL_005c
     NRec=35000000XXXXXXX
     CStat=104
     XMotivo=Lote processado
     CUF=35
     [NFE28]
     Versao=1.07
     Id=
     TpAmb=2
     VerAplic=SP_NFE_PL_005c
     CStat=100
     XMotivo=Autorizado o uso da NF-e
     CUF=35
     ChNFe=350XXXXXXXXXXXXXXXXX550010000000280000000281
     DhRecbto=2009-03-25T09:25:04
     NProt=13509000XXXXXXX
     DigVal=UNTpscTtknjN5UOBUHa9PZPHJnE=

     ERRO: Rejeição: Falha no Schema XML da NFe
     */
    NFE_CriarNFe,
    /*	
     Cria XML da NFe baseado em um arquivo INI.

     NFE.CriarNFe( cTextoIni,[ nRetornaXML])

     Parâmetros
     cTextoIni - Texto no formato de arquivo INI com informações da NFe.
     nRetornaXML - Coloque o valor 1 se quiser que o ACBrNFeMonitor retorne além do Path de onde o arquivo foi criado, o XML gerado. Por default não retorna o XML.

     O conteúdo do parâmetro cTextoIni, deve possuir o seguinte formato:

     [Identificacao]
     NaturezaOperacao=VENDA PRODUCAO DO ESTAB.
     Modelo=55
     Serie=1
     Codigo=18
     Numero=18
     Serie=1
     Emissao=24/03/2009
     Saida=24/03/2009
     Tipo=1
     FormaPag=0
     Finalidade=0
     [Emitente]
     CNPJ=
     IE=
     Razao=
     Fantasia=
     Fone=
     CEP=
     Logradouro=
     Numero=
     Complemento=
     Bairro=
     CidadeCod=
     Cidade=
     UF=
     *PaisCod= 
     *Pais=
     [Destinatario]
     CNPJ=
     IE=
     *ISUF=
     NomeRazao=
     Fone=
     CEP=
     Logradouro=
     Numero=
     Complemento=
     Bairro=
     CidadeCod=
     Cidade=
     UF=
     *PaisCod=
     *Pais=
     [Produto001]
     CFOP=
     Codigo=
     Descricao=
     *EAN=
     *NCM=
     Unidade=
     Quantidade=
     ValorUnitario=
     ValorTotal=
     *ValorDesconto=
     *NumeroDI=
     *DataRegistroDI=
     *LocalDesembaraco=
     *UFDesembaraco=
     *DataDesembaraco=
     *CodigoExportador=
     *[LADI001001]
     *NumeroAdicao=
     *CodigoFrabricante=
     *DescontoADI
     [ICMS001]
     CST=00
     *Origem=
     *Modalidade=
     *ValorBase=
     *Aliquota=
     *Valor=
     *ModalidadeST=
     *PercentualMargemST=
     *PercentualReducaoST=
     *ValorBaseST=
     *AliquotaST=
     *ValorST=
     *PercentualReducao=
     *[IPI001]
     *CST=
     *ClasseEnquadramento=
     *CNPJProdutor=
     *CodigoSeloIPI=
     *QuantidadeSelos=
     *CodigoEnquadramento=
     *ValorBase=
     *Quantidade=
     *ValorUnidade=
     *Aliquota=
     *Valor
     *[II001]
     *ValorBase=
     *ValorDespAduaneiras=
     *ValorII=
     *ValorIOF=
     *[PIS001]
     *CST=
     *ValorBase=
     *Aliquota=
     *Valor=
     *Quantidade=
     *TipoCalculo=
     *[PISST001]
     *ValorBase=
     *AliquotaPerc=
     *Quantidade=
     *AliquotaValor=
     *ValorPISST=
     *[COFINS001]
     *CST=
     *ValorBase=
     *Aliquota=
     *Valor=
     *TipoCalculo=
     *Quantidade=
     *[COFINSST001]
     *ValorBase=
     *AliquotaPerc=
     *Quantidade=
     *AliquotaValor=
     *ValorCOFINSST=
     [Total]
     BaseICMS=
     ValorICMS=
     ValorProduto=
     *BaseICMSSubstituicao=
     *ValorICMSSubstituicao=
     *ValorFrete=
     *ValorSeguro=
     *ValorDesconto=
     *ValorII=
     *ValorIPI=
     *ValorPIS=
     *ValorCOFINS=
     *ValorOutrasDespesas=
     ValorNota=
     *[Transportador]
     *FretePorConta=
     *CnpjCpf=
     *NomeRazao=
     *IE=
     *Endereco=
     *Cidade=
     *UF=
     *ValorServico=
     *ValorBase=
     *Aliquota=
     *Valor=
     *CFOP=
     *CidadeCod=
     *Placa=
     *UFPlaca=
     *RNTC=
     *[Volume001]
     *Quantidade=
     *Especie=
     *Marca=
     *Numeracao=
     *PesoLiquido=
     *PesoBruto=
     *[Fatura]
     *Numero=
     *ValorOriginal=
     *ValorDesconto=
     *ValorLiquido=
     *[Duplicata001]
     *Numero=
     *DataVencimento=
     *Valor=
     *[DadosAdicionais]
     *Complemento=
     *[InfAdic001]
     *Campo=
     *Texto=

     Observações
     - Campos com * são opcionais 
     - Algumas grupos podem ser repetidos. Ex: Para incluir dois produtos, existirão uma chave Produto001 e Produto002 e assim sucessivamente. As chaves de imposto (ICMS,IPI,COFINS, etc) devem ter o memo número do produto, ou seja, o ICMS da chave Produto0002 deve ser ICMS002. 
     - Acentos podem causar problemas na criação do NFe. Ao tentar criar uma NFe, caso receba o erro "Unable to Parse" verifique se não existem caracteres acentuados nos campos.

     Exemplo:
     NFE.CriarNFe("[Identificacao]
     NaturezaOperacao=VENDA PRODUCAO DO ESTAB.
     Modelo=55
     Serie=1
     Codigo=19
     Numero=19
     Serie=1
     Emissao=24/03/2009
     Saida=24/03/2009
     Tipo=1
     FormaPag=0
     [Emitente]
     CNPJ=XXXXXXXXXXXXXX
     IE=XXXXXXXXXXXX
     Razao=RAZAO SOCIAL DO DESTINATARIO LTDA EPP
     Fantasia=NOME FANTASIA
     Fone=1532599600
     CEP=18270000
     Logradouro=Rua Onze de Agosto
     Numero=1000
     Complemento=
     Bairro=Centro
     CidadeCod=3554003
     Cidade=Tatui
     UF=SP
     [Destinatario]
     CNPJ=05481336000137
     IE=687138770110
     ISUF=
     NomeRazao=D.J. COM. E LOCACAO DE SOFTWARES LTDA - ME
     Fone=1532599600
     CEP=18270410
     Logradouro=Praca Anita Costa
     Numero=0034
     Complemento=
     Bairro=Centro
     CidadeCod=3554003
     Cidade=Tatui
     UF=SP
     [Produto001]
     CFOP=5101
     Codigo=67
     Descricao=ALHO 400 G
     Unidade=KG
     Quantidade=100
     ValorUnitario=10
     ValorTotal=100
     [ICMS001]
     CST=00
     ValorBase=1000
     Aliquota=18
     Valor=180
     [Total]
     BaseICMS=1000
     ValorICMS=180
     ValorProduto=1000
     ValorNota=1000" )

     Exemplo de Resposta:
     OK: NFe criada em: C:\ACBrNFeMonitor\logs\35XXXXXXXXXXXXXXXX550010000000190000000193-nfe.xml
     */
    NFE_CriarEnviarNFe,
    /*	
     Cria o XML da NFe e já envia para o fisco.

     NFE.CriarEnviarNFe( cTextoIni, nNumLote,[ nImprimirDanfe ])

     Parâmetros
     cTextoIni - Texto no formato de arquivo INI com informações da NFe.
     nImprimirDanfe - Coloque 1 se quiser que o DANFe seja impresso logo após a autorização - Parâmetro Opcional

     Exemplo:
     NFe.CriarEnviarNFe("[Identificacao]
     NaturezaOperacao=VENDA PRODUCAO DO ESTAB.
     Modelo=55
     Serie=1
     Codigo=21
     Numero=21
     Serie=1
     Emissao=24/03/2009
     Saida=24/03/2009
     Tipo=1
     FormaPag=0
     [Emitente]
     CNPJ=XXXXXXXXXXXXXX
     IE=XXXXXXXXXXXX
     Razao=RAZAO SOCIAL DO DESTINATARIO LTDA EPP
     Fantasia=NOME FANTASIA
     Fone=1532599600
     CEP=18270000
     Logradouro=Rua Onze de Agosto
     Numero=1000
     Complemento=
     Bairro=Centro
     CidadeCod=3554003
     Cidade=TatuI
     UF=SP
     [Destinatario]
     CNPJ=05481336000137
     IE=687138770110
     ISUF=
     NomeRazao=D.J. COM. E LOCACAO DE SOFTWARES LTDA - ME
     Fone=1532599600
     CEP=18270410
     Logradouro=Praca Anita Costa
     Numero=0034
     Complemento=
     Bairro=Centro
     CidadeCod=3554003
     Cidade=TatuI
     UF=SP
     [Produto001]
     CFOP=5101
     Codigo=67
     Descricao=ALHO 400 G
     Unidade=KG
     Quantidade=100
     ValorUnitario=10
     ValorTotal=100
     [ICMS001]
     CST=00
     ValorBase=1000
     Aliquota=18
     Valor=180
     [Total]
     BaseICMS=1000
     ValorICMS=180
     ValorProduto=1000
     ValorNota=1000"
     ,2,0)

     Exemplo de Resposta:
     OK: Lote recebido com sucesso
     [ENVIO]
     Versao=1.10
     TpAmb=2
     VerAplic=SP_NFE_PL_005c
     CStat=103
     XMotivo=Lote recebido com sucesso
     CUF=35
     NRec=35000000XXXXXXX
     DhRecbto=2009-03-25T09:16:58
     TMed=1
     Lote processado
     [RETORNO]
     Versao=1.10
     TpAmb=2
     VerAplic=SP_NFE_PL_005c
     NRec=35000000XXXXXXX
     CStat=104
     XMotivo=Lote processado
     CUF=35
     [NFE26]
     Versao=1.07
     Id=
     TpAmb=2
     VerAplic=SP_NFE_PL_005c
     CStat=100
     XMotivo=Autorizado o uso da NF-e
     CUF=35
     ChNFe=350XXXXXXXXXXXXXXXXX550010000000260000000260
     DhRecbto=2009-03-25T09:16:59
     NProt=13509000XXXXXXX
     DigVal=CiHlzOOqJMNbnh8WGkY19pddhB8=

     Observações
     Será retornado uma chave NFE + o número da NFe enviada. O recebimento da reposta OK: Lote recebido com sucesso não significa que a nota foi autorizada, apenas que o lote foi recebido. Verifique os campos CStat e XMotivo da chave NFE para ter certeza que a nota foi autorizada
     */
    NFE_EnviarEmail;
    /*
     Envia uma NFe por email. Além do XML é possível enviar o DANFe em formato PDF. O Assunto do email que será enviado e a mensagem deste email, deverá ser configurado no ACBrNFeMonitor

     NFE.EnviarEmail( cPara, cArquivo, [ nEnviaDanfePDF ] )

     Parâmetros
     cPara - Email do destinatário. 
     cArquivo - Caminho do arquivo a ser enviado.
     nEnviaDanfePDF - Coloque 1 se quiser que o DANFe seja enviado em formato PDF- Parâmetro Opcional

     Exemplo:
     NFE.ENVIAREMAIL("andre@djsystem.com.br","c:\35XXXXXXXXXXXXXXXX550010000000050000000058-nfe.xml","1")

     Exemplo de Resposta:
     OK: Email enviado com sucesso
     */

    @Override
    public String toString() {
        return super.toString().replaceFirst("_", ".");
    }
;

}
