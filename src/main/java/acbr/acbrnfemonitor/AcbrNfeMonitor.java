package acbr.acbrnfemonitor;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

import org.apache.log4j.Logger;

/**
 * Classe que representa o ECF no sistema e todas suas funcionalidiades.
 *
 * @author Pedro H. Lira
 */
public final class AcbrNfeMonitor {

    /**
     * Numero de colunas.
     */
    public static int COL;
    /**
     * Linha Simples [---].
     */
    public static String LS;
    /**
     * Linha Dupla [===].
     */
    public static String LD;
    /**
     * Separador de linhas
     */
    public static final String SL = "|";
    public static final String CR = "\n";
    public static final String CRLF = "\n\r";
    /**
     * Expressao OK.
     */
    public static final String OK = "OK";
    /**
     * Expressao ERRO.
     */
    public static final String ERRO = "ERRO";
    private static Logger log;
    private static Socket acbr;
    private static PrintWriter saida = null;
    private static DataInputStream entrada = null;

    public static void desconectar() throws IOException {
        acbr.close();
    }

    /**
     * Construtor padrao.
     */
    private AcbrNfeMonitor() {
    }

    /**
     * Metodo que realiza a conexao com o ECF.
     *
     * @param servidor a URL ou IP do servidor.
     * @param porta o numero da porta de comunicacao. milisegundos.
     * @throws Exception dispara um excecao caso nao cosiga.
     */
    public static void conectar(String servidor, int porta) throws Exception {
        log = Logger.getLogger(AcbrNfeMonitor.class);

        try {
            InetAddress ip = InetAddress.getByName(servidor);
            SocketAddress url = new InetSocketAddress(ip, porta);
            acbr = new Socket();
            acbr.connect(url, 10000);
            saida = new PrintWriter(acbr.getOutputStream());
            entrada = new DataInputStream(acbr.getInputStream());
            lerDados();
        } catch (IOException ex) {
            log.error("Nao foi possivel se conectar ao ACBrNfeMonitor", ex);
            throw new Exception("Verifique se as configurações estão corretas e se o AcbrNfeMonitor está ativo no sistema.");
        }
    }

    /**
     * Metodo que envia um comando ao ACBr que repassa para a ECF.
     *
     * @param comando um EComandoECF que representa um comando aceito.
     * @param parametros um sequencia de parametros, opcionais usado somente em
     * alguns comandos.
     * @return
     */
    public static String[] enviar(EComandoNFE comando, String... parametros) {
        return enviar(comando.toString(), parametros);
    }

    /**
     * Metodo que envia um comando ao ECF que repassa para a ECF.
     *
     * @param comando uma String que representa um comando aceito.
     * @param parametros um sequencia de parametros, opcionais usado somente em
     * alguns comandos.
     */
    private static String[] enviar(String comando, String... parametros) {
        String[] resp = new String[]{"", ""};
        StringBuilder acao = new StringBuilder(comando);

        if (parametros != null && parametros.length > 0) {
            acao.append("(");
            for (String param : parametros) {
                acao.append(param).append(",");
            }
            acao.deleteCharAt(acao.length() - 1).append(")");
        }

        try {
            saida.print(acao.toString() + "\r\n.\r\n");
            saida.flush();

            String dados = lerDados();
            if ("".equals(dados)) {
                resp[0] = OK;
            } else if (dados.contains(":")) {
                String[] ret = dados.split(":", 2);
                resp[0] = ret[0].trim();
                resp[1] = ret[1].trim();
            } else {
                resp[0] = OK;
                resp[1] = dados.trim();
            }
        } catch (Exception ex) {
            log.error("Nao foi possivel enviar ou receber comando ao ACBRNFE" + acao.toString(), ex);
            resp[0] = ERRO;
            resp[1] = "Nao foi possivel enviar ou receber comando ao ACBRNFE";
        }
        return resp;
    }

    /**
     * Metodo que faz a leitura do retorno do ECF.
     *
     * @return uma String da resposta.
     */
    private static String lerDados() {
        StringBuilder sb = new StringBuilder();
        try {
            byte b;
            while ((b = (byte) entrada.read()) != 3) {
                sb.append(new String(new byte[]{b}));
            }
            return sb.toString();
        } catch (IOException ex) {
            return ERRO + ":" + ex.getMessage();
        }
    }

    /**
     * Metodo que faz a validacao se o sistema consegue ativar a ECF.
     *
     * @throws Exception dispara uma excecao caso nao consiga ativar.
     */
    /*public static void ativar() throws Exception {
     String[] resp = enviar(EComandoECF.NFE_Ativar);
     if (AcbrNfeMonitor.ERRO.equals(resp[0])) {
     throw new Exception(resp[1]);
     }
     // pega as colunas e forma as linhas
     resp = enviar(EComandoECF.NFE_Colunas);
     AcbrNfeMonitor.COL = AcbrNfeMonitor.OK.equals(resp[0]) ? Integer.valueOf(resp[1]) : 48;
     StringBuilder sb1 = new StringBuilder();
     StringBuilder sb2 = new StringBuilder();

     for (int i = 0; i < AcbrNfeMonitor.COL; i++) {
     sb1.append("-");
     sb2.append("=");
     }

     AcbrNfeMonitor.LS = sb1.toString();
     AcbrNfeMonitor.LD = sb2.toString();
     }
     */
    /**
     * Metodo que desativa o acesso ao ECF
     */
    /* public static void desativar() {
     enviar(EComandoECF.ECF_Desativar);
     }
     */
    /**
     * Metodo que retorna o estado do ECF.
     *
     * @return o tipo do estado do ECF.
     * @throws jACBrFramework.ACBrException
     */
    /* public static EEstadoECF validarEstado() throws ACBrException {
     String[] resp = enviar(EComandoECF.ECF_Estado);
     if (AcbrNfeMonitor.OK.equals(resp[0])) {
     return EEstadoECF.valueOf(resp[1]);
     } else {
     throw new ACBrException(resp[1]);
     }
     }*/
}
