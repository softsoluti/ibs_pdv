package acbr.acbrmonitor;

import br.com.cresceritsolutions.ibspdv.controller.ECF;
import jACBrFramework.ACBrException;
import jACBrFramework.OleDate;
import jACBrFramework.interop.ACBrECFInterop;
import jACBrFramework.serial.ecf.Aliquota;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;


import org.apache.log4j.Logger;

/**
 * Classe que representa o ECF no sistema e todas suas funcionalidiades.
 *
 * @author Pedro H. Lira
 */
public final class AcbrMonitor {

    /**
     * Numero de colunas.
     */
    public static int COL;
    /**
     * Linha Simples [---].
     */
    public static String LS;
    /**
     * Linha Dupla [===].
     */
    public static String LD;
    /**
     * Separador de linhas
     */
    public static final String SL = "|";
    public static final String CR = "\n";
    public static final String CRLF = "\n\r";
    /**
     * Expressao OK.
     */
    public static final String OK = "OK";
    /**
     * Expressao ERRO.
     */
    public static final String ERRO = "ERRO";
    private static Logger log;
    private static Socket acbr;
    private static PrintWriter saida = null;
    private static DataInputStream entrada = null;

    
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Properties">
    public static String getModelo() throws ACBrException {

        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_Modelo);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
        return resp[1];
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Properties">
    public static String getMFAdicional() throws ACBrException {

        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_MFAdicional);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
        return resp[1];
    }

    public static String getNumSerie() throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_NumSerie);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
        return resp[1];
    }

    public static void leituraX() throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_LeituraX);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
    }

    public static void cancelaCupom() throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_CancelaCupom);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
    }

    public static Object getGrandeTotal() throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_GrandeTotal);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
        return Double.parseDouble(resp[1].replace(".", "").replace(",", "."));
    }

    public static String getNumCupom() throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_NumCupom);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
        return resp[1];
    }

    public static String getNumGNF() throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_NumGNF);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
        return resp[1];
    }

    public static String getNumGRG() throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_NumGRG);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
        return resp[1];
    }

    public static String getNumCDC() throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_NumCDC);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
        return resp[1];
    }

    public static Date getDataHora() throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_DataHora);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
        try {
            return new SimpleDateFormat("dd/MM/yy HH:mm:ss").parse(resp[1]);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(AcbrMonitor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void corrigeEstadoErro(String reducaoZ) throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_CorrigeEstadoErro, String.valueOf(reducaoZ));
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
    }

    public static void abreCupom(String cnpj, String nome, String endereco) throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_AbreCupom, cnpj, nome, endereco);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
    }

    public static void vendeItem(String codigo, String descricao, String aliqIcms, String qtd, String valorUnit ,String descAcres, String unidade, String tipoAcre, String descAcre, String codDepartamento) throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_VendeItem, codigo, descricao, aliqIcms, qtd, valorUnit ,descAcres, unidade, tipoAcre, descAcre, codDepartamento);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
    }

    public static void reducaoZ() throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_ReducaoZ);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
    }

    public static Integer getEstado() throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_Estado);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
        return Integer.parseInt(resp[1]);
    }

    public static void pafMF_LMFC_Impressao(String param1, String param2) throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_PafMf_Lmfc_Impressao, param1, param2);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
    }

    public static String dadosUltimaReducaoZ() throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_DadosUltimaReducaoZ);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
        return resp[1];
    }

    public static void sangria(String valor, String obs , String descricaoCnf, String descricaoFpg ) throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_Sangria, valor, obs , descricaoCnf, descricaoFpg);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
    }

    public static void suprimento(String valor, String obs , String descricaoCnf, String descricaoFpg ) throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_Suprimento, valor, obs , descricaoCnf, descricaoFpg);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
    }

    public static void lerTotaisRelatoriosGerenciais() throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_LerTotaisRelatoriosGerenciais);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
    }

    public static void abreRelatorioGerencial(String indice) throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_AbreRelatorioGerencial, indice);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
    }

    public static void fechaRelatorio() throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_FechaRelatorio);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
    }

    public static void subtotalizaCupom(String descontoAcrescimo, String mensagemRodape) throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_SubtotalizaCupom, descontoAcrescimo, mensagemRodape);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
    }

    public static void efetuaPagamento(String codFormaPagamento, String Valor, String obs, String imprimeVinculado) throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_EfetuaPagamento,codFormaPagamento, Valor, obs, imprimeVinculado);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
    }

    public static void fechaCupom(String mensagemRodape) throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_FechaCupom, mensagemRodape);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
    }

    public static void abreCupomVinculado(String coo, String codFormaPag, String valor) throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_AbreCupomVinculado, coo, codFormaPag, valor);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
    }

    public static void cortaPapel(String corteParcial) throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_CortaPapel,corteParcial);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
    }

    public static void pulaLinhas(String numLinhas) throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_PulaLinhas,numLinhas);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
    }

    public static void linhaCupomVinculado(String linha) throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_LinhaCupomVinculado, linha);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
    }

    public static void linhaRelatorioGerencial(String param1) throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_LinhaRelatorioGerencial, param1);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
    }

    public static void leituraMemoriaFiscal(String ini, String fim, String simplificada) throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_LeituraMemoriaFiscal, ini, fim, simplificada);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
    }

    public static void pafMF_LMFC_Espelho(String ini, String fin,String arquivo) throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_PafMf_Lmfc_Espelho, ini, fin,arquivo);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
    }

    public static void pafMF_LMFC_Cotepe1704(String ini, String fin,String arquivo) throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_PafMf_Lmfc_Cotepe1704, ini, fin,arquivo);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
    }

    public static void pafMF_LMFS_Espelho(String ini, String fin,String arquivo) throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_PafMf_Lmfs_Espelho, ini, fin,arquivo);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
    }

    public static void pafMF_MFD_Espelho(String ini, String fin,String arquivo) throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_PafMf_Mfd_Espelho, ini, fin,arquivo);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
    }

    public static void abreGaveta() throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_AbreGaveta);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
    }

    public static String getNumCCF() throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_NumCCF);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
        return resp[1];
    }

    public static String getNumVersao() throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_NumVersao);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
        return resp[1];
    }

    public static Date getDataHoraSB() throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_DataHoraSB);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
        try {
            return new SimpleDateFormat("dd/MM/yy HH:mm:ss").parse(resp[1]);
        } catch (ParseException ex) {
            throw new ACBrException("ECF - Erro ao formatar data hora sb!");
        }
    }

    public static Integer getPosicaoAliquota(Double valor,String tipo) throws ACBrException {
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_AchaIcmsAliquota,String.valueOf(valor),tipo);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new ACBrException(resp[1]);
        }
        String retorno = "";
        
        if(resp[1].contains("T")){
            retorno = resp[1].split("T")[0];
        }else if(resp[1].contains("S")){
            retorno = resp[1].split("S")[0];
        }
        return Integer.parseInt(retorno.trim());
    }


    


    /**
     * Construtor padrao.
     */
    private AcbrMonitor() {
    }

    /**
     * Metodo que realiza a conexao com o ECF.
     *
     * @param servidor a URL ou IP do servidor.
     * @param porta o numero da porta de comunicacao. milisegundos.
     * @throws Exception dispara um excecao caso nao cosiga.
     */
    public static void conectar(String servidor, int porta) throws Exception {
        log = Logger.getLogger(AcbrMonitor.class);

        try {
            InetAddress ip = InetAddress.getByName(servidor);
            SocketAddress url = new InetSocketAddress(ip, porta);
            acbr = new Socket();
            acbr.connect(url, 10000);
            saida = new PrintWriter(acbr.getOutputStream());
            entrada = new DataInputStream(acbr.getInputStream());

            lerDados();
        } catch (IOException ex) {
            log.error("Nao foi possivel se conectar ao ACBrMonitor", ex);
            throw new Exception("Verifique se as configurações estão corretas e se está ativo no sistema.");
        }
    }

    
    /**
     * Metodo que envia um comando ao ACBr que repassa para a ECF.
     *
     * @param comando um EComandoECF que representa um comando aceito.
     * @param parametros um sequencia de parametros, opcionais usado somente em
     * alguns comandos.
     * @return
     */
    public static String[] enviar(EComandoECF comando, String... parametros) {
        return enviar(comando.toString(), parametros);
    }

    /**
     * Metodo que envia um comando ao ECF que repassa para a ECF.
     *
     * @param comando uma String que representa um comando aceito.
     * @param parametros um sequencia de parametros, opcionais usado somente em
     * alguns comandos.
     */
    private static String[] enviar(String comando, String... parametros) {
        String[] resp = new String[]{"", ""};
        StringBuilder acao = new StringBuilder(comando);

        if (parametros != null && parametros.length > 0) {
            acao.append("(");
            for (String param : parametros) {
                acao.append(param).append(",");
            }
            acao.deleteCharAt(acao.length() - 1).append(")");
        }

        try {
            saida.print(acao.toString() + "\r\n.\r\n");
            saida.flush();

            String dados = lerDados();
            if ("".equals(dados)) {
                resp[0] = OK;
            } else if (dados.contains(":")) {
                String[] ret = dados.split(":", 2);
                resp[0] = ret[0].trim();
                resp[1] = ret[1].trim();
            } else {
                resp[0] = OK;
                resp[1] = dados.trim();
            }
        } catch (Exception ex) {
            log.error("Nao foi possivel enviar ou receber comando ao ECF" + acao.toString(), ex);
            resp[0] = ERRO;
            resp[1] = "Nao foi possivel enviar ou receber comando ao ECF";
        }
        return resp;
    }

    /**
     * Metodo que faz a leitura do retorno do ECF.
     *
     * @return uma String da resposta.
     */
    private static String lerDados() {
        StringBuilder sb = new StringBuilder();
        try {
            byte b;
            while ((b = (byte) entrada.read()) != 3) {
                sb.append(new String(new byte[]{b}));
            }
            return sb.toString();
        } catch (IOException ex) {
            return ERRO + ":" + ex.getMessage();
        }
    }

    /**
     * Metodo que faz a validacao se o sistema consegue ativar a ECF.
     *
     * @throws Exception dispara uma excecao caso nao consiga ativar.
     */
    public static void ativar() throws Exception {
        String[] resp = enviar(EComandoECF.ECF_Ativar);
        if (AcbrMonitor.ERRO.equals(resp[0])) {
            throw new Exception(resp[1]);
        }
        // pega as colunas e forma as linhas
        resp = enviar(EComandoECF.ECF_Colunas);
        AcbrMonitor.COL = AcbrMonitor.OK.equals(resp[0]) ? Integer.valueOf(resp[1]) : 48;
        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();

        for (int i = 0; i < AcbrMonitor.COL; i++) {
            sb1.append("-");
            sb2.append("=");
        }

        AcbrMonitor.LS = sb1.toString();
        AcbrMonitor.LD = sb2.toString();
    }

    /**
     * Metodo que desativa o acesso ao ECF
     */
    public static void desativar() {
        enviar(EComandoECF.ECF_Desativar);
    }

    /**
     * Metodo que retorna o estado do ECF.
     *
     * @return o tipo do estado do ECF.
     * @throws jACBrFramework.ACBrException
     */
    public static EEstadoECF validarEstado() throws ACBrException {
        String[] resp = enviar(EComandoECF.ECF_Estado);
        if (AcbrMonitor.OK.equals(resp[0])) {
            return EEstadoECF.valueOf(resp[1]);
        } else {
            throw new ACBrException(resp[1]);
        }
    }
}
