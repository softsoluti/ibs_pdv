package acbr;

import br.com.cresceritsolutions.ibspdv.util.Chaves;
import com.sun.jna.ptr.IntByReference;
import jACBrFramework.ACBrClass;
import jACBrFramework.ACBrEventListener;
import jACBrFramework.ACBrException;
import jACBrFramework.ChaveEventObject;
import jACBrFramework.interop.ACBrEADInterop;
import java.nio.ByteBuffer;

/**
 *
 * @author Valde
 */
public class ACBrEAD extends ACBrClass {

    public ACBrEAD() throws ACBrException {
    }

    @Override
    protected void onInitialize() throws ACBrException {
        IntByReference handle = new IntByReference();
        int ret = ACBrEADInterop.INSTANCE.EAD_Create(handle);
        checkResult(ret);

        setHandle(handle.getValue());
    }

    @Override
    protected void onFinalize() throws ACBrException {
        int ret = ACBrEADInterop.INSTANCE.EAD_Destroy(getHandle());
        checkResult(ret);

        setHandle(0);
    }

    @Override
    protected void checkResult(int result) throws ACBrException {
        switch (result) {
            case -1:
                String message;

                int LEN = 1024;
                ByteBuffer buffer = ByteBuffer.allocate(LEN);
                int ret = ACBrEADInterop.INSTANCE.EAD_GetUltimoErro(getHandle(), buffer, LEN);
                message = fromUTF8(buffer, ret);
                throw new ACBrException(message);

            case -2:
                throw new ACBrException("ACBr EAD não inicializado.");
        }
    }

    public void addOnGetChavePrivada(ACBrEventListener<ChaveEventObject> listener) {
        if (!hasListeners("OnGetChavePrivada")) {
            ACBrEADInterop.INSTANCE.EAD_SetOnGetChavePrivada(getHandle(), new ACBrEADInterop.GetChavePrivadaCallback() {
                @Override
                public String invoke() {
                    return OnGetChavePrivada();
                }
            });
        }

        addListener("OnGetChavePrivada", listener);
    }

    public void removeOnPAFGetKeyRSA(ACBrEventListener<ChaveEventObject> listener) {
        removeListener("OnGetChavePrivada", listener);

        if (!hasListeners("OnGetChavePrivada")) {
            ACBrEADInterop.INSTANCE.EAD_SetOnGetChavePrivada(getHandle(), null);
        }
    }

    private String OnGetChavePrivada() {
        ChaveEventObject e = new ChaveEventObject(this, Chaves.privada);
        notifyListeners("OnGetChavePrivada", e);
        return e.getChave();
    }

    public void addOnGetChavePublica(ACBrEventListener<ChaveEventObject> listener) {
        if (!hasListeners("OnGetChavePublica")) {
            ACBrEADInterop.INSTANCE.EAD_SetOnGetChavePublica(getHandle(), new ACBrEADInterop.GetChavePublicaCallback() {
                @Override
                public String invoke() {
                    return OnGetChavePublica();
                }
            });
        }

        addListener("OnGetChavePublica", listener);
    }

    private void removeOnGetChavePublica(ACBrEventListener<ChaveEventObject> listener) {
        removeListener("OnGetChavePublica", listener);

        if (!hasListeners("OnGetChavePublica")) {
            ACBrEADInterop.INSTANCE.EAD_SetOnGetChavePublica(getHandle(), null);
        }
    }

    public String OnGetChavePublica() {
        ChaveEventObject e = new ChaveEventObject(this, Chaves.publica);
        notifyListeners("OnGetChavePublica", e);

        return e.getChave();
    }

    // Funções
    /**
     * @return String
     * @param arquivo local para gravar o arquivo
     * @param remove boolean
     * @throws ACBrException
     */
    public String assinarArquivoComEAD(String nomeArquivo, boolean remove) throws ACBrException {
        
        ByteBuffer returnBuffer = ByteBuffer.allocate(STR_BUFFER_LEN);
        int ret = ACBrEADInterop.INSTANCE.EAD_AssinarArquivoComEAD(getHandle(), toUTF8(nomeArquivo), remove, returnBuffer, STR_BUFFER_LEN);
        checkResult(ret);
        return fromUTF8(returnBuffer, ret);
    }

    public String calcularChavePublica() throws ACBrException {
        ByteBuffer returnBuffer = ByteBuffer.allocate(STR_BUFFER_LEN);
        int ret = ACBrEADInterop.INSTANCE.EAD_CalcularChavePublica(getHandle(), returnBuffer, STR_BUFFER_LEN);
        checkResult(ret);

        return fromUTF8(returnBuffer, ret);
    }

    public void calcularEADArquivo(String nomeArquivo) throws ACBrException {
        ByteBuffer returnBuffer = ByteBuffer.allocate(STR_BUFFER_LEN);
        int ret = ACBrEADInterop.INSTANCE.EAD_CalcularEADArquivo(getHandle(), toUTF8(nomeArquivo), returnBuffer, STR_BUFFER_LEN);
        checkResult(ret);
    }

    //int EAD_GerarChaves(int eadHandle, ByteBuffer ChavePUB, ByteBuffer ChavePRI, int bufferLen);
    //int EAD_GerarXMLeECFc(int eadHandle, String NomeSH, String PathArquivo);
    //int EAD_MD5FromFile(int eadHandle, String Arquivo, ByteBuffer MD5, int bufferLen);
    //int EAD_MD5FromString(int eadHandle, String AString, ByteBuffer MD5, int bufferLen);
    public void gerarChaves() throws ACBrException {
        ByteBuffer returnBufferPri = ByteBuffer.allocate(STR_BUFFER_LEN);
        ByteBuffer returnBufferPub = ByteBuffer.allocate(STR_BUFFER_LEN);
        int ret = ACBrEADInterop.INSTANCE.EAD_GerarChaves(getHandle(), returnBufferPub, returnBufferPri, STR_BUFFER_LEN);
        System.out.println("Chave pri : " + new String(returnBufferPri.array()));
        System.out.println("Chave pub : " + new String(returnBufferPub.array()));
        checkResult(ret);
    }

    public void gerarXMLeECFc(String nomeSH, String pathArquivo) throws ACBrException {
        int ret = ACBrEADInterop.INSTANCE.EAD_GerarXMLeECFc(getHandle(), toUTF8(nomeSH), toUTF8(pathArquivo));
        checkResult(ret);
    }

    public void removeEADArquivo(String nomeArquivo) throws ACBrException {
        int ret = ACBrEADInterop.INSTANCE.EAD_RemoveEADArquivo(getHandle(), toUTF8(nomeArquivo));
        checkResult(ret);
    }

    public boolean verificaEAD(String ead) throws ACBrException {
        int ret = ACBrEADInterop.INSTANCE.EAD_VerificarEAD(getHandle(), toUTF8(ead));
        checkResult(ret);
        return ret != 0;
    }

    public boolean verificaEADArquivo(String nomeArquivo) throws ACBrException {
        int ret = ACBrEADInterop.INSTANCE.EAD_VerificarEADArquivo(getHandle(), toUTF8(nomeArquivo));
        checkResult(ret);
        return ret != 0;
    }

}
