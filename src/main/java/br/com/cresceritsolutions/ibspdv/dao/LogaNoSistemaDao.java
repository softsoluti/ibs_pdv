/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.dao;

import br.com.cresceritsolutions.ibspdv.models.hb.Empresa;
import br.com.cresceritsolutions.ibspdv.models.hb.Operador;
import br.com.cresceritsolutions.ibspdv.models.hb.TipoUsuario;
import br.com.cresceritsolutions.ibspdv.models.hb.Usuario;
import br.com.cresceritsolutions.ibspdv.util.CriptografiaUtil;
import br.com.cresceritsolutions.ibspdv.util.ExibirParaUsuario;
import br.com.cresceritsolutions.ibspdv.util.HibernateUtil;
import br.com.cresceritsolutions.ibspdv.util.IbsException;
import br.com.cresceritsolutions.ibspdv.util.VariaveisDoSistema;
import java.awt.Component;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import org.hibernate.HibernateException;

/**
 *
 * @author gutemberg
 */
public class LogaNoSistemaDao {

    private static LogaNoSistemaDao me;

    public static LogaNoSistemaDao getInstance() {
        if (me == null) {
            me = new LogaNoSistemaDao();

        }
        return me;
    }

    public boolean executar(Component c, String login, String senha) throws NoSuchAlgorithmException, IbsException {
        HibernateUtil hu = HibernateUtil.getInstance();
        try {
            Usuario usuario = (Usuario) hu.getSession().createQuery("from Usuario s where s.login=:login").setString("login", login).uniqueResult();
            if (usuario != null) {
                //Seta as variaveis do sistema (Usuario,tipo TIPO USUARIO)
                VariaveisDoSistema.USUARIO = usuario;
                VariaveisDoSistema.TIPO_USUARIO = (TipoUsuario) usuario.getTipoUsuario();
                Operador operador = new Operador();
                operador.setUsuario(usuario);
                VariaveisDoSistema.OPERADOR = operador;
            }
            hu.refresh(VariaveisDoSistema.EMPRESA);
            return usuario != null && usuario.getLogin().equals(login) && CriptografiaUtil.getInstance().encryptMD5(login, senha).equals(usuario.getSenha()) && pertenceAempresa(c);
        } catch (HibernateException ex) {
            throw new IbsException("Erro ao verificar identidade na base de dados!", ex);
        }
    }

    private boolean pertenceAempresa(Component c) {
        Iterator<Empresa> lst = VariaveisDoSistema.USUARIO.getEmpresas().iterator();
        while (lst.hasNext()) {
            Empresa e = lst.next();
            if (VariaveisDoSistema.EMPRESA.equals(e)) {
                return true;
            }
        }
        ExibirParaUsuario.getInstance().atencao(c, "O usuário não pertence a essa empresa!");
        return false;
    }

}
