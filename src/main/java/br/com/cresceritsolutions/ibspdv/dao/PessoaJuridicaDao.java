/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.dao;

import br.com.cresceritsolutions.ibspdv.models.hb.PessoaJuridica;
import br.com.cresceritsolutions.ibspdv.util.HibernateUtil;
import br.com.cresceritsolutions.ibspdv.util.IbsException;
import org.hibernate.HibernateException;

/**
 *
 * @author gutemberg
 */
public class PessoaJuridicaDao {

    private static PessoaJuridicaDao me;

    public static PessoaJuridicaDao getInstance() {
        if (me == null) {
            me = new PessoaJuridicaDao();
        }
        return me;
    }

    //Salvar
    public void salvar(PessoaJuridica pessoaJuridica) throws IbsException {
        HibernateUtil hu = HibernateUtil.getInstance();
        try {
            hu.beginTransaction();
            hu.getSession().save(pessoaJuridica);
            hu.commit();
        }  catch (HibernateException ex) {
            hu.rollback();
            throw new IbsException("Erro inesperado!", ex);
        }
    }

}
