/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.dao;

import br.com.cresceritsolutions.ibspdv.models.factory.CanalVendaFactory;
import br.com.cresceritsolutions.ibspdv.models.factory.EmpresaFactory;
import br.com.cresceritsolutions.ibspdv.models.hb.Caixa;
import br.com.cresceritsolutions.ibspdv.models.hb.CanalVenda;
import br.com.cresceritsolutions.ibspdv.models.hb.Colaborador;
import br.com.cresceritsolutions.ibspdv.models.hb.Empresa;
import br.com.cresceritsolutions.ibspdv.models.hb.Usuario;
import br.com.cresceritsolutions.ibspdv.util.HibernateUtil;
import br.com.cresceritsolutions.ibspdv.util.IbsException;
import br.com.cresceritsolutions.ibspdv.util.VariaveisDoSistema;
import static br.com.cresceritsolutions.ibspdv.util.VariaveisDoSistema.AUXILIAR;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author gutemberg
 */
public class IniciaSistemaDao {

    private static IniciaSistemaDao me;
    private static String selectEmpresaCnpj = "from Empresa as e  where e.pessoaJuridica.cnpj=:cnpj";

    private IniciaSistemaDao() {
    }

    public static IniciaSistemaDao getInstance() {
        if (me == null) {
            me = new IniciaSistemaDao();
        }
        return me;
    }

    public static void executar() throws IbsException {
        HibernateUtil hu = HibernateUtil.getInstance();
        try {
            hu.beginTransaction();
            Empresa empresa = (Empresa) hu.getSession().createCriteria(Empresa.class, "empresa").createAlias("empresa.pessoaJuridica", "pessoaJuridica").add(Restrictions.eq("pessoaJuridica.cnpj", AUXILIAR.getProperty("cli.cnpj"))).uniqueResult();

            if (empresa == null) {
                empresa = EmpresaFactory.novo();
                empresa.getPessoaJuridica().getPessoa().getEndereco().setMunicipio(null);
                hu.getSession().save(empresa.getPessoaJuridica().getPessoa().getEndereco());
                hu.getSession().save(empresa.getPessoaJuridica().getPessoa());
                empresa.setDataAlteracao(new Date());
            }
            empresa.getPessoaJuridica().setRazao(AUXILIAR.getProperty("cli.razao"));
            empresa.getPessoaJuridica().setCnpj(AUXILIAR.getProperty("cli.cnpj"));
            empresa.getPessoaJuridica().setIe(AUXILIAR.getProperty("cli.ie"));
            empresa.getPessoaJuridica().setIm(AUXILIAR.getProperty("cli.im"));
            
            Usuario usuario = (Usuario) hu.getSession().createCriteria(Usuario.class,"usuario").uniqueResult();
            List<Empresa> empresas = hu.getSession().createCriteria(Empresa.class,"empresa").list();
            
            if(empresas.isEmpty()){
                usuario.getEmpresas().add(empresa);
            }
            
            hu.getSession().save(empresa.getPessoaJuridica());
            hu.getSession().save(empresa);
            hu.getSession().save(usuario);
            VariaveisDoSistema.EMPRESA = empresa;
            //Cria canal

            if (VariaveisDoSistema.CANAL_VENDA == null) {
                CanalVenda canalVenda = (CanalVenda) hu.getSession().createCriteria(CanalVenda.class).add(Restrictions.and(Restrictions.eq("empresa", VariaveisDoSistema.EMPRESA), Restrictions.like("descricao", "%PADRÃO%"))).uniqueResult();
                if (canalVenda == null) {
                    canalVenda = CanalVendaFactory.novo();
                }
                canalVenda.setDescricao("PADRÃO-LOJA");
                List<Colaborador> colaboradors = hu.getSession().createCriteria(Colaborador.class).add(Restrictions.or(Restrictions.eq("situacao", 0), Restrictions.eq("situacao", 1))).list();
                canalVenda.setColaboradors(new HashSet(colaboradors));
                if (colaboradors.size() > 0) {
                    canalVenda.setColaborador(colaboradors.get(0));
                }
                hu.getSession().refresh(VariaveisDoSistema.EMPRESA);
                canalVenda.setEndereco(VariaveisDoSistema.EMPRESA.getPessoaJuridica().getPessoa().getEndereco());
                hu.getSession().saveOrUpdate(canalVenda);
            }
            Iterator<CanalVenda> canalVendas = VariaveisDoSistema.EMPRESA.getCanalVendas().iterator();
            CanalVenda canalvenda = null;
            while (canalVendas.hasNext()) {
                CanalVenda temp = canalVendas.next();
                if (temp.getDescricao().equals("PADRÃO-LOJA")) {
                    canalvenda = temp;
                }
            }
            VariaveisDoSistema.CANAL_VENDA = canalvenda;
            List<Caixa> listCaixas = new ArrayList<>(empresa.getCaixas());
            Collections.sort(listCaixas);
            Iterator<Caixa> caixas = listCaixas.iterator();
            if (listCaixas.size() > 0) {
                VariaveisDoSistema.CAIXA = listCaixas.get(0);
            }
            while (caixas.hasNext()) {
                Caixa caixa = caixas.next();
                if (caixa.isAberto()) {
                    VariaveisDoSistema.CAIXA = caixa;
                }
            }
            hu.commit();
        } catch (HibernateException ex) {
            try{
                hu.rollback();
                throw new IbsException("Erro inesperado!\nErro Técnico: "+ex.getMessage(), ex);
            }catch(HibernateException ex2){
                throw new IbsException("Erro inesperado!\nErro Técnico: "+ex2.getMessage(), ex2);
            }            
        }
    }

}
