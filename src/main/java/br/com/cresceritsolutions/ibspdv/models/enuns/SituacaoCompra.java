/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.models.enuns;

/**
 *
 * @author Gutem
 */
public enum SituacaoCompra {

    LIBERADA("LIBERADA", 1), EM_TRANSFERENCIA("EM TRANSFERÊNCIA", 2);
    private final String valor;
    private final Integer id;

    private SituacaoCompra(String s, Integer id) {
        valor = s;
        this.id = id;
    }

    @Override
    public String toString() {
        return valor;
    }
    public Integer getId(){
        return id;
    }

}
