package br.com.cresceritsolutions.ibspdv.models.hb;
// Generated 09/02/2015 13:13:15 by Hibernate Tools 4.3.1

import java.util.HashSet;
import java.util.Set;

/**
 * Modelo generated by hbm2java
 */
public class Modelo implements java.io.Serializable, Comparable<Modelo> {

    private Integer id;
    private String codigo;
    private String descricao;
    private Set notaFiscals = new HashSet(0);

    public Modelo() {
    }

    public Modelo(String codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    public Modelo(String codigo, String descricao, Set notaFiscals) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.notaFiscals = notaFiscals;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Set getNotaFiscals() {
        return this.notaFiscals;
    }

    public void setNotaFiscals(Set notaFiscals) {
        this.notaFiscals = notaFiscals;
    }

    @Override
    public String toString() {
        return "[" + this.getCodigo() + "] - " + this.getDescricao(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int compareTo(Modelo t) {
        return this.toString().compareTo(t.toString());
    }

}
