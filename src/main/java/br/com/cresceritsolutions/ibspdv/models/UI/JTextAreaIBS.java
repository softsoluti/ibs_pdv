/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.models.UI;

import br.com.cresceritsolutions.ibspdv.util.ImagesUtil;
import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.AbstractAction;
import javax.swing.InputMap;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.JTextComponent;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;


/**
 *
 * @author Gutemberg
 */
public class JTextAreaIBS extends JTextArea {

    private static final long serialVersionUID = 1L;
    private byte maxLength = 0;

    public JTextAreaIBS(int maxLength) {
        super();
        this.maxLength = (byte) maxLength;
        this.addKeyListener(new LimitedKeyListener());
        this.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent fe) {
                JTextAreaIBS.this.selectAll();
            }
        });
        this.setFont(new FonteIBS());
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(final MouseEvent e) {
                if (e.isPopupTrigger()) {
                    final JTextComponent component = (JTextComponent) e.getComponent();
                    final JPopupMenu menu = new JPopupMenu();
                    JMenuItem item;
                    item = new JMenuItem(new DefaultEditorKit.CopyAction());
                    item.setText("Copiar");
                    item.setIcon(ImagesUtil.getInstance().getIconCopy());
                    item.setEnabled(component.getSelectionStart() != component.getSelectionEnd());
                    menu.add(item);
                    item = new JMenuItem(new DefaultEditorKit.CutAction());
                    item.setText("Recortar");
                    item.setIcon(ImagesUtil.getInstance().getIconCut());
                    item.setEnabled(component.isEditable() && component.getSelectionStart() != component.getSelectionEnd());
                    menu.add(item);
                    item = new JMenuItem(new DefaultEditorKit.PasteAction());
                    item.setText("Colar");
                    item.setIcon(ImagesUtil.getInstance().getIconPaste());
                    item.setEnabled(component.isEditable());
                    menu.add(item);
                    menu.show(e.getComponent(), e.getX(), e.getY());
                }
            }
        });
        this.getDocument().addUndoableEditListener(new JTextAreaIBS.MyUndoableEditListener());  
        configurarMapa(this);  
    }

    public void setMaxLength(int maxLength) {
        this.maxLength = (byte) maxLength;
        update();
    }

    private void update() {
        if (getText().length() > maxLength) {
            setText(getText().substring(0, maxLength));
            setCaretPosition(maxLength);
        }
    }

    @Override
    public void setText(String arg0) {
        super.setText(arg0);
        update();
    }

    @Override
    public void paste() {
        super.paste();
        update();
    }

    //Classes Internas
    private class LimitedKeyListener extends KeyAdapter {

        private boolean backspace = false;

        @Override
        public void keyPressed(KeyEvent e) {
            backspace = (e.getKeyCode() == 8);
        }

        @Override
        public void keyTyped(KeyEvent e) {
            if (!backspace
                    && getText().length() > maxLength - 1) {
                e.consume();
            }
        }
    }
    
     private UndoAction undoAction = new UndoAction();  
    private RedoAction redoAction = new RedoAction();  
    private UndoManager undo = new UndoManager();  
 
  
    private void configurarMapa(JTextAreaIBS txtField) {  
        InputMap inputMap = txtField.getInputMap();  
  
        // Ctrl-z desfaz  
        KeyStroke key = KeyStroke.getKeyStroke(KeyEvent.VK_Z, Event.CTRL_MASK);  
        inputMap.put(key, undoAction);  
  
        // Ctrl-y refaz  
        key = KeyStroke.getKeyStroke(KeyEvent.VK_Y, Event.CTRL_MASK);  
        inputMap.put(key, redoAction);  
  
    }  
      
    private class MyUndoableEditListener implements UndoableEditListener {  
  
        @Override  
        public void undoableEditHappened(UndoableEditEvent e) {  
            //Remember the edit and update the menus  
            undo.addEdit(e.getEdit());  
            undoAction.updateUndoState();  
            redoAction.updateRedoState();  
        }  
  
    }  
  
    private class UndoAction extends AbstractAction {  
  
        private UndoAction() {  
            setEnabled(false);  
        }  
  
        @Override  
        public void actionPerformed(ActionEvent e) {  
  
            try {  
                undo.undo();  
            } catch (CannotUndoException ex) {  
            }  
            updateUndoState();  
            redoAction.updateRedoState();  
  
        }  
  
        /* Controla Status do btnUndo */  
        private void updateUndoState() {  
            if (undo.canUndo()) {  
                setEnabled(true);  
            } else {  
            }  
        }  
  
    }  
  
    private class RedoAction extends AbstractAction {  
  
        private RedoAction() {  
            setEnabled(false);  
        }  
  
        @Override  
        public void actionPerformed(ActionEvent e) {  
  
            try {  
                undo.redo();  
            } catch (CannotRedoException ex) {  
            }  
            updateRedoState();  
            undoAction.updateUndoState();  
  
        }  
  
        /*Controla Status do btnRedo*/  
        private void updateRedoState() {  
            if (undo.canRedo()) {  
                setEnabled(true);  
                //   putValue(Action.NAME, undo.getRedoPresentationName());  
            } else {  
                setEnabled(false);  
                // putValue(Action.NAME, "Redo");  
            }  
        }  
  
    }  
}
