package br.com.cresceritsolutions.ibspdv.models.anexo1.v02_02.iv;

import br.com.cresceritsolutions.ibspdv.models.anexo1.Bean;



public class P2 extends Bean{//RELAÇÃO DE MERCADORIAS E SERVIÇOS
	private String cnpj;
	private String codigo;
	private String descricao;
	private String unidade;
	private String iat;
	private String ippt;
	private String situacaoTributaria;//I=ISENTO , N=NÃO TRIBUTADO , F=SUBSTITUIÇÃO TRIBUTARIA , T=TRIBUTADO PELO ICMS , S=TRIBUTADO PELO ISSQN
	private Double aliquota;
	private Double valorUnitario;
	public P2() {
		padrao = "P2";
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getUnidade() {
		return unidade;
	}
	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}
	public String getIat() {
		return iat;
	}
	public void setIat(String iat) {
		this.iat = iat;
	}
	public String getIppt() {
		return ippt;
	}
	public void setIppt(String ippt) {
		this.ippt = ippt;
	}
	public String getSituacaoTributaria() {
		return situacaoTributaria;
	}
	public void setSituacaoTributaria(String situacaoTributaria) {
		this.situacaoTributaria = situacaoTributaria;
	}
	public double getAliquota() {
		return aliquota*100;
	}
	public void setAliquota(double aliquota) {
		this.aliquota = aliquota;
	}
	public double getValorUnitario() {
		return valorUnitario*100;
	}
	public void setValorUnitario(double valorUnitario) {
		this.valorUnitario = valorUnitario;
	}
	
}
