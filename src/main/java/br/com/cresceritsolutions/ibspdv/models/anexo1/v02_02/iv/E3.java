package br.com.cresceritsolutions.ibspdv.models.anexo1.v02_02.iv;

import java.util.Date;

import br.com.cresceritsolutions.ibspdv.models.anexo1.Bean;

public class E3 extends Bean {
	//IDENTIFICAÇÃO DO ECF QUE EMITIU O DOCUMENTO BASE PARA A ATUALIZAÇÃO DO ESTQOUE
	private String numeroFabricacao;
	private String mfAdicional;
	private String tpEcf;
	private String marcaEcf;
	private String modeloEcf;
	private Date dataEstoque;
	private Date horaEstoque;
	
	public E3() {
		padrao ="E3";
	}

	public String getNumeroFabricacao() {
		return numeroFabricacao;
	}

	public void setNumeroFabricacao(String numeroFabricacao) {
		this.numeroFabricacao = numeroFabricacao;
	}

	public String getMfAdicional() {
		return mfAdicional;
	}

	public void setMfAdicional(String mfAdicional) {
		this.mfAdicional = mfAdicional;
	}

	public String getTpEcf() {
		return tpEcf;
	}

	public void setTpEcf(String tpEcf) {
		this.tpEcf = tpEcf;
	}

	public String getMarcaEcf() {
		return marcaEcf;
	}

	public void setMarcaEcf(String marcaEcf) {
		this.marcaEcf = marcaEcf;
	}

	public String getModeloEcf() {
		return modeloEcf;
	}

	public void setModeloEcf(String modeloEcf) {
		this.modeloEcf = modeloEcf;
	}

	public Date getDataEstoque() {
		return dataEstoque;
	}

	public void setDataEstoque(Date dataEstoque) {
		this.dataEstoque = dataEstoque;
	}

	public Date getHoraEstoque() {
		return horaEstoque;
	}

	public void setHoraEstoque(Date horaEstoque) {
		this.horaEstoque = horaEstoque;
	}

	
	
}
