/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.cresceritsolutions.ibspdv.models.factory;

import br.com.cresceritsolutions.ibspdv.models.hb.CanalVenda;
import br.com.cresceritsolutions.ibspdv.models.hb.Endereco;
import br.com.cresceritsolutions.ibspdv.models.hb.Estado;
import br.com.cresceritsolutions.ibspdv.models.hb.Municipio;
import br.com.cresceritsolutions.ibspdv.util.VariaveisDoSistema;

/**
 *
 * @author Gutem
 */
public class CanalVendaFactory {

    public static CanalVenda novo(){
        CanalVenda canalVenda = new CanalVenda();
        canalVenda.setEmpresa(VariaveisDoSistema.EMPRESA);
        canalVenda.setAtivo(VariaveisDoSistema.EMPRESA.isAtivo());
        canalVenda.setEndereco(new Endereco());
        canalVenda.getEndereco().setMunicipio(new Municipio());
        canalVenda.getEndereco().getMunicipio().setEstado(new Estado());
        return canalVenda;
    }
    
}
