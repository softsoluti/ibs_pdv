package br.com.cresceritsolutions.ibspdv.models.ecf;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

import br.com.cresceritsolutions.ibspdv.models.core.Dados;

/**
 * Classe que representa o pagamento da venda no sistama.
 *
 */
public class EcfPagamento extends Dados implements Serializable {

    private Integer ecfPagamentoId;
    private int ecfPagamentoGnf;
    private Date ecfPagamentoData;
    private Double ecfPagamentoValor;
    private String ecfPagamentoNsu;
    private char ecfPagamentoEstorno;
    private int ecfPagamentoEstornoGnf;
    private Date ecfPagamentoEstornoData;
    private Double ecfPagamentoEstornoValor;
    private String ecfPagamentoEstornoNsu;
    private EcfPagamentoTipo ecfPagamentoTipo;
    private List<EcfPagamentoParcela> ecfPagamentoParcelas;
    private transient String arquivo;

    /**
     * Construtor padrao
     */
    public EcfPagamento() {
        this(0);
    }

    /**
     * Contrutor padrao passando o id
     *
     * @param ecfPagamentoId o id do registro.
     */
    public EcfPagamento(Integer ecfPagamentoId) {
        super("EcfPagamento", "ecfPagamentoId", "ecfPagamentoId");
        this.ecfPagamentoId = ecfPagamentoId;
    }

    @Override
    public Integer getId() {
        return ecfPagamentoId;
    }

    @Override
    public void setId(Integer id) {
        ecfPagamentoId = id;
    }

    public Integer getecfPagamentoId() {
        return ecfPagamentoId;
    }

    //GETs e SETs
    public String getArquivo() {
        return arquivo;
    }

    public void setArquivo(String arquivo) {
        this.arquivo = arquivo;
    }

    public Date getEcfPagamentoData() {
        return ecfPagamentoData;
    }

    public void setEcfPagamentoData(Date ecfPagamentoData) {
        this.ecfPagamentoData = ecfPagamentoData;
    }

    public char getEcfPagamentoEstorno() {
        return ecfPagamentoEstorno;
    }

    public void setEcfPagamentoEstorno(char ecfPagamentoEstorno) {
        this.ecfPagamentoEstorno = ecfPagamentoEstorno;
    }

    public Date getEcfPagamentoEstornoData() {
        return ecfPagamentoEstornoData;
    }

    public void setEcfPagamentoEstornoData(Date ecfPagamentoEstornoData) {
        this.ecfPagamentoEstornoData = ecfPagamentoEstornoData;
    }

    public int getEcfPagamentoEstornoGnf() {
        return ecfPagamentoEstornoGnf;
    }

    public void setEcfPagamentoEstornoGnf(int ecfPagamentoEstornoGnf) {
        this.ecfPagamentoEstornoGnf = ecfPagamentoEstornoGnf;
    }

    public String getEcfPagamentoEstornoNsu() {
        return ecfPagamentoEstornoNsu;
    }

    public void setEcfPagamentoEstornoNsu(String ecfPagamentoEstornoNsu) {
        this.ecfPagamentoEstornoNsu = ecfPagamentoEstornoNsu;
    }

    public Double getEcfPagamentoEstornoValor() {
        return ecfPagamentoEstornoValor;
    }

    public void setEcfPagamentoEstornoValor(Double ecfPagamentoEstornoValor) {
        this.ecfPagamentoEstornoValor = ecfPagamentoEstornoValor;
    }

    public int getEcfPagamentoGnf() {
        return ecfPagamentoGnf;
    }

    public void setEcfPagamentoGnf(int ecfPagamentoGnf) {
        this.ecfPagamentoGnf = ecfPagamentoGnf;
    }

    public Integer getEcfPagamentoId() {
        return ecfPagamentoId;
    }

    public void setEcfPagamentoId(Integer ecfPagamentoId) {
        this.ecfPagamentoId = ecfPagamentoId;
    }

    public String getEcfPagamentoNsu() {
        return ecfPagamentoNsu;
    }

    public void setEcfPagamentoNsu(String ecfPagamentoNsu) {
        this.ecfPagamentoNsu = ecfPagamentoNsu;
    }

    public EcfPagamentoTipo getEcfPagamentoTipo() {
        return ecfPagamentoTipo;
    }

    public void setEcfPagamentoTipo(EcfPagamentoTipo ecfPagamentoTipo) {
        this.ecfPagamentoTipo = ecfPagamentoTipo;
    }

    public Double getEcfPagamentoValor() {
        return ecfPagamentoValor;
    }

    public void setEcfPagamentoValor(Double ecfPagamentoValor) {
        this.ecfPagamentoValor = ecfPagamentoValor;
    }

    public List<EcfPagamentoParcela> getEcfPagamentoParcelas() {
        return ecfPagamentoParcelas;
    }

    public void setEcfPagamentoParcelas(List<EcfPagamentoParcela> ecfPagamentoParcelas) {
        this.ecfPagamentoParcelas = ecfPagamentoParcelas;
    }
}
