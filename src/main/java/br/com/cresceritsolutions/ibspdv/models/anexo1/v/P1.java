package br.com.cresceritsolutions.ibspdv.models.anexo1.v;

import br.com.cresceritsolutions.ibspdv.models.anexo1.Cabecalho;

/**
 * Classe que representa o modelo P1 do anexo V.
 *
 * @author Pedro H. Lira
 */
public class P1 extends Cabecalho {

    public P1() {
        padrao = "P1";
    }
}
