package br.com.cresceritsolutions.ibspdv.models.ecf;


import java.io.Serializable;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.cresceritsolutions.ibspdv.models.core.Dados;

/**
 * Classe que representa os daods da leitura Z no sistama.
 *
 * @author Pedro H. Lira
 */
//@Entity
//@Table(name = "ecf_z")
//@XmlRootElement
public class EcfZ extends Dados implements Serializable {

    private Integer ecfZId;
    private int ecfzIdCaixa;
    
    public int getEcfzIdCaixa() {
		return ecfzIdCaixa;
	}

	public void setEcfzIdCaixa(int ecfzIdCaixa) {
		this.ecfzIdCaixa = ecfzIdCaixa;
	}

	private int ecfZUsuario;
    private int ecfZCrz;
    private int ecfZCooIni;
    private int ecfZCooFin;
    private int ecfZCro;
    private int ecfzGNF;
    private int ecfzCCF;
    private int ecfzCFD;
    private int ecfzCDC;
    private int ecfzGRG;
    private int ecfzNFC;
    private int ecfzCFC;
    private int ecfzNCN;
    private int ecfzCCDC;
    private Date ecfZMovimento;
    private Date ecfZEmissao;
    private Double ecfZBruto;
    private Double ecfZGt;
    private boolean ecfZIssqn;
    private EcfImpressora ecfImpressora;
    private List<EcfZTotais> ecfZTotais;
    private List<EcfVenda> ecfVendas;
    private List<EcfDocumento> ecfDocumentos;

    public int getEcfzGNF() {
		return ecfzGNF;
	}

	public void setEcfzGNF(int ecfzGNF) {
		this.ecfzGNF = ecfzGNF;
	}

	public int getEcfzCCF() {
		return ecfzCCF;
	}

	public void setEcfzCCF(int ecfzCCF) {
		this.ecfzCCF = ecfzCCF;
	}

	public int getEcfzCFD() {
		return ecfzCFD;
	}

	public void setEcfzCFD(int ecfzCFD) {
		this.ecfzCFD = ecfzCFD;
	}

	public int getEcfzCDC() {
		return ecfzCDC;
	}

	public void setEcfzCDC(int ecfzCDC) {
		this.ecfzCDC = ecfzCDC;
	}

	public int getEcfzGRG() {
		return ecfzGRG;
	}

	public void setEcfzGRG(int ecfzGRG) {
		this.ecfzGRG = ecfzGRG;
	}

	public int getEcfzNFC() {
		return ecfzNFC;
	}

	public void setEcfzNFC(int ecfzNFC) {
		this.ecfzNFC = ecfzNFC;
	}

	public int getEcfzCFC() {
		return ecfzCFC;
	}

	public void setEcfzCFC(int ecfzCFC) {
		this.ecfzCFC = ecfzCFC;
	}

	public int getEcfzNCN() {
		return ecfzNCN;
	}

	public void setEcfzNCN(int ecfzNCN) {
		this.ecfzNCN = ecfzNCN;
	}

	public int getEcfzCCDC() {
		return ecfzCCDC;
	}

	public void setEcfzCCDC(int ecfzCCDC) {
		this.ecfzCCDC = ecfzCCDC;
	}

	/**
     * Construtor padrao
     */
    public EcfZ() {
        this(0);
    }

    /**
     * Contrutor padrao passando o id
     *
     * @param ecfZId o id do registro.
     */
    public EcfZ(Integer ecfZId) {
        super("EcfZ", "ecfZId", "ecfZMovimento");
        this.ecfZId = ecfZId;
    }

    @Override
    public Integer getId() {
        return ecfZId;
    }

    @Override
    public void setId(Integer id) {
        ecfZId = id;
    }

    public Integer getEcfZId() {
        return ecfZId;
    }

    public void setEcfZId(Integer ecfZId) {
        this.ecfZId = ecfZId;
    }

    public int getEcfZUsuario() {
        return ecfZUsuario;
    }

    public void setEcfZUsuario(int ecfZUsuario) {
        this.ecfZUsuario = ecfZUsuario;
    }

    public int getEcfZCrz() {
        return ecfZCrz;
    }

    public void setEcfZCrz(int ecfZCrz) {
        this.ecfZCrz = ecfZCrz;
    }

    public int getEcfZCooIni() {
        return ecfZCooIni;
    }

    public void setEcfZCooIni(int ecfZCooIni) {
        this.ecfZCooIni = ecfZCooIni;
    }

    public int getEcfZCooFin() {
        return ecfZCooFin;
    }

    public void setEcfZCooFin(int ecfZCooFin) {
        this.ecfZCooFin = ecfZCooFin;
    }

    public int getEcfZCro() {
        return ecfZCro;
    }

    public void setEcfZCro(int ecfZCro) {
        this.ecfZCro = ecfZCro;
    }

    public Date getEcfZMovimento() {
        return ecfZMovimento;
    }

    public void setEcfZMovimento(Date ecfZMovimento) {
        this.ecfZMovimento = ecfZMovimento;
    }

    public Date getEcfZEmissao() {
        return ecfZEmissao;
    }

    public void setEcfZEmissao(Date ecfZEmissao) {
        this.ecfZEmissao = ecfZEmissao;
    }

    public Double getEcfZBruto() {
        return ecfZBruto;
    }

    public void setEcfZBruto(Double ecfZBruto) {
        this.ecfZBruto = ecfZBruto;
    }

    public Double getEcfZGt() {
        return ecfZGt;
    }

    public void setEcfZGt(Double ecfZGt) {
        this.ecfZGt = ecfZGt;
    }

    public boolean getEcfZIssqn() {
        return ecfZIssqn;
    }

    public void setEcfZIssqn(boolean ecfZIssqn) {
        this.ecfZIssqn = ecfZIssqn;
    }

    public EcfImpressora getEcfImpressora() {
        return ecfImpressora;
    }

    public void setEcfImpressora(EcfImpressora ecfImpressora) {
        this.ecfImpressora = ecfImpressora;
    }

    public List<EcfZTotais> getEcfZTotais() {
        return ecfZTotais;
    }

    public void setEcfZTotais(List<EcfZTotais> ecfZTotais) {
        this.ecfZTotais = ecfZTotais;
    }

    public List<EcfVenda> getEcfVendas() {
        return ecfVendas;
    }

    public void setEcfVendas(List<EcfVenda> ecfVendas) {
        this.ecfVendas = ecfVendas;
    }

    public List<EcfDocumento> getEcfDocumentos() {
        return ecfDocumentos;
    }

    public void setEcfDocumentos(List<EcfDocumento> ecfDocumentos) {
        this.ecfDocumentos = ecfDocumentos;
    }
/*
	public static EcfZ salvar(EcfZ z) {
		Class_Conexao_mysql cc = new  Class_Conexao_mysql();
		cc.set_Conexao_Ibs_Base();
		cc.Conectar();
		SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd");   
		try {
			cc.executaSqlInsert("START TRANSACTION");
			String sql = "INSERT INTO ecf_z (ID_USUARIO,CRZ,COO_INI,COO_FIN,CRO," +
					"GNF,CCF,CFD,CDC,GRG,NFC,CFC,NCN,CCDC," +
					"MOVIMENTO,EMISSAO,BRUTO,GT,ISSQN,ID_IMPRESSORA)"+
			"VALUES('"+z.getEcfZUsuario()+"'," +
					"'"+z.getEcfZCrz()+"'," +
					"'"+z.getEcfZCooIni()+"'," +
					"'"+z.getEcfZCooFin()+"'," +
					"'"+z.getEcfZCro()+"'," +
					"'"+z.getEcfzGNF()+"'," +
					"'"+z.getEcfzCCF()+"'," +
					"'"+z.getEcfzCFD()+"'," +
					"'"+z.getEcfzCDC()+"'," +
					"'"+z.getEcfzGRG()+"'," +
					"'"+z.getEcfzNFC()+"'," +
					"'"+z.getEcfzCFC()+"'," +
					"'"+z.getEcfzNCN()+"'," +
					"'"+z.getEcfzCCDC()+"'," +
					"'"+spf.format(z.getEcfZMovimento())+"'" +
					",'"+spf.format(z.getEcfZEmissao())+"'," +
					"'"+z.getEcfZBruto()+"','"+z.getEcfZGt()+"'," +
					""+String.valueOf(z.getEcfZIssqn()).replace("false", "0").replace("true", "1")+"," +
					"'"+z.getEcfImpressora().getId()+"')";
			cc.executaSqlInsert(sql);
			cc.executaSqlSelect("SELECT LAST_INSERT_ID()");
			if(cc.getRs().first()){
				z.setId(cc.getRs().getInt(1));
			}
			cc.executaSqlInsert("UPDATE caixa SET ID_RDZ='"+z.getId()+"' WHERE ID='"+z.getEcfzIdCaixa()+"'");
			cc.executaSqlInsert("COMMIT");
		} catch (SQLException e) {
			try {
				cc.executaSqlInsert("ROOLBACK");
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return z;
	}

	public static List<EcfZ> getEcfZ(EcfZ ecfZ, String inicio, String fim,
			EcfImpressora impressora) {
		Class_Conexao_mysql cc = new  Class_Conexao_mysql();
		cc.set_Conexao_Ibs_Base();
		cc.Conectar();
		//SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd");   
		List<EcfZ> l = new ArrayList<EcfZ>();
		try {
			String sql = "SELECT * FROM ecf_z WHERE MOVIMENTO>='"+inicio+"' AND MOVIMENTO<='"+fim+"' AND ID_IMPRESSORA='"+impressora+"'";
			cc.executaSqlSelect(sql);
			if(cc.getRs().first()){
				EcfZ temp = new EcfZ();
				temp.setEcfZId(cc.getRs().getInt("ID"));
				temp.setEcfzIdCaixa(cc.getRs().getInt("ID_CAIXA"));
			    temp.setEcfZUsuario(cc.getRs().getInt("ID_USUARIO"));
			    temp.setEcfZCrz(cc.getRs().getInt("CRZ"));
			    temp.setEcfZCooIni(cc.getRs().getInt("COO_INI"));
			    temp.setEcfZCooFin(cc.getRs().getInt("COO_FIN"));
			    temp.setEcfZCro(cc.getRs().getInt("CRO"));
			    
			    temp.setEcfzGNF(cc.getRs().getInt("GNF"));
			    temp.setEcfzCCF(cc.getRs().getInt("CCF"));
			    temp.setEcfzCFD(cc.getRs().getInt("CFD"));
			    temp.setEcfzCDC(cc.getRs().getInt("CDC"));
			    temp.setEcfzGRG(cc.getRs().getInt("GRG"));
			    temp.setEcfzNFC(cc.getRs().getInt("NFC"));
			    temp.setEcfzCFC(cc.getRs().getInt("CFC"));
			    temp.setEcfzNCN(cc.getRs().getInt("NCN"));
			    temp.setEcfzCCDC(cc.getRs().getInt("CCDC"));
			    
			    temp.setEcfZMovimento(cc.getRs().getDate("MOVIMENTO"));
			    temp.setEcfZEmissao(cc.getRs().getDate("EMISSAO"));
			    temp.setEcfZBruto(cc.getRs().getDouble("BRUTO"));
			    temp.setEcfZGt(cc.getRs().getDouble("GT"));
			    temp.setEcfZIssqn(Boolean.parseBoolean(cc.getRs().getString("ISSQN").replace("1", "true").replace("0", "false")));
			    temp.setEcfImpressora(impressora);
			    //temp.setEcfZTotais(cc.getRs().getInt("ID_TOTAIS"));
			    //temp.setEcfVendas(cc.getRs().getInt("ID_VENDAS"));
			    //temp.setEcfDocumentos(cc.getRs().getInt("ID_DOCUMENTOS"));
			    l.add(temp);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return l;
	}
*/
}
