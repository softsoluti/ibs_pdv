/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.models.enuns;

/**
 *
 * @author Gutem
 */
public enum SituacaoItemCompraVenda {

    VENDIDO("VENDIDO", 1), CANCELADO("CANCELADO", 2), DEVOLVIDO("DEVOLVIDO", 3), DISPONIVEL("DISPONÍVEL", 4),
    BLOQUEADO("BLOQUEADO", 5), TRANSFERIDO("TRANSFERIDO", 6), EM_TRANSFERENCIA("EM TRANSFERÊNCIA", 7), RESERVADO("RESERVADO", 8);

    public static String get(int situacao) {
        switch (situacao) {
            case 1:
                return "VENDIDO";
            case 2:
                return "CANCELADO";
            case 3:
                return "DEVOLVIDO";
            case 4:
                return "DISPONÍVEL";
            case 5:
                return "BLOQUEADO";
            case 6:
                return "TRANSFERIDO";
            case 7:
                return "EM TRANSFERÊNCIA";
            case 8:
                return "RESERVADO";
        }
        return null;
    }
    private final String valor;
    private final Integer id;

    private SituacaoItemCompraVenda(String s, Integer id) {
        valor = s;
        this.id = id;
    }

    @Override
    public String toString() {
        return valor;
    }

    public Integer getId() {
        return id;
    }

}
