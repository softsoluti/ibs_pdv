package br.com.cresceritsolutions.ibspdv.models.validacao;
// Generated 10/02/2015 12:58:38 by Hibernate Tools 4.3.1



/**
 * Albuns generated by hbm2java
 */
public class Albuns  implements java.io.Serializable {


     private Integer idalbum;
     private String capa;
     private String desc;

    public Albuns() {
    }

    public Albuns(String capa, String desc) {
       this.capa = capa;
       this.desc = desc;
    }
   
    public Integer getIdalbum() {
        return this.idalbum;
    }
    
    public void setIdalbum(Integer idalbum) {
        this.idalbum = idalbum;
    }
    public String getCapa() {
        return this.capa;
    }
    
    public void setCapa(String capa) {
        this.capa = capa;
    }
    public String getDesc() {
        return this.desc;
    }
    
    public void setDesc(String desc) {
        this.desc = desc;
    }




}


