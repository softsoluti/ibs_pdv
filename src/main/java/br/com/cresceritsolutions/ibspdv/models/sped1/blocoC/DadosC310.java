package br.com.cresceritsolutions.ibspdv.models.sped1.blocoC;

import br.com.cresceritsolutions.ibspdv.models.sped1.Bean;

public class DadosC310 extends Bean {

    private int num_doc_canc;

    public DadosC310() {
        super("C310");
    }

    public int getNum_doc_canc() {
        return num_doc_canc;
    }

    public void setNum_doc_canc(int num_doc_canc) {
        this.num_doc_canc = num_doc_canc;
    }
}
