/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.models.enuns;

/**
 *
 * @author Gutem
 */
public enum OpcoesValorPrecoFranquia {

    SO_A_VISTA("Só à VISTA"), PRODUTO_GRATIS("PRODUTO GRÁTIS"), INEXISTENTE("INEXISTENTE");
    private final String valor;

    private OpcoesValorPrecoFranquia(String s) {
        valor = s;
    }

    @Override
    public String toString() {
        return valor;
    }

}
