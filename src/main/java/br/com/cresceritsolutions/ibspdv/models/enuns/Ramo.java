/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.models.enuns;

/**
 *
 * @author Gutemberg
 */
public enum Ramo {

    NONE("", 0), ESTABELECIMENTOS_EM_GERAL("ESTABELECIMENTOS EM GERAL", 1), AGENTE_AUTORIZADO_CLARO("AGENTE AUTORIZADO CLARO", 2);

    public static Ramo get(Integer property) {
        switch (property) {
            case 1:
                return ESTABELECIMENTOS_EM_GERAL;
            case 2:
                return AGENTE_AUTORIZADO_CLARO;
            default:
                return NONE;

        }
    }
    private final String valor;
    private final Integer id;

    private Ramo(String s, Integer id) {
        valor = s;
        this.id = id;
    }

    @Override
    public String toString() {
        return valor;
    }

    public Integer getId() {
        return id;
    }

}
