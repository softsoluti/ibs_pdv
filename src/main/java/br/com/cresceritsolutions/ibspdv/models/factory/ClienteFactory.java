/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.models.factory;

import br.com.cresceritsolutions.ibspdv.models.hb.Cliente;
import br.com.cresceritsolutions.ibspdv.models.hb.Endereco;
import br.com.cresceritsolutions.ibspdv.models.hb.Estado;
import br.com.cresceritsolutions.ibspdv.models.hb.Municipio;
import br.com.cresceritsolutions.ibspdv.models.hb.Pessoa;
import br.com.cresceritsolutions.ibspdv.models.hb.PessoaFisica;
import br.com.cresceritsolutions.ibspdv.models.hb.PessoaJuridica;
import br.com.cresceritsolutions.ibspdv.models.hb.Telefone;
import java.util.Date;

/**
 *
 * @author Gutem
 */
public class ClienteFactory {

    public static Cliente novoFisica() {
        Cliente cliente = new Cliente();
        cliente.setPessoaFisica(new PessoaFisica());
        cliente.getPessoaFisica().setPessoa(new Pessoa());
        cliente.getPessoaFisica().getPessoa().setEndereco(new Endereco());
        cliente.getPessoaFisica().getPessoa().getTelefones().add(new Telefone());
        cliente.getPessoaFisica().getPessoa().getTelefones().add(new Telefone());
        cliente.getPessoaFisica().getPessoa().getEndereco().setMunicipio(new Municipio());
        cliente.getPessoaFisica().getPessoa().getEndereco().getMunicipio().setEstado(new Estado());
        cliente.setDataInclusao(new Date());
        return cliente;
    }
    public static Cliente novoJuridica() {
        Cliente cliente = new Cliente();
        cliente.setPessoaJuridica(new PessoaJuridica());
        cliente.getPessoaJuridica().setPessoa(new Pessoa());
        cliente.getPessoaJuridica().getPessoa().setEndereco(new Endereco());
        cliente.getPessoaJuridica().getPessoa().getTelefones().add(new Telefone());
        cliente.getPessoaJuridica().getPessoa().getTelefones().add(new Telefone());
        cliente.getPessoaJuridica().getPessoa().getEndereco().setMunicipio(new Municipio());
        cliente.getPessoaJuridica().getPessoa().getEndereco().getMunicipio().setEstado(new Estado());
        cliente.setDataInclusao(new Date());
        return cliente;
    }

}
