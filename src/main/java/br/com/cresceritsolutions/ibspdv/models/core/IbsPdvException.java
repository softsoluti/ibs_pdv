package br.com.cresceritsolutions.ibspdv.models.core;

/**
 * Classe que representa uma excecao geral do sistema.
 *
 */
public class IbsPdvException extends Exception {

    /**
     * Contrutor padrao com mensagem e falha NULL.
     */
    public IbsPdvException() {
        super();
    }

    /**
     * Contrutor que recebe o evento da falha original.
     *
     * @param cause Falha original.
     */
    public IbsPdvException(Throwable cause) {
        super(cause);
    }

    /**
     * Contrutor que recebe uma mensagem e o evento da falha original.
     *
     * @param message String com texto adicional.
     * @param cause Falha original.
     */
    public IbsPdvException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Contrutor que recebe uma mensagem adicional.
     *
     * @param message String com texto adicional.
     */
    public IbsPdvException(String message) {
        super(message);
    }
}
