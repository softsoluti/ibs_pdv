package br.com.cresceritsolutions.ibspdv.models.anexo1.v1_13.iv;

import br.com.cresceritsolutions.ibspdv.models.anexo1.Rodape;

/**
 * Classe que representa o modelo E9 do anexo IV.
 *
 * @author Pedro H. Lira
 */
public class E9 extends Rodape {

    public E9() {
        padrao = "E9";
    }
}
