/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.cresceritsolutions.ibspdv.models.UI;

import br.com.cresceritsolutions.ibspdv.models.enuns.Fontes;
import java.awt.Font;

/**
 *
 * @author Gutem
 */
public class FonteIBS extends Font{

    public FonteIBS() {
        super(Fontes.ARIAL.toString(), Font.PLAIN, 12);
    }
    
    public FonteIBS(int plainBold , int size) {
        super(Fontes.ARIAL.toString(), plainBold, size);
    }

    
    
}
