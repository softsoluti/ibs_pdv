package br.com.cresceritsolutions.ibspdv.models.ecf;


import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import br.com.cresceritsolutions.ibspdv.models.core.Dados;

/**
 * Classe que representa os totais da leitura Z no sistama.
 *
 * @author Pedro H. Lira
 */
//@Entity
//@Table(name = "ecf_z_totais")
//@XmlRootElement
public class EcfZTotais extends Dados implements Serializable {

    //@Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    //@Column(name = "ecf_z_totais_id")
    private Integer ecfZTotaisId;
    //@Column(name = "ecf_z_totais_codigo")
    private String ecfZTotaisCodigo;
    //@Column(name = "ecf_z_totais_valor")
    private Double ecfZTotaisValor;
    //@ManyToOne
    //@JoinColumn(name = "ecf_z_id")
    //@XmlInverseReference(mappedBy = "ecfZTotais")
    private EcfZ ecfZ;

    /**
     * Construtor padrao
     */
    public EcfZTotais() {
        this(0);
    }

    /**
     * Contrutor padrao passando o id
     *
     * @param ecfZTotaisId o id do registro.
     */
    public EcfZTotais(Integer ecfZTotaisId) {
        super("EcfZTotais", "ecfZTotaisId", "ecfZTotaisId");
        this.ecfZTotaisId = ecfZTotaisId;
    }

    @Override
    public Integer getId() {
        return ecfZTotaisId;
    }

    @Override
    public void setId(Integer id) {
        ecfZTotaisId = id;
    }

    public Integer getEcfZTotaisId() {
        return ecfZTotaisId;
    }

    public void setEcfZTotaisId(Integer ecfZTotaisId) {
        this.ecfZTotaisId = ecfZTotaisId;
    }

    public String getEcfZTotaisCodigo() {
        return ecfZTotaisCodigo;
    }

    public void setEcfZTotaisCodigo(String ecfZTotaisCodigo) {
        this.ecfZTotaisCodigo = ecfZTotaisCodigo;
    }

    public Double getEcfZTotaisValor() {
        return ecfZTotaisValor;
    }

    public void setEcfZTotaisValor(Double ecfZTotaisValor) {
        this.ecfZTotaisValor = ecfZTotaisValor;
    }

    public EcfZ getEcfZ() {
        return ecfZ;
    }

    public void setEcfZ(EcfZ ecfZ) {
        this.ecfZ = ecfZ;
    }
/*
	public static List<EcfZTotais> getZTotais(int id) {
		Class_Conexao_mysql cc = new Class_Conexao_mysql();
		cc.set_Conexao_Ibs_Base();
		cc.Conectar();
		List<EcfZTotais> l = new ArrayList<EcfZTotais>();
		try {
			cc.executaSqlSelect("SELECT Id,CODIGO,VALOR FROM ecf_z_totais WHERE ID='" +id+"'");
			while(cc.getRs().next()){
				EcfZTotais temp = new EcfZTotais();
				temp.setId(cc.getRs().getInt("ID"));
				temp.setEcfZTotaisCodigo(cc.getRs().getString("CODIGO"));
				temp.setEcfZTotaisValor(cc.getRs().getDouble("MFADICIONAL"));
				l.add(temp);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			cc.Desconectar();
		}
		return l;
	}

	public static void salvar(EcfZTotais ecfZTotais) {
		Class_Conexao_mysql cc = new Class_Conexao_mysql();
		cc.set_Conexao_Ibs_Base();
		cc.Conectar();
		try {
			cc.executaSqlInsert("INSERT INTO ecf_z_totais (ID_ECF_Z,CODIGO,VALOR) VALUES ('"+ecfZTotais.getEcfZ().getId()+"','"+ecfZTotais.getEcfZTotaisCodigo()+"','"+ecfZTotais.getEcfZTotaisValor()+"')");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			cc.Desconectar();
		}
	}
	
	public static void salvar(Map<String, EcfZTotais> totais) {
		Class_Conexao_mysql cc = new Class_Conexao_mysql();
		cc.set_Conexao_Ibs_Base();
		cc.Conectar();
		try {
			cc.executaSqlInsert("START TRANSACTION");
			List<EcfZTotais> l = new ArrayList<EcfZTotais>(totais.values());
			for(int i=0;i<l.size();i++){
				cc.executaSqlInsert("INSERT INTO ecf_z_totais (ID_ECF_Z,CODIGO,VALOR) VALUES ('"+l.get(i).getEcfZ().getId()+"','"+l.get(i).getEcfZTotaisCodigo()+"','"+l.get(i).getEcfZTotaisValor()+"')");
			}
			cc.executaSqlInsert("COMMIT");
		} catch (SQLException e) {
			try {
				cc.executaSqlInsert("ROOLBACK");
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			cc.Desconectar();
		}
	}
    */
}
