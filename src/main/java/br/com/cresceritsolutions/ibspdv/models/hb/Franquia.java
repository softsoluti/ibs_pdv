package br.com.cresceritsolutions.ibspdv.models.hb;
// Generated 09/02/2015 13:13:15 by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * Franquia generated by hbm2java
 */
public class Franquia  implements java.io.Serializable {


     private Integer id;
     private Extranet extranet;
     private IwClaro iwClaro;
     private Set empresas = new HashSet(0);

    public Franquia() {
    }

    public Franquia(Extranet extranet, IwClaro iwClaro, Set empresas) {
       this.extranet = extranet;
       this.iwClaro = iwClaro;
       this.empresas = empresas;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public Extranet getExtranet() {
        return this.extranet;
    }
    
    public void setExtranet(Extranet extranet) {
        this.extranet = extranet;
    }
    public IwClaro getIwClaro() {
        return this.iwClaro;
    }
    
    public void setIwClaro(IwClaro iwClaro) {
        this.iwClaro = iwClaro;
    }
    public Set getEmpresas() {
        return this.empresas;
    }
    
    public void setEmpresas(Set empresas) {
        this.empresas = empresas;
    }




}


