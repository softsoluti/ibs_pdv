/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.models.enuns;

/**
 *
 * @author Gutem
 */
public enum SituacaoPagamento {

    RECEBIDO("RECEBIDO", true), DEVOLVIDO("DEVOLVIDO", false), PAGO("PAGO", false);//Pago ou devolvido seria dinheiro que sai da loja

    private final String texto;
    private final Boolean valor;

    private SituacaoPagamento(String texto, Boolean valor) {
        this.texto = texto;
        this.valor = valor;
    }

    public static String get(Boolean situacao) {
        if (situacao) {
            return "RECEBIDO";
        } else {
            return "DEVOLVIDO";
        }

    }

    @Override
    public String toString() {
        return texto;
    }

    public Boolean getValor() {
        return valor;
    }

}
