package br.com.cresceritsolutions.ibspdv.models.hb;
// Generated 09/02/2015 13:13:15 by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Balanco generated by hbm2java
 */
public class Balanco  implements java.io.Serializable {


     private Integer id;
     private Empresa empresa;
     private Usuario usuario;
     private Boolean tipoSerial;
     private String descricao;
     private Date abertoEm;
     private Date fechadoEm;
     private String status;
     private String observacao;
     private Set itemBalancos = new HashSet(0);

    public Balanco() {
    }

	
    public Balanco(Empresa empresa, Usuario usuario) {
        this.empresa = empresa;
        this.usuario = usuario;
    }
    public Balanco(Empresa empresa, Usuario usuario, Boolean tipoSerial, String descricao, Date abertoEm, Date fechadoEm, String status, String observacao, Set itemBalancos) {
       this.empresa = empresa;
       this.usuario = usuario;
       this.tipoSerial = tipoSerial;
       this.descricao = descricao;
       this.abertoEm = abertoEm;
       this.fechadoEm = fechadoEm;
       this.status = status;
       this.observacao = observacao;
       this.itemBalancos = itemBalancos;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public Empresa getEmpresa() {
        return this.empresa;
    }
    
    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }
    public Usuario getUsuario() {
        return this.usuario;
    }
    
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    public Boolean getTipoSerial() {
        return this.tipoSerial;
    }
    
    public void setTipoSerial(Boolean tipoSerial) {
        this.tipoSerial = tipoSerial;
    }
    public String getDescricao() {
        return this.descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    public Date getAbertoEm() {
        return this.abertoEm;
    }
    
    public void setAbertoEm(Date abertoEm) {
        this.abertoEm = abertoEm;
    }
    public Date getFechadoEm() {
        return this.fechadoEm;
    }
    
    public void setFechadoEm(Date fechadoEm) {
        this.fechadoEm = fechadoEm;
    }
    public String getStatus() {
        return this.status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }
    public String getObservacao() {
        return this.observacao;
    }
    
    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
    public Set getItemBalancos() {
        return this.itemBalancos;
    }
    
    public void setItemBalancos(Set itemBalancos) {
        this.itemBalancos = itemBalancos;
    }




}


