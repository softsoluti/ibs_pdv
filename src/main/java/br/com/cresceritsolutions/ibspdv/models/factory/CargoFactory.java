/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.cresceritsolutions.ibspdv.models.factory;

import br.com.cresceritsolutions.ibspdv.models.hb.Cargo;
import br.com.cresceritsolutions.ibspdv.models.hb.Departamento;

/**
 *
 * @author Gutem
 */
public class CargoFactory {

    public static Cargo novo(){
        Cargo cargo = new Cargo();
        cargo.setDepartamento(new Departamento());
        return cargo;
    }
    
}
