package br.com.cresceritsolutions.ibspdv.models.anexo1.x;

import br.com.cresceritsolutions.ibspdv.models.anexo1.Cabecalho;

/**
 * Classe que representa o modelo N1 do anexo X.
 *
 * @author Pedro H. Lira
 */
public class N1 extends Cabecalho {

    public N1() {
        padrao = "N1";
    }
}
