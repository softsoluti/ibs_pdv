package br.com.cresceritsolutions.ibspdv.models.ecf;


import java.io.Serializable;
import java.sql.SQLException;


import br.com.cresceritsolutions.ibspdv.models.core.Dados;

/**
 * Classe que representa a impressora do sistama.
 *
 * @author Pedro H. Lira
 */
public class EcfImpressora extends Dados implements Serializable {

    private Integer ecfImpressoraId;
    private String ecfImpressoraCodigo;
    private String ecfImpressoraMfadicional;
    private String ecfImpressoraIdentificacao;
    private String ecfImpressoraSerie;
    private String ecfImpressoraTipo;
    private String ecfImpressoraMarca;
    private String ecfImpressoraModelo;
    private int ecfImpressoraCaixa;
    private boolean ecfImpressoraAtivo;

    /**
     * Construtor padrao
     */
    public EcfImpressora() {
        this(0);
    }

    /**
     * Contrutor padrao passando o id
     *
     * @param ecfImpressoraId o id do registro.
     */
    public EcfImpressora(Integer ecfImpressoraId) {
        super("EcfImpressora", "ecfImpressoraId", "ecfImpressoraId");
        this.ecfImpressoraId = ecfImpressoraId;
    }

    @Override
    public Integer getId() {
        return ecfImpressoraId;
    }

    @Override
    public void setId(Integer id) {
        ecfImpressoraId = id;
    }

    // GETs e SETs
    public Integer getEcfImpressoraId() {
        return ecfImpressoraId;
    }

    public void setEcfImpressoraId(Integer ecfImpressoraId) {
        this.ecfImpressoraId = ecfImpressoraId;
    }

    public String getEcfImpressoraCodigo() {
        return ecfImpressoraCodigo;
    }

    public void setEcfImpressoraCodigo(String ecfImpressoraCodigo) {
        this.ecfImpressoraCodigo = ecfImpressoraCodigo;
    }

    public String getEcfImpressoraMfadicional() {
        return ecfImpressoraMfadicional;
    }

    public void setEcfImpressoraMfadicional(String ecfImpressoraMfadicional) {
        this.ecfImpressoraMfadicional = ecfImpressoraMfadicional;
    }

    public String getEcfImpressoraIdentificacao() {
        return ecfImpressoraIdentificacao;
    }

    public void setEcfImpressoraIdentificacao(String ecfImpressoraIdentificacao) {
        this.ecfImpressoraIdentificacao = ecfImpressoraIdentificacao;
    }

    public String getEcfImpressoraSerie() {
        return ecfImpressoraSerie;
    }

    public void setEcfImpressoraSerie(String ecfImpressoraSerie) {
        this.ecfImpressoraSerie = ecfImpressoraSerie;
    }

    public String getEcfImpressoraTipo() {
        return ecfImpressoraTipo;
    }

    public void setEcfImpressoraTipo(String ecfImpressoraTipo) {
        this.ecfImpressoraTipo = ecfImpressoraTipo;
    }

    public String getEcfImpressoraMarca() {
        return ecfImpressoraMarca;
    }

    public void setEcfImpressoraMarca(String ecfImpressoraMarca) {
        this.ecfImpressoraMarca = ecfImpressoraMarca;
    }

    public String getEcfImpressoraModelo() {
        return ecfImpressoraModelo;
    }

    public int getEcfImpressoraCaixa() {
		return ecfImpressoraCaixa;
	}

	public void setEcfImpressoraCaixa(int ecfImpressoraCaixa) {
		this.ecfImpressoraCaixa = ecfImpressoraCaixa;
	}

	public void setEcfImpressoraModelo(String ecfImpressoraModelo) {
        this.ecfImpressoraModelo = ecfImpressoraModelo;
    }

    public boolean isEcfImpressoraAtivo() {
        return ecfImpressoraAtivo;
    }

    public void setEcfImpressoraAtivo(boolean ecfImpressoraAtivo) {
        this.ecfImpressoraAtivo = ecfImpressoraAtivo;
    }
    
}
