/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.models.enuns;

/**
 *
 * @author Gutem
 */
public enum TipoDaNota {

    ENTRADA("ENTRADA", 1), DEVOLUCAO("DEVOLUÇÃO", 2);

    public static Object get(int tipo) {
        if (tipo == 1) {
            return "ENTRADA";
        } else {
            return "DEVOLUÇÃO";
        }

    }
    private final String valor;
    private final Integer id;

    private TipoDaNota(String s, Integer id) {
        valor = s;
        this.id = id;
    }

    @Override
    public String toString() {
        return valor;
    }

    public Integer getId() {
        return id;
    }

}
