package br.com.cresceritsolutions.ibspdv.models.ecf;


import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.cresceritsolutions.ibspdv.models.core.Dados;

/**
 * Classe que representa o tipo de pagamento do sistama.
 *
 */
public class EcfPagamentoTipo extends Dados implements Serializable {

    private Integer ecfPagamentoTipoId;
    private String ecfPagamentoTipoCodigo;
    private String ecfPagamentoTipoDescricao;
    private boolean ecfPagamentoTipoTef;
    private boolean ecfPagamentoTipoVinculado;
    private boolean ecfPagamentoTipoDebito;
    private String ecfPagamentoTipoRede;

    /**
     * Construtor padrao
     */
    public EcfPagamentoTipo() {
        this(0);
    }

    /**
     * Contrutor padrao passando o id
     *
     * @param ecfPagamentoTipoId o id do registro.
     */
    public EcfPagamentoTipo(Integer ecfPagamentoTipoId) {
        super("EcfPagamentoTipo", "ecfPagamentoTipoId", "ecfPagamentoTipoCodigo");
        this.ecfPagamentoTipoId = ecfPagamentoTipoId;
    }

    @Override
    public Integer getId() {
        return ecfPagamentoTipoId;
    }

    @Override
    public void setId(Integer id) {
        ecfPagamentoTipoId = id;
    }

    // GETs e SETs
    public String getEcfPagamentoTipoCodigo() {
        return ecfPagamentoTipoCodigo;
    }

    public void setEcfPagamentoTipoCodigo(String ecfPagamentoTipoCodigo) {
        this.ecfPagamentoTipoCodigo = ecfPagamentoTipoCodigo;
    }

    public String getEcfPagamentoTipoDescricao() {
        return ecfPagamentoTipoDescricao;
    }

    public void setEcfPagamentoTipoDescricao(String ecfPagamentoTipoDescricao) {
        this.ecfPagamentoTipoDescricao = ecfPagamentoTipoDescricao;
    }

    public Integer getEcfPagamentoTipoId() {
        return ecfPagamentoTipoId;
    }

    public void setEcfPagamentoTipoId(Integer ecfPagamentoTipoId) {
        this.ecfPagamentoTipoId = ecfPagamentoTipoId;
    }

    public String getEcfPagamentoTipoRede() {
        return ecfPagamentoTipoRede;
    }

    public void setEcfPagamentoTipoRede(String ecfPagamentoTipoRede) {
        this.ecfPagamentoTipoRede = ecfPagamentoTipoRede;
    }

    public boolean isEcfPagamentoTipoTef() {
        return ecfPagamentoTipoTef;
    }

    public void setEcfPagamentoTipoTef(boolean ecfPagamentoTipoTef) {
        this.ecfPagamentoTipoTef = ecfPagamentoTipoTef;
    }

    public boolean isEcfPagamentoTipoVinculado() {
        return ecfPagamentoTipoVinculado;
    }

    public void setEcfPagamentoTipoVinculado(boolean ecfPagamentoTipoVinculado) {
        this.ecfPagamentoTipoVinculado = ecfPagamentoTipoVinculado;
    }

    public boolean isEcfPagamentoTipoDebito() {
        return ecfPagamentoTipoDebito;
    }

    public void setEcfPagamentoTipoDebito(boolean ecfPagamentoTipoDebito) {
        this.ecfPagamentoTipoDebito = ecfPagamentoTipoDebito;
    }

	public static List<EcfPagamentoTipo> getListTiposEcfPagamento() throws SQLException {
		
       /* Class_Conexao_mysql cc = new Class_Conexao_mysql();
        cc.set_Conexao_Ibs_Base();
        cc.Conectar();
        
        List<EcfPagamentoTipo> listaTipoPagamento = new ArrayList<EcfPagamentoTipo>();
        
        try {
            
        	cc.executaSqlSelect("select * from formas_pg_validas order by id");
            
            while(cc.getRs().next()) {
            	
                EcfPagamentoTipo tipo = new EcfPagamentoTipo();
                tipo.setId(cc.getRs().getInt("ID"));
                tipo.setEcfPagamentoTipoTef(cc.getRs().getInt("TEF")==1? true: false);
                tipo.setEcfPagamentoTipoDescricao(cc.getRs().getString("DESCRICAO"));
                
                tipo.setEcfPagamentoTipoCodigo(cc.getRs().getString("CODIGO"));
                tipo.setEcfPagamentoTipoRede(cc.getRs().getString("REDE"));
                tipo.setEcfPagamentoTipoVinculado(cc.getRs().getInt("VINCULADO")==1?true:false);
                tipo.setEcfPagamentoTipoDebito(cc.getRs().getInt("DEBITO")==1?true:false);
                listaTipoPagamento.add(tipo);
                
            }
        } catch (SQLException ex) {
            new Class_EnvioDeErro(ex);
            java.awt.Toolkit.getDefaultToolkit().beep();
            throw ex;
        } finally {
        	cc.Desconectar();
        }*/
        return null;
	}
	
}