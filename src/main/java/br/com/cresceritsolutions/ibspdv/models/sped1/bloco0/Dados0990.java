package br.com.cresceritsolutions.ibspdv.models.sped1.bloco0;

import br.com.cresceritsolutions.ibspdv.models.sped1.Bean;

public class Dados0990 extends Bean {

    private int qtd_lin;

    public Dados0990() {
        super("0990");
    }

    public int getQtd_lin() {
        return qtd_lin;
    }

    public void setQtd_lin(int qtd_lin) {
        this.qtd_lin = qtd_lin;
    }
}
