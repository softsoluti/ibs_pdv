package br.com.cresceritsolutions.ibspdv.models.sped1;

import java.io.FileWriter;

public interface Bloco {

    public void gerar(FileWriter fw) throws Exception;
}
