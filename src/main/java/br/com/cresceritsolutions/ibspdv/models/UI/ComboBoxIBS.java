/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.models.UI;

import java.awt.Point;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

/**
 *
 * @author Gutem
 */
public class ComboBoxIBS extends JComboBox {

    public ComboBoxIBS() {
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PainelSelecionaItensCbo itensCbo = new PainelSelecionaItensCbo(null, ComboBoxIBS.this, new Point(e.getXOnScreen(), e.getYOnScreen()));
                itensCbo.setVisible(true);
            }
        });
        addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == 32) {
                    PainelSelecionaItensCbo itensCbo = new PainelSelecionaItensCbo(null, ComboBoxIBS.this, e.getComponent().getLocationOnScreen());
                    itensCbo.setVisible(true);
                }
            }
        });
        this.setFont(new FonteIBS());
    }
    public ComboBoxIBS(DefaultComboBoxModel boxModel) {
        this.setModel(boxModel);
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PainelSelecionaItensCbo itensCbo = new PainelSelecionaItensCbo(null, ComboBoxIBS.this, new Point(e.getXOnScreen(), e.getYOnScreen()));
                itensCbo.setVisible(true);
            }
        });
        addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == 32) {
                    PainelSelecionaItensCbo itensCbo = new PainelSelecionaItensCbo(null, ComboBoxIBS.this, e.getComponent().getLocationOnScreen());
                    itensCbo.setVisible(true);
                }
            }
        });
    }

    @Override
    public void addItem(Object e) {
        super.addItem(e); //To change body of generated methods, choose Tools | Templates.
    }
    
    

    
    
    
    

}
