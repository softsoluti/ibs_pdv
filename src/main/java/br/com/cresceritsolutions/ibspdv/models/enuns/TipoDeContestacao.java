/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.models.enuns;

/**
 *
 * @author Gutem
 */
public enum TipoDeContestacao {

    SO_COMISSAO("SÓ COMISSÃO"), SO_REBATE("SÓ REBATE"), COMISSAO_REBATE("COMISSÃO + REBATE");
    private final String valor;

    private TipoDeContestacao(String s) {
        valor = s;
    }

    @Override
    public String toString() {
        return valor;
    }

}
