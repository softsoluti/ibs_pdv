package br.com.cresceritsolutions.ibspdv.models.sped1.blocoG;

import br.com.cresceritsolutions.ibspdv.models.sped1.Bean;

public class DadosG990 extends Bean {

    private int qtd_lin;

    public DadosG990() {
        super("G990");
    }

    public int getQtd_lin() {
        return qtd_lin;
    }

    public void setQtd_lin(int qtd_lin) {
        this.qtd_lin = qtd_lin;
    }
}
