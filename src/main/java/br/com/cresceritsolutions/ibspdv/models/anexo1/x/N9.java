package br.com.cresceritsolutions.ibspdv.models.anexo1.x;

import br.com.cresceritsolutions.ibspdv.models.anexo1.Rodape;

/**
 * Classe que representa o modelo N9 do anexo X.
 *
 * @author Pedro H. Lira
 */
public class N9 extends Rodape {

    public N9() {
        padrao = "N9";
    }
}
