package br.com.cresceritsolutions.ibspdv.models.hb;
// Generated 09/02/2015 13:13:15 by Hibernate Tools 4.3.1

import java.util.HashSet;
import java.util.Set;

/**
 * TipoPagamento generated by hbm2java
 */
public class TipoPagamento implements java.io.Serializable, Comparable<TipoPagamento> {

    private Integer id;
    private String descricao;
    private Set itensPrecoFranquias = new HashSet(0);
    private Set vendas = new HashSet(0);

    public TipoPagamento() {
    }

    public TipoPagamento(String descricao, Set itensPrecoFranquias, Set vendas) {
        this.descricao = descricao;
        this.itensPrecoFranquias = itensPrecoFranquias;
        this.vendas = vendas;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Set getItensPrecoFranquias() {
        return this.itensPrecoFranquias;
    }

    public void setItensPrecoFranquias(Set itensPrecoFranquias) {
        this.itensPrecoFranquias = itensPrecoFranquias;
    }

    public Set getVendas() {
        return this.vendas;
    }

    public void setVendas(Set vendas) {
        this.vendas = vendas;
    }

    @Override
    public String toString() {
        return this.descricao; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int compareTo(TipoPagamento t) {
        return this.toString().compareTo(t.toString());
    }

}
