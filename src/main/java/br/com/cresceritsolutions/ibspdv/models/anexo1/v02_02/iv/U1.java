package br.com.cresceritsolutions.ibspdv.models.anexo1.v02_02.iv;

import br.com.cresceritsolutions.ibspdv.models.anexo1.Bean;



public class U1 extends Bean {

    // IDENTIFICAÇÃO  DO  ESTABELECIMENTO  USUÁRIO  DO  PAFECF
    private String cnpj;
    private String ie;
    private String im;
    private String razao;

    public U1() {
        padrao = "U1";
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getIe() {
        return ie;
    }

    public void setIe(String ie) {
        this.ie = ie;
    }

    public String getIm() {
        return im;
    }

    public void setIm(String im) {
        this.im = im;
    }

    public String getRazao() {
        return razao;
    }

    public void setRazao(String razao) {
        this.razao = razao;
    }

}
