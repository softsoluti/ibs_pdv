package br.com.cresceritsolutions.ibspdv.models.hb;
// Generated 09/02/2015 13:13:15 by Hibernate Tools 4.3.1

import java.util.HashSet;
import java.util.Set;

/**
 * SituacaoTributaria generated by hbm2java
 */
public class SituacaoTributaria implements java.io.Serializable, Comparable<SituacaoTributaria> {

    private Integer id;
    private Character codigo;
    private String descricao;
    private Set produtos = new HashSet(0);

    public SituacaoTributaria() {
    }

    public SituacaoTributaria(Character codigo, String descricao, Set produtos) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.produtos = produtos;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Character getCodigo() {
        return this.codigo;
    }

    public void setCodigo(Character codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Set getProdutos() {
        return this.produtos;
    }

    public void setProdutos(Set produtos) {
        this.produtos = produtos;
    }

    @Override
    public String toString() {
        return "[" + this.getCodigo() + "] " + this.getDescricao(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int compareTo(SituacaoTributaria t) {
        return this.toString().compareTo(t.toString());
    }

}
