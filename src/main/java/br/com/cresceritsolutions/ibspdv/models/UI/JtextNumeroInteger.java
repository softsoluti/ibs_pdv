/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.models.UI;

import br.com.cresceritsolutions.ibspdv.util.Util;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JTextField;

/**
 *
 * @author Gutem
 */
public class JtextNumeroInteger extends JTextField {

    public JtextNumeroInteger() {
        this.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent fe) {
                JtextNumeroInteger.this.selectAll();
            }
        });
        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                JtextNumeroInteger.this.setText(Util.getRetornaApenasNumero(JtextNumeroInteger.this.getText()));
            }
        });
    }

}
