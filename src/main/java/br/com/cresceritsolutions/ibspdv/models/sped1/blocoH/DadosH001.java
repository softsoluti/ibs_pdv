package br.com.cresceritsolutions.ibspdv.models.sped1.blocoH;

import br.com.cresceritsolutions.ibspdv.models.sped1.Bean;

public class DadosH001 extends Bean {

    private int ind_mov;

    public DadosH001() {
        super("H001");
    }

    public int getInd_mov() {
        return ind_mov;
    }

    public void setInd_mov(int ind_mov) {
        this.ind_mov = ind_mov;
    }
}
