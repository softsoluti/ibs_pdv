/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.cresceritsolutions.ibspdv.models.factory;

import br.com.cresceritsolutions.ibspdv.models.hb.Transportadora;
import br.com.cresceritsolutions.ibspdv.models.hb.Endereco;
import br.com.cresceritsolutions.ibspdv.models.hb.Estado;
import br.com.cresceritsolutions.ibspdv.models.hb.Municipio;
import br.com.cresceritsolutions.ibspdv.models.hb.Pessoa;
import br.com.cresceritsolutions.ibspdv.models.hb.PessoaJuridica;
import br.com.cresceritsolutions.ibspdv.models.hb.Telefone;
import br.com.cresceritsolutions.ibspdv.models.hb.Transportadora;
import java.util.Date;

/**
 *
 * @author Gutem
 */
public class TransportadoraFactory {

    public static Transportadora novo() {
    Transportadora transportadora = new Transportadora();
        transportadora.setPessoaJuridica(new PessoaJuridica());
        transportadora.getPessoaJuridica().setPessoa(new Pessoa());
        transportadora.getPessoaJuridica().getPessoa().setEndereco(new Endereco());
        transportadora.getPessoaJuridica().getPessoa().getTelefones().add(new Telefone());
        transportadora.getPessoaJuridica().getPessoa().getTelefones().add(new Telefone());
        transportadora.getPessoaJuridica().getPessoa().getEndereco().setMunicipio(new Municipio());
        transportadora.getPessoaJuridica().getPessoa().getEndereco().getMunicipio().setEstado(new Estado());
        transportadora.setDataInclusao(new Date());
        return transportadora;
    }
    
}
