/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.models.enuns;

/**
 *
 * @author Gutem
 */
public enum SituacaoTransferencia {

    EM_TRANSFERENCIA("EM_TRANSFERENCIA", 0), CONCLUIDO("CANCELADO", 1), CANCELADO("CANCELADO", 2);

    private final String valor;
    private final Integer id;

    private SituacaoTransferencia(String s, Integer id) {
        valor = s;
        this.id = id;
    }

    public static String get(int situacao) {
        switch (situacao) {
            case 0:
                return "EM TRANSFERÊNCIA";
            case 1:
                return "CONCLUÍDO";
            case 2:
                return "CANCELADO";
        }
        return null;
    }

    @Override
    public String toString() {
        return valor;
    }

    public Integer getId() {
        return id;
    }

}
