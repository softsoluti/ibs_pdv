package br.com.cresceritsolutions.ibspdv.models.UI;

import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JCheckBox;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;


/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
/**
 *
 * @author Gutemberg
 */
public class JCheckListIBS {

    Object[] lista;
    JList listCheckBox;
    JList listDescription;

    public JCheckListIBS() {
        listCheckBox = new JList(new DefaultListModel());
        listDescription = new JList(new DefaultListModel());
    }
    public JCheckListIBS(DefaultListModel dlm) {
        listCheckBox = new JList(dlm);
        listDescription = new JList(dlm);
    }

    public void criaCheckList(JScrollPane jScrollPane, Object[] listData, Object[] values) {
        lista = listData;
        listCheckBox = new JList(buildCheckBoxItems(listData, values));
        setListDescription(new JList(listData));
        getListDescription().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        getListDescription().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                if (me.getClickCount() != 2) {
                    return;
                }
                int selectedIndex = getListDescription().locationToIndex(me.getPoint());
                if (selectedIndex < 0) {
                    return;
                }
                CheckBoxItem item = (CheckBoxItem) listCheckBox.getModel().getElementAt(selectedIndex);
                item.setChecked(!item.isChecked());
                listCheckBox.repaint();

            }
        });
        getListDescription().addKeyListener(
                new KeyAdapter() {
                    @Override
                    public void keyPressed(KeyEvent e) {
                        if (e.getKeyCode() == 32) {
                            CheckBoxItem item = (CheckBoxItem) listCheckBox.getModel().getElementAt(getListDescription().getSelectedIndex());
                            item.setChecked(!item.isChecked());
                            listCheckBox.repaint();
                        }
                    }
                });
        listCheckBox.setCellRenderer(new CheckBoxRenderer());
        listCheckBox.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listCheckBox.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                int selectedIndex = listCheckBox.locationToIndex(me.getPoint());
                if (selectedIndex < 0) {
                    return;
                }
                CheckBoxItem item = (CheckBoxItem) listCheckBox.getModel().getElementAt(selectedIndex);
                item.setChecked(!item.isChecked());
                getListDescription().setSelectedIndex(selectedIndex);
                listCheckBox.repaint();
            }
        });
        listCheckBox.addKeyListener(
                new KeyAdapter() {
                    @Override
                    public void keyPressed(KeyEvent e) {
                        if (e.getKeyCode() == 32) {
                            CheckBoxItem item = (CheckBoxItem) listCheckBox.getModel().getElementAt(getListDescription().getSelectedIndex());
                            item.setChecked(!item.isChecked());
                            getListDescription().setSelectedIndex(listCheckBox.getSelectedIndex());
                            listCheckBox.repaint();
                        }
                    }
                });
        jScrollPane.setRowHeaderView(listCheckBox);
        jScrollPane.setViewportView(getListDescription());
        getListDescription().setFixedCellHeight(20);
        listCheckBox.setFixedCellHeight(getListDescription().getFixedCellHeight());
        listCheckBox.setFixedCellWidth(20);
    }

    private CheckBoxItem[] buildCheckBoxItems(Object[] descricao, Object[] values) {
        CheckBoxItem[] checkboxItems = new CheckBoxItem[descricao.length];
        for (int counter = 0; counter < descricao.length; counter++) {
            CheckBoxItem cbi = new CheckBoxItem();
            cbi.setChecked((boolean) values[counter]);
            checkboxItems[counter] = cbi;
        }
        return checkboxItems;
    }

    /**
     * @return the listDescription
     */
    public JList getListDescription() {
        return listDescription;
    }

    /**
     * @param listDescription the listDescription to set
     */
    public void setListDescription(JList listDescription) {
        this.listDescription = listDescription;
    }


    /*
     * Inner class to hold data for JList with checkboxes
     */
    class CheckBoxItem {

        private boolean isChecked;

        public CheckBoxItem() {
            isChecked = false;
        }

        public boolean isChecked() {
            return isChecked;
        }

        public void setChecked(boolean value) {
            isChecked = value;
        }
    }

    /*
     * Inner class that renders JCheckBox to JList
     */
    class CheckBoxRenderer extends JCheckBox implements ListCellRenderer {

        public CheckBoxRenderer() {
            setBackground(UIManager.getColor("List.textBackground"));
            setForeground(UIManager.getColor("List.textForeground"));
        }

        public Component getListCellRendererComponent(JList listBox, Object obj, int currentindex,
                boolean isChecked, boolean hasFocus) {
            setSelected(((CheckBoxItem) obj).isChecked());
            return this;
        }
    }

    public boolean[] getValueSelects() {
        boolean[] selects = new boolean[listCheckBox.getModel().getSize()];
        for (int i = 0; i < listCheckBox.getModel().getSize(); i++) {
            CheckBoxItem item = (CheckBoxItem) listCheckBox.getModel().getElementAt(i);
            selects[i] = item.isChecked();
        }
        return selects;
    }

    public Object[] getDescricaoItens() {
        return lista;
    }

    public List getListSelects() {
        List selects = new ArrayList<Object>();
        for (int i = 0; i < listCheckBox.getModel().getSize(); i++) {
            CheckBoxItem item = (CheckBoxItem) listCheckBox.getModel().getElementAt(i);
            if (item.isChecked) {
                selects.add(getListDescription().getModel().getElementAt(i));
            }
        }
        return selects;
    }

    private int getContSelects() {
        int contador = 0;
        for (int i = 0; i < listCheckBox.getModel().getSize(); i++) {
            CheckBoxItem item = (CheckBoxItem) listCheckBox.getModel().getElementAt(i);
            if (item.isChecked) {
                contador++;
            }
        }
        return contador;
    }

    public void selectAll(boolean s) {
        for (int i = 0; i < listCheckBox.getModel().getSize(); i++) {
            CheckBoxItem item = (CheckBoxItem) listCheckBox.getModel().getElementAt(i);
            item.setChecked(s);
        }
        listCheckBox.repaint();
    }
    public void setSelect(boolean s,int i) {
        CheckBoxItem item = (CheckBoxItem) listCheckBox.getModel().getElementAt(i);
        item.setChecked(s);
        listCheckBox.repaint();
    }

    public int getSelectIndex() {
        return listDescription.getSelectedIndex();
    }

    public Object getSelectValue() {
        return listDescription.getSelectedValue().toString();
    }
}
