package br.com.cresceritsolutions.ibspdv.models.anexo1.v02_02.iv;

import br.com.cresceritsolutions.ibspdv.models.anexo1.Bean;
import java.util.Date;


public class A2 extends Bean{//TOTAL DIÁRIO DE MEIOS DE PAGAMENTO
	private Date data;
	private String meioPagamento;
	private String codigoTpDoc;
	private Double valor;
	public A2() {
		padrao = "A2";
	}

	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public String getMeioPagamento() {
		return meioPagamento;
	}
	public void setMeioPagamento(String meioPagamento) {
		this.meioPagamento = meioPagamento;
	}
	public String getCodigoTpDoc() {// 1=cupom fiscal, 2=Comprovante Não Fiscal, 3=Nota Fiscal
		return codigoTpDoc;
	}
	public void setCodigoTpDoc(String codigoTpDoc) {
		this.codigoTpDoc = codigoTpDoc;
	}
	public Double getValor() {
		return valor*100;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	
}
