/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.models.factory;

import br.com.cresceritsolutions.ibspdv.models.hb.Cargo;
import br.com.cresceritsolutions.ibspdv.models.hb.Endereco;
import br.com.cresceritsolutions.ibspdv.models.hb.Estado;
import br.com.cresceritsolutions.ibspdv.models.hb.Colaborador;
import br.com.cresceritsolutions.ibspdv.models.hb.Municipio;
import br.com.cresceritsolutions.ibspdv.models.hb.Pessoa;
import br.com.cresceritsolutions.ibspdv.models.hb.PessoaFisica;
import br.com.cresceritsolutions.ibspdv.models.hb.Telefone;
import java.util.Date;

/**
 *
 * @author Gutem
 */
public class ColaboradorFactory {

    public static Colaborador novoFisica() {
        Colaborador colaborador = new Colaborador();
        colaborador.setPessoaFisica(new PessoaFisica());
        colaborador.getPessoaFisica().setPessoa(new Pessoa());
        colaborador.getPessoaFisica().getPessoa().setEndereco(new Endereco());
        colaborador.getPessoaFisica().getPessoa().getTelefones().add(new Telefone());
        colaborador.getPessoaFisica().getPessoa().getTelefones().add(new Telefone());
        colaborador.getPessoaFisica().getPessoa().getEndereco().setMunicipio(new Municipio());
        colaborador.getPessoaFisica().getPessoa().getEndereco().getMunicipio().setEstado(new Estado());
        colaborador.setDataInclusao(new Date());
        colaborador.setCargo(new Cargo());
        return colaborador;
    }


}
