package br.com.cresceritsolutions.ibspdv.models.anexo1.v02_02.iv;

import java.util.Date;

import br.com.cresceritsolutions.ibspdv.models.anexo1.Bean;

public class H2 extends Bean{
	//IDENTIFICAÇÃO DO TÍTULO, DO ADQUIRENTE, DO ECF E DA CREDENCIADORA
	private String cnpj;
	private String nFabricacaoEcf;
	private String mfAdicional;
	private String tpEcf;
	private String marcaEcf;
	private String modeloEcf;
	private String coo;
	private String ccf;
	private Double valorTroco;
	private Date dataTroco;
	private String cpf;
	private String titulo;
	public H2() {
		padrao = "H2";
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getnFabricacaoEcf() {
		return nFabricacaoEcf;
	}
	public void setnFabricacaoEcf(String nFabricacaoEcf) {
		this.nFabricacaoEcf = nFabricacaoEcf;
	}
	public String getMfAdicional() {
		return mfAdicional;
	}
	public void setMfAdicional(String mfAdicional) {
		this.mfAdicional = mfAdicional;
	}
	public String getTpEcf() {
		return tpEcf;
	}
	public void setTpEcf(String tpEcf) {
		this.tpEcf = tpEcf;
	}
	public String getMarcaEcf() {
		return marcaEcf;
	}
	public void setMarcaEcf(String marcaEcf) {
		this.marcaEcf = marcaEcf;
	}
	public String getModeloEcf() {
		return modeloEcf;
	}
	public void setModeloEcf(String modeloEcf) {
		this.modeloEcf = modeloEcf;
	}
	public String getCoo() {
		return coo;
	}
	public void setCoo(String coo) {
		this.coo = coo;
	}
	public String getCcf() {
		return ccf;
	}
	public void setCcf(String ccf) {
		this.ccf = ccf;
	}
	public double getValorTroco() {
		return valorTroco;
	}
	public void setValorTroco(double valorTroco) {
		this.valorTroco = valorTroco;
	}
	public Date getDataTroco() {
		return dataTroco;
	}
	public void setDataTroco(Date dataTroco) {
		this.dataTroco = dataTroco;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	
}
