package br.com.cresceritsolutions.ibspdv.models.ecf;


import br.com.cresceritsolutions.ibspdv.models.core.Dados;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Classe que representa um documento impresso pelo sistama.
 *
 * @author Pedro H. Lira
 */
public class EcfDocumento extends Dados implements Serializable {

    private Integer ecfDocumentoId;
    private int ecfDocumentoUsuario;
    private int ecfDocumentoCoo;
    private int ecfDocumentoGnf;
    private int ecfDocumentoGrg;
    private int ecfDocumentoCdc;
    private String ecfDocumentoTipo;
    private Date ecfDocumentoData;
    private EcfImpressora ecfImpressora;

    /**
     * Construtor padrao
     */
    public EcfDocumento() {
        this(0);
    }

    /**
     * Contrutor padrao passando o id
     *
     * @param ecfDocumentoId o id do registro.
     */
    public EcfDocumento(Integer ecfDocumentoId) {
        super("EcfDocumento", "ecfDocumentoId", "ecfDocumentoData");
        this.ecfDocumentoId = ecfDocumentoId;
    }

    @Override
    public Integer getId() {
        return ecfDocumentoId;
    }

    @Override
    public void setId(Integer id) {
        ecfDocumentoId = id;
    }

    public Integer getEcfDocumentoId() {
        return ecfDocumentoId;
    }

    public void setEcfDocumentoId(Integer ecfDocumentoId) {
        this.ecfDocumentoId = ecfDocumentoId;
    }

    public int getEcfDocumentoUsuario() {
        return ecfDocumentoUsuario;
    }

    public void setEcfDocumentoUsuario(int ecfDocumentoUsuario) {
        this.ecfDocumentoUsuario = ecfDocumentoUsuario;
    }

    public int getEcfDocumentoCoo() {
        return ecfDocumentoCoo;
    }

    public void setEcfDocumentoCoo(int ecfDocumentoCoo) {
        this.ecfDocumentoCoo = ecfDocumentoCoo;
    }

    public int getEcfDocumentoGnf() {
        return ecfDocumentoGnf;
    }

    public void setEcfDocumentoGnf(int ecfDocumentoGnf) {
        this.ecfDocumentoGnf = ecfDocumentoGnf;
    }

    public int getEcfDocumentoGrg() {
        return ecfDocumentoGrg;
    }

    public void setEcfDocumentoGrg(int ecfDocumentoGrg) {
        this.ecfDocumentoGrg = ecfDocumentoGrg;
    }

    public int getEcfDocumentoCdc() {
        return ecfDocumentoCdc;
    }

    public void setEcfDocumentoCdc(int ecfDocumentoCdc) {
        this.ecfDocumentoCdc = ecfDocumentoCdc;
    }

    public String getEcfDocumentoTipo() {
        return ecfDocumentoTipo;
    }

    public void setEcfDocumentoTipo(String ecfDocumentoTipo) {
        this.ecfDocumentoTipo = ecfDocumentoTipo;
    }

    public Date getEcfDocumentoData() {
        return ecfDocumentoData;
    }

    public void setEcfDocumentoData(Date ecfDocumentoData) {
        this.ecfDocumentoData = ecfDocumentoData;
    }

    public EcfImpressora getEcfImpressora() {
        return ecfImpressora;
    }

    public void setEcfImpressora(EcfImpressora ecfImpressora) {
        this.ecfImpressora = ecfImpressora;
    }

    public static List<EcfDocumento> getDocumentos(int id) {
		/*Class_Conexao_mysql cc = new Class_Conexao_mysql();
		cc.set_Conexao_Ibs_Base();
		cc.Conectar();
		List<EcfDocumento> l = new ArrayList<EcfDocumento>();
		try {
			cc.executaSqlSelect("SELECT ID,ID_USUARIO,COO,GNF,GRG,CDC,TIPO,DATA FROM ecf_documento WHERE ID='" +id+"'");
			while(cc.getRs().next()){
				EcfDocumento temp = new EcfDocumento();
				temp.setId(cc.getRs().getInt("ID"));
				temp.setEcfDocumentoUsuario(cc.getRs().getInt("ID_USUARIO"));
				temp.setEcfDocumentoCoo(cc.getRs().getInt("COO"));
				temp.setEcfDocumentoGnf(cc.getRs().getInt("GNG"));
				temp.setEcfDocumentoGrg(cc.getRs().getInt("GRF"));
				temp.setEcfDocumentoCdc(cc.getRs().getInt("CDC"));
				temp.setEcfDocumentoTipo(cc.getRs().getString("TIPO"));
				temp.setEcfDocumentoData(cc.getRs().getDate("DATA"));
				l.add(temp);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			cc.Desconectar();
		}*/
		return null;
	}

	public static List<EcfDocumento> getDocumento(EcfDocumento ecfDocumento,
			String inicio, String fim, EcfImpressora impressora) {
		/*Class_Conexao_mysql cc = new Class_Conexao_mysql();
		cc.set_Conexao_Ibs_Base();
		cc.Conectar();
		List<EcfDocumento> l = new ArrayList<EcfDocumento>();
		try {
			cc.executaSqlSelect("SELECT ID,ID_USUARIO,COO,GNF,GRF,CDC,TIPO,DATA,ID_IMPRESSORA FROM ecf_documento WHERE DATA>='" +inicio+"' AND DATA<='"+fim+"'");
			while(cc.getRs().next()){
				EcfDocumento temp = new EcfDocumento();
				temp.setId(cc.getRs().getInt("ID"));
				temp.setEcfDocumentoUsuario(cc.getRs().getInt("ID_USUARIO"));
				temp.setEcfDocumentoCoo(cc.getRs().getInt("COO"));
				temp.setEcfDocumentoGnf(cc.getRs().getInt("GNF"));
				temp.setEcfDocumentoGrg(cc.getRs().getInt("GRF"));
				temp.setEcfDocumentoCdc(cc.getRs().getInt("CDC"));
				temp.setEcfDocumentoTipo(cc.getRs().getString("TIPO"));
				temp.setEcfDocumentoData(cc.getRs().getDate("DATA"));
				temp.setEcfImpressora(EcfImpressora.getImpressora(cc.getRs().getInt("ID_IMPRESSORA"), new EcfImpressora()));
				l.add(temp);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			cc.Desconectar();
		}*/
		return null;
	}

	public static int salvar(EcfDocumento doc, String nPedido) throws SQLException {
		/*Class_Conexao_mysql cc = new Class_Conexao_mysql();
		cc.set_Conexao_Ibs_Base();
		cc.Conectar();
		try {
			cc.executaSqlInsert("INSERT INTO ecf_documento (Id,ID_PEDIDO,ID_USUARIO,COO,GNF,GRG,CDC,TIPO,DATA,ID_IMPRESSORA,ID_ENTERPRISE)" +
					" VALUES("+doc.getId() + 
					",'" + nPedido +
					"','" + doc.getEcfDocumentoUsuario() +
					"','" + doc.getEcfDocumentoCoo() +
					"','" + doc.getEcfDocumentoGnf() +
					"','" + doc.getEcfDocumentoGrg() +
					"','" + doc.getEcfDocumentoCdc() +
					"','" + doc.getEcfDocumentoTipo() +
					"','" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(doc.getEcfDocumentoData()) +
					"','" + doc.getEcfImpressora().getId() +
					"','"+Class_Usuario_Logado.getIdEmpresa()+"')");
			cc.executaSqlSelect("SELECT LAST_INSERT_ID()");
			if(cc.getRs().first()){
				return cc.getRs().getInt(1);
			}
		}finally{
			cc.Desconectar();
		}*/
		return 0;
	}
}
