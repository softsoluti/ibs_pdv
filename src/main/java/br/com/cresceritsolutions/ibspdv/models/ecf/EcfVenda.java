package br.com.cresceritsolutions.ibspdv.models.ecf;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

import br.com.cresceritsolutions.ibspdv.models.core.Dados;
import br.com.cresceritsolutions.ibspdv.models.core.EDirecao;
import br.com.cresceritsolutions.ibspdv.models.hb.Cliente;
import br.com.cresceritsolutions.ibspdv.models.hb.Usuario;


/**
 * Classe que representa a venda do sistama.
 *
 * @author Pedro H. Lira
 */
public class EcfVenda extends Dados implements Serializable {

    private Integer ecfVendaId;
    private int ecfVendaCcf;
    private int ecfVendaCoo;
    private Date ecfVendaData;
    private Double ecfVendaBruto;
    private Double ecfVendaDesconto;
    private Double ecfVendaAcrescimo;
    private Double ecfVendaLiquido;
    private boolean ecfVendaFechada;
    private boolean ecfVendaCancelada;
    private String ecfVendaObservacao;
    private Usuario sisUsuario;
    private Usuario sisVendedor;
    private Usuario sisGerente;
    private EcfZ ecfZ;
    private Cliente sisCliente;
    private List<EcfVendaProduto> ecfVendaProdutos;
    private List<EcfPagamento> ecfPagamentos;
    private transient boolean informouCliente;

    /**
     * Construtor padrao
     */
    public EcfVenda() {
        this(0);
    }

    /**
     * Contrutor padrao passando o id
     *
     * @param ecfVendaId o id do registro.
     */
    public EcfVenda(Integer ecfVendaId) {
        super("EcfVenda", "ecfVendaId", "ecfVendaData", EDirecao.DESC);
        this.ecfVendaId = ecfVendaId;
    }

    @Override
    public Integer getId() {
        return ecfVendaId;
    }

    @Override
    public void setId(Integer id) {
        ecfVendaId = id;
    }

    // GETs e SETs
    public Integer getEcfVendaId() {
        return ecfVendaId;
    }

    public void setEcfVendaId(Integer ecfVendaId) {
        this.ecfVendaId = ecfVendaId;
    }

    public int getEcfVendaCcf() {
        return ecfVendaCcf;
    }

    public void setEcfVendaCcf(int ecfVendaCcf) {
        this.ecfVendaCcf = ecfVendaCcf;
    }

    public int getEcfVendaCoo() {
        return ecfVendaCoo;
    }

    public void setEcfVendaCoo(int ecfVendaCoo) {
        this.ecfVendaCoo = ecfVendaCoo;
    }

    public Date getEcfVendaData() {
        return ecfVendaData;
    }

    public void setEcfVendaData(Date ecfVendaData) {
        this.ecfVendaData = ecfVendaData;
    }

    public Double getEcfVendaBruto() {
        return ecfVendaBruto;
    }

    public void setEcfVendaBruto(Double ecfVendaBruto) {
        this.ecfVendaBruto = ecfVendaBruto;
    }

    public Double getEcfVendaDesconto() {
        return ecfVendaDesconto;
    }

    public void setEcfVendaDesconto(Double ecfVendaDesconto) {
        this.ecfVendaDesconto = ecfVendaDesconto;
    }

    public Double getEcfVendaAcrescimo() {
        return ecfVendaAcrescimo;
    }

    public void setEcfVendaAcrescimo(Double ecfVendaAcrescimo) {
        this.ecfVendaAcrescimo = ecfVendaAcrescimo;
    }

    public Double getEcfVendaLiquido() {
        return ecfVendaLiquido;
    }

    public void setEcfVendaLiquido(Double ecfVendaLiquido) {
        this.ecfVendaLiquido = ecfVendaLiquido;
    }

    public boolean getEcfVendaFechada() {
        return ecfVendaFechada;
    }

    public void setEcfVendaFechada(boolean ecfVendaFechada) {
        this.ecfVendaFechada = ecfVendaFechada;
    }

    public boolean getEcfVendaCancelada() {
        return ecfVendaCancelada;
    }

    public void setEcfVendaCancelada(boolean ecfVendaCancelada) {
        this.ecfVendaCancelada = ecfVendaCancelada;
    }

    public EcfZ getEcfZ() {
        return ecfZ;
    }

    public void setEcfZ(EcfZ ecfZ) {
        this.ecfZ = ecfZ;
    }

    public List<EcfVendaProduto> getEcfVendaProdutos() {
        return ecfVendaProdutos;
    }

    public void setEcfVendaProdutos(List<EcfVendaProduto> ecfVendaProdutos) {
        this.ecfVendaProdutos = ecfVendaProdutos;
    }

    public List<EcfPagamento> getEcfPagamentos() {
        return ecfPagamentos;
    }

    public void setEcfPagamentos(List<EcfPagamento> ecfPagamentos) {
        this.ecfPagamentos = ecfPagamentos;
    }

    public Cliente getSisCliente() {
        return sisCliente;
    }

    public void setSisCliente(Cliente sisCliente) {
        this.sisCliente = sisCliente;
    }

    public Usuario getSisUsuario() {
        return sisUsuario;
    }

    public void setSisUsuario(Usuario sisUsuario) {
        this.sisUsuario = sisUsuario;
    }

    public Usuario getSisVendedor() {
        return sisVendedor;
    }

    public void setSisVendedor(Usuario sisVendedor) {
        this.sisVendedor = sisVendedor;
    }

    public Usuario getSisGerente() {
        return sisGerente;
    }

    public void setSisGerente(Usuario sisGerente) {
        this.sisGerente = sisGerente;
    }

    public boolean isInformouCliente() {
        return informouCliente;
    }

    public void setInformouCliente(boolean informouCliente) {
        this.informouCliente = informouCliente;
    }

    public String getEcfVendaObservacao() {
        return ecfVendaObservacao;
    }

    public void setEcfVendaObservacao(String ecfVendaObservacao) {
        this.ecfVendaObservacao = ecfVendaObservacao;
    }
}
