package br.com.cresceritsolutions.ibspdv.models.anexo1.v;

import br.com.cresceritsolutions.ibspdv.models.anexo1.Rodape;

/**
 * Classe que representa o modelo P9 do anexo V.
 *
 * @author Pedro H. Lira
 */
public class P9 extends Rodape {

    public P9() {
        padrao = "P9";
    }
}
