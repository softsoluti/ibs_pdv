/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.models.factory;

import br.com.cresceritsolutions.ibspdv.models.hb.Endereco;
import br.com.cresceritsolutions.ibspdv.models.hb.Estado;
import br.com.cresceritsolutions.ibspdv.models.hb.Empresa;
import br.com.cresceritsolutions.ibspdv.models.hb.Municipio;
import br.com.cresceritsolutions.ibspdv.models.hb.Pessoa;
import br.com.cresceritsolutions.ibspdv.models.hb.PessoaJuridica;
import br.com.cresceritsolutions.ibspdv.models.hb.Telefone;
import java.util.Date;

/**
 *
 * @author Gutem
 */
public class EmpresaFactory {

    public static Empresa novo() {
        Empresa empresa = new Empresa();
        empresa.setAtivo(true);
        empresa.setPessoaJuridica(new PessoaJuridica());
        empresa.getPessoaJuridica().setPessoa(new Pessoa());
        empresa.getPessoaJuridica().getPessoa().setEndereco(new Endereco());
        empresa.getPessoaJuridica().getPessoa().getTelefones().add(new Telefone());
        empresa.getPessoaJuridica().getPessoa().getTelefones().add(new Telefone());
        empresa.getPessoaJuridica().getPessoa().getEndereco().setMunicipio(new Municipio());
        empresa.getPessoaJuridica().getPessoa().getEndereco().getMunicipio().setEstado(new Estado());
        empresa.setDataInclusao(new Date());
        return empresa;
    }
}
