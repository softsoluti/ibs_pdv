/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.cresceritsolutions.ibspdv.models.UI;

import br.com.cresceritsolutions.ibspdv.util.ImagesUtil;
import java.awt.Component;
import javax.swing.JDialog;

/**
 *
 * @author Gutem
 */
public class Dialog extends JDialog{
        
    public Dialog(Component c, boolean unDecoreted, boolean modal) {
        super.setUndecorated(unDecoreted);
        super.setModal(modal);
        super.setIconImage(ImagesUtil.getInstance().getMarcaIcone().getImage());
        super.setSize(c.getPreferredSize().width, c.getPreferredSize().height + 30);
        super.add(c);
    }

}
