/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.models.UI;

import br.com.cresceritsolutions.ibspdv.util.ImagesUtil;
import br.com.cresceritsolutions.ibspdv.view.core.PrincipalPDV;
import javax.swing.JDialog;

/**
 *
 * @author gutemberg
 */
public class DialogIBS extends JDialog {

    public DialogIBS() {
        super(PrincipalPDV.me);
    }

    @Override
    public void setVisible(boolean b) {
        if (b) {
            super.setIconImage(ImagesUtil.getInstance().getMarcaIcone().getImage());
            super.setLocationRelativeTo(null);
            super.setModal(true);
        }
        super.setVisible(b); //To change body of generated methods, choose Tools | Templates.
    }

}
