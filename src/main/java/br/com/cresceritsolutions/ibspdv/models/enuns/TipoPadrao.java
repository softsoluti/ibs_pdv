/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.models.enuns;

/**
 *
 * @author Gutem
 */
public enum TipoPadrao {

    CELULAR("CELULAR"), CHIP_NP("CHIP NP"), CHIP_PRE_ATIVO("CHIP PRÉ ATIVO"), DIVERSO("DIVERSO");
    private final String valor;

    private TipoPadrao(String s) {
        valor = s;
    }

    @Override
    public String toString() {
        return valor;
    }

}
