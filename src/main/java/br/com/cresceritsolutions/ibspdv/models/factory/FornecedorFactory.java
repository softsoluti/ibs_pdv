/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.cresceritsolutions.ibspdv.models.factory;

import br.com.cresceritsolutions.ibspdv.models.hb.Fornecedor;
import br.com.cresceritsolutions.ibspdv.models.hb.Endereco;
import br.com.cresceritsolutions.ibspdv.models.hb.Estado;
import br.com.cresceritsolutions.ibspdv.models.hb.Municipio;
import br.com.cresceritsolutions.ibspdv.models.hb.Pessoa;
import br.com.cresceritsolutions.ibspdv.models.hb.PessoaJuridica;
import br.com.cresceritsolutions.ibspdv.models.hb.Telefone;
import br.com.cresceritsolutions.ibspdv.models.hb.Fornecedor;
import java.util.Date;

/**
 *
 * @author Gutem
 */
public class FornecedorFactory {

    public static Fornecedor novo() {
    Fornecedor fornecedor = new Fornecedor();
        fornecedor.setPessoaJuridica(new PessoaJuridica());
        fornecedor.getPessoaJuridica().setPessoa(new Pessoa());
        fornecedor.getPessoaJuridica().getPessoa().setEndereco(new Endereco());
        fornecedor.getPessoaJuridica().getPessoa().getTelefones().add(new Telefone());
        fornecedor.getPessoaJuridica().getPessoa().getTelefones().add(new Telefone());
        fornecedor.getPessoaJuridica().getPessoa().getEndereco().setMunicipio(new Municipio());
        fornecedor.getPessoaJuridica().getPessoa().getEndereco().getMunicipio().setEstado(new Estado());
        fornecedor.setDataInclusao(new Date());
        return fornecedor;
    }
    
}
