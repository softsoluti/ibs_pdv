/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.models.enuns;

/**
 *
 * @author Gutem
 */
public enum OpcoesTituloPrecoFranquia {

    NADA("--"), FABRICANTE("-FABRICANTE"), MODELO("-MODELO"), CODIGO_SAP("-CÓDIGO SAP"), GAMA("-GAMA"), PLANO("-PLANO");
    private final String valor;

    private OpcoesTituloPrecoFranquia(String s) {
        valor = s;
    }

    @Override
    public String toString() {
        return valor;
    }

}
