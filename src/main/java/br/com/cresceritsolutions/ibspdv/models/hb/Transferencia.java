package br.com.cresceritsolutions.ibspdv.models.hb;
// Generated 09/02/2015 13:13:15 by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Transferencia generated by hbm2java
 */
public class Transferencia  implements java.io.Serializable {


     private Integer id;
     private CanalVenda canalVendaByCanalVendaDestino;
     private CanalVenda canalVendaByCanalVendaOrigem;
     private Empresa empresa;
     private Usuario usuario;
     private Date data;
     private Integer situacao;
     private Set itemTransferencias = new HashSet(0);

    public Transferencia() {
    }

	
    public Transferencia(CanalVenda canalVendaByCanalVendaDestino, CanalVenda canalVendaByCanalVendaOrigem, Empresa empresa, Usuario usuario) {
        this.canalVendaByCanalVendaDestino = canalVendaByCanalVendaDestino;
        this.canalVendaByCanalVendaOrigem = canalVendaByCanalVendaOrigem;
        this.empresa = empresa;
        this.usuario = usuario;
    }
    public Transferencia(CanalVenda canalVendaByCanalVendaDestino, CanalVenda canalVendaByCanalVendaOrigem, Empresa empresa, Usuario usuario, Date data, Integer situacao, Set itemTransferencias) {
       this.canalVendaByCanalVendaDestino = canalVendaByCanalVendaDestino;
       this.canalVendaByCanalVendaOrigem = canalVendaByCanalVendaOrigem;
       this.empresa = empresa;
       this.usuario = usuario;
       this.data = data;
       this.situacao = situacao;
       this.itemTransferencias = itemTransferencias;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public CanalVenda getCanalVendaByCanalVendaDestino() {
        return this.canalVendaByCanalVendaDestino;
    }
    
    public void setCanalVendaByCanalVendaDestino(CanalVenda canalVendaByCanalVendaDestino) {
        this.canalVendaByCanalVendaDestino = canalVendaByCanalVendaDestino;
    }
    public CanalVenda getCanalVendaByCanalVendaOrigem() {
        return this.canalVendaByCanalVendaOrigem;
    }
    
    public void setCanalVendaByCanalVendaOrigem(CanalVenda canalVendaByCanalVendaOrigem) {
        this.canalVendaByCanalVendaOrigem = canalVendaByCanalVendaOrigem;
    }
    public Empresa getEmpresa() {
        return this.empresa;
    }
    
    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }
    public Usuario getUsuario() {
        return this.usuario;
    }
    
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    public Date getData() {
        return this.data;
    }
    
    public void setData(Date data) {
        this.data = data;
    }
    public Integer getSituacao() {
        return this.situacao;
    }
    
    public void setSituacao(Integer situacao) {
        this.situacao = situacao;
    }
    public Set getItemTransferencias() {
        return this.itemTransferencias;
    }
    
    public void setItemTransferencias(Set itemTransferencias) {
        this.itemTransferencias = itemTransferencias;
    }




}


