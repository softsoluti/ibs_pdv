/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.models.enuns;

/**
 *
 * @author Gutem
 */
public enum SituacaoVenda {

    VENDIDO("VENDIDO", 1), CANCELADO("CANCELADO", 2), DEVOLVIDO("DEVOLVIDO", 3), DEFEITO("DEFEITO", 4), EM_ABERTO("EM ABERTO", 5), PENDENTE("PENDENTE", 6);

    private final String valor;
    private final Integer id;

    private SituacaoVenda(String s, Integer id) {
        valor = s;
        this.id = id;
    }

    public static String get(int situacao) {
        switch (situacao) {
            case 1:
                return "VENDIDO";
            case 2:
                return "CANCELADO";
            case 3:
                return "DEVOLVIDO";
            case 4:
                return "DEFEITO";
            case 5:
                return "EM ABERTO";
            case 6:
                return "PENDENTE";
        }
        return null;
    }

    @Override
    public String toString() {
        return valor;
    }

    public Integer getId() {
        return id;
    }

}
