/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.models.enuns;

/**
 *
 * @author Gutem
 */
public enum EnumTipoVenda {

    CONCOMITANTE("CONCOMITANTE", 1), PRE_VENDA("PRÉ-VENDA", 2), NFE("NF-e", 3);

    private final String valor;
    private final Integer id;

    private EnumTipoVenda(String s, Integer id) {
        valor = s;
        this.id = id;
    }

    public static String get(int situacao) {
        switch (situacao) {
            case 1:
                return "CONCOMITANTE";
            case 2:
                return "PRÉ-VENDA";
            case 3:
                return "NF-e";
        }
        return null;
    }

    @Override
    public String toString() {
        return valor;
    }

    public Integer getId() {
        return id;
    }

}
