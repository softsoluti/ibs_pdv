package br.com.cresceritsolutions.ibspdv.models.hb;
// Generated 09/02/2015 13:13:15 by Hibernate Tools 4.3.1

import java.util.Date;

/**
 * ServicoPacote generated by hbm2java
 */
public class ServicoPacote implements java.io.Serializable, Comparable<ServicoPacote> {

    private Integer id;
    private ItemVenda itemVenda;
    private PlanoDeVenda planoDeVenda;
    private boolean ativo;
    private Date data;
    private String linha;
    private String iccid;
    private String protocolo;
    private String observacao;

    public ServicoPacote() {
    }

    public ServicoPacote(PlanoDeVenda planoDeVenda, boolean ativo, Date data) {
        this.planoDeVenda = planoDeVenda;
        this.ativo = ativo;
        this.data = data;
    }

    public ServicoPacote(ItemVenda itemVenda, PlanoDeVenda planoDeVenda, boolean ativo, Date data, String linha, String iccid, String protocolo, String observacao) {
        this.itemVenda = itemVenda;
        this.planoDeVenda = planoDeVenda;
        this.ativo = ativo;
        this.data = data;
        this.linha = linha;
        this.iccid = iccid;
        this.protocolo = protocolo;
        this.observacao = observacao;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ItemVenda getItemVenda() {
        return this.itemVenda;
    }

    public void setItemVenda(ItemVenda itemVenda) {
        this.itemVenda = itemVenda;
    }

    public PlanoDeVenda getPlanoDeVenda() {
        return this.planoDeVenda;
    }

    public void setPlanoDeVenda(PlanoDeVenda planoDeVenda) {
        this.planoDeVenda = planoDeVenda;
    }

    public boolean isAtivo() {
        return this.ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public Date getData() {
        return this.data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getLinha() {
        return this.linha;
    }

    public void setLinha(String linha) {
        this.linha = linha;
    }

    public String getIccid() {
        return this.iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public String getProtocolo() {
        return this.protocolo;
    }

    public void setProtocolo(String protocolo) {
        this.protocolo = protocolo;
    }

    public String getObservacao() {
        return this.observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    @Override
    public String toString() {
        return this.getPlanoDeVenda().getPlano().getDescricao() + " " + this.getPlanoDeVenda().getProtocolo(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int compareTo(ServicoPacote t) {
        return this.toString().compareTo(t.toString());
    }

}
