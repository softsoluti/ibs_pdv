/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.util;

import java.awt.Color;

/**
 *
 * @author walsirene
 */
public class CoresUtil {

    private static CoresUtil me;

    public static CoresUtil getInstance() {
        if (me == null) {
            me = new CoresUtil();
        }
        return me;
    }

    private CoresUtil() {
    }

    public Color getZebrado() {
        return zebrado;
    }

    public void setZebrado(Color zebrado) {
        this.zebrado = zebrado;
    }

    
    private Color verdeClaro = new Color(0, 150, 0);
    private Color preto = new Color(0, 0, 0);
    private Color branco = new Color(255, 255, 255);
    private Color vermelho = new Color(255, 0, 0);
    private Color vermelhoClaro = new Color(230, 180, 180);
    private Color cinza = new Color(200, 200, 200);
    private Color cinzaClaro = new Color(240, 240, 240);
    private Color Azul = new Color(50, 50, 200);
    private Color AzulClaro = new Color(105, 105, 255);
    private Color verdeMar = new Color(120, 150, 120);
    private Color laranja = new Color(255, 192, 0);
    private Color marfim = new Color(192, 81, 72);
    private Color zebrado = new Color(235, 235, 255);
    /**
     * @return the verdeClaro
     */
    public Color getVerde() {
        return getVerdeClaro();
    }

    /**
     * @return the preto
     */
    public Color getPreto() {
        return preto;
    }

    /**
     * @return the branco
     */
    public Color getBranco() {
        return branco;
    }

    /**
     * @return the vermelho
     */
    public Color getVermelho() {
        return vermelho;
    }

    /**
     * @return the cinza
     */
    public Color getCinza() {
        return cinza;
    }

    /**
     * @return the cinzaClaro
     */
    public Color getCinzaClaro() {
        return cinzaClaro;
    }

    /**
     * @return the Azul
     */
    public Color getAzul() {
        return Azul;
    }

    /**
     * @return the AzulClaro
     */
    public Color getAzulClaro() {
        return AzulClaro;
    }

    /**
     * @return the verdeMar
     */
    public Color getVerdeMar() {
        return verdeMar;
    }

    /**
     * @return the vermelhoClaro
     */
    public Color getVermelhoClaro() {
        return vermelhoClaro;
    }

    /**
     * @return the verdeClaro
     */
    public Color getVerdeClaro() {
        return verdeClaro;
    }

    /**
     * @param verdeClaro the verdeClaro to set
     */
    public void setVerdeClaro(Color verdeClaro) {
        this.verdeClaro = verdeClaro;
    }

    /**
     * @param preto the preto to set
     */
    public void setPreto(Color preto) {
        this.preto = preto;
    }

    /**
     * @param branco the branco to set
     */
    public void setBranco(Color branco) {
        this.branco = branco;
    }

    /**
     * @param vermelho the vermelho to set
     */
    public void setVermelho(Color vermelho) {
        this.vermelho = vermelho;
    }

    /**
     * @param vermelhoClaro the vermelhoClaro to set
     */
    public void setVermelhoClaro(Color vermelhoClaro) {
        this.vermelhoClaro = vermelhoClaro;
    }

    /**
     * @param cinza the cinza to set
     */
    public void setCinza(Color cinza) {
        this.cinza = cinza;
    }

    /**
     * @param cinzaClaro the cinzaClaro to set
     */
    public void setCinzaClaro(Color cinzaClaro) {
        this.cinzaClaro = cinzaClaro;
    }

    /**
     * @param Azul the Azul to set
     */
    public void setAzul(Color Azul) {
        this.Azul = Azul;
    }

    /**
     * @param AzulClaro the AzulClaro to set
     */
    public void setAzulClaro(Color AzulClaro) {
        this.AzulClaro = AzulClaro;
    }

    /**
     * @param verdeMar the verdeMar to set
     */
    public void setVerdeMar(Color verdeMar) {
        this.verdeMar = verdeMar;
    }

    /**
     * @return the laranja
     */
    public Color getLaranja() {
        return laranja;
    }

    /**
     * @param laranja the laranja to set
     */
    public void setLaranja(Color laranja) {
        this.laranja = laranja;
    }

    /**
     * @return the marfim
     */
    public Color getMarfim() {
        return marfim;
    }

    /**
     * @param marfim the marfim to set
     */
    public void setMarfim(Color marfim) {
        this.marfim = marfim;
    }
}
