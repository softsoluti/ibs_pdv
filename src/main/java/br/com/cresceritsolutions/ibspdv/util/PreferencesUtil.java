package br.com.cresceritsolutions.ibspdv.util;

import java.util.prefs.Preferences;

public class PreferencesUtil {

    private static PreferencesUtil me;
    private Preferences preferences;

    private PreferencesUtil() {
        preferences = Preferences.userNodeForPackage(PreferencesUtil.class);
    }

    public static PreferencesUtil getInstance() {
        if (me == null) {
            me = new PreferencesUtil();
        }
        return me;
    }

    public void setCredentials(String Variavel, String Dado) {
        preferences.put(Variavel, Dado);
    }

    public String getVariavel(String Variavel) {
        return preferences.get(Variavel, null);
    }
}
