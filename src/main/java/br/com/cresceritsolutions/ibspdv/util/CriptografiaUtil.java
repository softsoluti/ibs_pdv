/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.util;

import static br.com.cresceritsolutions.ibspdv.util.VariaveisDoSistema.AUXILIAR;
import com.gargoylesoftware.htmlunit.javascript.host.Event;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;
import org.jasypt.util.text.BasicTextEncryptor;

/**
 *
 * @author Administrador
 */
public class CriptografiaUtil {

    private static CriptografiaUtil me;

    private CriptografiaUtil() {
    }

    public static CriptografiaUtil getInstance() {
        if (me == null) {
            me = new CriptografiaUtil();
        }
        return me;
    }

    public void encriptarAuxiliar() throws Exception {
        CriptografiaUtil.getInstance().criptografar("conf/auxiliar.txt", AUXILIAR);
    }

    /**
     * Metodo que criptografa o arquivo auxiliar do sistema.
     *
     * @param path local de geracao do arquivo, se null salva no padrao.
     * @param mapa conjunto de dados chave/valor.
     * @throws Exception dispara caso nao consiga.
     */
    public void criptografar(String path, Properties mapa) throws Exception {
        // recuperando os valores
        StringBuilder sb = new StringBuilder();
        for (String chave : mapa.stringPropertyNames()) {
            sb.append(chave).append("=").append(mapa.getProperty(chave)).append("\n");
        }
        if (new File(path).exists()) {
            FileWriter outArquivo = new FileWriter(path);
            String dados = getInstance().encriptar(sb.toString());
            outArquivo.write(dados);
            outArquivo.flush();
        } else {
            throw new Exception("Arquivo nao existe -> " + path);
        }
    }

    /**
     * Metodo que descriptografa o arquivo auxiliar do sistema.
     *
     * @param path local de geracao do arquivo, se null recupera do padrao.
     * @param mapa conjunto de dados chave/valor.
     * @throws Exception dispara caso nao consiga.
     */
    public void descriptografar(String path, Properties mapa) throws Exception {
        // lendo dados do arquivo para assinar
        mapa.clear();
        byte[] bytes;
        if (new File(path).exists()) {
            FileInputStream inArquivo = new FileInputStream(path);
            bytes = new byte[inArquivo.available()];
            inArquivo.read(bytes);
        } else {
            throw new Exception("Arquivo nao existe -> " + path);
        }
        // inserindo os valores
        String[] props = getInstance().descriptar(new String(bytes)).split("\n");
        for (String prop : props) {
            if (prop.contains("=")) {
                String[] chaveValor = prop.split("=");
                if (chaveValor.length == 2) {
                    mapa.put(chaveValor[0], chaveValor[1]);
                }
            }
        }
    }

    /**
     * Metodo que criptografa um texto passado usando a chave privada.
     *
     * @param texto valor a ser criptografado.
     * @return o texto informado criptografado.
     */
    private String encriptar(String texto) {
        if (texto != null) {
            BasicTextEncryptor encryptor = new BasicTextEncryptor();
            encryptor.setPassword(Chaves.privada);
            return encryptor.encrypt(texto);
        } else {
            return null;
        }
    }

    /**
     * Metodo que descriptografa um texto passado usando a chave privada.
     *
     * @param texto valor a ser descriptografado.
     * @return o texto informado descriptografado.
     */
    private String descriptar(String texto) {
        if (texto != null) {
            BasicTextEncryptor encryptor = new BasicTextEncryptor();
            encryptor.setPassword(Chaves.privada);
            return encryptor.decrypt(texto);
        } else {
            return null;
        }
    }

    /*public static void main(String args[]) throws Exception {
     getInstance().criptografar("", VariaveisDoSistema.AUXILIAR);
     }*/
    public String encryptMD5(String user, String password) throws NoSuchAlgorithmException {
        String sign = user + password;

        java.security.MessageDigest md
                = java.security.MessageDigest.getInstance("MD5");
        md.update(sign.getBytes());
        byte[] hash = md.digest();
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            if ((0xff & hash[i]) < 0x10) {
                hexString.append(
                        "0" + Integer.toHexString((0xFF & hash[i])));
            } else {
                hexString.append(Integer.toHexString(0xFF & hash[i]));
            }
        }
        sign = hexString.toString();

        return sign;
    }

    public static String SE() {//SENHA EMAIL
        String retorno = "";
        Integer[] senha = {71 * 578, 117 * 578, 116 * 578, 51 * 578, 109 * 578, 52 * 578, 53 * 578, 54 * 578, 42 * 578, 47 * 578};
        for (int i = 0; i < senha.length; i++) {
            retorno += String.valueOf(Character.toChars(senha[i] / 578));
        }
        return retorno;
    }

    public static String SBL() {//SENHA BANCO LOCAL
        String retorno = "";
        Integer[] senha = {52 * 578, 114 * 578, 67 * 578, 109 * 578, 51 * 578, 100 * 578, 51 * 578, 53 * 578};
        for (int i = 0; i < senha.length; i++) {
            retorno += String.valueOf(Character.toChars(senha[i] / 578));
        }
        return retorno;
    }

    public static String SBV() {//SENHA BANCO VALIDACAO
        String retorno = "";
        Integer[] senha = {52 * 578, 114 * 578, 67 * 578, 109 * 578, 51 * 578, 100 * 578, 51 * 578, 53 * 578};
        for (int i = 0; i < senha.length; i++) {
            retorno += String.valueOf(Character.toChars(senha[i] / 578));
        }
        return retorno;
    }

    public static void main(String args[]) {
        String retorno = "";

        Integer[] senha = {71 * 578, 117 * 578, 116 * 578, 101 * 578, 109 * 578, 52 * 578, 53 * 578, 54 * 578, 42 * 578, 47 * 578};
        for (int i = 0; i < senha.length; i++) {
            retorno += String.valueOf(Character.toChars(senha[i] / 578));
        }
        //Site para conversão http://jdstiles.com/java/cct.html
        System.out.println(retorno);
    }

}
