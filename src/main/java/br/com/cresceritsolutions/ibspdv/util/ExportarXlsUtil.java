/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.util;

import br.com.cresceritsolutions.ibspdv.util.ExibirParaUsuario;
import br.com.cresceritsolutions.ibspdv.util.Util;
import br.com.cresceritsolutions.ibspdv.util.Zip;
import br.com.cresceritsolutions.ibspdv.view.core.PrincipalPDV;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.compress.archivers.zip.ZipUtil;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

/**
 *
 * @author Gutemberg
 */
public class ExportarXlsUtil {

    private static ExportarXlsUtil me;

    public static ExportarXlsUtil getInstance() {
        if (me == null) {
            me = new ExportarXlsUtil();
        }
        return me;
    }

    private HSSFWorkbook wb;

    public void renameSheet(int sheet, String name) {
        getWb().setSheetName(sheet, name);
    }

    @SuppressWarnings("ConvertToTryWithResources")
    public void setPlanilhaEntrada(String file) {
        try {
            //arquivo origem
            InputStream inp = new FileInputStream(file);
            setWb(new HSSFWorkbook(inp));
            inp.close();
        } catch (IOException ex) {
            ExibirParaUsuario.getInstance().erro(PrincipalPDV.getInstance(), ex, "Erro ao carregar arquivo de modelo!", false);
        }
    }

    public void escrevePlanilha(String texto, int i, int j, int ws, int tp) {
        HSSFSheet sheet = getWb().getSheetAt(ws);
        HSSFRow row = sheet.getRow(i);//linha
        if (row == null) {
            row = sheet.createRow(i);
        }
        HSSFCell cell = row.getCell(j);//coluna
        if (cell == null) {
            cell = row.createCell(j);
        }
        if (tp == 1) {
            cell.setCellType(HSSFCell.CELL_TYPE_STRING);
        }
        if (tp == 2) {
            texto = texto.replace(".", ",");
        }
        if (tp == 3) {
            Util.FormataDataPT(texto);
        }
        if (tp == 4) {
            cell.setCellType(Cell.CELL_TYPE_FORMULA);
            cell.setCellFormula(texto);
        } else {
            if (tp == 5) {
                cell.setCellType(Cell.CELL_TYPE_NUMERIC);
            }
            cell.setCellValue(texto);
        }
    }

    @SuppressWarnings("ConvertToTryWithResources")
    public void salvaPlanilhaEm(String file) throws OutOfMemoryError, IOException {//salva planilha
        FileOutputStream fileOut = new FileOutputStream(file);
        getWb().write(fileOut);
        fileOut.close();
        Zip.compactar(file);
    }

    public void escrevePlanilhaTexto(int sheet, int linha, int coluna, String texto) {
        escrevePlanilha(texto, linha, coluna, sheet, 1);
    }
    //Mapa de vendas

    public void setEscreveValorCordenadaMA(String texto, int sheet, String servico, String dia) {
        int linha = -1, coluna = -1;
        Sheet sheet1 = wb.getSheetAt(sheet);
        for (Row row : sheet1) {
            for (Cell cell : row) {
                CellReference cellRef = new CellReference(row.getRowNum(), cell.getColumnIndex());

                if (cellRef.getRow() == 3 && cellRef.getCol() >= 2 && cellRef.getCol() <= 32) {
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    if (cell.getStringCellValue().equals(dia.trim())) {
                        coluna = cellRef.getCol();
                    }
                }
                if (cellRef.getRow() >= 4 && cellRef.getRow() <= 68 && cellRef.getCol() == 1) {
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    if (cell.getStringCellValue().equals(servico.trim())) {
                        linha = cellRef.getRow();
                    }
                }

                if (cellRef.getCol() == coluna && cellRef.getRow() == linha
                        && coluna < 33 && coluna > 1
                        && linha > 3 && linha < 68) {
                    escrevePlanilhaTexto(sheet, linha, coluna, texto);
                }
            }
        }
    }

    public void setEscreveValorCordenada(String texto, int sheet, String servico, String dia) {
        int linha = -1, coluna = -1;
        Sheet sheet1 = getWb().getSheetAt(sheet);
        for (Row row : sheet1) {
            for (Cell cell : row) {
                CellReference cellRef = new CellReference(row.getRowNum(), cell.getColumnIndex());

                if (cellRef.getRow() == 7 && cellRef.getCol() >= 2 && cellRef.getCol() <= 33) {
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    if (cell.getStringCellValue().equals(dia.trim())) {
                        coluna = cellRef.getCol();
                    }
                }
                if (cellRef.getRow() >= 8 && cellRef.getCol() == 1) {
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    if (cell.getStringCellValue().trim().equals(servico.trim())) {
                        linha = cellRef.getRow();
                    }
                }

                if (cellRef.getCol() == coluna && cellRef.getRow() == linha
                        && coluna < 33 && coluna > 1
                        && linha > 7) {
                    escrevePlanilhaTexto(sheet, linha, coluna, texto);
                }
            }
        }
    }

    /**
     * @return the wb
     */
    public HSSFWorkbook getWb() {
        return wb;
    }

    /**
     * @param wb the wb to set
     */
    public void setWb(HSSFWorkbook wb) {
        this.wb = wb;
    }
}
