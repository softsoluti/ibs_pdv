/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.util;

import br.com.cresceritsolutions.ibspdv.models.util.Correios;
import br.com.cresceritsolutions.ibspdv.view.core.PrincipalPDV;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.text.MaskFormatter;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * Classe responsavel para funcoes utilitarias.
 *
 * @author Pedro H. Lira
 */
public class Util {

    // tabela com vinculos das letras
    private static Map<String, String> config;
    private static final int[] pesoCPF = {11, 10, 9, 8, 7, 6, 5, 4, 3, 2};
    private static final int[] pesoCNPJ = {6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2};
    public static final String[] OPCOES = {"Sim", "Não"};

    public static boolean isParcebleDouble(String valor) {
        try {
            Double.parseDouble(valor);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    /**
     * Constutor padrao.
     */
    private Util() {
    }

    /**
     * Metodo que normaliza os caracteres removendo os acentos.
     *
     * @param texto o texto acentuado.
     * @return o texto sem acentos.
     */
    public static String normaliza(String texto) {
        CharSequence cs = new StringBuilder(texto == null ? "" : texto);
        return Normalizer.normalize(cs, Normalizer.Form.NFKD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
    }

    /**
     * Metodo que normaliza os caracteres removendo os acentos de todos os
     * campos de um objeto.
     *
     * @param bloco o objeto que sera modificado.
     */
    public static void normaliza(Object bloco) {
        for (Method metodo : bloco.getClass().getMethods()) {
            try {
                if (isGetter(metodo)) {
                    Object valorMetodo = metodo.invoke(bloco, new Object[]{});

                    if (metodo.getReturnType() == String.class) {
                        String nomeMetodo = metodo.getName().replaceFirst("get", "set");
                        Method set = bloco.getClass().getMethod(nomeMetodo, new Class[]{String.class});
                        String valor = valorMetodo == null ? "" : valorMetodo.toString();
                        valor = normaliza(valor);
                        set.invoke(bloco, new Object[]{valor.trim()});
                    }
                }
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException ex) {
                // pula o item
            }
        }
    }

    /**
     * Metodo que informa se o metodo da classe é do tipo GET.
     *
     * @param method usando reflection para descrobrir os metodos.
     * @return verdadeiro se o metodo for GET, falso caso contrario.
     */
    public static boolean isGetter(Method method) {
        if (!method.getName().startsWith("get")) {
            return false;
        }
        if (method.getParameterTypes().length != 0) {
            return false;
        }
        if (void.class.equals(method.getReturnType())) {
            return false;
        }
        return true;
    }

    /**
     * Metodo que informa se o metodo da classe é do tipo SET.
     *
     * @param method usando reflection para descrobrir os metodos.
     * @return verdadeiro se o metodo for SET, falso caso contrario.
     */
    public static boolean isSetter(Method method) {
        if (!method.getName().startsWith("set")) {
            return false;
        }
        if (method.getParameterTypes().length == 0) {
            return false;
        }
        if (!void.class.equals(method.getReturnType())) {
            return false;
        }
        return true;
    }

    /**
     * Metodo que formata um texto em data no padrao dd/MM/aaaa
     *
     * @param data o texto da data.
     * @return um objeto Date ou null caso nao consiga fazer o parser.
     */
    public static Date getData(String data) {
        return formataData(data, "dd/MM/yyyy");
    }

    /**
     * Metodo que formata uma data em texto no padrao dd/MM/aaaa
     *
     * @param data o objeto Date.
     * @return uma String formatada ou null caso a data nao seja valida.
     */
    public static String getData(Date data) {
        return formataData(data, "dd/MM/yyyy");
    }

    /**
     * Metodo que formata um texto em data no padrao dd/MM/aaaa HH:mm:ss
     *
     * @param data o texto da data.
     * @return um objeto Date ou null caso nao consiga fazer o parser.
     */
    public static Date getDataHora(String data) {
        return formataData(data, "dd/MM/yyyy HH:mm:ss");
    }

    /**
     * Metodo que formata uma data em texto no padrao dd/MM/aaaa HH:mm:ss
     *
     * @param data o objeto Date.
     * @return uma String formatada ou null caso a data nao seja valida.
     */
    public static String getDataHora(Date data) {
        return formataData(data, "dd/MM/yyyy HH:mm:ss");
    }

    /**
     * Metodo que formata a data.
     *
     * @param data a data do tipo Date.
     * @param formato o formado desejado.
     * @return a data formatada como solicidato.
     */
    public static String formataData(Date data, String formato) {
        try {
            return new SimpleDateFormat(formato).format(data);
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Metodo que formata a data.
     *
     * @param data a data em formato string.
     * @param formato o formado desejado.
     * @return a data como objeto ou null se tiver erro.
     */
    public static Date formataData(String data, String formato) {
        try {
            return new SimpleDateFormat(formato).parse(data);
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * @see #formataNumero(double, int, int, boolean)
     */
    public static String formataNumero(String valor, int inteiros, int decimal, boolean grupo) {
        return formataNumero(Double.valueOf(valor), inteiros, decimal, grupo);
    }

    /**
     * Metodo que faz a formatacao de numeros com inteiros e fracoes
     *
     * @param valor o valor a ser formatado
     * @param inteiros o minimo de inteiros, que serao completados com ZEROS se
     * preciso
     * @param decimal o minimo de decimais, que serao completados com ZEROS se
     * preciso
     * @param grupo se sera colocado separador de grupo de milhar
     * @return uma String com o numero formatado
     */
    public static String formataNumero(double valor, int inteiros, int decimal, boolean grupo) {
        NumberFormat nf = NumberFormat.getIntegerInstance();
        nf.setMinimumIntegerDigits(inteiros);
        nf.setMinimumFractionDigits(decimal);
        nf.setMaximumFractionDigits(decimal);
        nf.setGroupingUsed(grupo);
        return nf.format(valor);
    }

    /**
     * Metodo que formata o texto usando a mascara passada.
     *
     * @param texto o texto a ser formatado.
     * @param mascara a mascara a ser usada.
     * @return o texto formatado.
     * @throws ParseException caso ocorra erro.
     */
    public static String formataTexto(String texto, String mascara) throws ParseException {
        MaskFormatter mf = new MaskFormatter(mascara);
        mf.setValueContainsLiteralCharacters(false);
        return mf.valueToString(texto);
    }

    /**
     * Metodo que formata o texto.
     *
     * @param texto o texto a ser formatado.
     * @param caracter o caracter que sera repetido.
     * @param tamanho o tamanho total do texto de resposta.
     * @param direita a direcao onde colocar os caracteres.
     * @return o texto formatado.
     */
    public static String formataTexto(String texto, String caracter, int tamanho, boolean direita) {
        StringBuilder sb = new StringBuilder();
        int fim = tamanho - texto.length();
        for (int i = 0; i < fim; i++) {
            sb.append(caracter);
        }
        return direita ? texto + sb.toString() : sb.toString() + texto;
    }

    /**
     * Metodo que calcula o digito.
     *
     * @param str valor do texto.
     * @param peso array de pesos.
     * @return um numero calculado.
     */
    private static int calcularDigito(String str, int[] peso) {
        int soma = 0;
        for (int indice = str.length() - 1, digito; indice >= 0; indice--) {
            digito = Integer.parseInt(str.substring(indice, indice + 1));
            soma += digito * peso[peso.length - str.length() + indice];
        }
        soma = 11 - soma % 11;
        return soma > 9 ? 0 : soma;
    }

    /**
     * Metodo que valida se e CPF
     *
     * @param cpf o valor do texto.
     * @return verdadeiro se valido, falso caso contrario.
     */
    public static boolean isCPF(String cpf) {
        cpf = cpf.replaceAll("\\D", "");
        if ((cpf == null) || (cpf.length() != 11)) {
            return false;
        } else {
            Pattern p = Pattern.compile(cpf.charAt(0) + "{11}");
            Matcher m = p.matcher(cpf);
            if (m.find()) {
                return false;
            }
        }

        Integer digito1 = calcularDigito(cpf.substring(0, 9), pesoCPF);
        Integer digito2 = calcularDigito(cpf.substring(0, 9) + digito1, pesoCPF);
        return cpf.equals(cpf.substring(0, 9) + digito1.toString() + digito2.toString());
    }

    /**
     * Metodo que valida se e CNPJ
     *
     * @param cnpj o valor do texto.
     * @return verdadeiro se valido, falso caso contrario.
     */
    public static boolean isCNPJ(String cnpj) {
        cnpj = cnpj.replaceAll("\\D", "");
        if ((cnpj == null) || (cnpj.length() != 14)) {
            return false;
        } else {
            Pattern p = Pattern.compile(cnpj.charAt(0) + "{14}");
            Matcher m = p.matcher(cnpj);
            if (m.find()) {
                return false;
            }
        }

        Integer digito1 = calcularDigito(cnpj.substring(0, 12), pesoCNPJ);
        Integer digito2 = calcularDigito(cnpj.substring(0, 12) + digito1, pesoCNPJ);
        return cnpj.equals(cnpj.substring(0, 12) + digito1.toString() + digito2.toString());
    }

    /**
     * Metodo que recupera as configuracoes do sistema.
     *
     * @return Um mapa de String contendo chave/valor.
     */
    public static Map<String, String> getConfig() {
        if (config == null) {
            Properties props = new Properties();
            try (FileInputStream fis = new FileInputStream("conf" + System.getProperty("file.separator") + "config.properties")) {
                props.load(fis);
                config = new HashMap<>();
                for (String chave : props.stringPropertyNames()) {
                    config.put(chave, props.getProperty(chave));
                }
            } catch (Exception ex) {
                config = null;
            }
        }
        return config;
    }

    /**
     * Metodo que seleciona um item da combo pelo valor.
     *
     * @param combo a ser verificada.
     * @param valor a ser comparado.
     */
    public static void selecionarCombo(JComboBox combo, String valor) {
        for (int i = 0; i < combo.getItemCount(); i++) {
            String item = combo.getItemAt(i).toString();
            if (item.startsWith(valor)) {
                combo.setSelectedIndex(i);
                break;
            }
        }
    }

    public static String getRetornaDataUs(String Data) {
        String data = Data, hora = "00:00:00";

        data = data.trim();
        String dia = "00", mes = "00", ano = "0000";
        String c1;
        String c2;
        String c3;
        String c4;
        String c5;
        String c6;
        String c7;
        String c8;
        String c9;
        String c10;
        String c11;
        String c12;
        String c13;
        String c14;
        String c15;
        String c16;
        String c17;
        String c18;
        String c19;

        if (data.length() != 0 || data != null) {
            if (data.length() == 6) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                if (c2.equals("/") && c4.equals("/")) {
                    dia = "0" + c1;
                    mes = "0" + c3;
                    ano = "20" + c5 + c6;
                }
            }
            if (data.length() == 7) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                if (c3.equals("/") && c5.equals("/")) {
                    dia = c1 + c2;
                    mes = "0" + c4;
                    ano = "20" + c6 + c7;
                }
                if (c2.equals("/") && c5.equals("/")) {
                    dia = "0" + c1;
                    mes = c3 + c4;
                    ano = "20" + c6 + c7;
                }
            }
            if (data.length() == 8) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                if (c3.equals("/") && c6.equals("/")) {
                    dia = c1 + c2;
                    mes = c4 + c5;
                    ano = "20" + c7 + c8;
                }
                if (c2.equals("/") && c4.equals("/")) {
                    dia = "0" + c1;
                    mes = "0" + c3;
                    ano = c5 + c6 + c7 + c8;
                }
            }
            if (data.length() == 9) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                if (c3.equals("/") && c5.equals("/")) {
                    dia = c1 + c2;
                    mes = "0" + c4;
                    ano = c6 + c7 + c8 + c9;
                }
                if (c2.equals("/") && c5.equals("/")) {
                    dia = "0" + c1;
                    mes = c3 + c4;
                    ano = c6 + c7 + c8 + c9;
                }
            }
            if (data.length() == 10) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                c10 = String.valueOf(data.charAt(9));
                if (c3.equals("/") && c6.equals("/")) {
                    dia = c1 + c2;
                    mes = c4 + c5;
                    ano = c7 + c8 + c9 + c10;
                }
            }

            if (data.length() == 19) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                c10 = String.valueOf(data.charAt(9));
                c11 = String.valueOf(data.charAt(10));
                c12 = String.valueOf(data.charAt(11));
                c13 = String.valueOf(data.charAt(12));
                c14 = String.valueOf(data.charAt(13));
                c15 = String.valueOf(data.charAt(14));
                c16 = String.valueOf(data.charAt(15));
                c17 = String.valueOf(data.charAt(16));
                c18 = String.valueOf(data.charAt(17));
                c19 = String.valueOf(data.charAt(18));

                dia = c1 + c2;
                mes = c4 + c5;
                ano = c7 + c8 + c9 + c10;

                hora = c12 + c13 + c14 + c15 + c16 + c17 + c18 + c19;

            }
        }
        return ano + "-" + mes + "-" + dia;
    }

    @SuppressWarnings("AssignmentToMethodParameter")
    public static String getDiaDoMes(String dataPt) {
        String data = dataPt, hora = "00:00:00";

        data = data.trim();
        String dia = "00", mes = "00", ano = "0000";
        String c1;
        String c2;
        String c3;
        String c4;
        String c5;
        String c6;
        String c7;
        String c8;
        String c9;
        String c10;
        String c11;
        String c12;
        String c13;
        String c14;
        String c15;
        String c16;
        String c17;
        String c18;
        String c19;

        if (data.length() != 0 || data != null) {
            if (data.length() == 6) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                if (c2.equals("/") && c4.equals("/")) {
                    dia = "0" + c1;
                    mes = "0" + c3;
                    ano = "20" + c5 + c6;
                }
            }
            if (data.length() == 7) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                if (c3.equals("/") && c5.equals("/")) {
                    dia = c1 + c2;
                    mes = "0" + c4;
                    ano = "20" + c6 + c7;
                }
                if (c2.equals("/") && c5.equals("/")) {
                    dia = "0" + c1;
                    mes = c3 + c4;
                    ano = "20" + c6 + c7;
                }
            }
            if (data.length() == 8) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                if (c3.equals("/") && c6.equals("/")) {
                    dia = c1 + c2;
                    mes = c4 + c5;
                    ano = "20" + c7 + c8;
                }
                if (c2.equals("/") && c4.equals("/")) {
                    dia = "0" + c1;
                    mes = "0" + c3;
                    ano = c5 + c6 + c7 + c8;
                }
            }
            if (data.length() == 9) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                if (c3.equals("/") && c5.equals("/")) {
                    dia = c1 + c2;
                    mes = "0" + c4;
                    ano = c6 + c7 + c8 + c9;
                }
                if (c2.equals("/") && c5.equals("/")) {
                    dia = "0" + c1;
                    mes = c3 + c4;
                    ano = c6 + c7 + c8 + c9;
                }
            }
            if (data.length() == 10) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                c10 = String.valueOf(data.charAt(9));
                if (c3.equals("/") && c6.equals("/")) {
                    dia = c1 + c2;
                    mes = c4 + c5;
                    ano = c7 + c8 + c9 + c10;
                }
            }

            if (data.length() == 19) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                c10 = String.valueOf(data.charAt(9));
                c11 = String.valueOf(data.charAt(10));
                c12 = String.valueOf(data.charAt(11));
                c13 = String.valueOf(data.charAt(12));
                c14 = String.valueOf(data.charAt(13));
                c15 = String.valueOf(data.charAt(14));
                c16 = String.valueOf(data.charAt(15));
                c17 = String.valueOf(data.charAt(16));
                c18 = String.valueOf(data.charAt(17));
                c19 = String.valueOf(data.charAt(18));

                dia = c1 + c2;
                mes = c4 + c5;
                ano = c7 + c8 + c9 + c10;

                hora = c12 + c13 + c14 + c15 + c16 + c17 + c18 + c19;

            }
        }
        if (dia.length() == 1) {
            dia = "0" + dia;
        }
        return dia;
    }

    public static int getMes(String Data) {
        String data = Data, hora = "00:00:00";

        data = data.trim();
        String dia = "00", mes = "00", ano = "0000";
        String c1;
        String c2;
        String c3;
        String c4;
        String c5;
        String c6;
        String c7;
        String c8;
        String c9;
        String c10;
        String c11;
        String c12;
        String c13;
        String c14;
        String c15;
        String c16;
        String c17;
        String c18;
        String c19;

        if (data.length() != 0 || data != null) {
            if (data.length() == 6) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                if (c2.equals("/") && c4.equals("/")) {
                    dia = "0" + c1;
                    mes = "0" + c3;
                    ano = "20" + c5 + c6;
                }
            }
            if (data.length() == 7) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                if (c3.equals("/") && c5.equals("/")) {
                    dia = c1 + c2;
                    mes = "0" + c4;
                    ano = "20" + c6 + c7;
                }
                if (c2.equals("/") && c5.equals("/")) {
                    dia = "0" + c1;
                    mes = c3 + c4;
                    ano = "20" + c6 + c7;
                }
            }
            if (data.length() == 8) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                if (c3.equals("/") && c6.equals("/")) {
                    dia = c1 + c2;
                    mes = c4 + c5;
                    ano = "20" + c7 + c8;
                }
                if (c2.equals("/") && c4.equals("/")) {
                    dia = "0" + c1;
                    mes = "0" + c3;
                    ano = c5 + c6 + c7 + c8;
                }
            }
            if (data.length() == 9) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                if (c3.equals("/") && c5.equals("/")) {
                    dia = c1 + c2;
                    mes = "0" + c4;
                    ano = c6 + c7 + c8 + c9;
                }
                if (c2.equals("/") && c5.equals("/")) {
                    dia = "0" + c1;
                    mes = c3 + c4;
                    ano = c6 + c7 + c8 + c9;
                }
            }
            if (data.length() == 10) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                c10 = String.valueOf(data.charAt(9));
                if (c3.equals("/") && c6.equals("/")) {
                    dia = c1 + c2;
                    mes = c4 + c5;
                    ano = c7 + c8 + c9 + c10;
                }
            }

            if (data.length() == 19) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                c10 = String.valueOf(data.charAt(9));
                c11 = String.valueOf(data.charAt(10));
                c12 = String.valueOf(data.charAt(11));
                c13 = String.valueOf(data.charAt(12));
                c14 = String.valueOf(data.charAt(13));
                c15 = String.valueOf(data.charAt(14));
                c16 = String.valueOf(data.charAt(15));
                c17 = String.valueOf(data.charAt(16));
                c18 = String.valueOf(data.charAt(17));
                c19 = String.valueOf(data.charAt(18));

                dia = c1 + c2;
                mes = c4 + c5;
                ano = c7 + c8 + c9 + c10;

                hora = c12 + c13 + c14 + c15 + c16 + c17 + c18 + c19;

            }
        }
        return Integer.parseInt(mes);
    }

    public static int getAno(String Data) {
        String data = Data, hora = "00:00:00";

        data = data.trim();
        String dia = "00", mes = "00", ano = "0000";
        String c1;
        String c2;
        String c3;
        String c4;
        String c5;
        String c6;
        String c7;
        String c8;
        String c9;
        String c10;
        String c11;
        String c12;
        String c13;
        String c14;
        String c15;
        String c16;
        String c17;
        String c18;
        String c19;

        if (data.length() != 0 || data != null) {
            if (data.length() == 6) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                if (c2.equals("/") && c4.equals("/")) {
                    dia = "0" + c1;
                    mes = "0" + c3;
                    ano = "20" + c5 + c6;
                }
            }
            if (data.length() == 7) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                if (c3.equals("/") && c5.equals("/")) {
                    dia = c1 + c2;
                    mes = "0" + c4;
                    ano = "20" + c6 + c7;
                }
                if (c2.equals("/") && c5.equals("/")) {
                    dia = "0" + c1;
                    mes = c3 + c4;
                    ano = "20" + c6 + c7;
                }
            }
            if (data.length() == 8) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                if (c3.equals("/") && c6.equals("/")) {
                    dia = c1 + c2;
                    mes = c4 + c5;
                    ano = "20" + c7 + c8;
                }
                if (c2.equals("/") && c4.equals("/")) {
                    dia = "0" + c1;
                    mes = "0" + c3;
                    ano = c5 + c6 + c7 + c8;
                }
            }
            if (data.length() == 9) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                if (c3.equals("/") && c5.equals("/")) {
                    dia = c1 + c2;
                    mes = "0" + c4;
                    ano = c6 + c7 + c8 + c9;
                }
                if (c2.equals("/") && c5.equals("/")) {
                    dia = "0" + c1;
                    mes = c3 + c4;
                    ano = c6 + c7 + c8 + c9;
                }
            }
            if (data.length() == 10) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                c10 = String.valueOf(data.charAt(9));
                if (c3.equals("/") && c6.equals("/")) {
                    dia = c1 + c2;
                    mes = c4 + c5;
                    ano = c7 + c8 + c9 + c10;
                }
            }

            if (data.length() == 19) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                c10 = String.valueOf(data.charAt(9));
                c11 = String.valueOf(data.charAt(10));
                c12 = String.valueOf(data.charAt(11));
                c13 = String.valueOf(data.charAt(12));
                c14 = String.valueOf(data.charAt(13));
                c15 = String.valueOf(data.charAt(14));
                c16 = String.valueOf(data.charAt(15));
                c17 = String.valueOf(data.charAt(16));
                c18 = String.valueOf(data.charAt(17));
                c19 = String.valueOf(data.charAt(18));

                dia = c1 + c2;
                mes = c4 + c5;
                ano = c7 + c8 + c9 + c10;

                hora = c12 + c13 + c14 + c15 + c16 + c17 + c18 + c19;

            }
        }
        return Integer.parseInt(ano);
    }

    public static String FormataDataPT(String Data) {
        if (Data.length() == 10) {
            String data = Data;

            data = data.trim();
            String dia = "00", mes = "00", ano = "0000";
            String c1;
            String c2;
            String c3;
            String c4;
            String c5;
            String c6;
            String c7;
            String c8;
            String c9;
            String c10;
            if (data.length() == 0 || data == null) {
            }
            {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                c10 = String.valueOf(data.charAt(9));
                dia = c9 + c10;
                mes = c6 + c7;
                ano = c1 + c2 + c3 + c4;
            }
            ano = String.valueOf(ano.charAt(2)) + String.valueOf(ano.charAt(3));
            return dia + "/" + mes + "/" + ano;
        } else {
            return Data;
        }
    }//Retorna a data no formato yyyy-mm-dd

    public static String FormataDataPT2(String Data) {
        if (Data.length() == 10) {
            String data = Data;

            data = data.trim();
            String dia = "00", mes = "00", ano = "0000";
            String c1;
            String c2;
            String c3;
            String c4;
            String c5;
            String c6;
            String c7;
            String c8;
            String c9;
            String c10;
            if (data.length() == 0 || data == null) {
            }
            {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                c10 = String.valueOf(data.charAt(9));
                dia = c9 + c10;
                mes = c6 + c7;
                ano = c1 + c2 + c3 + c4;
            }
            ano = String.valueOf(ano.charAt(2)) + String.valueOf(ano.charAt(3));

            return dia + "/" + mes + "/20" + ano;
        } else {
            return Data;
        }
    }

    public static String getValidaComoDecimal(String texto) {
        String dado = texto;
        dado = dado.replace(",", ".");
        if (dado.length() == 0) {
            dado = "0";
        }
        return dado;
    }

    public static String getFormatoDataMysql(String campo) {
        String dado = "";
        dado = "DATE_FORMAT(" + campo + ",'%d/%m/%Y')";
        return dado;
    }

    public static String getFormatoDataMysqlComHora(String campo) {
        String dado = "";
        dado = "DATE_FORMAT(" + campo + ",'%d/%m/%X %k:%i:%S')";
        return dado;
    }

    public static boolean isTelefone(String tel) {
        String formato = "(\\d\\d)\\d\\d\\d\\d-\\d\\d\\d\\d";
        return (tel != null) && (tel.length() == 13) && (tel.matches(formato));
    }

    public static String formatarString(String texto, String mascara) throws ParseException {
        MaskFormatter mf = new MaskFormatter(mascara);
        mf.setValueContainsLiteralCharacters(false);
        return mf.valueToString(texto);
    }

    @SuppressWarnings("AssignmentToMethodParameter")
    public static String colocaVirgulaNaSegundaCasa(String texto) {
        String dado = "";
        String numerico = "0123456789";
        texto = texto.replace(",", "");
        int localVirg = texto.length() - 2;
        for (int i = 0; i < texto.length(); i++) {
            if (numerico.contains(new Character(texto.charAt(i)).toString())) {
                if (i == localVirg) {
                    dado += ",";
                }
                dado += texto.charAt(i);
            }
        }
        return dado;
    }

    @SuppressWarnings("AssignmentToMethodParameter")
    public static String colocaVirgulaNaSegundaCasaPorcentagem(String texto) {
        String dado = "";
        String numerico = "0123456789";
        texto = texto.replace(",", "");
        int localVirg = texto.length() - 2;
        for (int i = 0; i < texto.length(); i++) {
            if (numerico.contains(new Character(texto.charAt(i)).toString())) {
                if (i == localVirg) {
                    dado += ",";
                }
                dado += texto.charAt(i);
            }
        }
        return dado + "%";
    }

    @SuppressWarnings("AssignmentToMethodParameter")
    public static String getFormatoCep(String texto) {
        String dado = "";
        String numerico = "0123456789";
        texto = texto.replace("-", "");
        int localVirg = texto.length() - 3;
        for (int i = 0; i < texto.length(); i++) {
            if (numerico.contains(new Character(texto.charAt(i)).toString())) {
                if (i == localVirg) {
                    dado += "-";
                }
                dado += texto.charAt(i);
            }
        }
        return dado;
    }

    @SuppressWarnings("AssignmentToMethodParameter")
    public static String getFormatoCnpj(String texto) {
        String dado = "";
        String numerico = "0123456789";
        texto = texto.replace(".", "");
        texto = texto.replace("/", "");
        texto = texto.replace("-", "");
        int local1Ponto = texto.length() - 12;
        int local2Ponto = texto.length() - 9;
        int localBarra = texto.length() - 6;
        int localTraco = texto.length() - 2;
        for (int i = 0; i < texto.length(); i++) {
            if (numerico.contains(new Character(texto.charAt(i)).toString())) {
                if (i == local1Ponto) {
                    dado += ".";
                }
                if (i == local2Ponto) {
                    dado += ".";
                }
                if (i == localBarra) {
                    dado += "/";
                }
                if (i == localTraco) {
                    dado += "-";
                }
                dado += texto.charAt(i);
            }
        }
        return dado;
    }

    @SuppressWarnings("AssignmentToMethodParameter")
    public static String getFormatoCpf(String texto) {
        String dado = "";
        String numerico = "0123456789";
        texto = texto.replace(".", "");
        texto = texto.replace("-", "");
        int local1Ponto = texto.length() - 8;
        int local2Ponto = texto.length() - 5;
        int localTraco = texto.length() - 2;
        for (int i = 0; i < texto.length(); i++) {
            if (numerico.contains(new Character(texto.charAt(i)).toString())) {

                if (i == local1Ponto) {
                    dado += ".";
                }
                if (i == local2Ponto) {
                    dado += ".";
                }
                if (i == localTraco) {
                    dado += "-";
                }
                dado += texto.charAt(i);
            }
        }
        return dado;
    }

    @SuppressWarnings("AssignmentToMethodParameter")
    public static String colocaFormatoData(String texto) {
        String dado = "";
        String numerico = "0123456789";
        texto = texto.replace("/", "");
        int local1asp = texto.length() - 4;
        int local2asp = texto.length() - 6;
        for (int i = 0; i < texto.length(); i++) {
            if (numerico.contains(new Character(texto.charAt(i)).toString())) {
                if (i == local1asp) {
                    dado += "/";
                }
                if (i == local2asp) {
                    dado += "/";
                }
                dado += texto.charAt(i);
            }
        }
        return dado;
    }

    public static String getRetornaApenasNumero(String texto) {
        String dado = "";
        String numerico = "0123456789";
        for (int i = 0; i < texto.length(); i++) {
            if (numerico.contains(new Character(texto.charAt(i)).toString())) {
                dado += texto.charAt(i);
            }
        }
        return dado;
    }

    @SuppressWarnings("AssignmentToMethodParameter")
    public static String trocaAcentuacao(String acentuada) {
        if (acentuada == null || acentuada.equals("")) {
            return acentuada;
        }
        char[] acentuados = new char[]{'ç', 'á', 'à', 'ã', 'â', 'ä', 'é',
            'è', 'ê', 'ë', 'í', 'ì', 'î', 'ï', 'ó', 'ò', 'õ', 'ô', 'ö',
            'ú', 'ù', 'û', 'ü'};

        char[] naoAcentuados = new char[]{'c', 'a', 'a', 'a', 'a', 'a', 'e',
            'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o',
            'u', 'u', 'u', 'u'};

        for (int i = 0; i < acentuados.length; i++) {
            acentuada = acentuada.replace(acentuados[i], naoAcentuados[i]);
            acentuada = acentuada.replace(Character.toUpperCase(acentuados[i]), Character.toUpperCase(naoAcentuados[i]));
        }
        return acentuada;
    }

    public static String retornaColunaBdValida(String texto) {//Permite a digitação apenas de caracteres validos para criação de coluna no bd mysql
        String dado = "";
        String validos = " ABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890_!'@#$%&*()-+={[}]><.,";
        for (int i = 0; i < texto.length(); i++) {
            if (validos.contains(new Character(texto.charAt(i)).toString().toUpperCase())) {
                dado += texto.charAt(i);
            }
        }
        return dado;
    }

    public static String getCodSimbol(String simbol) {//Codifica para colunas validas mysql//
        return simbol.replace("'", "simbol_1").replace("!", "simbol_2").replace("@", "simbol_3").replace("#", "simbol_4").replace("$", "simbol_5").replace("%", "simbol_6").replace("&", "simbol_7").replace("*", "simbol_8").replace("(", "simbol_9").replace(")", "simbol_10").replace("-", "simbol_11").replace("+", "simbol_12").replace("=", "simbol_13").replace("[", "simbol_14").replace("]", "simbol_15").replace("{", "simbol_16").replace("}", "simbol_17").replace("<", "simbol_18").replace(">", "simbol_19").replace(",", "simbol_20").replace(".", "simbol_21");
    }

    public static String getSimbolCod(String simbol) {//Decodifica colunas mysql
        return simbol.replace(".", "simbol_21").replace(",", "simbol_20").replace(">", "simbol_19").replace("<", "simbol_18").replace("}", "simbol_17").replace("{", "simbol_16").replace("]", "simbol_15").replace("[", "simbol_14").replace("=", "simbol_13").replace("+", "simbol_12").replace("-", "simbol_11").replace(")", "simbol_10").replace("(", "simbol_9").replace("*", "simbol_8").replace("&", "simbol_7").replace("%", "simbol_6").replace("$", "simbol_5").replace("#", "simbol_4").replace("@", "simbol_3").replace("!", "simbol_2").replace("'", "simbol_1");
    }

    public static String getFormatoMoeda(Object o) {
        try {
            String texto = String.valueOf(o);
            if (texto.equals("null")) {
                texto = "0.00";
            }

            double precoDouble = Double.parseDouble(texto.replace(",", "."));

            DecimalFormat fmt = new DecimalFormat("0.00");    //limita o número de casas decimais      
            String string = fmt.format(precoDouble);
            String[] part = string.split("[,]");
            String preco = part[0] + "." + part[1];
            String dado = "";
            String numerico = "0123456789";
            preco = preco.replace(".", "");
            int local1virg = preco.length() - 2;
            int local2ponto = preco.length() - 5;
            int local3ponto = preco.length() - 8;
            int local4ponto = preco.length() - 11;
            int local5ponto = preco.length() - 14;

            for (int i = 0; i < preco.length(); i++) {
                if (numerico.contains(new Character(preco.charAt(i)).toString())) {
                    if (i == local1virg) {
                        dado += ",";
                    }
                    if (i == local2ponto && preco.length() > 6) {
                        dado += ".";
                    }
                    if (i == local3ponto && preco.length() > 9) {
                        dado += ".";
                    }
                    if (i == local4ponto && preco.length() > 12) {
                        dado += ".";
                    }
                    if (i == local5ponto && preco.length() > 15) {
                        dado += ".";
                    }
                    dado += preco.charAt(i);
                }
            }
            if (precoDouble < 0) {
                return "R$ -" + dado;
            } else {
                return "R$ " + dado;
            }
        } catch (NumberFormatException ex) {
            return o.toString();
        }
    }

    public static float getArredondaNumero(float numero) {
        String v1 = String.valueOf(numero);
        if (numero < 0) {
            if (v1.contains(".97")) {
                numero += 0.02;
            }
            if (v1.contains(".98")) {
                numero -= 0.02;
            }
            if (v1.contains(".99")) {
                numero -= 0.01;
            }
            if (v1.contains(".01")) {
                numero += 0.01;
            }
            if (v1.contains(".02")) {
                numero += 0.02;
            }
            if (v1.contains(".03")) {
                numero -= 0.02;
            }
        } else {
            if (v1.contains(".97")) {
                numero -= 0.02;
            }
            if (v1.contains(".98")) {
                numero += 0.02;
            }
            if (v1.contains(".99")) {
                numero += 0.01;
            }
            if (v1.contains(".01")) {
                numero -= 0.01;
            }
            if (v1.contains(".02")) {
                numero -= 0.02;
            }
            if (v1.contains(".03")) {
                numero += 0.02;
            }
        }
        return numero;
    }

    public static DefaultComboBoxModel ordenar(DefaultComboBoxModel boxModel) {
        List<String> lista = new ArrayList<String>();
        for (int i = 0; i < boxModel.getSize(); i++) {
            lista.add(boxModel.getElementAt(i).toString());
        }
        Collections.sort(lista);
        boxModel.removeAllElements();
        for (int i = 0; i < lista.size(); i++) {
            boxModel.addElement(lista.get(i));
        }
        return boxModel;
    }

    public static String getCelulaValida(Cell cell) {
        String dado = "";
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                dado = cell.getRichStringCellValue().getString();
                break;
            case Cell.CELL_TYPE_NUMERIC:

                if (DateUtil.isCellDateFormatted(cell)) {
                    dado = getRetornaDataUs(formato.format(cell.getDateCellValue()));
                } else {
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    dado = getValidaComoDecimal(cell.getStringCellValue());
                }

                break;
            case Cell.CELL_TYPE_BOOLEAN:
                dado = "" + cell.getBooleanCellValue();
                break;
            case Cell.CELL_TYPE_FORMULA:
                dado = cell.getCellFormula();
                break;
            default:
                dado = "";
        }
        return dado;
    }

    public static String getDataTexto(String texto) {
        String s = "";
        boolean p = false;
        int conta = 0;
        texto += "----------";
        for (int i = 0; i < texto.length(); i++) {
            if (String.valueOf(texto.charAt(i)).equals("/")) {
                p = true;
            }
            if (p) {
                if (conta == 10) {
                    break;
                }
                s += String.valueOf(texto.charAt(i - 2));
                conta++;
            }
        }
        return s.trim();
    }

    public static String getDescricaoMes(int i) {
        switch (i) {
            case 1:
                return "Janeiro";
            case 2:
                return "Fevereiro";
            case 3:
                return "Março";
            case 4:
                return "Abril";
            case 5:
                return "Maio";
            case 6:
                return "Junho";
            case 7:
                return "Julho";
            case 8:
                return "Agosto";
            case 9:
                return "Setembro";
            case 10:
                return "Novembro";
            case 11:
                return "Dezembro";
            case 12:
                return "Janeiro";
            default:
                return "";
        }
    }

    public static int getIdMes(String descricao) {
        switch (descricao.toUpperCase()) {
            case "JANEIRO":
                return 1;
            case "FEVEREIRO":
                return 2;
            case "MARÇO":
                return 3;
            case "MARCO":
                return 3;
            case "ABRIL":
                return 4;
            case "MAIO":
                return 5;
            case "JUNHO":
                return 6;
            case "JULHO":
                return 7;
            case "AGOSTO":
                return 8;
            case "SETEMBRO":
                return 9;
            case "OUTUBRO":
                return 10;
            case "NOVEMBRO":
                return 11;
            case "DEZEMBRO":
                return 12;
            default:
                return 0;
        }
    }

    public static void calculaXls(HSSFWorkbook w) {
        for (int i = 0; i < w.getNumberOfSheets(); i++) {
            HSSFSheet s = w.getSheetAt(i);
            for (int j = 0; j < 65536; j++) {
                Row r = s.getRow(j);
                if (r != null) {
                    for (int h = 0; h < 256; h++) {
                        Cell a = r.getCell(h);
                        if (a != null) {
                            if (a.getCellType() == Cell.CELL_TYPE_FORMULA) {
                                a.setCellFormula(a.getCellFormula());
                            }
                        }
                    }
                }
            }
        }
    }

    public static void calculaXls(Workbook w) {
        for (int i = 0; i < w.getNumberOfSheets(); i++) {
            Sheet s = w.getSheetAt(i);
            for (int j = 0; j < 65536; j++) {
                Row r = s.getRow(j);
                if (r != null) {
                    for (int h = 0; h < 256; h++) {
                        Cell a = r.getCell(h);
                        if (a != null) {
                            if (a.getCellType() == Cell.CELL_TYPE_FORMULA) {
                                a.setCellFormula(a.getCellFormula());
                            }
                        }
                    }
                }
            }
        }
    }

    public static List<Correios> buscaCep(String pesquisa) throws IbsException {
        List<Correios> retorno = new ArrayList<>();

        try {
            HtmlPage currentPage = SiteCorreios.getInstance().getPage();
            HtmlForm form = currentPage.getElementByName("Geral");
            form.setActionAttribute("resultadoBuscaCepEndereco.cfm");
            HtmlTextInput input = currentPage.getElementByName("relaxation");
            input.setText(pesquisa);
            HtmlSubmitInput submit = (HtmlSubmitInput) currentPage.getByXPath("//input[@value=\"Buscar\"]").get(0);
            HtmlPage nova = submit.click();
            List<?> bodys = nova.getByXPath("//tbody");
            if (bodys.size() > 0) {
                HtmlElement tabela = (HtmlElement) bodys.get(0);
                List<?> linhas = tabela.getByXPath("//tr");
                for (int i = 1; i < linhas.size(); i++) {
                    HtmlElement linha = (HtmlElement) linhas.get(i);
                    List<?> colunas = linha.getElementsByTagName("td");
                    Correios correios = new Correios();
                    for (int j = 0; j < colunas.size(); j++) {
                        HtmlElement celula = (HtmlElement) colunas.get(j);
                        if (j == 0) {
                            correios.setLogradouro(celula.asText());
                        }
                        if (j == 1) {
                            correios.setBairro(celula.asText());
                        }
                        if (j == 2) {
                            correios.setCidade(celula.asText().split("/")[0]);
                            correios.setUf(celula.asText().split("/")[1]);
                        }
                        if (j == 3) {
                            correios.setCep(celula.asText());
                        }
                    }
                    retorno.add(correios);
                }
            }
        } catch (MalformedURLException ex) {
            throw new IbsException("Url Malformada!", ex);
        } catch (IOException ex) {
            throw new IbsException("Erro ao buscar cep!", ex);
        } catch (FailingHttpStatusCodeException ex) {
            throw new IbsException("Falha na página!", ex);
        }
        return retorno;
    }

    public static Correios carregaEndCep(String cep) throws IbsException {
        Correios correios = new Correios();
        try {
            HtmlPage currentPage = SiteCorreios.getInstance().getPage();
            HtmlForm form = currentPage.getElementByName("Geral");
            form.setActionAttribute("resultadoBuscaCepEndereco.cfm");
            HtmlTextInput input = currentPage.getElementByName("relaxation");
            input.setText(cep);
            HtmlSubmitInput submit = (HtmlSubmitInput) currentPage.getByXPath("//input[@value=\"Buscar\"]").get(0);
            HtmlPage nova = submit.click();
            
            List<?> bodys = nova.getByXPath("//tbody");
            if (bodys.size() > 0) {
                HtmlElement tabela = (HtmlElement) bodys.get(0);
                List<?> linhas = tabela.getByXPath("//tr");
                HtmlElement linha = (HtmlElement) linhas.get(1);
                List<?> colunas = linha.getElementsByTagName("td");
                for (int j = 0; j < colunas.size(); j++) {
                    HtmlElement celula = (HtmlElement) colunas.get(j);
                    if (j == 0) {
                        correios.setLogradouro(celula.asText());
                    }
                    if (j == 1) {
                        correios.setBairro(celula.asText());
                    }
                    if (j == 2) {
                        correios.setCidade(celula.asText().split("/")[0]);
                        correios.setUf(celula.asText().split("/")[1]);
                    }
                    if (j == 3) {
                        correios.setCep(celula.asText());
                    }
                }
            }
        } catch (MalformedURLException ex) {
            ExibirParaUsuario.getInstance().erro(PrincipalPDV.getInstance(), ex, "Url Malformada!", false);
        } catch (IOException ex) {
            ExibirParaUsuario.getInstance().erro(PrincipalPDV.getInstance(), ex, "Erro ao buscar cep!", false);
        } catch (FailingHttpStatusCodeException ex) {
            ExibirParaUsuario.getInstance().erro(PrincipalPDV.getInstance(), ex, "Falha na página!", false);
        } catch(RuntimeException ex){
            ExibirParaUsuario.getInstance().erro(PrincipalPDV.getInstance(), ex, "Erro inesperado na página dos correios!", false);
        }
        return correios;
    }

}
