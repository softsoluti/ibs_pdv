//Desenvolvedor : Gutemberg Lucas Vieira Lima
//Celular : (62)9112-0642
//Telefone : (62)3701-1466
//Email : gGutemberg@gmail.com/g_uto_lima@hotmail.com
package br.com.cresceritsolutions.ibspdv.util;

import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author Gutemberg
 */
public class ImagesUtil {

    private static ImagesUtil me;

    private ImagesUtil() {
        // TODO Auto-generated constructor stub
    }

    public static ImagesUtil getInstance() {
        if (me == null) {
            me = new ImagesUtil();
        }
        return me;
    }

    //Frame icon 
    public ImageIcon getMarcaIcone() {
        return new ImageIcon(getClass().getResource("/images/N LOGO 256.png"));
    }

    public String getLogoRelatorio() {
        return "./LOGO REL.png";
    }

    public ImageIcon getIconCerto() {
        return new ImageIcon(getClass().getResource("/images/checkmark_16x16.png"));
    }
    public ImageIcon getIconCertoGrande() {
        return new ImageIcon(getClass().getResource("/images/checkmark 64.png"));
    }
    public ImageIcon getIconError() {
        return new ImageIcon(getClass().getResource("/images/error 64.png"));
    }
    public ImageIcon getIconAtencao() {
        return new ImageIcon(getClass().getResource("/images/atencao 64.png"));
    }
    public ImageIcon getIconQuestion() {
        return new ImageIcon(getClass().getResource("/images/question 64.png"));
    }

    public Icon getIconCopy() {
        return new ImageIcon(getClass().getResource("/images/copy 16.png"));
    }

    public Icon getIconCut() {
        return new ImageIcon(getClass().getResource("/images/cut 16.png"));
    }

    public Icon getIconPaste() {
        return new ImageIcon(getClass().getResource("/images/paste 16.png"));
    }
}
