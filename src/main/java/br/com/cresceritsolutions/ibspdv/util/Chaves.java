/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.util;

/**
 * Classe que contem a chave privada. Altere a extensao para .java e compile
 *
 * @author Pedro H. Lira
 */
public class Chaves {

    /**
     * Chave de 1024 bits RSA em base64 usando o formato PKCS8, abaixo os
     * comando para gerar uma chave assim usando o openssl.
     *
     * openssl genrsa -out ./chave_privada.rsa 1024
     *
     * openssl pkcs8 -topk8 -nocrypt -in ./chave_privada.rsa -out
     * ./chave_privada.pcks8
     *
     * openssl rsa -in ./chave_privada.rsa -modulus -out modulos.key
     *
     * O texto abaixo e o conteudo do arquivo chave_privada.pcks8
     */
        public static String privada = ""
            + "MIICXAIBAAKBgQDRmTeoyk5hhZ68IyeTrkjkQHLFf+nwArKZ32mgUqUwnKpSCVdp"
            + "R1yei04/GvRxM7xAwe4nUTdwETDkOSVNq6GgD/n8+duXgcq8OabQmf5a1DpdYsM5"
            + "k3pfK9kZIXbGsTlU25vZjDWz3ARgnSo7R8cgvbK2p8wfKfJDcSCSBi8ySwIDAQAB"
            + "AoGAGKk9ake7ZzAQmDROsMqd2";

    public static String publica = ""
            + "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDRmTeoyk5hhZ68IyeTrkjkQHLF"
            + "f+nwArKZ32mgUqUwnKpSCVdpR1yei04/GvRxM7xAwe4nUTdwETDkOSVNq6GgD/n8"
            + "+duXgcq8OabQmf5a1DpdYsM5k3pfK9kZIXbGsTlU25vZjDWz3ARgnSo7R8cgvbK2"
            + "p8wfKfJDcSCSBi8ySwIDAQAB";

    
}
