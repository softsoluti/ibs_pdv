/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.util;

/**
 *
 * @author Administrador
 */
public class IbsException extends Exception {

    public IbsException() {
        super();
    }

    /**
     * Contrutor que recebe o evento da falha original.
     *
     * @param cause Falha original.
     */
    public IbsException(Throwable cause) {
        super(cause);
    }

    /**
     * Contrutor que recebe uma mensagem e o evento da falha original.
     *
     * @param message String com texto adicional.
     * @param cause Falha original.
     */
    public IbsException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Contrutor que recebe uma mensagem adicional.
     *
     * @param message String com texto adicional.
     */
    public IbsException(String message) {
        super(message);
    }

    public IbsException(Exception ex, String erro_ao_conectar_no_AcbrNfeMonitor) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
