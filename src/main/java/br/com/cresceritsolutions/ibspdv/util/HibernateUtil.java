/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.util;

import br.com.cresceritsolutions.ibspdv.models.hb.Cargo;
import br.com.cresceritsolutions.ibspdv.models.hb.Empresa;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.connection.ConnectionProvider;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.SessionFactoryImplementor;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author Administrador
 */
public class HibernateUtil {

    private static HibernateUtil me;
    private static final SessionFactory sessionFactory;
    private Session session;

    static {
        try {
            // Create the SessionFactory from standard (hibernate.cfg.xml) 
            // config file.
            String url = VariaveisDoSistema.AUXILIAR.getProperty("bd.url");
            String porta= VariaveisDoSistema.AUXILIAR.getProperty("bd.porta");
            String banco= VariaveisDoSistema.AUXILIAR.getProperty("bd.banco");
            String usuario= VariaveisDoSistema.AUXILIAR.getProperty("bd.usuario");
            String senha= VariaveisDoSistema.AUXILIAR.getProperty("bd.senha");
            
            sessionFactory = new AnnotationConfiguration()
                    .setProperty("hibernate.connection.url", "jdbc:mysql://"+url+":"+porta+"/"+banco+"?autoReconnect=true")
                    .setProperty("hibernate.connection.username", usuario)
                    .setProperty("hibernate.connection.password", senha)
                    .configure().buildSessionFactory();
        } catch (Throwable ex) {
            // Log the exception. 
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static HibernateUtil getInstance() {
        if (me == null) {
            me = new HibernateUtil();
        }
        return me;
    }

    private HibernateUtil() {
    }

    private static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    private Session openConection() {
        session = HibernateUtil.getSessionFactory().openSession();
        return session;
    }

    //Publics
    public void close() {
        session.clear();
        session.close();
    }

    public void clear() {
        session.clear();
    }

    public void beginTransaction() {
        getSession().beginTransaction().begin();
    }

    public void rollback() {
        session.getTransaction().rollback();
        session.clear();
    }

    public void commit() {
        /*session.flush();
         session.clear();*/
        session.getTransaction().commit();
    }

    public Session getSession() {
        if (session == null || !session.isConnected() || !session.isOpen()) {
            openConection();
        }
        return session;
    }

    public void refresh(Object o) {
        session.refresh(o);
    }

    public void saveCollection(Collection c) {
        Iterator i = c.iterator();
        while (i.hasNext()) {
            session.saveOrUpdate(i.next());
        }
    }

    public void setSession(Session session) {
        this.session = session;
    }

    //Conection Relatorio JASPER
    public Connection getConnection() throws SQLException {
        SessionFactoryImplementor sfi = (SessionFactoryImplementor) getSession().getSessionFactory();
        ConnectionProvider cp = sfi.getConnectionProvider();
        return cp.getConnection();
    }

}
