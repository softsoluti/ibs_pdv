/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.util;

import br.com.cresceritsolutions.ibspdv.models.ecf.EcfImpressora;
import br.com.cresceritsolutions.ibspdv.models.enuns.Ramo;
import br.com.cresceritsolutions.ibspdv.models.hb.Caixa;
import br.com.cresceritsolutions.ibspdv.models.hb.CanalVenda;
import br.com.cresceritsolutions.ibspdv.models.hb.Ecf;
import br.com.cresceritsolutions.ibspdv.models.hb.Empresa;
import br.com.cresceritsolutions.ibspdv.models.hb.Operador;
import br.com.cresceritsolutions.ibspdv.models.hb.TipoUsuario;
import br.com.cresceritsolutions.ibspdv.models.hb.Usuario;
import java.util.Properties;

/**
 *
 * @author Administrador
 */
public class VariaveisDoSistema {

    //Dados do arquivo auxiliar referente a homologação sefaz
    public static Properties AUXILIAR = new Properties();
    //Dados do arquivo conf
    public static Properties CONFIG = new Properties();
    //Empresa logada
    public static Empresa EMPRESA;
    //Canal padrao loja
    public static CanalVenda CANAL_VENDA;
    //Usuairo logado
    public static Usuario USUARIO;
    //Operador de caixa logado
    public static Operador OPERADOR;
    //Tipo de usuario logado
    public static TipoUsuario TIPO_USUARIO;
    //Ecf ativo
    public static Ecf V_ECF;

    public static Caixa CAIXA;

    public static boolean INICIALIZA_MODO_FISCAL;

    private static Boolean sistemaPdv;
    //public static Boolean ECF_ATIVO = SISTEMA_PDV;
    private static Boolean ecfAtivo;

    private static String nomeSistema;
    
    public static boolean RECRIAR_ARQUIVO_AUXILIAR;
    public static Ramo RAMO;

    public static Boolean getSistemaPdv() {
        sistemaPdv = Boolean.parseBoolean(AUXILIAR.getProperty("out.sistema_pdv"));
        return sistemaPdv;
    }

    public static void setSistemaPdv(Boolean sistemaPdv) {
        VariaveisDoSistema.sistemaPdv = sistemaPdv;
    }

    public static Boolean getEcfAtivo() {
        ecfAtivo = Boolean.parseBoolean(AUXILIAR.getProperty("out.ecf_ativo"));
        return ecfAtivo;
    }

    public static void setEcfAtivo(Boolean ecfAtivo) {
        VariaveisDoSistema.ecfAtivo = ecfAtivo;
    }

    public static String getNomeSistema() {
        nomeSistema = getSistemaPdv() ? "IBS-PDV" : "IBS-GESTÃO";
        return nomeSistema;
    }

    public static void setNomeSistema(String nomeSistema) {
        VariaveisDoSistema.nomeSistema = nomeSistema;
    }

    public static EcfImpressora getImpressora() {
        EcfImpressora ecfImpressora = new EcfImpressora();
        ecfImpressora.setId(V_ECF.getId());
        ecfImpressora.setEcfImpressoraAtivo(true);
        ecfImpressora.setEcfImpressoraCaixa(1);
        ecfImpressora.setEcfImpressoraCodigo(String.valueOf(V_ECF.getUsuario()));
        ecfImpressora.setEcfImpressoraMarca(V_ECF.getMarca());
        ecfImpressora.setEcfImpressoraModelo(V_ECF.getModelo());
        ecfImpressora.setEcfImpressoraMfadicional(V_ECF.getMfadicional());
        ecfImpressora.setEcfImpressoraSerie(V_ECF.getSerie());
        ecfImpressora.setEcfImpressoraTipo(V_ECF.getTipo());
        
        return ecfImpressora;
    }
}
