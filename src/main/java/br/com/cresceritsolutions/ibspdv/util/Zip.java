/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.util;

import br.com.cresceritsolutions.ibspdv.view.core.PrincipalPDV;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 *
 * @author Gutem
 */
public class Zip {

    public static void zip(File[] files, File outputFile) throws IOException {

        if (files != null && files.length > 0) {
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(outputFile));
            Stack<File> parentDirs = new Stack<File>();
            zipFiles(parentDirs, files, out);
            out.close();
        }
    }

    private static void zipFiles(Stack<File> parentDirs, File[] files, ZipOutputStream out) throws IOException {
        byte[] buf = new byte[1024];
        for (int i = 0; i < files.length; i++) {
            if (files[i].isDirectory()) {
                //se a entrad é um diretório, empilha o diretório e chama o mesmo método recursivamente
                parentDirs.push(files[i]);
                zipFiles(parentDirs, files[i].listFiles(), out);

                //após processar as entradas do diretório, desempilha
                parentDirs.pop();
            } else {
                FileInputStream in = new FileInputStream(files[i]);
                //itera sobre os itens da pilha para montar o caminho completo do arquivo
                String path = "";
                for (File parentDir : parentDirs) {
                    path += parentDir.getName() + "/";
                }

                //grava os dados no arquivo zip
                out.putNextEntry(new ZipEntry(path + files[i].getName()));

                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }

                out.closeEntry();
                in.close();
            }
        }
    }

    public void unzip(File zipFile, File dir) throws IOException, IbsException {
        ZipFile zip = null;
        File arquivo = null;
        InputStream is = null;
        OutputStream os = null;
        byte[] buffer = new byte[1024];

        try {
            // cria diretório informado, caso não exista
            if (!dir.exists()) {
                dir.mkdirs();
            }
            if (!dir.exists() || !dir.isDirectory()) {
                ExibirParaUsuario.getInstance().atencao(PrincipalPDV.getInstance(), "O diretório " + dir.getName() + " não é um diretório válido!");

            }

            zip = new ZipFile(zipFile);
            Enumeration e = zip.entries();
            while (e.hasMoreElements()) {
                ZipEntry entrada = (ZipEntry) e.nextElement();
                arquivo = new File(dir, entrada.getName());

                // se for diretório inexistente, cria a estrutura e pula pra próxima entrada
                if (entrada.isDirectory() && !arquivo.exists()) {
                    arquivo.mkdirs();
                    continue;
                }

                // se a estrutura de diretórios não existe, cria
                if (!arquivo.getParentFile().exists()) {
                    arquivo.getParentFile().mkdirs();
                }
                try {
                    // lê o arquivo do zip e grava em disco
                    is = zip.getInputStream(entrada);
                    os = new FileOutputStream(arquivo);
                    int bytesLidos = 0;
                    if (is == null) {
                        throw new IbsException("Erro ao ler a entrada do zip: " + entrada.getName());
                    }
                    while ((bytesLidos = is.read(buffer)) > 0) {
                        os.write(buffer, 0, bytesLidos);
                    }
                } finally {
                    if (is != null) {
                        try {
                            is.close();
                        } catch (IOException ex) {
                            throw new IbsException("Erro ao fechar arquivo!",ex);
                        }
                    }
                    if (os != null) {
                        try {
                            os.close();
                        } catch (IOException ex) {
                            throw new IbsException("Erro ao fechar arquivo!",ex);
                        }
                    }
                }
            }
        } finally {
            if (zip != null) {
                try {
                    zip.close();
                } catch (IOException ex) {
                    throw new IbsException("Erro ao fechar zip!",ex);
                }
            }
        }
    }

    public static void compactar(String fIn) {
        File fin = new File(fIn);
        File fout = new File(fIn.replace(fin.getName(), "") + "/" + fin.getName().split("\\.")[0] + ".zip");
        File[] fs = new File[1];
        fs[0] = fin;
        Zip cz = new Zip();
        try {
            cz.zip(fs, fout);
        } catch (IOException ex) {
            Logger.getLogger(Zip.class.getName()).log(Level.SEVERE, null, ex);
        }
        deleteDir(fin);
    }

    public static void compactar(String fIn, String dirOut, boolean remover) {
        File fin = new File(fIn);
        File fout = new File(dirOut + "/" + fin.getName().split("\\.")[0] + ".zip");
        File[] fs = new File[1];
        fs[0] = fin;
        Zip cz = new Zip();
        try {
            cz.zip(fs, fout);
        } catch (IOException ex) {
            Logger.getLogger(Zip.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (remover) {
            deleteDir(fin);
        }
    }

    public static void descompactar(String in, String out, boolean remover) throws IbsException {
        Zip class_Zip = new Zip();
        File fin = new File(in);
        try {
            class_Zip.unzip(fin, new File(out));
        } catch (IOException ex) {
            Logger.getLogger(Zip.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (remover) {
            deleteDir(fin);
        }
    }

    public static void main(String[] args) throws IbsException {
        compactar("c:/teste", "D:", true);
        descompactar("d:/teste.zip", "d:/descompactado", true);
    }

    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        // Agora o diretório está vazio, restando apenas deletá-lo.   
        return dir.delete();
    }

}
