/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.util;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author Gutem
 */
public class SiteCorreios {

    private static SiteCorreios me;
    private WebClient webClient = null;
    private WebRequest requestSettings;

    public static SiteCorreios getInstance() throws IOException {
        if (me == null) {
            me = new SiteCorreios();
        }
        return me;
    }

    public SiteCorreios() throws MalformedURLException {
        webClient = new WebClient(BrowserVersion.CHROME);
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        webClient.getOptions().setUseInsecureSSL(true);
        //webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
        requestSettings = new WebRequest(new URL("http://www.buscacep.correios.com.br/"));
    }

    public HtmlPage getPage() throws IOException {
        return webClient.getPage(requestSettings);
    }     

}
