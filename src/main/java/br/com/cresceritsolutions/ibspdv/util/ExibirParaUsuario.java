/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.util;

import br.com.cresceritsolutions.ibspdv.controller.classes.EnviarEmail;
import br.com.cresceritsolutions.ibspdv.view.core.Aguarde;
import java.awt.Component;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import javax.swing.JOptionPane;
import org.apache.commons.mail.EmailException;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author Administrador
 */
public class ExibirParaUsuario {

    private static ExibirParaUsuario me;

    public static ExibirParaUsuario getInstance() {
        if (me == null) {
            me = new ExibirParaUsuario();
        }
        Aguarde.getInstance().setVisible(false);
        return me;
    }

    private ExibirParaUsuario() {
    }

    public void erro(Component c, Exception ex, String mensagemUI, boolean fechar) {
        try {
            JOptionPane.showMessageDialog(c, mensagemUI, "Erro", JOptionPane.ERROR_MESSAGE, ImagesUtil.getInstance().getIconError());
            Logger logger = Logger.getLogger(ExibirParaUsuario.class);
            BasicConfigurator.configure();
            PropertyConfigurator.configure("./conf/log4j.properties");
            BufferedReader reader;
            logger.error(ex.getMessage(), ex);
            String textoErro = "";
            File f = new File("./arquivos/ibspdv.log");
            reader = new BufferedReader(new FileReader(f));
            String linha;
            while ((linha = reader.readLine()) != null) {
                textoErro += linha + "\r\n";
            }
            reader.close();
            logger.shutdown();
            f.delete();
            EnviarEmail.enviarErro(textoErro);
            if (fechar) {
                System.exit(0);
            }
        } catch (FileNotFoundException ex1) {
            ex.printStackTrace();
        } catch (EmailException | IOException ex1) {
            //ex.printStackTrace();
        }
    }

    public Boolean confirmacao(Component c, String mensagemUI) {
        return JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(c, mensagemUI, "Confirme", JOptionPane.YES_NO_OPTION, JOptionPane.YES_NO_OPTION, ImagesUtil.getInstance().getIconQuestion());
    }

    public void mensagem(Component c, String mensagem, Boolean icone) {
        if (icone) {
            JOptionPane.showMessageDialog(c, mensagem, "Mensagem", JOptionPane.INFORMATION_MESSAGE, ImagesUtil.getInstance().getIconCertoGrande());
        } else {
            JOptionPane.showMessageDialog(c, mensagem, "Mensagem", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public void atencao(Component c, String mensagem) {
        JOptionPane.showMessageDialog(c, mensagem, "Atenção", JOptionPane.WARNING_MESSAGE, ImagesUtil.getInstance().getIconAtencao());
    }

    public String entrada(Component c, String email) {
        return JOptionPane.showInputDialog(c, email);
    }
}
