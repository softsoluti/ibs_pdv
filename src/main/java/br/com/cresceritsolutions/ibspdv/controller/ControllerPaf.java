/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.controller;

import acbr.acbrmonitor.AcbrMonitor;
import acbr.acbrmonitor.TEF;
import br.com.cresceritsolutions.ibspdv.models.core.IbsPdvException;
import br.com.cresceritsolutions.ibspdv.models.enuns.SituacaoPagamento;
import br.com.cresceritsolutions.ibspdv.models.enuns.TipoPadrao;
import br.com.cresceritsolutions.ibspdv.models.hb.Caixa;
import br.com.cresceritsolutions.ibspdv.models.hb.Cliente;
import br.com.cresceritsolutions.ibspdv.models.hb.Documento;
import br.com.cresceritsolutions.ibspdv.models.hb.Formas;
import br.com.cresceritsolutions.ibspdv.models.hb.Colaborador;
import br.com.cresceritsolutions.ibspdv.models.hb.ItemVenda;
import br.com.cresceritsolutions.ibspdv.models.hb.Movimento;
import br.com.cresceritsolutions.ibspdv.models.hb.Pagamentos;
import br.com.cresceritsolutions.ibspdv.models.hb.Parcela;
import br.com.cresceritsolutions.ibspdv.models.hb.Produto;
import br.com.cresceritsolutions.ibspdv.models.hb.ReducaoZ;
import br.com.cresceritsolutions.ibspdv.models.hb.ReducaoZTotais;
import br.com.cresceritsolutions.ibspdv.models.hb.SituacaoTributaria;
import br.com.cresceritsolutions.ibspdv.models.hb.TipoDeDocumento;
import br.com.cresceritsolutions.ibspdv.models.hb.Venda;
import br.com.cresceritsolutions.ibspdv.util.CriptografiaUtil;
import br.com.cresceritsolutions.ibspdv.util.ExibirParaUsuario;
import br.com.cresceritsolutions.ibspdv.util.HibernateUtil;
import br.com.cresceritsolutions.ibspdv.util.IbsException;
import br.com.cresceritsolutions.ibspdv.util.Util;
import br.com.cresceritsolutions.ibspdv.util.VariaveisDoSistema;
import br.com.cresceritsolutions.ibspdv.view.core.Aguarde;
import br.com.cresceritsolutions.ibspdv.view.core.PrincipalPDV;
import br.com.cresceritsolutions.ibspdv.view.venda.FinalizaVenda;
import jACBrFramework.ACBrException;
import jACBrFramework.serial.ecf.AcbrComponent;
import jACBrFramework.serial.ecf.Aliquota;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.ini4j.Wini;

/**
 *
 * @author gutemberg
 */
public class ControllerPaf {

    private static ControllerPaf me;

    public static ControllerPaf getInstance() {
        if (me == null) {
            me = new ControllerPaf();
        }
        return me;
    }
    private static boolean tentarDeNovo = false;

    private boolean auto;

    private boolean pagCartao;

    public void reducaoZ() throws IbsException {

        // verifica se e a primeira Z do mes
        HibernateUtil hu = HibernateUtil.getInstance();
        try {
            emitirLMFC(hu.getSession());
            // emite a reducao no ECF

            emitirReducaoZEcf();
            // salva os dados no banco
            //verificar
            ReducaoZ z = emitirReducaoZBanco(hu.getSession());
            // gera o arquivo Movimento do ECF do dia
            //AINDA NÃO ESTÁ PRONTO
            ControllerGeraRegistrosPafEcf.getInstance().gerar(0, "", new SimpleDateFormat("yyyy-MM-dd").format(z.getMovimento()), new SimpleDateFormat("yyyy-MM-dd").format(z.getMovimento()));
        } catch (HibernateException ex) {
            throw new IbsException("Erro ao emitir Redução Z no banco de dados!", ex);
        } catch (Exception ex) {
            throw new IbsException("Erro ao emitir Redução Z no banco de dados!", ex);
        }
    }

    public void emitirReducaoZEcf() {
        try {
            rdZ();
        } catch (ACBrException ex) {
            Integer estado;
            do {
                if (ExibirParaUsuario.getInstance().confirmacao(PrincipalPDV.getInstance(), "O documento ainda está sendo impresso?")) {
                    try {
                        // aguarda meio minuto
                        Thread.sleep(30000);
                    } catch (InterruptedException ex1) {
                        Logger.getLogger(ControllerPaf.class.getName()).log(Level.SEVERE, null, ex1);
                    }

                } else {
                    break;
                }

                // recupera o estado
                try {
                    estado = getEstado();
                } catch (Exception ex1) {
                    estado = 0;
                }
            } while (estado != 2);
        }
    }

    /**
     * Metodo que emite a LMFC caso seja a primeira Reducao Z do mes.
     *
     * @param session
     * @throws br.com.cresceritsolutions.ibspdv.util.IbsException
     */
    public void emitirLMFC(Session session) throws IbsException {
        // data atual do ECF
        // transforma a data para o dia 1º do mes
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(getDataHoraEcf());
        } catch (ACBrException ex) {
            throw new IbsException("Erro ao obter data e hora da impressora fiscal!", ex);
        }
        cal.set(Calendar.DAY_OF_MONTH, 1);
        // procura por uma Reducao Z no mes que esta o ECF

        ReducaoZ reducaoZ = (ReducaoZ) session.createCriteria(ReducaoZ.class).add(Restrictions.eq("movimento", cal.getTime())).uniqueResult();
        if (reducaoZ == null) {
            // somente gera caso tenha algum registro de Z.
            List<ReducaoZ> reducaoZs = session.createCriteria(ReducaoZ.class).addOrder(Order.desc("id")).add(Restrictions.eq("caixa", VariaveisDoSistema.CAIXA)).list();
            if (reducaoZs.size() > 0) {
                reducaoZ = (ReducaoZ) reducaoZs.get(0);
            }
            if (reducaoZ != null) {
                cal.add(Calendar.MONTH, - 1);
                String inicio = Util.getData(cal.getTime());
                cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
                String fim = Util.getData(cal.getTime());
                try {
                    leituraMemoriaFiscalCompletaImpressao(inicio, fim);
                } catch (ACBrException ex) {
                    throw new IbsException("Erro ao fazer leitura da memoria fiscal na impressora!", ex);
                }
            }
        }
    }

    /**
     * Metodo que emite salva os dados da ultima Reducao Z no Banco.
     *
     * @param session
     * @return
     * @throws br.com.cresceritsolutions.ibspdv.util.IbsException
     */
    public ReducaoZ emitirReducaoZBanco(Session session) throws IbsException {
        session.clear();
        Caixa caixa = VariaveisDoSistema.CAIXA;
        String dadosRdz;
        try {
            dadosRdz = dadosUltimaReducaoZ();
        } catch (ACBrException ex) {
            throw new IbsException("Erro ao obter dados da última Redução Z!", ex);
        }
        // pega os dados
        InputStream stream;
        try {
            stream = new ByteArrayInputStream(dadosRdz.replace(",", ".").getBytes("UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            throw new IbsException("Erro ao carregar dados da última Redução Z!", ex);
        }
        Wini ini;
        try {
            ini = new Wini(stream);
        } catch (IOException ex) {
            throw new IbsException("Erro ao converter dados da última Redução Z para Wini!", ex);
        }
        // recuperando a ultima Z emitida desta impressora e pega o ultimo coo
        int cooIni;
        //ReducaoZ ultZ = (ReducaoZ) hu.getSession().createCriteria(ReducaoZ.class).addOrder(Order.desc("movimento")).uniqueResult();

        //SELECIONAR RDZ DO BD ORDERNAR POR MOVIMENTO DESC
        List<ReducaoZ> reducaoZs = session.createCriteria(ReducaoZ.class).add(Restrictions.eq("ecf", VariaveisDoSistema.V_ECF)).addOrder(Order.desc("movimento")).list();
        if (reducaoZs == null || reducaoZs.isEmpty()) {
            cooIni = 1;
        } else {
            cooIni = reducaoZs.get(0).getCooFin() + 1;
        }

        // gera o registro 
        ReducaoZ z = new ReducaoZ();

        z.setCaixa(caixa);
        z.setUsuario(VariaveisDoSistema.USUARIO);

        //DATA EMISSÃO
        z.setEmissao(new Date());
        //data movimento
        String movimento = ini.get("ECF", "DataMovimento");
        if (movimento == null) {
            movimento = new SimpleDateFormat("dd/MM/yy HH:mm:ss").format(caixa.getDataAbertura());
        }
        try {
            z.setMovimento(new SimpleDateFormat("dd/MM/yy").parse(movimento));
        } catch (ParseException ex) {
            throw new IbsException("Erro ao formatar data de movimento da Redução Z!", ex);
        }
        //impressora
        z.setEcf(VariaveisDoSistema.V_ECF);

        z.setCooIni(cooIni);
        z.setCooFin(ini.get("ECF", "NumCOO", int.class));
        z.setCrz(ini.get("ECF", "NumCRZ", int.class));
        z.setCro(ini.get("ECF", "NumCRO", int.class));

        z.setGnf(ini.get("ECF", "NumGNF", int.class));
        z.setCcf(ini.get("ECF", "NumCCF", int.class));
        z.setCfd(ini.get("ECF", "NumCFD", int.class));
        z.setCdc(ini.get("ECF", "NumCDC", int.class));
        z.setGrg(ini.get("ECF", "NumGRG", int.class));
        z.setNfc(ini.get("ECF", "NumNFC", int.class));
        z.setCcdc(ini.get("ECF", "NumCCDC", int.class));

        //VENDA BRUTA
        z.setBruto(ini.get("Totalizadores", "VendaBruta", double.class));
        //GRANDE TOTAL
        z.setGt(ini.get("Totalizadores", "GrandeTotal", double.class));
        String im = VariaveisDoSistema.AUXILIAR.getProperty("cli.im").replaceAll("\\D", "");//inscrição municipal
        z.setIssqn(!im.equals(""));

        // salva 
        session.beginTransaction().begin();
        session.refresh(VariaveisDoSistema.CAIXA);
        VariaveisDoSistema.CAIXA.setAberto(false);
        VariaveisDoSistema.CAIXA.setDataFechamento(new Date());
        session.save(VariaveisDoSistema.CAIXA);
        session.save(z);
        // gera os registros Totais
        gerarRegistrosEcfTotais(ini, z, session);
        session.getTransaction().commit();
        return z;

    }

    private void gerarRegistrosEcfTotais(Wini ini, ReducaoZ z, Session session) {
        //aliquotas
        Map<String, ReducaoZTotais> totais = new HashMap<>();
        Map<String, String> aliq = ini.get("Aliquotas");
        if (aliq != null) {
            for (String chave : aliq.keySet()) {
                double valor = Double.valueOf(aliq.get(chave));
                if (valor > 0.00) {
                    ReducaoZTotais total = new ReducaoZTotais();
                    total.setReducaoZ(z);
                    total.setCodigo(chave);
                    total.setValor(valor);
                    if (!totais.containsKey(total.getCodigo())) {
                        totais.put(total.getCodigo(), total);
                    }
                }
            }
        }
        // outros icms
        Map<String, String> outras = ini.get("OutrasICMS");
        if (outras != null) {
            for (String chave : outras.keySet()) {
                double valor = Double.valueOf(outras.get(chave));
                if (valor > 0.00) {
                    // valida qual o tipo
                    String codigo = "";
                    if (chave.contains("Substituicao")) {
                        codigo = "F";
                    } else if (chave.contains("NaoTributado")) {
                        codigo = "N";
                    } else if (chave.contains("Isencao")) {
                        codigo = "I";
                    }
                    // se achou um tipo valido adiciona
                    if (!codigo.equals("")) {
                        codigo += chave.contains("ISSQN") ? "S1" : "1";
                        ReducaoZTotais total = new ReducaoZTotais();
                        total.setReducaoZ(z);
                        total.setCodigo(codigo);
                        total.setValor(valor);
                        if (!totais.containsKey(total.getCodigo())) {
                            totais.put(total.getCodigo(), total);
                        }
                    }
                }
            }
        }
        //TOTALIZADORES
        // operacao nao fiscal
        double opnf = ini.get("Totalizadores", "TotalNaoFiscal", double.class);
        if (opnf > 0.00) {
            ReducaoZTotais total = new ReducaoZTotais();
            total.setReducaoZ(z);
            total.setCodigo("OPNF");
            total.setValor(opnf);
            if (!totais.containsKey(total.getCodigo())) {
                totais.put(total.getCodigo(), total);
            }
        }

        // descontos
        double descT = ini.get("Totalizadores", "TotalDescontos", double.class);
        if (descT > 0.00) {
            ReducaoZTotais total = new ReducaoZTotais();
            total.setReducaoZ(z);
            total.setCodigo("DT");
            total.setValor(descT);
            if (!totais.containsKey(total.getCodigo())) {
                totais.put(total.getCodigo(), total);
            }
        }
        double descS = ini.get("Totalizadores", "TotalDescontosISSQN", double.class);
        if (descS > 0.00) {
            ReducaoZTotais total = new ReducaoZTotais();
            total.setReducaoZ(z);
            total.setCodigo("DS");
            total.setValor(descS);
            if (!totais.containsKey(total.getCodigo())) {
                totais.put(total.getCodigo(), total);
            }
        }

        // acrescimos
        double acresT = ini.get("Totalizadores", "TotalAcrescimos", double.class);
        if (acresT > 0.00) {
            ReducaoZTotais total = new ReducaoZTotais();
            total.setReducaoZ(z);
            total.setCodigo("AT");
            total.setValor(acresT);
            if (!totais.containsKey(total.getCodigo())) {
                totais.put(total.getCodigo(), total);
            }
        }
        double acresS = ini.get("Totalizadores", "TotalAcrescimosISSQN", double.class);
        if (acresS > 0.00) {
            ReducaoZTotais total = new ReducaoZTotais();
            total.setReducaoZ(z);
            total.setCodigo("AS");
            total.setValor(acresS);
            if (!totais.containsKey(total.getCodigo())) {
                totais.put(total.getCodigo(), total);
            }
        }

        // cancelamentos
        double canT = ini.get("Totalizadores", "TotalCancelamentos", double.class);
        if (canT > 0.00) {
            ReducaoZTotais total = new ReducaoZTotais();
            total.setReducaoZ(z);
            total.setCodigo("Can-T");
            total.setValor(canT);
            if (!totais.containsKey(total.getCodigo())) {
                totais.put(total.getCodigo(), total);
            }
        }
        double canS = ini.get("Totalizadores", "TotalCancelamentosISSQN", double.class);
        if (canS > 0.00) {
            ReducaoZTotais total = new ReducaoZTotais();
            total.setReducaoZ(z);
            total.setCodigo("Can-S");
            total.setValor(canS);
            if (!totais.containsKey(total.getCodigo())) {
                totais.put(total.getCodigo(), total);
            }
        }

        // salva os totais do z
        Iterator<ReducaoZTotais> i = new ArrayList<>(totais.values()).iterator();
        while (i.hasNext()) {
            session.save(i.next());
        }
    }

    public void tef() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String id = TEF.gerarId();
                    TEF.abrirADM(id);
                    String msg = TEF.getDados().get("030-000");

                    // imprime as vias
                    if (TEF.getDados().get("028-000") != null && Integer.valueOf(TEF.getDados().get("028-000")) > 0) {
                        // verifica se tem mensagem
                        if (msg != null && !msg.equals("")) {
                            Aguarde.getInstance().getLblInfo().setText(msg);
                            Aguarde.getInstance().setVisible(true);
                        }

                        try {
                            TEF.bloquear(true);
                            ControllerPaf.getInstance().lerTotaisRelatoriosGerenciais();
                            ControllerPaf.getInstance().abreRelatorioGerencial(Integer.parseInt(Util.getConfig().get("ecf.reltef")));

                            TEF.imprimirVias(TEF.getDados(), false);
                            ControllerPaf.getInstance().fechaRelatorio();
                            TEF.bloquear(false);
                            TEF.confirmarTransacao(id, true);
                        } catch (ACBrException ex) {
                            TEF.bloquear(false);
                            TEF.confirmarTransacao(id, false);
                            throw new Exception("Impressora não responde!");
                        } finally {
                            TEF.bloquear(false);
                        }
                    } else {
                        TEF.confirmarTransacao(id, true);
                        ExibirParaUsuario.getInstance().mensagem(PrincipalPDV.getInstance(), msg, false);
                    }
                    Aguarde.getInstance().setVisible(false);
                } catch (Exception ex) {
                    Aguarde.getInstance().setVisible(false);
                    if (ex.getMessage() != null) {
                        ExibirParaUsuario.getInstance().mensagem(PrincipalPDV.getInstance(), ex.getMessage(), false);
                    }
                }
            }
        }).start();
        Aguarde.getInstance().getLblInfo().setText("Aguarde o processamento...");
        Aguarde.getInstance().setVisible(true);

    }

    public void validarSerial(String serie) throws ACBrException {
        String serieImpressora = getNumSerie();
        if (!serie.contains(serieImpressora)) {
            throw new ACBrException("O ECF conectado tem o Número de Série = " + serieImpressora
                    + "\nO Númnero de Série do ECF autorizado deste PAF é = " + serie);
        }

    }

    public void finalizaCupom(List<Pagamentos> pagamentos, Venda venda) throws IbsException {
        HibernateUtil hu = HibernateUtil.getInstance();
        try {
            // sub totaliza
            StringBuilder sb = new StringBuilder();

            sb.append(Util.formataTexto("MD5: " + VariaveisDoSistema.AUXILIAR.getProperty("out.autenticado"), " ", AcbrMonitor.COL, true));
            if (venda.getPreVenda()) {
                sb.append(Util.formataTexto("\nPV: " + venda.getId(), " ", AcbrMonitor.COL, true));
            }
            // identifica o operador do caixa e o vendedor
            hu.getSession().refresh(venda.getUsuario());
            hu.getSession().refresh(venda.getColaborador());
            String operador = "\nOperador: " + venda.getUsuario().getColaborador().getPessoaFisica().getNome().toUpperCase();
            if (VariaveisDoSistema.USUARIO.getLogin() != null) {
                operador += "\nVendedor: " + venda.getColaborador().getPessoaFisica().getNome().toUpperCase();
            }
            sb.append(Util.formataTexto(operador, " ", AcbrMonitor.COL, true));

            // caso nao tenha sido informado o cliente
            /*if (nomeCli == null) {
             sb.append(Util.formataTexto("CONSUMIDOR NAO INFORMOU O CPF/CNPJ", " ", ECF.COL, true));
             } else {
             sb.append("CNPJ/CPF: ").append(cpfCli).append(ECF.SL);
             if (!nomeCli.equals("")) {
             sb.append("NOME:     ").append(nomeCli).append(ECF.SL);
             }
             if (!endCli.equals("")) {
             sb.append("ENDEREÇO: ").append(endCli).append(ECF.SL);
             }
             }*/
            // caso seja no estado de MG, colocar o minas legal
            if (VariaveisDoSistema.AUXILIAR.getProperty("paf.minas_legal").equalsIgnoreCase("SIM")) {
                sb.append("MINAS LEGAL: ");
                sb.append(VariaveisDoSistema.AUXILIAR.getProperty("cli.cnpj")).append(" ");
                sb.append(Util.formataData(venda.getData(), "ddMMyyyy")).append(" ");
                sb.append(Util.formataNumero(venda.getTotal(), 0, 2, true).replace(",", ""));
            } else if (VariaveisDoSistema.AUXILIAR.getProperty("paf.cupom_mania").equalsIgnoreCase("SIM")) {
                // caso seja no estado de RJ, colocar o cupom mania
                sb.append(Util.formataTexto("CUPOM MANIA - CONCORRA A PREMIOS", " ", AcbrMonitor.COL, true));
                sb.append("ENVIE SMS P/ 6789: ");
                sb.append(Util.formataNumero(VariaveisDoSistema.AUXILIAR.getProperty("cli.ie"), 8, 0, false));
                sb.append(Util.formataData(venda.getData(), "ddMMyyyy"));
                sb.append(Util.formataNumero(venda.getCoo(), 6, 0, false));
                sb.append(Util.formataNumero(VariaveisDoSistema.getImpressora().getEcfImpressoraCaixa(), 3, 0, false));
            }

            // caso a opcao de mostrar os valores de impostos esteja ativa
            boolean mostraIbpt = Boolean.valueOf(Util.getConfig().get("nfe.ibpt"));
            if (mostraIbpt) {
                double impostos = 0.00;
                double porcent = venda.getAcrescimoDesconto() / venda.getBruto();
                //IBPT
                Iterator<ItemVenda> i = venda.getItemVendas().iterator();
                while (i.hasNext()) {

                    ItemVenda itemVenda = i.next();
                    //if (!vp.getEcfVendaProdutoCancelado()) {
                    Produto produto = itemVenda.getItemCompra().getProduto();
                    String ncm = produto.getMarca().getTipoProduto().getNcm();
                    Double aliquotaNac;
                    try {
                        aliquotaNac = getIbptAliquotaNac(ncm);
                    } catch (IOException | InvalidFormatException ex) {
                        throw new IbsException("Erro ao obter alíquota nacional na tabela ibpt.", ex);
                    }
                    Double aliquotaImp;
                    try {
                        aliquotaImp = getIbptAliquotaImp(ncm);
                    } catch (IOException | InvalidFormatException ex) {
                        throw new IbsException("Erro ao obter alíquota de importação na tabela ibpt.", ex);
                    }
                    char ori = produto.getOrigem().getDescricao().split(" - ")[0].toCharArray()[0];
                    double taxa = (ori == '0' || ori == '3' || ori == '4' || ori == '5') ? aliquotaNac : aliquotaImp;
                    double rateado = itemVenda.getBruto() * porcent;
                    impostos += (itemVenda.getBruto() + rateado) * itemVenda.getQtd() * taxa / 100;
                    //}
                }
                double porcentagem = impostos / (venda.getAcrescimoDesconto() + venda.getBruto()) * 100;
                sb.append("\nVal Aprox Trib R$ ");
                sb.append(Util.formataNumero(impostos, 1, 2, false).replace(",", ".")).append(" [");
                sb.append(Util.formataNumero(porcentagem, 1, 2, false).replace(",", ".")).append("%] Fonte: IBPT");
            }

            try {
                Double descAcr = venda.getAcrescimoDesconto();
                subtotalizaCupom(descAcr, "");
            } catch (ACBrException ex) {
                if (!tentarDeNovo) {
                    throw new IbsException(ex.getMessage());
                }
            }
            // soma os pagamento que possuem o mesmo codigo
            SortedMap<String, Double> pags = new TreeMap<>();
            Iterator<Pagamentos> i = pagamentos.iterator();
            while (i.hasNext()) {
                Pagamentos pag = i.next();
                String codigo = pag.getFormas().getCodigo();
                if (pags.containsKey(codigo)) {
                    double valor = pag.getValor() + pags.get(pag.getFormas().getCodigo());
                    pags.put(pag.getFormas().getCodigo(), valor);
                } else {
                    pags.put(pag.getFormas().getCodigo(), pag.getValor());
                }
            }
            try {
                // garante que o dinheiro é impressos primeiro
                String dinheiro = Util.getConfig().get("ecf.dinheiro");
                if (pags.containsKey(dinheiro)) {
                    Double valor = Double.parseDouble(Util.formataNumero(pags.remove(dinheiro), 1, 2, false).replace(",", "."));
                    efetuaPagamento(Util.getConfig().get("ecf.dinheiro"), valor, "", false);
                }
                // garante que a troca é impressos em segundo se houver dinheiro
                String troca = Util.getConfig().get("ecf.troca");
                if (pags.containsKey(troca)) {
                    Double valor = Double.parseDouble(Util.formataNumero(pags.remove(troca), 1, 2, false).replace(",", "."));
                    efetuaPagamento(troca, valor, "", true);
                }
                // imprime os demais
                for (Entry<String, Double> pag : pags.entrySet()) {
                    Double valor = Double.parseDouble(Util.formataNumero(pag.getValue(), 1, 2, false).replace(",", "."));
                    efetuaPagamento(pag.getKey(), valor, "", true);

                }
            } catch (ACBrException ex) {
                if (!tentarDeNovo) {
                    throw new IbsException(ex.getMessage());
                }
            }
            // fecha a venda
            if (!Util.getConfig().get("ecf.mensagem").equals("")) {
                sb.append("\n").append(Util.getConfig().get("ecf.mensagem"));
            }
            sb.append("\n").append(VariaveisDoSistema.AUXILIAR.getProperty("paf.nome")).append(" ").append(VariaveisDoSistema.AUXILIAR.getProperty("paf.versao"));
            fechaCupom(sb.toString());
            // atualiza o gt
            VariaveisDoSistema.AUXILIAR.setProperty("ecf.gt", String.valueOf(getGrandeTotal()));
            try {
                CriptografiaUtil.getInstance().encriptarAuxiliar();
            } catch (Exception ex) {
                throw new IbsException("Erro ao encriptar o arquivo auxiliar.", ex);
            }
        } catch (ACBrException ex) {
            TEF.bloquear(false);
            if (ExibirParaUsuario.getInstance().confirmacao(FinalizaVenda.getInstance(), "Impressora não responde, tentar novamente?")) {
                TEF.bloquear(true);
                tentarDeNovo = true;
                finalizaCupom(pagamentos, venda);
            }
            TEF.bloquear(true);
        }

    }

    public void comandoSalvarDocumento(String tipo, Venda venda, Session session) throws IbsException {
        // seta os dados do documento.
        Documento doc = new Documento();
        doc.setEcf(VariaveisDoSistema.V_ECF);
        doc.setUsuario(VariaveisDoSistema.USUARIO);
        try {
            doc.setCoo(Integer.parseInt(getNumCupom()));
        } catch (ACBrException ex) {
            throw new IbsException("Erro ao obter número do cupom na impressora.", ex);
        }
        if (!tipo.equals("RV")) {
            try {
                //Se não for Relatório válido
                //Geral de operações não fiscais
                doc.setGnf(Integer.parseInt(getNumGNF()));
            } catch (ACBrException ex) {
                throw new IbsException("Erro ao obter número GNF na impressora.", ex);
            }
        }

        if (tipo.equals("RG")) {
            try {
                //Relatório Geral
                //Geral relatório Gerencial
                doc.setGrg(Integer.parseInt(getNumGRG()));
            } catch (ACBrException ex) {
                throw new IbsException("Erro ao obter número GRG na impressora.", ex);
            }
        } else if (tipo.equals("CC")) {
            try {
                //Comprovante cartão
                //Comprovante crédito ou débito
                doc.setCdc(Integer.parseInt(getNumCDC()));
            } catch (ACBrException ex) {
                throw new IbsException("Erro ao obter número CDC na impressora.", ex);
            }
        }

        doc.setTipo(tipo);
        Date data;

        try {
            data = getDataHoraEcf();
        } catch (ACBrException ex) {
            throw new IbsException("Erro ao obter data e hora da impressora.", ex);
        }

        doc.setData(data);
        // salva
        if (venda != null) {
            doc.setVenda(venda);
        }
        session.save(doc);

    }

    public void comandoImprimirCartao(List<Pagamentos> pagamentos, double troco, Session session) throws ACBrException {
        String comando = null;
        List<File> impressos = new ArrayList<>();

        // percorre os pagamentos para inserir no banco e imprimir cartoes
        for (int i = 0; i < pagamentos.size(); i++) {
            Pagamentos pag = pagamentos.get(i);
            List<Parcela> parcelas = null;

            if (pag.getFormas().isTef()) {
                try {
                    // abre o relatorio vinculado
                    if (comando == null) {
                        // soma os valores de todos os cartoes, para colocar no CCD
                        double valCard = pag.getValor();
                        for (int j = i + 1; j < pagamentos.size(); j++) {
                            if (pagamentos.get(j).getFormas().isTef()) {
                                valCard += pagamentos.get(j).getValor();
                            }
                        }

                        String coo = FinalizaVenda.getInstance().txtCOO.getText() + "";
                        String codigo = pag.getFormas().getCodigo();
                        String valor = Util.formataNumero(valCard, 1, 2, false).replace(",", ".");

                        //aCBrECF.fechaRelatorio();
                        abreCupomVinculado(coo, codigo, Double.parseDouble(valor));
                        comando = "ECF_LinhaCupomVinculado";
                    }

                    // imprime as vias
                    String arq = null;
                    if (pag.getArquivo().contains("pendente")) {
                        arq = TEF.lerArquivo(TEF.getRespIntPos001(), 0);
                    }
                    if (arq == null) {
                        arq = TEF.lerArquivo(pag.getArquivo(), 0);
                    }
                    Map<String, String> dados = TEF.iniToMap(arq);
                    TEF.imprimirVias(dados, comando.equals("ECF_LinhaCupomVinculado"));

                    // se for o pendente, precisa confirmar no GP e sera o ultimo cartao
                    if (pag.getArquivo().contains("pendente")) {
                        String id = pag.getArquivo().replaceAll("\\D", "");
                        fechaRelatorio();
                        TEF.confirmarTransacao(id, true);

                        // pega o numero GNF da impressao do cartao
                        pag.setGnf(Integer.valueOf(getNumGNF()));
                        // salva o documento para relatorio e deleta o arquivo
                        comandoSalvarDocumento(comando.equals("ECF_LinhaCupomVinculado") ? "CC" : "RG", null, session);
                    }

                    // gerar as parcelas e deleta o arquivo
                    parcelas = gerarParcela(dados, pag);
                    impressos.add(new File(pag.getArquivo()));
                } catch (Exception ex) {
                    Logger.getLogger(ControllerPaf.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else {
                parcelas = new ArrayList<>();
                Parcela parcela = new Parcela();
                parcela.setData(pag.getData());
                parcela.setValor(pag.getValor());
                parcela.setNsu("");
                parcelas.add(parcela);
            }

            // salva o pagamento
            pag.setParcelas_1(new HashSet<Parcela>(parcelas));
            //salvarPagamento(pag);
        }

        // caso todos os cartoes tenham todas as vias impressas, deleta os arquivos pendentes dos mesmos
        for (File file : impressos) {
            file.delete();
        }
    }

    /**
     * Metodo que adiciona a lista de parcelas as parcelas correspondente do
     * pagamento informado.
     *
     * @param dados o mapa de dados lidos do arquivo.
     * @param pagamento o objeto de pagamento a ser considerado.
     * @return uma lista de parcelas efetuadas pelo cartao.
     */
    private List<Parcela> gerarParcela(Map<String, String> dados, Pagamentos pagamento) {
        // recupera as parcelas
        List<Parcela> parcelas = new ArrayList<>();

        if (dados.get("018-000") == null) {
            Parcela parcela = new Parcela();
            parcela.setData(pagamento.getData());
            parcela.setValor(pagamento.getValor());
            parcela.setNsu(pagamento.getNsu());
            parcelas.add(parcela);
        } else {
            int faturas = Integer.valueOf(dados.get("018-000"));
            for (int fat = 1; fat <= faturas; fat++) {
                Date dt;
                Double vl;
                String nsu;
                String chave = "019-" + Util.formataNumero(fat, 3, 0, false);

                if (dados.get(chave) != null) {
                    // data
                    try {
                        dt = new SimpleDateFormat("ddMMyyyy").parse(dados.get(chave));
                    } catch (ParseException ex) {
                        dt = pagamento.getData();
                    }
                    // valor
                    vl = Double.valueOf(dados.get(chave.replace("019", "020"))) / 100;
                    // nsu
                    nsu = dados.get(chave.replace("019", "021"));
                } else {
                    // data
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(pagamento.getData());
                    cal.add(Calendar.MONTH, fat);
                    dt = cal.getTime();
                    // valor
                    vl = pagamento.getValor() / faturas;
                    // nsu
                    nsu = pagamento.getNsu();
                }
                // adicionando
                Parcela parcela = new Parcela();
                parcela.setData(dt);
                parcela.setValor(vl);
                parcela.setNsu(nsu);
                parcelas.add(parcela);
            }
        }
        return parcelas;
    }

    public void cancelar() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    // cancela a venda no cupom.
                    cancelaCupom();
                } catch (ACBrException ex) {
                }
                try {
                    if (Util.getConfig().get("tef.titulo") != null) {
                        TEF.cancelarPendentes(auto);
                    }
                } catch (IbsPdvException ex) {
                    ExibirParaUsuario.getInstance().erro(PrincipalPDV.getInstance(), ex, "Ocorreram problemas ao cancelar os cartões!\nSistema ficará indisponível até resolver o problema!", false);
                } catch (Exception ex) {
                    ExibirParaUsuario.getInstance().erro(PrincipalPDV.getInstance(), ex, "Ocorreram problemas ao cancelar os cartões!\nSistema ficará indisponível até resolver o problema!", false);
                } finally {
                    FinalizaVenda.getInstance().dispose();
                    Aguarde.getInstance().setVisible(false);
                }
            }
        }).start();
        Aguarde.getInstance().setVisible(true);

    }

    public Double getIbptAliquotaNac(String ncm) throws IOException, org.apache.poi.openxml4j.exceptions.InvalidFormatException {
        Double retorno = 0.0;
        File file = new File("arquivos/tab ibpt.xls");
        Workbook wb = WorkbookFactory.create(new FileInputStream(file));
        linha:
        for (Row r : wb.getSheetAt(0)) {
            for (Cell c : r) {
                if (c.getColumnIndex() == 0) {
                    if (getCelulaValida(c).equals(ncm)) {
                        retorno = Double.parseDouble(getCelulaValida(r.getCell(4)));
                        break linha;
                    }
                }
            }
        }
        return retorno;
    }

    public Double getIbptAliquotaImp(String ncm) throws IOException, org.apache.poi.openxml4j.exceptions.InvalidFormatException {
        Double retorno = 0.0;
        File file = new File("arquivos/tab ibpt.xls");
        Workbook wb = WorkbookFactory.create(new FileInputStream(file));
        linha:
        for (Row r : wb.getSheetAt(0)) {
            for (Cell c : r) {
                if (c.getColumnIndex() == 0) {
                    if (getCelulaValida(c).equals(ncm)) {
                        retorno = Double.parseDouble(getCelulaValida(r.getCell(5)));
                        break linha;
                    }
                }
            }
        }
        return retorno;
    }

    public static String getCelulaValida(Cell cell) {
        String dado = "";
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                dado = cell.getRichStringCellValue().getString();
                break;
            case Cell.CELL_TYPE_NUMERIC:

                if (DateUtil.isCellDateFormatted(cell)) {
                    dado = Util.getRetornaDataUs(formato.format(cell
                            .getDateCellValue()));
                } else {
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    dado = Util.getValidaComoDecimal(cell
                            .getStringCellValue());
                }

                break;
            case Cell.CELL_TYPE_BOOLEAN:
                dado = "" + cell.getBooleanCellValue();
                break;
            case Cell.CELL_TYPE_FORMULA:
                dado = cell.getCellFormula();
                break;
            default:
                dado = "";
        }
        return dado;
    }

    //OK
    public boolean ativarEcf() throws ACBrException {
        try {
            switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
                case 0:
                    AcbrComponent.getInstance().desativar();
                    AcbrComponent.getInstance().ativar();
                    break;
                case 1:
                    AcbrMonitor.conectar(Util.getConfig().get("ecf.servidor"), Integer.parseInt(Util.getConfig().get("ecf.porta")));
                    AcbrMonitor.desativar();
                    AcbrMonitor.ativar();
                    break;
            }
            return true;
        } catch (Exception ex) {
            throw new ACBrException(ex.getMessage());
        }
    }

    public void sangria(Double valor) throws ACBrException, IbsException {
        if (VariaveisDoSistema.getEcfAtivo()) {
            switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
                case 0:
                    AcbrComponent.getInstance().sangria(valor, "", "", "Dinheiro", 0);
                    break;
                case 1:
                    AcbrMonitor.sangria(String.valueOf(valor), "", "", "Dinheiro");
            }
        }
        HibernateUtil hu = HibernateUtil.getInstance();
        try {
            Formas forma = (Formas) hu.getSession().createCriteria(Formas.class).add(Restrictions.eq("descricao", "DINHEIRO")).uniqueResult();
            TipoDeDocumento tdd = (TipoDeDocumento) hu.getSession().createCriteria(TipoDeDocumento.class).add(Restrictions.eq("codigo", 2)).uniqueResult();
            Pagamentos pagamentos = new Pagamentos(forma, tdd, valor, SituacaoPagamento.PAGO.getValor());
            Movimento movimento = new Movimento();
            movimento.setObs("Sangria");
            movimento.setTipo(true);//Saida
            movimento.setValor(valor);
            movimento.setPagamentos(pagamentos);
            movimento.setCaixa(VariaveisDoSistema.CAIXA);
            hu.beginTransaction();
            hu.getSession().saveOrUpdate(pagamentos);
            hu.getSession().saveOrUpdate(movimento);
            hu.commit();
        } catch (HibernateException ex) {
            hu.rollback();
            throw new IbsException("Erro ao efetuar sangria!", ex);
        }
    }

    public void suprimento(Double valor) throws ACBrException, IbsException {
        if (VariaveisDoSistema.getEcfAtivo()) {
            switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
                case 0:
                    AcbrComponent.getInstance().suprimento(valor, "", "", "Dinheiro", 0);
                    break;
                case 1:
                    AcbrMonitor.suprimento(String.valueOf(valor), "", "", "Dinheiro");
            }
        }
        HibernateUtil hu = HibernateUtil.getInstance();
        try {
            Formas forma = (Formas) hu.getSession().createCriteria(Formas.class).add(Restrictions.eq("descricao", "DINHEIRO")).uniqueResult();
            TipoDeDocumento tdd = (TipoDeDocumento) hu.getSession().createCriteria(TipoDeDocumento.class).add(Restrictions.eq("codigo", 2)).uniqueResult();
            Pagamentos pagamentos = new Pagamentos(forma, tdd, valor, SituacaoPagamento.RECEBIDO.getValor());
            Movimento movimento = new Movimento();
            movimento.setObs("Suprimento");
            movimento.setTipo(false);//Entrada
            movimento.setValor(valor);
            movimento.setPagamentos(pagamentos);
            movimento.setCaixa(VariaveisDoSistema.CAIXA);
            hu.beginTransaction();
            hu.getSession().saveOrUpdate(pagamentos);
            hu.getSession().saveOrUpdate(movimento);
            hu.commit();
        } catch (HibernateException ex) {
            hu.rollback();
            throw new IbsException("Erro ao efetuar suprimento!", ex);
        }

    }

    public void aberturaCaixa(Double valor) throws ACBrException, IbsException {
        if (VariaveisDoSistema.getEcfAtivo()) {
            switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
                case 0:
                    if (valor > 0) {
                        AcbrComponent.getInstance().suprimento(valor, "", "", "Dinheiro", 0);
                    }
                    break;
                case 1:
                    if (valor > 0) {
                        AcbrMonitor.suprimento(String.valueOf(valor), "", "", "Dinheiro");
                    }
            }
        }
    }

    public String getNumSerie() throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                return AcbrComponent.getInstance().getNumSerie();
            case 1:
                return AcbrMonitor.getNumSerie();
        }
        return null;
    }

    public void leituraX() throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                AcbrComponent.getInstance().leituraX();
                break;
            case 1:
                AcbrMonitor.leituraX();
        }

    }

    public void abreCupomVinculado(String coo, String codigo, Double sTotal) throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                AcbrComponent.getInstance().abreCupomVinculado(coo, codigo, sTotal);
                break;
            case 1:
                AcbrMonitor.abreCupomVinculado(coo, codigo, String.valueOf(sTotal));
        }

    }

    public void abreRelatorioGerencial(int indice) throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                AcbrComponent.getInstance().abreRelatorioGerencial(indice);
                break;
            case 1:
                AcbrMonitor.abreRelatorioGerencial(String.valueOf(indice));
        }

    }

    public void fechaRelatorio() throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                AcbrComponent.getInstance().fechaRelatorio();
                break;
            case 1:
                AcbrMonitor.fechaRelatorio();
        }

    }

    public String getNumGNF() throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                return AcbrComponent.getInstance().getNumGNF();
            case 1:
                return AcbrMonitor.getNumGNF();
        }
        return null;
    }

    public void corrigeEstadoErro(boolean reducao) throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                AcbrComponent.getInstance().corrigeEstadoErro(reducao);
                break;
            case 1:
                AcbrMonitor.corrigeEstadoErro(String.valueOf(reducao));
        }

    }

    public void pulaLinhas(int numLinhas) throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                AcbrComponent.getInstance().pulaLinhas(numLinhas);
                break;
            case 1:
                AcbrMonitor.pulaLinhas(String.valueOf(numLinhas));
        }

    }

    public void cortaPapel(boolean cortaParcial) throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                AcbrComponent.getInstance().cortaPapel(cortaParcial);
                break;
            case 1:
                AcbrMonitor.cortaPapel(cortaParcial ? "True" : "False");
        }

    }

    public void linhaCupomVinculado(String replace) throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                AcbrComponent.getInstance().linhaCupomVinculado(replace.replace("\"", ""));
                break;
            case 1:
                AcbrMonitor.linhaCupomVinculado(replace);
        }
    }

    public void linhaRelatorioGerencial(String param1) throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                AcbrComponent.getInstance().linhaRelatorioGerencial(param1.replace("\"", ""), 0);
                break;
            case 1:
                AcbrMonitor.linhaRelatorioGerencial(param1);
        }

    }

    public void leituraMemoriaFiscal(String param1, String param2, String param3) throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                if (param1.contains("/")) {
                    AcbrComponent.getInstance().leituraMemoriaFiscal(Util.getData(param1), Util.getData(param2), Boolean.parseBoolean(param3));
                } else {
                    AcbrComponent.getInstance().leituraMemoriaFiscal(Integer.parseInt(param1), Integer.parseInt(param2), Boolean.parseBoolean(param3));
                }
                break;
            case 1:
                AcbrMonitor.leituraMemoriaFiscal(param1, param2, param3);
        }

    }

    public void pafMF_LMFC_Espelho(String ini, String fin, String arquivo) throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                if (ini.contains("/")) {
                    AcbrComponent.getInstance().pafMF_LMFC_Espelho(Util.getData(ini), Util.getData(fin), arquivo);
                } else {
                    AcbrComponent.getInstance().pafMF_LMFC_Espelho(Integer.parseInt(ini), Integer.parseInt(fin), arquivo);
                }
                break;
            case 1:
                AcbrMonitor.pafMF_LMFC_Espelho(ini, fin, arquivo);
        }

    }

    public void pafMF_LMFC_Cotepe1704(String ini, String fin, String arquivo) throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                if (ini.contains("/")) {
                    AcbrComponent.getInstance().pafMF_LMFC_Cotepe1704(Util.getData(ini), Util.getData(fin), arquivo);
                } else {
                    AcbrComponent.getInstance().pafMF_LMFC_Cotepe1704(Integer.parseInt(ini), Integer.parseInt(fin), arquivo);
                }
                break;
            case 1:
                AcbrMonitor.pafMF_LMFC_Cotepe1704(ini, fin, arquivo);
        }
    }

    public void pafMF_LMFS_Espelho(String ini, String fin, String arquivo) throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                if (ini.contains("/")) {
                    AcbrComponent.getInstance().pafMF_LMFS_Espelho(Util.getData(ini), Util.getData(fin), arquivo);
                } else {
                    AcbrComponent.getInstance().pafMF_LMFS_Espelho(Integer.parseInt(ini), Integer.parseInt(fin), arquivo);
                }
                break;
            case 1:
                AcbrMonitor.pafMF_LMFS_Espelho(ini, fin, arquivo);
        }

    }

    public void pafMF_MFD_Espelho(String ini, String fin, String arquivo) throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                if (ini.contains("/")) {
                    AcbrComponent.getInstance().pafMF_MFD_Espelho(Util.getData(ini), Util.getData(fin), arquivo);
                } else {
                    AcbrComponent.getInstance().pafMF_MFD_Espelho(Integer.parseInt(ini), Integer.parseInt(fin), arquivo);
                }
                break;
            case 1:
                AcbrMonitor.pafMF_MFD_Espelho(ini, fin, arquivo);
        }
    }

    public void abreGaveta() throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                AcbrComponent.getInstance().abreGaveta();
                break;
            case 1:
                AcbrMonitor.abreGaveta();
        }
    }

    public String getNumCupom() throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                return AcbrComponent.getInstance().getNumCupom();
            case 1:
                return AcbrMonitor.getNumCupom();
        }
        return null;
    }

    public String getNumCCF() throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                return AcbrComponent.getInstance().getNumCCF();
            case 1:
                return AcbrMonitor.getNumCCF();
        }
        return null;
    }

    private void lerTotaisRelatoriosGerenciais() throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                AcbrComponent.getInstance().lerTotaisRelatoriosGerenciais();
                break;
            case 1:
                AcbrMonitor.lerTotaisRelatoriosGerenciais();
        }
    }

    private void leituraMemoriaFiscalCompletaImpressao(String ini, String fin) throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                if (ini.contains("/")) {
                    AcbrComponent.getInstance().pafMF_LMFC_Impressao(Util.getData(ini), Util.getData(fin));
                } else {
                    AcbrComponent.getInstance().pafMF_LMFC_Impressao(Integer.parseInt(ini), Integer.parseInt(fin));
                }
                break;
            case 1:
                AcbrMonitor.pafMF_LMFC_Impressao(ini, fin);
        }
    }

    private Date getDataHoraEcf() throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                return AcbrComponent.getInstance().getDataHora();
            case 1:
                return AcbrMonitor.getDataHora();
        }
        return null;
    }

    public static void abreCupom(Cliente cliente, Colaborador colaborador, boolean modoPreVenda) throws ACBrException {
        HibernateUtil hu = HibernateUtil.getInstance();
        try {
            hu.getSession().refresh(cliente);
            String cpfCnpj = cliente.getPessoaFisica() != null ? cliente.getPessoaFisica().getCpf() : cliente.getPessoaJuridica().getCnpj();
            String nomeRazao = cliente.getPessoaFisica() != null ? cliente.getPessoaFisica().getNome() : cliente.getPessoaJuridica().getRazao();
            String endereco = cliente.getPessoaFisica() != null
                    ? cliente.getPessoaFisica().getPessoa().getEndereco().getLogradouro() + " "
                    + cliente.getPessoaFisica().getPessoa().getEndereco().getComplemento()
                    + " N:"
                    + cliente.getPessoaFisica().getPessoa().getEndereco().getNumero() + " "
                    + " " + cliente.getPessoaFisica().getPessoa().getEndereco().getBairro()
                    + " CEP:" + cliente.getPessoaFisica().getPessoa().getEndereco().getCep()
                    + " - " + cliente.getPessoaFisica().getPessoa().getEndereco().getMunicipio().getNome()
                    + " " + cliente.getPessoaFisica().getPessoa().getEndereco().getMunicipio().getEstado().getUf()
                    : cliente.getPessoaJuridica().getPessoa().getEndereco().getLogradouro() + " "
                    + cliente.getPessoaJuridica().getPessoa().getEndereco().getComplemento()
                    + " Nº "
                    + cliente.getPessoaJuridica().getPessoa().getEndereco().getNumero() + " "
                    + " " + cliente.getPessoaJuridica().getPessoa().getEndereco().getBairro()
                    + " CEP : " + cliente.getPessoaJuridica().getPessoa().getEndereco().getCep()
                    + " - " + cliente.getPessoaJuridica().getPessoa().getEndereco().getMunicipio().getNome()
                    + " " + cliente.getPessoaJuridica().getPessoa().getEndereco().getMunicipio().getEstado().getUf();
            switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
                case 0:
                    AcbrComponent.getInstance().corrigeEstadoErro(false);
                    AcbrComponent.getInstance().abreCupom(cpfCnpj, nomeRazao, endereco, modoPreVenda);
                    break;
                case 1:
                    AcbrMonitor.corrigeEstadoErro("false");
                    AcbrMonitor.abreCupom(
                            cpfCnpj,
                            nomeRazao,
                            endereco
                    );
            }
        } catch(HibernateException ex){
            ExibirParaUsuario.getInstance().erro(null, ex, "Erro ao abrir o cupom!", false);
        }
    }

    public void imprime(ItemVenda itemVenda) throws ACBrException {
        String subTipo = itemVenda.getItemCompra().getProduto().getMarca().getTipoProduto().getTipoPadrao();
        String descricao = "";
        String plano = "";
        String p = getPlano(itemVenda);
        if (p != null) {
            plano = " PLANO:" + p;
        }
        if (subTipo.equals(TipoPadrao.CELULAR.toString())) {
            descricao = itemVenda.getItemCompra().getProduto().getMarca().getDescricao() + " " + itemVenda.getItemCompra().getProduto().getDescricao()
                    + " IMEI:" + itemVenda.getItemCompra().getCelular().getImei() + " NUMERO:" + itemVenda.getLinhaVinculada() + plano;
        } else if (subTipo.equals(TipoPadrao.CHIP_NP.toString())) {

            descricao = itemVenda.getItemCompra().getProduto().getMarca().getDescricao() + " " + itemVenda.getItemCompra().getProduto().getDescricao()
                    + " ICCID:" + itemVenda.getIccidVinculado() + " NUMERO:" + itemVenda.getLinhaVinculada() + plano;
        } else if (subTipo.equals(TipoPadrao.CHIP_PRE_ATIVO.toString())) {
            descricao = itemVenda.getItemCompra().getProduto().getMarca().getDescricao() + " " + itemVenda.getItemCompra().getProduto().getDescricao()
                    + " ICCID:" + itemVenda.getIccidVinculado() + " NUMERO:" + itemVenda.getLinhaVinculada() + plano;
        } else if (subTipo.equals(TipoPadrao.DIVERSO.toString())) {
            descricao = itemVenda.getItemCompra().getProduto().getMarca().getDescricao() + " " + itemVenda.getItemCompra().getProduto().getDescricao();
        }
        String aliquota = getAliquota(itemVenda);
        if (String.valueOf(itemVenda.getItemCompra().getProduto().getSituacaoTributaria().getCodigo()).equalsIgnoreCase("T")
                || String.valueOf(itemVenda.getItemCompra().getProduto().getSituacaoTributaria().getCodigo()).equalsIgnoreCase("S")) {
            itemVenda.setIcms(itemVenda.getItemCompra().getProduto().getImposto().getAliquotaIcms());
            itemVenda.setPosicaoTotalizadorParcial(getPosicaoTotalizador(itemVenda.getIcms(), itemVenda.getItemCompra().getProduto().getSituacaoTributaria().getCodigo()));
        }

        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                AcbrComponent.getInstance().vendeItem(
                        itemVenda.getItemCompra().getProduto().getReferencia(),
                        descricao,
                        aliquota,
                        itemVenda.getQtd(),
                        itemVenda.getBruto(),
                        itemVenda.getDesconto(),
                        itemVenda.getItemCompra().getProduto().getUnidadeMedida().getUm(),
                        "$",
                        "D",
                        0);

                break;
            case 1:
                AcbrMonitor.vendeItem(
                        itemVenda.getItemCompra().getProduto().getReferencia(),
                        descricao,
                        aliquota,
                        String.valueOf(itemVenda.getQtd()),
                        String.valueOf(itemVenda.getBruto()),
                        String.valueOf(itemVenda.getDesconto()),
                        itemVenda.getItemCompra().getProduto().getUnidadeMedida().getUm(),
                        "$",
                        "D",
                        String.valueOf(0));
        }

    }

    private Integer getEstado() throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                return AcbrComponent.getInstance().getEstado();
            case 1:
                return AcbrMonitor.getEstado();
        }
        return null;
    }

    private void rdZ() throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                AcbrComponent.getInstance().reducaoZ();
                break;
            case 1:
                AcbrMonitor.reducaoZ();
        }
    }

    private String dadosUltimaReducaoZ() throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                return AcbrComponent.getInstance().dadosUltimaReducaoZ();
            case 1:
                return AcbrMonitor.dadosUltimaReducaoZ();
        }
        return null;
    }

    private void subtotalizaCupom(Double descontoAcrescimo, String mensagemRodape) throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                AcbrComponent.getInstance().subtotalizaCupom(descontoAcrescimo, mensagemRodape);
                break;
            case 1:
                AcbrMonitor.subtotalizaCupom(String.valueOf(descontoAcrescimo), mensagemRodape);
        }
    }

    private void efetuaPagamento(String codFormaPagamento, Double valor, String obs, Boolean impVinc) throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                AcbrComponent.getInstance().efetuaPagamento(codFormaPagamento, valor, obs, impVinc);
                break;
            case 1:
                AcbrMonitor.efetuaPagamento(codFormaPagamento, String.valueOf(valor), obs, String.valueOf(impVinc));
        }
    }

    private void fechaCupom(String obs) throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                AcbrComponent.getInstance().fechaCupom(obs);
                break;
            case 1:
                AcbrMonitor.fechaCupom(obs);
        }
    }

    private Double getGrandeTotal() throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                return AcbrComponent.getInstance().getGrandeTotal();
            case 1:
                return (Double) AcbrMonitor.getGrandeTotal();
        }
        return null;
    }

    public void cancelaCupom() throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                AcbrComponent.getInstance().cancelaCupom();
                break;
            case 1:
                AcbrMonitor.cancelaCupom();
        }
    }

    private String getNumCDC() throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                return AcbrComponent.getInstance().getNumCDC();
            case 1:
                return AcbrMonitor.getNumCDC();
        }
        return null;
    }

    private String getNumGRG() throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                return AcbrComponent.getInstance().getNumGRG();
            case 1:
                return AcbrMonitor.getNumGRG();
        }
        return null;
    }

    public double validarGT(double gt) throws ACBrException {
        double gt1 = getGrandeTotal();
        return gt1 != gt ? gt1 : 0.00;
    }

    private String getPlano(ItemVenda itemVenda) {
        Venda v = itemVenda.getVenda();
        Iterator<ItemVenda> i = v.getItemVendas().iterator();
        while (i.hasNext()) {
            ItemVenda iv = i.next();
            if (iv.getLinhaVinculada().equals(itemVenda.getLinhaVinculada()) && iv.getPlanoDeVenda() != null) {
                return iv.getPlanoDeVenda().getPlano().getDescricao();
            }
        }
        return null;
    }

    public String getNumVersaoSB() throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                return AcbrComponent.getInstance().getNumVersao();
            case 1:
                return AcbrMonitor.getNumVersao();
        }
        return null;
    }

    public String getMFAdicional() throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                return AcbrComponent.getInstance().getMFAdicional();
            case 1:
                return AcbrMonitor.getMFAdicional();
        }
        return null;
    }
    
    public String getModelo() throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                return AcbrComponent.getInstance().getModelo();
            case 1:
                return AcbrMonitor.getModelo();
        }
        return null;
    }
    
    public Date getDataSB() throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                return AcbrComponent.getInstance().getDataHoraSB();
            case 1:
                return AcbrMonitor.getDataHoraSB();
        }
        return null;
    }

    private String getAliquota(ItemVenda itemVenda) {
        SituacaoTributaria st = itemVenda.getItemCompra().getProduto().getSituacaoTributaria();
        if (String.valueOf(st.getCodigo()).equalsIgnoreCase("T")) {
            return String.valueOf(itemVenda.getItemCompra().getProduto().getImposto().getAliquotaIcms()) + "T";
        }
        if (String.valueOf(st.getCodigo()).equalsIgnoreCase("S")) {
            return String.valueOf(itemVenda.getItemCompra().getProduto().getImposto().getAliquotaIcms()) + "S";
        }

        if (String.valueOf(st.getCodigo()).equalsIgnoreCase("F")) {
            return "FF";
        }
        if (String.valueOf(st.getCodigo()).equalsIgnoreCase("I")) {
            return "II";
        }
        if (String.valueOf(st.getCodigo()).equalsIgnoreCase("N")) {
            return "NN";
        }
        return null;
    }

    private Integer getPosicaoTotalizador(Double icms, Character codigo) throws ACBrException {
        switch (Integer.parseInt(Util.getConfig().get("ecf.acbr"))) {
            case 0:
                Iterator<Aliquota> i = AcbrComponent.getInstance().lerTotaisAliquota().iterator();
                while (i.hasNext()) {
                    Aliquota aliquota = i.next();
                    if (aliquota.getTipo() == String.valueOf(codigo).equalsIgnoreCase("T") && aliquota.getAliquota() == icms) {//0 = T, 1= S
                        return Integer.parseInt(aliquota.getIndice());
                    }
                }
                break;
            case 1:
                return AcbrMonitor.getPosicaoAliquota(icms, String.valueOf(codigo));
        }

        return null;
    }

}
