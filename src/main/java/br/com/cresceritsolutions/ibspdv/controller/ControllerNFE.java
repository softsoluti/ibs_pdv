/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.controller;

import acbr.acbrnfemonitor.AcbrNfeMonitor;
import acbr.acbrnfemonitor.EComandoNFE;
import br.com.cresceritsolutions.ibspdv.models.hb.Imposto;
import br.com.cresceritsolutions.ibspdv.models.hb.ItemVenda;
import br.com.cresceritsolutions.ibspdv.models.hb.NotaFiscal;
import br.com.cresceritsolutions.ibspdv.models.hb.Produto;
import br.com.cresceritsolutions.ibspdv.models.hb.Telefone;
import br.com.cresceritsolutions.ibspdv.util.IbsException;
import br.com.cresceritsolutions.ibspdv.util.Util;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.ini4j.Wini;

/**
 *
 * @author Gutem
 */
public class ControllerNFE {

    private static ControllerNFE me;

    public static ControllerNFE getInstance() {
        if (me == null) {
            me = new ControllerNFE();
        }
        return me;
    }
    public Wini iniStatus;

    public void conectar() throws IbsException {
        try {
            AcbrNfeMonitor.conectar(Util.getConfig().get("nfe.servidor"), Integer.parseInt(Util.getConfig().get("nfe.porta")));
        } catch (Exception ex) {
            throw new IbsException("Erro ao conectar no AcbrNfeMonitor", ex);
        }
    }

    public void desconectar() throws IbsException {
        try {
            AcbrNfeMonitor.desconectar();
        } catch (Exception ex) {
            throw new IbsException(ex.getMessage(), ex);
        }
    }

    public boolean statusServico() throws IbsException {
        String[] resp = AcbrNfeMonitor.enviar(EComandoNFE.NFE_StatusServico);
        if (resp[0].equals(AcbrNfeMonitor.OK)) {
            InputStream stream = null;
            try {
                stream = new ByteArrayInputStream(resp[1].getBytes("UTF-8"));
                iniStatus = new Wini(stream);
                return true;
            } catch (UnsupportedEncodingException ex) {
                throw new IbsException(ex.getMessage(), ex);
            } catch (IOException ex) {
                throw new IbsException(ex.getMessage(), ex);
            } finally {
                try {
                    stream.close();
                } catch (IOException ex) {
                    throw new IbsException(ex.getMessage(), ex);
                }
            }
        } else {
            IbsException ex = new IbsException(resp[1]);
            throw new IbsException(ex.getMessage(), ex);
        }
    }

    public String[] assinarNfe(String arquivo) {
        return AcbrNfeMonitor.enviar(EComandoNFE.NFE_AssinarNFe, arquivo);
    }

    public String[] validarNfe(String arquivo) {
        return AcbrNfeMonitor.enviar(EComandoNFE.NFE_ValidarNFe, arquivo);
    }

    public String[] consultarNfe(String chave) {
        return AcbrNfeMonitor.enviar(EComandoNFE.NFE_ConsultarNFe, chave);
    }

    public String[] cancelarNfe(String chave, String justificativa) {
        return AcbrNfeMonitor.enviar(EComandoNFE.NFE_CancelarNFe, chave, justificativa);
    }

    public String[] imprimirDanfe(String arquivo) {
        return AcbrNfeMonitor.enviar(EComandoNFE.NFE_ImprimirDanfe, arquivo);
    }

    public String[] inutilizarNfe(String cnpj, String justificativa, String ano, String modelo, String numeroSerie, String numInicial, String numFinal) {
        return AcbrNfeMonitor.enviar(EComandoNFE.NFE_InutilizarNFe, cnpj, justificativa, ano, modelo, numeroSerie, numInicial, numFinal);
    }

    public String[] enviarNfe(String arquivo, String lote, String assina, String imprimirDanfe) {
        return AcbrNfeMonitor.enviar(EComandoNFE.NFE_EnviarNFe, arquivo, lote, assina, imprimirDanfe);
    }

    public String[] criarNfe(String texto, String retornaXml) {
        return AcbrNfeMonitor.enviar(EComandoNFE.NFE_CriarNFe, texto, retornaXml);
    }

    public String[] criarEnviarNfe(String texto, String lote, String imprimirDanfe) {
        return AcbrNfeMonitor.enviar(EComandoNFE.NFE_CriarEnviarNFe, texto, lote, imprimirDanfe);
    }

    public String[] enviarEmail(String para, String arquivo, String enviarDanfePdf) {
        return AcbrNfeMonitor.enviar(EComandoNFE.NFE_EnviarEmail, para, arquivo, enviarDanfePdf);
    }

    public String converteVendaEmTexto(NotaFiscal nf) throws IOException, InvalidFormatException {
        StringBuilder texto = new StringBuilder();
        texto.append("[Identificacao]").append("\n");
        texto.append("Codigo=").append("1").append("\n");
        texto.append("NaturezaOperacao=").append(nf.getNaturezaOperacao().getDescricao().length() > 60 ? nf.getNaturezaOperacao().getDescricao().substring(0, 59) : nf.getNaturezaOperacao().getDescricao()).append("\n");
        texto.append("FormaPag=").append(nf.getFormaPagamento() ? "0" : "1").append("\n");//0 -Vista, 1 - Prazo, 2 - Outras
        texto.append("Modelo=").append(nf.getModelo().getCodigo()).append("\n");
        texto.append("Serie=").append(nf.getSerie()).append("\n");
        texto.append("Numero=").append(nf.getNumero()).append("\n");
        texto.append("Emissao=").append(Util.getData(nf.getDataEmissao())).append("\n");
        texto.append("Saida=").append(Util.getData(nf.getDataEmissao())).append("\n");
        texto.append("Tipo=").append(nf.isTipo() ? "1" : "0").append("\n");//0 -Entrada, 1 -Saida
        texto.append("Finalidade=").append("\n");// - 0 -Normal, 1 - Complementar, 2 -Ajuste
        //texto.append("verProc=").append("\n");//
        texto.append("cUF=").append(nf.getVenda().getCaixa().getEmpresa().getPessoaJuridica().getPessoa().getEndereco().getMunicipio().getEstado().getIbge()).append("\n");

        texto.append("[NFRef001]").append("\n");//onde XXX deve conter o nÃƒÆ’Ã‚Âºmero sequÃƒÆ’Ã‚Âªncia para cada NFe Referenciada.
        texto.append("cUF=").append(nf.getVenda().getCaixa().getEmpresa().getPessoaJuridica().getPessoa().getEndereco().getMunicipio().getEstado().getIbge()).append("\n");
        texto.append("AAMM=").append(Util.formataData(nf.getDataEmissao(), "yyMM")).append("\n");
        texto.append("CNPJ=").append(nf.getVenda().getCaixa().getEmpresa().getPessoaJuridica().getCnpj()).append("\n");
        texto.append("Modelo=").append(nf.getModelo()).append("\n");
        texto.append("Serie=").append(nf.getSerie()).append("\n");
        texto.append("nNF=").append(nf.getNumero()).append("\n");

        texto.append("[Emitente]").append("\n");

        texto.append("CNPJ=").append(nf.getVenda().getCaixa().getEmpresa().getPessoaJuridica().getCnpj()).append("\n");
        texto.append("IE=").append(nf.getVenda().getCaixa().getEmpresa().getPessoaJuridica().getIe()).append("\n");
        texto.append("Razao=").append(nf.getVenda().getCaixa().getEmpresa().getPessoaJuridica().getRazao()).append("\n");
        texto.append("Fantasia=").append(nf.getVenda().getCaixa().getEmpresa().getPessoaJuridica().getFantasia()).append("\n");
        List<Telefone> telefones = new ArrayList(nf.getVenda().getCaixa().getEmpresa().getPessoaJuridica().getPessoa().getTelefones());
        texto.append("Fone=").append(telefones.get(0).getNumero()).append("\n");
        texto.append("CEP=").append(nf.getVenda().getCaixa().getEmpresa().getPessoaJuridica().getPessoa().getEndereco().getCep()).append("\n");
        texto.append("Logradouro=").append(nf.getVenda().getCaixa().getEmpresa().getPessoaJuridica().getPessoa().getEndereco().getLogradouro()).append("\n");
        texto.append("Numero=").append(nf.getVenda().getCaixa().getEmpresa().getPessoaJuridica().getPessoa().getEndereco().getNumero()).append("\n");
        texto.append("Complemento=").append(nf.getVenda().getCaixa().getEmpresa().getPessoaJuridica().getPessoa().getEndereco().getComplemento()).append("\n");
        texto.append("Bairro=").append(nf.getVenda().getCaixa().getEmpresa().getPessoaJuridica().getPessoa().getEndereco().getBairro()).append("\n");
        texto.append("CidadeCod=").append(nf.getVenda().getCaixa().getEmpresa().getPessoaJuridica().getPessoa().getEndereco().getMunicipio().getIbge()).append("\n");
        texto.append("Cidade=").append(nf.getVenda().getCaixa().getEmpresa().getPessoaJuridica().getPessoa().getEndereco().getMunicipio().getNome()).append("\n");
        texto.append("UF=").append(nf.getVenda().getCaixa().getEmpresa().getPessoaJuridica().getPessoa().getEndereco().getMunicipio().getEstado().getUf()).append("\n");
        texto.append("PaisCod=").append(nf.getVenda().getCaixa().getEmpresa().getPessoaJuridica().getPessoa().getEndereco().getMunicipio().getEstado().getPais().getCodigo()).append("\n");
        texto.append("Pais=").append(nf.getVenda().getCaixa().getEmpresa().getPessoaJuridica().getPessoa().getEndereco().getMunicipio().getEstado().getPais().getNome()).append("\n");
        /*texto.append("IEST=").append("\n");
         texto.append("IM=").append("\n");
         texto.append("CNAE=").append("\n");

         texto.append("[Avulsa]").append("\n");
         texto.append("CNPJ=").append("\n");
         texto.append("xOrgao=").append("\n");
         texto.append("matr=").append("\n");
         texto.append("xAgente=").append("\n");
         texto.append("fone=").append("\n");
         texto.append("UF=").append("\n");
         texto.append("nDAR=").append("\n");
         texto.append("dEmi=").append("\n");
         texto.append("vDAR=").append("\n");
         texto.append("repEmi=").append("\n");
         texto.append("dPag=").append("\n");*/

        texto.append("[Destinatario]").append("\n");

        texto.append("CNPJ=").append(nf.getVenda().getCliente().getPessoaFisica() != null ? nf.getVenda().getCliente().getPessoaFisica().getCpf() : nf.getVenda().getCliente().getPessoaJuridica().getCnpj()).append("\n");
        texto.append("IE=").append(nf.getVenda().getCliente().getPessoaFisica() != null ? "" : nf.getVenda().getCliente().getPessoaJuridica().getIe()).append("\n");
        //texto.append("ISUF=").append("\n");
        texto.append("NomeRazao=").append(nf.getVenda().getCliente().getPessoaFisica() != null ? nf.getVenda().getCliente().getPessoaFisica().getNome() : nf.getVenda().getCliente().getPessoaJuridica().getRazao()).append("\n");
        List<Telefone> telefones1 = new ArrayList(nf.getVenda().getCliente().getPessoaFisica() != null ? nf.getVenda().getCliente().getPessoaFisica().getPessoa().getTelefones() : nf.getVenda().getCliente().getPessoaJuridica().getPessoa().getTelefones());
        texto.append("Fone=").append(telefones1.get(0).getNumero()).append("\n");
        texto.append("CEP=").append(nf.getVenda().getCliente().getPessoaFisica() != null ? nf.getVenda().getCliente().getPessoaFisica().getPessoa().getEndereco().getCep() : nf.getVenda().getCliente().getPessoaJuridica().getPessoa().getEndereco().getCep()).append("\n");
        texto.append("Logradouro=").append(nf.getVenda().getCliente().getPessoaFisica() != null ? nf.getVenda().getCliente().getPessoaFisica().getPessoa().getEndereco().getLogradouro() : nf.getVenda().getCliente().getPessoaJuridica().getPessoa().getEndereco().getLogradouro()).append("\n");
        texto.append("Numero=").append(nf.getVenda().getCliente().getPessoaFisica() != null ? nf.getVenda().getCliente().getPessoaFisica().getPessoa().getEndereco().getNumero() : nf.getVenda().getCliente().getPessoaJuridica().getPessoa().getEndereco().getNumero()).append("\n");
        texto.append("Complemento=").append(nf.getVenda().getCliente().getPessoaFisica() != null ? nf.getVenda().getCliente().getPessoaFisica().getPessoa().getEndereco().getLogradouro() : nf.getVenda().getCliente().getPessoaJuridica().getPessoa().getEndereco().getLogradouro()).append("\n");
        texto.append("Bairro=").append(nf.getVenda().getCliente().getPessoaFisica() != null ? nf.getVenda().getCliente().getPessoaFisica().getPessoa().getEndereco().getBairro() : nf.getVenda().getCliente().getPessoaJuridica().getPessoa().getEndereco().getBairro()).append("\n");
        texto.append("CidadeCod=").append(nf.getVenda().getCliente().getPessoaFisica() != null ? nf.getVenda().getCliente().getPessoaFisica().getPessoa().getEndereco().getMunicipio().getIbge() : nf.getVenda().getCliente().getPessoaJuridica().getPessoa().getEndereco().getMunicipio().getIbge()).append("\n");
        texto.append("Cidade=").append(nf.getVenda().getCliente().getPessoaFisica() != null ? nf.getVenda().getCliente().getPessoaFisica().getPessoa().getEndereco().getMunicipio().getNome() : nf.getVenda().getCliente().getPessoaJuridica().getPessoa().getEndereco().getMunicipio().getNome()).append("\n");
        texto.append("UF=").append(nf.getVenda().getCliente().getPessoaFisica() != null ? nf.getVenda().getCliente().getPessoaFisica().getPessoa().getEndereco().getMunicipio().getEstado().getUf() : nf.getVenda().getCliente().getPessoaJuridica().getPessoa().getEndereco().getMunicipio().getEstado().getUf()).append("\n");
        texto.append("PaisCod=").append(nf.getVenda().getCliente().getPessoaFisica() != null ? nf.getVenda().getCliente().getPessoaFisica().getPessoa().getEndereco().getMunicipio().getEstado().getPais().getCodigo() : nf.getVenda().getCliente().getPessoaJuridica().getPessoa().getEndereco().getMunicipio().getEstado().getPais().getCodigo()).append("\n");
        texto.append("Pais=").append(nf.getVenda().getCliente().getPessoaFisica() != null ? nf.getVenda().getCliente().getPessoaFisica().getPessoa().getEndereco().getMunicipio().getEstado().getPais().getNome() : nf.getVenda().getCliente().getPessoaJuridica().getPessoa().getEndereco().getMunicipio().getEstado().getPais().getNome()).append("\n");

        /* 
         Codigo= - cProd
         EAN= - cEAN
         Descricao= - xProd
         NCM=
         EXTIPI=
         genero=
         CFOP=
         Unidade= - uCom
         Quantidade= - qCom
         ValorUnitario= - vUnCom
         ValorTotal= - vProd
         cEANTrib=
         uTrib=
         qTrib=
         vUnTrib=
         vFrete=
         vSeg=
         ValorDesconto= - vDesc
         infAdProd=
         */
        //Insere itens
        int contadorItem = 1;
        Iterator<ItemVenda> i = nf.getVenda().getItemVendas().iterator();
        Double icmsItem = 0.0;
        while (i.hasNext()) {
            ItemVenda itemVenda = i.next();

            texto.append("[Produto").append(String.format("%03d", contadorItem++)).append("]").append("\n");

            texto.append("Codigo=").append(itemVenda.getItemCompra().getProduto().getReferencia()).append("\n");
            //texto.append("EAN=").append("\n");
            texto.append("Descricao=").append(itemVenda.getItemCompra().getProduto().getMarca().getDescricao()).append(" ").append(itemVenda.getItemCompra().getProduto().getDescricao()).append("\n");
            texto.append("NCM=").append(itemVenda.getItemCompra().getProduto().getMarca().getTipoProduto().getNcm()).append("\n");
            texto.append("CFOP=").append(nf.getNaturezaOperacao().getCodigo()).append("\n");
            texto.append("Unidade=").append(itemVenda.getItemCompra().getProduto().getUnidadeMedida().getUm()).append("\n");
            texto.append("Quantidade=").append(itemVenda.getQtd()).append("\n");
            texto.append("ValorUnitario=").append(itemVenda.getLiquido()).append("\n");
            texto.append("ValorDesconto=").append(itemVenda.getDesconto()).append("\n");
            texto.append("ValorTotal=").append(itemVenda.getLiquido()).append("\n");

            Imposto imposto = itemVenda.getItemCompra().getProduto().getImposto();
            int contadorIcms = 1;

            texto.append("[ICMS").append(String.format("%03d", contadorIcms++)).append("]").append("\n");
            texto.append("CST=").append(imposto.getCstByCstIcms()).append("\n");
            texto.append("ValorBase=").append(itemVenda.getLiquido()).append("\n");
            texto.append("Aliquota=").append(imposto.getAliquotaIcms()).append("\n");
            icmsItem += itemVenda.getLiquido() / 100 * imposto.getAliquotaIcms();
            texto.append("Valor=").append(Util.getFormatoMoeda(itemVenda.getLiquido() / 100 * imposto.getAliquotaIcms()).replace("R$ ", "").replace(",", ".")).append("\n");
        }

        texto.append("[Total]").append("\n");
        texto.append("BaseICMS=").append(nf.getVenda().getTotal()).append("\n");
        texto.append("ValorICMS=").append(Util.getFormatoMoeda(icmsItem).replace("R$ ", "").replace(",", ".")).append("\n");
        texto.append("ValorProduto=").append(nf.getVenda().getTotal()).append("\n");
        texto.append("ValorNota=").append(nf.getVenda().getTotal()).append("\n");
        // caso a opcao de mostrar os valores de impostos esteja ativa
        boolean mostraIbpt = Boolean.valueOf(Util.getConfig().get("nfe.ibpt"));
        int contadorInfoAdicional = 1;
        if (mostraIbpt) {
            double impostos = 0.00;
            double porcent = nf.getVenda().getAcrescimoDesconto() / nf.getVenda().getBruto();
            //IBPT
            Iterator<ItemVenda> it = nf.getVenda().getItemVendas().iterator();
            while (it.hasNext()) {

                ItemVenda itemVenda = it.next();
                //if (!vp.getEcfVendaProdutoCancelado()) {
                Produto produto = itemVenda.getItemCompra().getProduto();
                String ncm = produto.getMarca().getTipoProduto().getNcm();
                Double aliquotaNac = ControllerPaf.getInstance().getIbptAliquotaNac(ncm);
                Double aliquotaImp = ControllerPaf.getInstance().getIbptAliquotaImp(ncm);
                char ori = produto.getOrigem().getDescricao().split(" - ")[0].toCharArray()[0];
                double taxa = (ori == '0' || ori == '3' || ori == '4' || ori == '5') ? aliquotaNac : aliquotaImp;
                double rateado = itemVenda.getBruto() * porcent;
                impostos += (itemVenda.getLiquido() + rateado) * itemVenda.getQtd() * taxa / 100;
                //}
            }
            double porcentagem = impostos / (nf.getVenda().getAcrescimoDesconto() + nf.getVenda().getBruto()) * 100;
            texto.append("[InfAdic00").append(contadorInfoAdicional++).append("]").append("\n");
            texto.append("Campo=").append("Val Aprox Trib R$ ").append("\n");
            texto.append("Texto=").append(Util.formataNumero(impostos, 1, 2, false).replace(",", ".")).append(" [").append(Util.formataNumero(porcentagem, 1, 2, false).replace(",", ".")).append("%] Fonte: IBPT").append("\n");
        }

        return texto.toString();
    }

    /**
     * Metodo que gera a chave unica da NFe.
     */
    private String getChaveAcesso(NotaFiscal nfe) {
        Integer codUf = nfe.getVenda().getCaixa().getEmpresa().getPessoaJuridica().getPessoa().getEndereco().getMunicipio().getEstado().getIbge();
        String AMM = Util.formataData(nfe.getDataEmissao(), "yyMM");// Ano e MÃƒÆ’Ã‚Âªs de emissÃƒÆ’Ã‚Â£o da NF-e.  
        String cnpj = nfe.getVenda().getCaixa().getEmpresa().getPessoaJuridica().getCnpj();// CNPJ do emitente.  
        String modelo = "55"; // Modelo do Documento Fiscal nfe=55
        String serie = Util.getConfig().get("nfe.serie");//SÃƒÆ’Ã‚Â©rie do Documento Fiscal
        String nNF = "" + (int) (Math.random() * 99999999);// NÃƒÆ’Ã‚Âºmero do Documento Fiscal.  
        String cNF = (nfe.getDataEmissao().getTime() + "").substring(0, 8);// CÃƒÆ’Ã‚Â³digo NumÃƒÆ’Ã‚Â©rico que compÃƒÆ’Ã‚Âµe a Chave de Acesso.  

        StringBuilder sb = new StringBuilder();
        // uf
        sb.append(codUf);
        // data
        sb.append(AMM);
        // cnpj
        sb.append(cnpj.replaceAll("\\D", ""));
        //sb.append(empresa.getSisEmpresaCnpj().replaceAll("\\D", ""));
        // / modo
        sb.append(modelo);
        // serie
        sb.append(Util.formataNumero(serie, 3, 0, false));
        // numero nf
        sb.append(nNF);
        // tipo emissao
        sb.append("1");//NOrmal
        // codigo nfe
        sb.append(cNF);
        // dv nfe 1 digito, por 9 casas
        String cDV = modulo11(sb.toString(), 1, 9);
        sb.append(cDV);
        // coloca no config
        return sb.toString();
    }

    /**
     * Metodo que faz o cÃƒÆ’Ã‚Â¡lculo de modulo 11.
     *
     * @param fonte o numero a ser usado para calculo.
     * @param dig quantos digitos de retorno, 1 ou 2.
     * @param limite quantos digitos usados para multiplicacao.
     * @return o dv do fonte.
     */
    private String modulo11(String fonte, int dig, int limite) {
        for (int n = 1; n <= dig; n++) {
            int soma = 0;
            int mult = 2;

            for (int i = fonte.length() - 1; i >= 0; i--) {
                soma += (mult * Integer.valueOf(fonte.substring(i, i + 1)));
                if (++mult > limite) {
                    mult = 2;
                }
            }
            fonte += ((soma * 10) % 11) % 10;
        }
        return fonte.substring(fonte.length() - dig);
    }
}
