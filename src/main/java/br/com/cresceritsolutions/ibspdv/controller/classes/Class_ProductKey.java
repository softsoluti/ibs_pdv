/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.controller.classes;

import br.com.cresceritsolutions.ibspdv.util.VariaveisDoSistema;


/**
 *
 * @author Gutemberg
 */
public class Class_ProductKey {

    public static String getCodValid() {
        return VariaveisDoSistema.AUXILIAR.getProperty("out.chave");
    }

    public static String getUser() {
        return VariaveisDoSistema.AUXILIAR.getProperty("out.empresa");
    }
}
