/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.controller.classes;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

public class EscritorAuto {
    public void escreve(String texto) throws AWTException {
        char[] digitos = texto.toCharArray();
        Robot robot = new Robot();
        for (int i = 0; i < digitos.length; i++) {
            robot.delay(50);
            // Verificando se o caracter esta em caixa alta.
            boolean isUpperCase = Character.isUpperCase(digitos[i]);
            // Passando o digito para caixa alta para que funcione na Robot.
            digitos[i] = Character.toUpperCase(digitos[i]);
            if (isUpperCase) {
                // Pressionando o Shift caso a letra que estiver em caixa alta
                robot.keyPress(KeyEvent.VK_SHIFT);
            }
            // Pressionando e soltando tecla.
            robot.keyPress((int) digitos[i]);
            robot.keyRelease((int) digitos[i]);
            if (isUpperCase) {
                // Soltando o Shift caso a letra estiver em caixa alta
                robot.keyRelease(KeyEvent.VK_SHIFT);
            }
        }
    }
}
