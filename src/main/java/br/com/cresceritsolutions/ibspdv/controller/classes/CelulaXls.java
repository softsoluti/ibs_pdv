/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.controller.classes;

/**
 *
 * @author Gutemberg
 */
public class CelulaXls {

    private int numCol;
    private String nomeCol;

    public CelulaXls(String nomeCol,int numCol) {
          this.nomeCol=nomeCol;
          this.numCol=numCol;
    }

    /**
     * @return the numCol
     */
    public int getNumCol() {
        return numCol;
    }

    /**
     * @param numCol the numCol to set
     */
    public void setNumCol(int numCol) {
        this.numCol = numCol;
    }

    /**
     * @return the nomeCol
     */
    public String getNomeCol() {
        return nomeCol;
    }

    /**
     * @param nomeCol the nomeCol to set
     */
    public void setNomeCol(String nomeCol) {
        this.nomeCol = nomeCol;
    }
    public int toInt(){
        return getNumCol();
    }
}
