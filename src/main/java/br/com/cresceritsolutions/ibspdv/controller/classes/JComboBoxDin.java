/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.controller.classes;

import java.awt.event.KeyListener;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;

/**
 *
 * @author Gutemberg
 */
public class JComboBoxDin extends JComboBox {

    public JComboBoxDin(Object[] object) {
        super(object);
    }

    public JComboBoxDin() {
        super();
    }


    @Override
    public void setKeySelectionManager(KeySelectionManager aManager) {
        
        super.setKeySelectionManager(new JComboBox.KeySelectionManager() {
            private final long delay = 500;
            private long lastTime = -1;
            private String searchTerm;

            @Override
            public int selectionForKey(char key, ComboBoxModel model) {
                final long currentTime = System.currentTimeMillis();
                final int size = model.getSize();
                final String[] formatted = new String[size];
                final Object selectedItem = model.getSelectedItem();
                int selectedIndex = -1;

                for (int i = 0; i < size; i++) {
                    Object element = model.getElementAt(i);

                    if (selectedItem == element && selectedIndex == -1) {
                        selectedIndex = i;
                    }

                    formatted[i] = element.toString();
                }

                key = Character.toLowerCase(key);
                int start = selectedIndex;

                if (lastTime == -1 || currentTime - lastTime > delay) {
                    searchTerm = String.valueOf(key);
                } else {
                    searchTerm += key;
                }

                lastTime = currentTime;

                for (int i = Math.max(start, 0); i < size; i++) {
                    if (matches(formatted[i])) {
                        return i;
                    }
                }

                for (int i = 0; i <= start; i++) {
                    if (matches(formatted[i])) {
                        return i;
                    }
                }

                return -1;
            }

            private boolean matches(final String s) {
                return s.toLowerCase().contains(searchTerm);
            }
        });
    }
   
}
