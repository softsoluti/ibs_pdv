/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.controller.classes;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JTextField;

/**
 *
 * @author Gutemberg
 */
public class Class_LimiteDeCharTxt extends JTextField {

    private static final long serialVersionUID = 1L;
    private byte maxLength = 0;

    public Class_LimiteDeCharTxt(int maxLength) {
        super();
        this.maxLength = (byte) maxLength;
        this.addKeyListener(new LimitedKeyListener());
    }

    public void setMaxLength(int maxLength) {
        this.maxLength = (byte) maxLength;
        update();
    }

    private void update() {
        if (getText().length() > maxLength) {
            setText(getText().substring(0, maxLength));
            setCaretPosition(maxLength);
        }
    }

    @Override
    public void setText(String arg0) {
        super.setText(arg0);
        update();
    }

    @Override
    public void paste() {
        super.paste();
        update();
    }

    //Classes Internas
    private class LimitedKeyListener extends KeyAdapter {

        private boolean backspace = false;

        @Override
        public void keyPressed(KeyEvent e) {
            backspace = (e.getKeyCode() == 8);
        }

        @Override
        public void keyTyped(KeyEvent e) {
            if (!backspace
                    && getText().length() > maxLength - 1) {
                e.consume();
            }
        }
    }
}
