/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.controller;

import br.com.cresceritsolutions.ibspdv.models.factory.CanalVendaFactory;
import br.com.cresceritsolutions.ibspdv.models.hb.CanalVenda;
import br.com.cresceritsolutions.ibspdv.models.hb.Colaborador;
import br.com.cresceritsolutions.ibspdv.util.HibernateUtil;
import br.com.cresceritsolutions.ibspdv.util.IbsException;
import br.com.cresceritsolutions.ibspdv.util.VariaveisDoSistema;
import java.util.HashSet;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Gutem
 */
public class ControllerCriaCanalPadrao {

    public static void executar() throws IbsException {
        HibernateUtil hu = HibernateUtil.getInstance();
        try {
            hu.beginTransaction();
            
            CanalVenda canalVenda = (CanalVenda) hu.getSession().createCriteria(CanalVenda.class).add(Restrictions.and(Restrictions.eq("empresa", VariaveisDoSistema.EMPRESA), Restrictions.like("descricao", "%PADRÃO%"))).uniqueResult();
            if (canalVenda == null) {
                canalVenda = CanalVendaFactory.novo();
            }
            canalVenda.setDescricao("PADRÃO-LOJA");
            List<Colaborador> colaboradors = hu.getSession().createCriteria(Colaborador.class).add(Restrictions.or(Restrictions.eq("situacao", 0), Restrictions.eq("situacao", 1))).list();
            canalVenda.setColaboradors(new HashSet(colaboradors));
            if (colaboradors.size() > 0) {
                canalVenda.setColaborador(colaboradors.get(0));
            }
            hu.getSession().refresh(VariaveisDoSistema.EMPRESA);
            canalVenda.setEndereco(VariaveisDoSistema.EMPRESA.getPessoaJuridica().getPessoa().getEndereco());
            hu.getSession().saveOrUpdate(canalVenda);
            hu.commit();
        } catch (HibernateException ex) {
            hu.rollback();
            throw new IbsException("Erro ao criar canal padrão!", ex);
        }
    }

}
