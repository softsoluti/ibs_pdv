//Desenvolvedor : Gutemberg Lucas Vieira Lima
//Celular : (62)9112-0642
//Telefone : (62)3701-1466
//Email : ggutemberg@gmail.com/g_uto_lima@hotmail.com
package br.com.cresceritsolutions.ibspdv.controller.classes;

import javax.swing.ImageIcon;

/**
 *
 * @author Gutemberg
 */
public class Class_Imagens {

    private ImageIcon icone;

    public ImageIcon getMarcaIcone() {
        icone = new ImageIcon(getClass().getResource("/images/N LOGO 256.png"));
        //icone = new ImageIcon(getClass().getResource(""));
        return icone;
    }

    public String getLogoRelatorio() {
        return "./LOGO REL.png";
    }

    public ImageIcon getIconCerto() {
        icone = new ImageIcon(getClass().getResource("/images/checkmark_16x16.png"));
        //icone = new ImageIcon(getClass().getResource(""));
        return icone;
    }

    public ImageIcon getIconErrado() {
        icone = new ImageIcon(getClass().getResource("/images/close_16x16.png"));
        //icone = new ImageIcon(getClass().getResource(""));
        return icone;
    }

    public ImageIcon getIconNaoLida() {
        icone = new ImageIcon(getClass().getResource("/images/email 16.png"));
        //icone = new ImageIcon(getClass().getResource(""));
        return icone;
    }

    public ImageIcon getIconLida() {
        icone = new ImageIcon(getClass().getResource("/images/email2 16.png"));
        //icone = new ImageIcon(getClass().getResource(""));
        return icone;
    }

    public ImageIcon getIcone2() {
        //icone = new ImageIcon(getClass().getResource("/images/N LOGO 16.png"));
        icone = new ImageIcon(getClass().getResource("C:/Users/Gutemberg/Desktop/padraoTray.jpg"));
        return icone;
    }
}
