/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.controller;

import br.com.cresceritsolutions.ibspdv.util.Util;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellReference;

/**
 *
 * @author Gutem
 */
public class ControllerPOI {

    private Workbook wb;

    public void setPlanilhaEntrada(String file) throws IOException, InvalidFormatException  {
        //arquivo origem
        setWb(WorkbookFactory.create(new File(file)));
    }

    public void escrevePlanilha(String texto, int i, int j, int ws, int tp) {
        Sheet sheet = getWb().getSheetAt(ws);
        Row row = sheet.getRow(i);//linha
        if (row == null) {
            row = sheet.createRow(i);
        }
        Cell cell = row.getCell(j);//coluna
        if (cell == null) {
            cell = row.createCell(j);
        }
        if (tp == 1) {
            cell.setCellType(Cell.CELL_TYPE_STRING);
        }
        if (tp == 2) {
            texto = texto.replace(".", ",");
        }
        if (tp == 3) {
            Util.FormataDataPT(texto);
        }
        cell.setCellValue(texto);
    }

    public void salvaPlanilhaEm(String file) throws FileNotFoundException, IOException {//salva planilha
        FileOutputStream fileOut = new FileOutputStream(file);
        getWb().write(fileOut);
        fileOut.close();
        ControllerZip.compactar(file);
    }

    public void escrevePlanilhaTexto(int sheet, int linha, int coluna, String texto) {
        escrevePlanilha(texto, linha, coluna, sheet, 1);
    }

    public void setEscreveValorCordenadaMA(String texto, int sheet, String servico, String dia) {
        int linha = -1, coluna = -1;
        Sheet sheet1 = wb.getSheetAt(sheet);
        for (Row row : sheet1) {
            for (Cell cell : row) {
                CellReference cellRef = new CellReference(row.getRowNum(), cell.getColumnIndex());

                if (cellRef.getRow() == 3 && cellRef.getCol() >= 2 && cellRef.getCol() <= 32) {
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    if (cell.getStringCellValue().equals(dia.trim())) {
                        coluna = cellRef.getCol();
                    }
                }
                if (cellRef.getRow() >= 4 && cellRef.getRow() <= 68 && cellRef.getCol() == 1) {
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    if (cell.getStringCellValue().equals(servico.trim())) {
                        linha = cellRef.getRow();
                    }
                }

                if (cellRef.getCol() == coluna && cellRef.getRow() == linha
                        && coluna < 33 && coluna > 1
                        && linha > 3 && linha < 68) {
                    escrevePlanilhaTexto(sheet, linha, coluna, texto);
                }
            }
        }
    }

    public void setEscreveValorCordenada(String texto, int sheet, String servico, String dia) {
        int linha = -1, coluna = -1;
        Sheet sheet1 = getWb().getSheetAt(sheet);
        for (Row row : sheet1) {
            for (Cell cell : row) {
                CellReference cellRef = new CellReference(row.getRowNum(), cell.getColumnIndex());

                if (cellRef.getRow() == 7 && cellRef.getCol() >= 2 && cellRef.getCol() <= 33) {
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    if (cell.getStringCellValue().equals(dia.trim())) {
                        coluna = cellRef.getCol();
                    }
                }
                if (cellRef.getRow() >= 8 && cellRef.getCol() == 1) {
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    if (cell.getStringCellValue().trim().equals(servico.trim())) {
                        linha = cellRef.getRow();
                    }
                }

                if (cellRef.getCol() == coluna && cellRef.getRow() == linha
                        && coluna < 33 && coluna > 1
                        && linha > 7) {
                    escrevePlanilhaTexto(sheet, linha, coluna, texto);
                }
            }
        }
    }

    public Workbook getWb() {
        return wb;
    }

    public void setWb(Workbook wb) {
        this.wb = wb;
    }
}
