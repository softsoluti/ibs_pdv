//Desenvolvedor : Gutemberg Lucas Vieira Lima
//Celular : (62)9112-0642
//Telefone : (62)3701-1466
//Email : ggutemberg@gmail.com/g_uto_lima@hotmail.com
package br.com.cresceritsolutions.ibspdv.controller.classes;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Class_PegaNomeMaquina {

    private static String hostName, ipMaquina;

    public static String getHostName() throws UnknownHostException {
            hostName = InetAddress.getLocalHost().getHostName();
        return hostName;
    }

    public static String getIp() throws UnknownHostException {
            ipMaquina = InetAddress.getLocalHost().getHostAddress();
        return ipMaquina;
    }

    public static String getMac() throws SocketException, UnknownHostException {
        String smac = "";
            InetAddress address = InetAddress.getLocalHost();
            NetworkInterface ni = NetworkInterface.getByInetAddress(address);
            byte[] mac = ni.getHardwareAddress();
            for (int i = 0; i < mac.length; i++) {
                smac += String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : "");
            }

        return smac;
    }

}
