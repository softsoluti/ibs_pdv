//Desenvolvedor : Gutemberg Lucas Vieira Lima
//Celular : (62)9112-0642
//Telefone : (62)3701-1466
//Email : ggutemberg@gmail.com/g_uto_lima@hotmail.com
package br.com.cresceritsolutions.ibspdv.controller.classes;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.text.MaskFormatter;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/**
 *
 * @author Gutemberg
 */
public class Class_Formato {

    public static String getRetornaDataUs(String Data) {
        String data = Data, hora = "00:00:00";

        data = data.trim();
        String dia = "00", mes = "00", ano = "0000";
        String c1;
        String c2;
        String c3;
        String c4;
        String c5;
        String c6;
        String c7;
        String c8;
        String c9;
        String c10;
        String c11;
        String c12;
        String c13;
        String c14;
        String c15;
        String c16;
        String c17;
        String c18;
        String c19;

        if (data.length() != 0 || data != null) {
            if (data.length() == 6) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                if (c2.equals("/") && c4.equals("/")) {
                    dia = "0" + c1;
                    mes = "0" + c3;
                    ano = "20" + c5 + c6;
                }
            }
            if (data.length() == 7) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                if (c3.equals("/") && c5.equals("/")) {
                    dia = c1 + c2;
                    mes = "0" + c4;
                    ano = "20" + c6 + c7;
                }
                if (c2.equals("/") && c5.equals("/")) {
                    dia = "0" + c1;
                    mes = c3 + c4;
                    ano = "20" + c6 + c7;
                }
            }
            if (data.length() == 8) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                if (c3.equals("/") && c6.equals("/")) {
                    dia = c1 + c2;
                    mes = c4 + c5;
                    ano = "20" + c7 + c8;
                }
                if (c2.equals("/") && c4.equals("/")) {
                    dia = "0" + c1;
                    mes = "0" + c3;
                    ano = c5 + c6 + c7 + c8;
                }
            }
            if (data.length() == 9) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                if (c3.equals("/") && c5.equals("/")) {
                    dia = c1 + c2;
                    mes = "0" + c4;
                    ano = c6 + c7 + c8 + c9;
                }
                if (c2.equals("/") && c5.equals("/")) {
                    dia = "0" + c1;
                    mes = c3 + c4;
                    ano = c6 + c7 + c8 + c9;
                }
            }
            if (data.length() == 10) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                c10 = String.valueOf(data.charAt(9));
                if (c3.equals("/") && c6.equals("/")) {
                    dia = c1 + c2;
                    mes = c4 + c5;
                    ano = c7 + c8 + c9 + c10;
                }
            }

            if (data.length() == 19) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                c10 = String.valueOf(data.charAt(9));
                c11 = String.valueOf(data.charAt(10));
                c12 = String.valueOf(data.charAt(11));
                c13 = String.valueOf(data.charAt(12));
                c14 = String.valueOf(data.charAt(13));
                c15 = String.valueOf(data.charAt(14));
                c16 = String.valueOf(data.charAt(15));
                c17 = String.valueOf(data.charAt(16));
                c18 = String.valueOf(data.charAt(17));
                c19 = String.valueOf(data.charAt(18));

                dia = c1 + c2;
                mes = c4 + c5;
                ano = c7 + c8 + c9 + c10;

                hora = c12 + c13 + c14 + c15 + c16 + c17 + c18 + c19;

            }
        }
        return ano + "-" + mes + "-" + dia;
    }

    @SuppressWarnings("AssignmentToMethodParameter")
    public static String getDiaDoMes(String dataPt) {
        String data = dataPt, hora = "00:00:00";

        data = data.trim();
        String dia = "00", mes = "00", ano = "0000";
        String c1;
        String c2;
        String c3;
        String c4;
        String c5;
        String c6;
        String c7;
        String c8;
        String c9;
        String c10;
        String c11;
        String c12;
        String c13;
        String c14;
        String c15;
        String c16;
        String c17;
        String c18;
        String c19;

        if (data.length() != 0 || data != null) {
            if (data.length() == 6) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                if (c2.equals("/") && c4.equals("/")) {
                    dia = "0" + c1;
                    mes = "0" + c3;
                    ano = "20" + c5 + c6;
                }
            }
            if (data.length() == 7) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                if (c3.equals("/") && c5.equals("/")) {
                    dia = c1 + c2;
                    mes = "0" + c4;
                    ano = "20" + c6 + c7;
                }
                if (c2.equals("/") && c5.equals("/")) {
                    dia = "0" + c1;
                    mes = c3 + c4;
                    ano = "20" + c6 + c7;
                }
            }
            if (data.length() == 8) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                if (c3.equals("/") && c6.equals("/")) {
                    dia = c1 + c2;
                    mes = c4 + c5;
                    ano = "20" + c7 + c8;
                }
                if (c2.equals("/") && c4.equals("/")) {
                    dia = "0" + c1;
                    mes = "0" + c3;
                    ano = c5 + c6 + c7 + c8;
                }
            }
            if (data.length() == 9) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                if (c3.equals("/") && c5.equals("/")) {
                    dia = c1 + c2;
                    mes = "0" + c4;
                    ano = c6 + c7 + c8 + c9;
                }
                if (c2.equals("/") && c5.equals("/")) {
                    dia = "0" + c1;
                    mes = c3 + c4;
                    ano = c6 + c7 + c8 + c9;
                }
            }
            if (data.length() == 10) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                c10 = String.valueOf(data.charAt(9));
                if (c3.equals("/") && c6.equals("/")) {
                    dia = c1 + c2;
                    mes = c4 + c5;
                    ano = c7 + c8 + c9 + c10;
                }
            }

            if (data.length() == 19) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                c10 = String.valueOf(data.charAt(9));
                c11 = String.valueOf(data.charAt(10));
                c12 = String.valueOf(data.charAt(11));
                c13 = String.valueOf(data.charAt(12));
                c14 = String.valueOf(data.charAt(13));
                c15 = String.valueOf(data.charAt(14));
                c16 = String.valueOf(data.charAt(15));
                c17 = String.valueOf(data.charAt(16));
                c18 = String.valueOf(data.charAt(17));
                c19 = String.valueOf(data.charAt(18));

                dia = c1 + c2;
                mes = c4 + c5;
                ano = c7 + c8 + c9 + c10;

                hora = c12 + c13 + c14 + c15 + c16 + c17 + c18 + c19;

            }
        }
        if (dia.length() == 1) {
            dia = "0" + dia;
        }
        return dia;
    }

    public static int getMes(String Data) {
        String data = Data, hora = "00:00:00";

        data = data.trim();
        String dia = "00", mes = "00", ano = "0000";
        String c1;
        String c2;
        String c3;
        String c4;
        String c5;
        String c6;
        String c7;
        String c8;
        String c9;
        String c10;
        String c11;
        String c12;
        String c13;
        String c14;
        String c15;
        String c16;
        String c17;
        String c18;
        String c19;

        if (data.length() != 0 || data != null) {
            if (data.length() == 6) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                if (c2.equals("/") && c4.equals("/")) {
                    dia = "0" + c1;
                    mes = "0" + c3;
                    ano = "20" + c5 + c6;
                }
            }
            if (data.length() == 7) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                if (c3.equals("/") && c5.equals("/")) {
                    dia = c1 + c2;
                    mes = "0" + c4;
                    ano = "20" + c6 + c7;
                }
                if (c2.equals("/") && c5.equals("/")) {
                    dia = "0" + c1;
                    mes = c3 + c4;
                    ano = "20" + c6 + c7;
                }
            }
            if (data.length() == 8) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                if (c3.equals("/") && c6.equals("/")) {
                    dia = c1 + c2;
                    mes = c4 + c5;
                    ano = "20" + c7 + c8;
                }
                if (c2.equals("/") && c4.equals("/")) {
                    dia = "0" + c1;
                    mes = "0" + c3;
                    ano = c5 + c6 + c7 + c8;
                }
            }
            if (data.length() == 9) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                if (c3.equals("/") && c5.equals("/")) {
                    dia = c1 + c2;
                    mes = "0" + c4;
                    ano = c6 + c7 + c8 + c9;
                }
                if (c2.equals("/") && c5.equals("/")) {
                    dia = "0" + c1;
                    mes = c3 + c4;
                    ano = c6 + c7 + c8 + c9;
                }
            }
            if (data.length() == 10) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                c10 = String.valueOf(data.charAt(9));
                if (c3.equals("/") && c6.equals("/")) {
                    dia = c1 + c2;
                    mes = c4 + c5;
                    ano = c7 + c8 + c9 + c10;
                }
            }

            if (data.length() == 19) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                c10 = String.valueOf(data.charAt(9));
                c11 = String.valueOf(data.charAt(10));
                c12 = String.valueOf(data.charAt(11));
                c13 = String.valueOf(data.charAt(12));
                c14 = String.valueOf(data.charAt(13));
                c15 = String.valueOf(data.charAt(14));
                c16 = String.valueOf(data.charAt(15));
                c17 = String.valueOf(data.charAt(16));
                c18 = String.valueOf(data.charAt(17));
                c19 = String.valueOf(data.charAt(18));

                dia = c1 + c2;
                mes = c4 + c5;
                ano = c7 + c8 + c9 + c10;

                hora = c12 + c13 + c14 + c15 + c16 + c17 + c18 + c19;

            }
        }
        return Integer.parseInt(mes);
    }

    public static int getAno(String Data) {
        String data = Data, hora = "00:00:00";

        data = data.trim();
        String dia = "00", mes = "00", ano = "0000";
        String c1;
        String c2;
        String c3;
        String c4;
        String c5;
        String c6;
        String c7;
        String c8;
        String c9;
        String c10;
        String c11;
        String c12;
        String c13;
        String c14;
        String c15;
        String c16;
        String c17;
        String c18;
        String c19;

        if (data.length() != 0 || data != null) {
            if (data.length() == 6) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                if (c2.equals("/") && c4.equals("/")) {
                    dia = "0" + c1;
                    mes = "0" + c3;
                    ano = "20" + c5 + c6;
                }
            }
            if (data.length() == 7) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                if (c3.equals("/") && c5.equals("/")) {
                    dia = c1 + c2;
                    mes = "0" + c4;
                    ano = "20" + c6 + c7;
                }
                if (c2.equals("/") && c5.equals("/")) {
                    dia = "0" + c1;
                    mes = c3 + c4;
                    ano = "20" + c6 + c7;
                }
            }
            if (data.length() == 8) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                if (c3.equals("/") && c6.equals("/")) {
                    dia = c1 + c2;
                    mes = c4 + c5;
                    ano = "20" + c7 + c8;
                }
                if (c2.equals("/") && c4.equals("/")) {
                    dia = "0" + c1;
                    mes = "0" + c3;
                    ano = c5 + c6 + c7 + c8;
                }
            }
            if (data.length() == 9) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                if (c3.equals("/") && c5.equals("/")) {
                    dia = c1 + c2;
                    mes = "0" + c4;
                    ano = c6 + c7 + c8 + c9;
                }
                if (c2.equals("/") && c5.equals("/")) {
                    dia = "0" + c1;
                    mes = c3 + c4;
                    ano = c6 + c7 + c8 + c9;
                }
            }
            if (data.length() == 10) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                c10 = String.valueOf(data.charAt(9));
                if (c3.equals("/") && c6.equals("/")) {
                    dia = c1 + c2;
                    mes = c4 + c5;
                    ano = c7 + c8 + c9 + c10;
                }
            }

            if (data.length() == 19) {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                c10 = String.valueOf(data.charAt(9));
                c11 = String.valueOf(data.charAt(10));
                c12 = String.valueOf(data.charAt(11));
                c13 = String.valueOf(data.charAt(12));
                c14 = String.valueOf(data.charAt(13));
                c15 = String.valueOf(data.charAt(14));
                c16 = String.valueOf(data.charAt(15));
                c17 = String.valueOf(data.charAt(16));
                c18 = String.valueOf(data.charAt(17));
                c19 = String.valueOf(data.charAt(18));

                dia = c1 + c2;
                mes = c4 + c5;
                ano = c7 + c8 + c9 + c10;

                hora = c12 + c13 + c14 + c15 + c16 + c17 + c18 + c19;

            }
        }
        return Integer.parseInt(ano);
    }

    public static String FormataDataPT(String Data) {
        if (Data.length() == 10) {
            String data = Data;

            data = data.trim();
            String dia = "00", mes = "00", ano = "0000";
            String c1;
            String c2;
            String c3;
            String c4;
            String c5;
            String c6;
            String c7;
            String c8;
            String c9;
            String c10;
            if (data.length() == 0 || data == null) {
            }
            {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                c10 = String.valueOf(data.charAt(9));
                dia = c9 + c10;
                mes = c6 + c7;
                ano = c1 + c2 + c3 + c4;
            }
            ano = String.valueOf(ano.charAt(2)) + String.valueOf(ano.charAt(3));
            return dia + "/" + mes + "/" + ano;
        } else {
            return Data;
        }
    }//Retorna a data no formato yyyy-mm-dd

    public static String FormataDataPT2(String Data) {
        if (Data.length() == 10) {
            String data = Data;

            data = data.trim();
            String dia = "00", mes = "00", ano = "0000";
            String c1;
            String c2;
            String c3;
            String c4;
            String c5;
            String c6;
            String c7;
            String c8;
            String c9;
            String c10;
            if (data.length() == 0 || data == null) {
            }
            {
                c1 = String.valueOf(data.charAt(0));
                c2 = String.valueOf(data.charAt(1));
                c3 = String.valueOf(data.charAt(2));
                c4 = String.valueOf(data.charAt(3));
                c5 = String.valueOf(data.charAt(4));
                c6 = String.valueOf(data.charAt(5));
                c7 = String.valueOf(data.charAt(6));
                c8 = String.valueOf(data.charAt(7));
                c9 = String.valueOf(data.charAt(8));
                c10 = String.valueOf(data.charAt(9));
                dia = c9 + c10;
                mes = c6 + c7;
                ano = c1 + c2 + c3 + c4;
            }
            ano = String.valueOf(ano.charAt(2)) + String.valueOf(ano.charAt(3));

            return dia + "/" + mes + "/20" + ano;
        } else {
            return Data;
        }
    }

    public static String getValidaComoDecimal(String texto) {
        String dado = texto;
        dado = dado.replace(",", ".");
        if (dado.length() == 0) {
            dado = "0";
        }
        return dado;
    }

    public static String getFormatoDataMysql(String campo) {
        String dado = "";
        dado = "DATE_FORMAT(" + campo + ",'%d/%m/%Y')";
        return dado;
    }

    public static String getFormatoDataMysqlComHora(String campo) {
        String dado = "";
        dado = "DATE_FORMAT(" + campo + ",'%d/%m/%X %k:%i:%S')";
        return dado;
    }

    public static String getFormatoCelular(String texto) {
        String dado = "";
        String c1 = "", c2 = "", c3 = "", c4 = "", c5 = "", c6 = "", c7 = "", c8 = "", c9 = "", c10 = "";

        c1 = String.valueOf(texto.charAt(0));
        c2 = String.valueOf(texto.charAt(1));
        c3 = String.valueOf(texto.charAt(2));
        c4 = String.valueOf(texto.charAt(3));
        c5 = String.valueOf(texto.charAt(4));
        c6 = String.valueOf(texto.charAt(5));
        c7 = String.valueOf(texto.charAt(6));
        c8 = String.valueOf(texto.charAt(7));
        c9 = String.valueOf(texto.charAt(8));
        c10 = String.valueOf(texto.charAt(9));

        dado = "(" + c1 + c2 + ")" + c3 + c4 + c5 + c6 + "-" + c7 + c8 + c9 + c10;
        return dado;
    }

    public static String formatarString(String texto, String mascara) throws ParseException {
        MaskFormatter mf = new MaskFormatter(mascara);
        mf.setValueContainsLiteralCharacters(false);
        return mf.valueToString(texto);
    }

    @SuppressWarnings("AssignmentToMethodParameter")
    public static String colocaVirgulaNaSegundaCasa(String texto) {
        String dado = "";
        String numerico = "0123456789";
        texto = texto.replace(",", "");
        int localVirg = texto.length() - 2;
        for (int i = 0; i < texto.length(); i++) {
            if (numerico.contains(new Character(texto.charAt(i)).toString())) {
                if (i == localVirg) {
                    dado += ",";
                }
                dado += texto.charAt(i);
            }
        }
        return dado;
    }

    @SuppressWarnings("AssignmentToMethodParameter")
    public static String colocaVirgulaNaSegundaCasaPorcentagem(String texto) {
        String dado = "";
        String numerico = "0123456789";
        texto = texto.replace(",", "");
        int localVirg = texto.length() - 2;
        for (int i = 0; i < texto.length(); i++) {
            if (numerico.contains(new Character(texto.charAt(i)).toString())) {
                if (i == localVirg) {
                    dado += ",";
                }
                dado += texto.charAt(i);
            }
        }
        return dado + "%";
    }

    @SuppressWarnings("AssignmentToMethodParameter")
    public static String colocaFormatoCep(String texto) {
        String dado = "";
        String numerico = "0123456789";
        texto = texto.replace("-", "");
        int localVirg = texto.length() - 3;
        for (int i = 0; i < texto.length(); i++) {
            if (numerico.contains(new Character(texto.charAt(i)).toString())) {
                if (i == localVirg) {
                    dado += "-";
                }
                dado += texto.charAt(i);
            }
        }
        return dado;
    }

    @SuppressWarnings("AssignmentToMethodParameter")
    public static String colocaFormatoCnpj(String texto) {
        String dado = "";
        String numerico = "0123456789";
        texto = texto.replace(".", "");
        texto = texto.replace("/", "");
        texto = texto.replace("-", "");
        int local1Ponto = texto.length() - 12;
        int local2Ponto = texto.length() - 9;
        int localBarra = texto.length() - 6;
        int localTraco = texto.length() - 2;
        for (int i = 0; i < texto.length(); i++) {
            if (numerico.contains(new Character(texto.charAt(i)).toString())) {
                if (i == local1Ponto) {
                    dado += ".";
                }
                if (i == local2Ponto) {
                    dado += ".";
                }
                if (i == localBarra) {
                    dado += "/";
                }
                if (i == localTraco) {
                    dado += "-";
                }
                dado += texto.charAt(i);
            }
        }
        return dado;
    }

    @SuppressWarnings("AssignmentToMethodParameter")
    public static String colocaFormatoCpf(String texto) {
        String dado = "";
        String numerico = "0123456789";
        texto = texto.replace(".", "");
        texto = texto.replace("-", "");
        int local1Ponto = texto.length() - 8;
        int local2Ponto = texto.length() - 5;
        int localTraco = texto.length() - 2;
        for (int i = 0; i < texto.length(); i++) {
            if (numerico.contains(new Character(texto.charAt(i)).toString())) {

                if (i == local1Ponto) {
                    dado += ".";
                }
                if (i == local2Ponto) {
                    dado += ".";
                }
                if (i == localTraco) {
                    dado += "-";
                }
                dado += texto.charAt(i);
            }
        }
        return dado;
    }

    @SuppressWarnings("AssignmentToMethodParameter")
    public static String colocaFormatoData(String texto) {
        String dado = "";
        String numerico = "0123456789";
        texto = texto.replace("/", "");
        int local1asp = texto.length() - 4;
        int local2asp = texto.length() - 6;
        for (int i = 0; i < texto.length(); i++) {
            if (numerico.contains(new Character(texto.charAt(i)).toString())) {
                if (i == local1asp) {
                    dado += "/";
                }
                if (i == local2asp) {
                    dado += "/";
                }
                dado += texto.charAt(i);
            }
        }
        return dado;
    }

    public static String getRetornaApenasNumero(String texto) {
        String dado = "";
        String numerico = "0123456789";
        for (int i = 0; i < texto.length(); i++) {
            if (numerico.contains(new Character(texto.charAt(i)).toString())) {
                dado += texto.charAt(i);
            }
        }
        return dado;
    }

    @SuppressWarnings("AssignmentToMethodParameter")
    public static String trocaAcentuacao(String acentuada) {
        if (acentuada == null || acentuada.equals("")) {
            return acentuada;
        }
		char[] acentuados = new char[] { 'ç', 'á', 'à', 'ã', 'â', 'ä', 'é',
				'è', 'ê', 'ë', 'í', 'ì', 'î', 'ï', 'ó', 'ò', 'õ', 'ô', 'ö',
				'ú', 'ù', 'û', 'ü' };

		char[] naoAcentuados = new char[] { 'c', 'a', 'a', 'a', 'a', 'a', 'e',
				'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o',
				'u', 'u', 'u', 'u' };

        for (int i = 0; i < acentuados.length; i++) {
            acentuada = acentuada.replace(acentuados[i], naoAcentuados[i]);
            acentuada = acentuada.replace(Character.toUpperCase(acentuados[i]), Character.toUpperCase(naoAcentuados[i]));
        }
        return acentuada;
    }

    public static String retornaColunaBdValida(String texto) {//Permite a digitação apenas de caracteres validos para criação de coluna no bd mysql
        String dado = "";
        String validos = " ABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890_!'@#$%&*()-+={[}]><.,";
        for (int i = 0; i < texto.length(); i++) {
            if (validos.contains(new Character(texto.charAt(i)).toString().toUpperCase())) {
                dado += texto.charAt(i);
            }
        }
        return dado;
    }

    public static String getCodSimbol(String simbol) {//Codifica para colunas validas mysql//
        return simbol.replace("'", "simbol_1").replace("!", "simbol_2").replace("@", "simbol_3").replace("#", "simbol_4").replace("$", "simbol_5").replace("%", "simbol_6").replace("&", "simbol_7").replace("*", "simbol_8").replace("(", "simbol_9").replace(")", "simbol_10").replace("-", "simbol_11").replace("+", "simbol_12").replace("=", "simbol_13").replace("[", "simbol_14").replace("]", "simbol_15").replace("{", "simbol_16").replace("}", "simbol_17").replace("<", "simbol_18").replace(">", "simbol_19").replace(",", "simbol_20").replace(".", "simbol_21");
    }

    public static String getSimbolCod(String simbol) {//Decodifica colunas mysql
        return simbol.replace(".", "simbol_21").replace(",", "simbol_20").replace(">", "simbol_19").replace("<", "simbol_18").replace("}", "simbol_17").replace("{", "simbol_16").replace("]", "simbol_15").replace("[", "simbol_14").replace("=", "simbol_13").replace("+", "simbol_12").replace("-", "simbol_11").replace(")", "simbol_10").replace("(", "simbol_9").replace("*", "simbol_8").replace("&", "simbol_7").replace("%", "simbol_6").replace("$", "simbol_5").replace("#", "simbol_4").replace("@", "simbol_3").replace("!", "simbol_2").replace("'", "simbol_1");
    }

    public static String getFormatoMoeda(Object texto) {
        double precoDouble = 0;
        if(texto instanceof String){
            precoDouble = Double.parseDouble(texto.toString().replace(",", "."));
        }else if(texto instanceof Double){
            precoDouble = (double) texto;
        }
        DecimalFormat fmt = new DecimalFormat("0.00");    //limita o número de casas decimais      
        String string = fmt.format(precoDouble);
        String[] part = string.split("[,]");
        String preco = part[0] + "." + part[1];
        String dado = "";
        String numerico = "0123456789";
        preco = preco.replace(".", "");
        int local1virg = preco.length() - 2;
        int local2ponto = preco.length() - 5;
        int local3ponto = preco.length() - 8;
        int local4ponto = preco.length() - 11;
        int local5ponto = preco.length() - 14;

        for (int i = 0; i < preco.length(); i++) {
            if (numerico.contains(new Character(preco.charAt(i)).toString())) {
                if (i == local1virg) {
                    dado += ",";
                }
                if (i == local2ponto && preco.length() > 6) {
                    dado += ".";
                }
                if (i == local3ponto && preco.length() > 9) {
                    dado += ".";
                }
                if (i == local4ponto && preco.length() > 12) {
                    dado += ".";
                }
                if (i == local5ponto && preco.length() > 15) {
                    dado += ".";
                }
                dado += preco.charAt(i);
            }
        }
        if (precoDouble < 0) {
            return "R$ -" + dado;
        } else {
            return "R$ " + dado;
        }
    }

    public static float getArredondaNumero(float numero) {
        String v1 = String.valueOf(numero);
        if (numero < 0) {
            if (v1.contains(".97")) {
                numero += 0.02;
            }
            if (v1.contains(".98")) {
                numero -= 0.02;
            }
            if (v1.contains(".99")) {
                numero -= 0.01;
            }
            if (v1.contains(".01")) {
                numero += 0.01;
            }
            if (v1.contains(".02")) {
                numero += 0.02;
            }
            if (v1.contains(".03")) {
                numero -= 0.02;
            }
        } else {
            if (v1.contains(".97")) {
                numero -= 0.02;
            }
            if (v1.contains(".98")) {
                numero += 0.02;
            }
            if (v1.contains(".99")) {
                numero += 0.01;
            }
            if (v1.contains(".01")) {
                numero -= 0.01;
            }
            if (v1.contains(".02")) {
                numero -= 0.02;
            }
            if (v1.contains(".03")) {
                numero += 0.02;
            }
        }
        return numero;
    }

    public static DefaultComboBoxModel ordenar(DefaultComboBoxModel boxModel) {
        List<String> lista = new ArrayList<String>();
        for (int i = 0; i < boxModel.getSize(); i++) {
            lista.add(boxModel.getElementAt(i).toString());
        }
        Collections.sort(lista);
        boxModel.removeAllElements();
        for (int i = 0; i < lista.size(); i++) {
            boxModel.addElement(lista.get(i));
        }
        return boxModel;
    }

    public static String getCelulaValida(Cell cell) {
        String dado = "";
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                dado = cell.getRichStringCellValue().getString();
                break;
            case Cell.CELL_TYPE_NUMERIC:

                if (DateUtil.isCellDateFormatted(cell)) {
                    dado = Class_Formato.getRetornaDataUs(formato.format(cell.getDateCellValue()));
                } else {
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    dado = Class_Formato.getValidaComoDecimal(cell.getStringCellValue());
                }

                break;
            case Cell.CELL_TYPE_BOOLEAN:
                dado = "" + cell.getBooleanCellValue();
                break;
            case Cell.CELL_TYPE_FORMULA:
                dado = cell.getCellFormula();
                break;
            default:
                dado = "";
        }
        return dado;
    }

    public static String getDataTexto(String texto) {
        String s = "";
        boolean p = false;
        int conta = 0;
        texto += "----------";
        for (int i = 0; i < texto.length(); i++) {
            if (String.valueOf(texto.charAt(i)).equals("/")) {
                p = true;
            }
            if (p) {
                if (conta == 10) {
                    break;
                }
                s += String.valueOf(texto.charAt(i - 2));
                conta++;
            }
        }
        return s.trim();
    }

    public static String getDescricaoMes(int i) {
        switch (i) {
            case 1:
                return "Janeiro";
            case 2:
                return "Fevereiro";
            case 3:
                return "Março";
            case 4:
                return "Abril";
            case 5:
                return "Maio";
            case 6:
                return "Junho";
            case 7:
                return "Julho";
            case 8:
                return "Agosto";
            case 9:
                return "Setembro";
            case 10:
                return "Novembro";
            case 11:
                return "Dezembro";
            case 12:
                return "Janeiro";
            default:
                return "";
        }
    }

    public static int getIdMes(String descricao) {
        switch (descricao.toUpperCase()) {
            case "JANEIRO":
                return 1;
            case "FEVEREIRO":
                return 2;
            case "MARÇO":
                return 3;
            case "MARCO":
                return 3;
            case "ABRIL":
                return 4;
            case "MAIO":
                return 5;
            case "JUNHO":
                return 6;
            case "JULHO":
                return 7;
            case "AGOSTO":
                return 8;
            case "SETEMBRO":
                return 9;
            case "OUTUBRO":
                return 10;
            case "NOVEMBRO":
                return 11;
            case "DEZEMBRO":
                return 12;
            default:
                return 0;
        }
    }

    public static void calculaXls(HSSFWorkbook w) {
        for (int i = 0; i < w.getNumberOfSheets(); i++) {
            HSSFSheet s = w.getSheetAt(i);
            for (int j = 0; j < 65536; j++) {
                Row r = s.getRow(j);
                if (r != null) {
                    for (int h = 0; h < 256; h++) {
                        Cell a = r.getCell(h);
                        if (a != null) {
                            if (a.getCellType() == Cell.CELL_TYPE_FORMULA) {
                                a.setCellFormula(a.getCellFormula());
                            }
                        }
                    }
                }
            }
        }
    }

    public static void calculaXls(Workbook w) {
        for (int i = 0; i < w.getNumberOfSheets(); i++) {
            Sheet s = w.getSheetAt(i);
            for (int j = 0; j < 65536; j++) {
                Row r = s.getRow(j);
                if (r != null) {
                    for (int h = 0; h < 256; h++) {
                        Cell a = r.getCell(h);
                        if (a != null) {
                            if (a.getCellType() == Cell.CELL_TYPE_FORMULA) {
                                a.setCellFormula(a.getCellFormula());
                            }
                        }
                    }
                }
            }
        }
    }

    public static void main(String args[]) {
        System.out.println("");
    }
}
