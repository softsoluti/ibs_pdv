//Desenvolvedor : Gutemberg Lucas Vieira Lima
//Celular : (62)9112-0642
//Telefone : (62)3701-1466
//Email : ggutemberg@gmail.com/g_uto_lima@hotmail.com
package br.com.cresceritsolutions.ibspdv.controller.classes;

/**
 *
 * @author Gutemberg
 */
public class Class_Pega_Separador_AlfaNumerico {

    public static String getInteiro(String texto) {
        String dado = "";
        String numeros = "0123456789";
        for (int i = 0; i < texto.length(); i++) {
            String s = new Character(texto.charAt(i)).toString();
            if (s.contains(",")) {
                break;
            }
            if (numeros.contains(s)) {
                dado += s;
            }
        }
        return dado;
    }

    public static String getData(String texto) {
        String dado = "";
        boolean escreve = false;
        for (int i = 0; i < texto.length(); i++) {
            String s = new Character(texto.charAt(i)).toString();
            if (escreve) {
                dado += s;
            }
            if (s.contains("-")) {
                escreve = true;
            }
        }
        return dado;
    }

    public static int getRetornaNumeroDaColuna(String texto) {

        texto = texto.toUpperCase();
        int l1 = 0;
        int l2 = 0;
        int l3 = 0;
        int l4 = 0;
        int dado = 0;
        int Ql = 0;
        String numeros = "0123456789";
        for (int i = 0; i < texto.length(); i++) {
            String s = new Character(texto.charAt(i)).toString();
            if (numeros.contains(s)) {
                break;
            }
            Ql++;//conta quantidade de letras
        }
        String letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        switch (Ql) {
            case 1://caso possua 1 letra
                for (int i = 0; i < letras.length(); i++) {
                    if (letras.charAt(i) == texto.charAt(0)) {
                        break;
                    }
                    l1++;
                }
                break;
            case 2://caso possua 2 letras
                for (int i = 0; i < letras.length(); i++) {
                    l1++;
                    if (letras.charAt(i) == texto.charAt(1)) {
                        break;
                    }
                }

                for (int i = 0; i < letras.length(); i++) {
                    l2++;
                    if (letras.charAt(i) == texto.charAt(0)) {
                        break;
                    }
                }
                l2 = (l2 * 26) - 1;
                break;
            case 3://caso possua 3 letras
                for (int i = 0; i < letras.length(); i++) {
                    if (letras.charAt(i) == texto.charAt(2)) {
                        break;
                    }
                    l1++;
                }

                for (int i = 0; i < letras.length(); i++) {
                    l2++;
                    if (letras.charAt(i) == texto.charAt(1)) {
                        break;
                    }
                }
                l2 = l2 * 26;
                for (int i = 0; i < letras.length(); i++) {
                    l3++;
                    if (letras.charAt(i) == texto.charAt(0)) {
                        break;
                    }
                }
                l3 = l3 * 676;
                break;
            case 4://caso possua 3 letras
                for (int i = 0; i < letras.length(); i++) {
                    if (letras.charAt(i) == texto.charAt(3)) {
                        break;
                    }
                    l1++;
                }

                for (int i = 0; i < letras.length(); i++) {
                    l2++;
                    if (letras.charAt(i) == texto.charAt(2)) {
                        break;
                    }
                }
                l2 = l2 * 26;
                for (int i = 0; i < letras.length(); i++) {
                    l3++;
                    if (letras.charAt(i) == texto.charAt(1)) {
                        break;
                    }
                }
                l3 = l3 * 676;
                for (int i = 0; i < letras.length(); i++) {
                    l4++;
                    if (letras.charAt(i) == texto.charAt(0)) {
                        break;
                    }
                }
                l4 = l4 * 17576;
                break;
        }
        dado = l4 + l3 + l2 + l1;
        return dado;
    }

    public static int getRetornaNumeroDaLinha(String texto) {
        int dado = 0;
        String txt = "";
        String numeros = "0123456789";
        for (int i = 0; i < texto.length(); i++) {
            String s = new Character(texto.charAt(i)).toString();
            if (numeros.contains(s)) {
                if (numeros.contains(s)) {
                    txt += s;
                }
            }
        }
        dado = Integer.parseInt(txt);
        dado -= 1;
        return dado;
    }

    public static String retornaLetrasColuna(int col, int row) {
        String[] letras = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
        int cont4 = 0;
        int cont3 = -1;
        int cont2 = -1;
        int cont1 = -1;
        for (int i = 0; i < col; i++) {
            if (cont4 == 25) {
                if (cont3 == 25) {
                    if (cont2 == 25) {
                        cont2 = -1;
                        cont1++;
                    }
                    cont3 = -1;
                    cont2++;
                }
                cont4 = -1;
                cont3++;
            }
            cont4++;
        }
        if (cont1 != -1) {
            return letras[cont1] + "" + letras[cont2] + "" + letras[cont3] + "" + letras[cont4] + (row + 1);
        } else {
            if (cont2 != -1) {
                return letras[cont2] + "" + letras[cont3] + "" + letras[cont4] + (row + 1);
            } else {
                if (cont3 != -1) {
                    return letras[cont3] + "" + letras[cont4] + (row + 1);
                } else {
                    return letras[cont4] + (row + 1);

                }
            }
        }
    }
}
