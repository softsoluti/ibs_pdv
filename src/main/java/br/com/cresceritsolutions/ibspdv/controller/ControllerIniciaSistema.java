/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.controller;

import br.com.cresceritsolutions.ibspdv.dao.IniciaSistemaDao;
import acbr.acbrmonitor.TEF;
import br.com.cresceritsolutions.ibspdv.models.hb.Ecf;
import br.com.cresceritsolutions.ibspdv.util.CriptografiaUtil;
import br.com.cresceritsolutions.ibspdv.util.ExibirParaUsuario;
import br.com.cresceritsolutions.ibspdv.util.HibernateUtil;
import br.com.cresceritsolutions.ibspdv.util.IbsException;
import br.com.cresceritsolutions.ibspdv.util.Util;
import br.com.cresceritsolutions.ibspdv.util.VariaveisDoSistema;
import static br.com.cresceritsolutions.ibspdv.util.VariaveisDoSistema.*;
import br.com.cresceritsolutions.ibspdv.view.core.Logo;
import jACBrFramework.ACBrException;
import java.io.File;
import java.io.FileInputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Administrador
 */
public class ControllerIniciaSistema {

    private static ControllerIniciaSistema me;
    private static Logger log;

    public static ControllerIniciaSistema getInstance() {
        if (me == null) {
            me = new ControllerIniciaSistema();
        }
        return me;
    }

    private ControllerIniciaSistema() {
    }

    //Metodo de carregameto inicial do sistema
    public void getLoad() throws Exception {
        //Seta a pasta que de configurações do log4j
        PropertyConfigurator.configure("conf/log4j.properties");
        //Modelo das janelas swing
        setaModeloUI();
    }

    /*
     Metodo que carrega a preferencia de janelas swing ui
     */
    private static void setaModeloUI() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
        String tema = null;
        if (tema != null) {
            switch (Integer.parseInt(tema)) {
                case 0:
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                    break;
                case 1:
                    UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
                    break;
                case 2:
                    UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
                    break;
                case 3:
                    for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                        if ("Nimbus".equals(info.getName())) {
                            javax.swing.UIManager.setLookAndFeel(info.getClassName());
                            break;
                        }
                    }
            }
        } else {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
    }

    public void validaSistema() throws ACBrException, IbsException {
        Logo.getInstance().getBarraProgresso().setStringPainted(true);
        //GERA ARQUIVO MD5
        Logo.getInstance().getBarraProgresso().setValue(20);
        Logo.getInstance().getBarraProgresso().setString("Gerando arquivoMD5.txt");
        geraArquivoMd5();

        //criaEcf();
        //Validando ecf
        if (VariaveisDoSistema.getEcfAtivo()) {
            if (!validaComunicacaoEcf()) {
                System.exit(0);
            }
            VariaveisDoSistema.V_ECF = recuperaEcfAtiva(ControllerPaf.getInstance().getNumSerie());
            if (VariaveisDoSistema.V_ECF == null || !VariaveisDoSistema.V_ECF.getAtivo()) {
                if (VariaveisDoSistema.V_ECF == null) {
                    Ecf ecf = new Ecf();
                    ecf.setAtivo(false);
                    ecf.setIdentificacao("");
                    ecf.setMarca("");
                    ecf.setMfadicional(ControllerPaf.getInstance().getMFAdicional());
                    ecf.setModelo(String.valueOf(ControllerPaf.getInstance().getModelo()));
                    ecf.setSerie(ControllerPaf.getInstance().getNumSerie());
                    ecf.setTipo("");
                    ecf.setUsuario(1);
                    ecf.setVersaoSbEcf(ControllerPaf.getInstance().getNumVersaoSB());
                    ecf.setDataSbEcf(ControllerPaf.getInstance().getDataSB());
                    try {
                        HibernateUtil hu = HibernateUtil.getInstance();
                        hu.beginTransaction();
                        hu.getSession().saveOrUpdate(ecf);
                        hu.commit();
                    } catch (HibernateException ex) {
                        throw new IbsException("Ecf-DB - Erro inesperado!", ex);
                    }

                }
                ExibirParaUsuario.getInstance().erro(Logo.getInstance(), new Exception("Impressora não cadastrada!"), "Nenhuma impressora cadastrada e ativa no banco local.", true);

            }
            //Valida tef
            //VALIDA TEF
            Logo.getInstance().getBarraProgresso().setValue(60);
            Logo.getInstance().getBarraProgresso().setString("Validando o TEF...");
            validaTef();
        }

        //Seta dados iniciais da empresa como RAZAO,CNPJ,IE,IM
        IniciaSistemaDao.executar();

    }

    private boolean criaArquivoAuxiliar() {
        try {
            //Criptografa o arquivo auxiliar
            FileInputStream fis = new FileInputStream("conf/auxiliar.properties");
            AUXILIAR.load(fis);
            CriptografiaUtil.getInstance().criptografar("conf/auxiliar.txt", AUXILIAR);
        } catch (Exception ex) {
            // caso tenha algum problema tenta recuperar usando o backup
            try {
                CriptografiaUtil.getInstance().descriptografar("conf/auxiliar.bak", AUXILIAR);
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                AUXILIAR.setProperty("out.recebimento", Util.getData(cal.getTime()));
                cal.add(Calendar.DAY_OF_MONTH, -1);
                AUXILIAR.setProperty("out.envio", Util.getData(cal.getTime()));
                ExibirParaUsuario.getInstance().atencao(Logo.getInstance(), "Backup do arquivo auxiliar recuperado, necessita validar o sistema!");
            } catch (Exception ex1) {
                ExibirParaUsuario.getInstance().erro(Logo.getInstance(), ex1, "Problemas ao ler o arquivo auxiliar.txt o sistema será encerrado!", true);
                return false;
            }
        }
        try {
            //Criptografa o arquivo configuração
            FileInputStream fis1 = new FileInputStream("conf/config.properties");
            CONFIG.load(fis1);
            CriptografiaUtil.getInstance().criptografar("conf/config.txt", CONFIG);
            Logo.getInstance().getBarraProgresso().setValue(30);
        } catch (Exception ex) {
            ExibirParaUsuario.getInstance().erro(Logo.getInstance(), ex, "Problemas ao ler o arquivo config.txt o sistema será encerrado!", true);
            return false;
        }
        return true;
    }

    private static boolean validaComunicacaoEcf() {
        // valida a comunicao e ativacao com o ECF
        boolean ecfAtivo = false;
        try {
            Logo.getInstance().getBarraProgresso().setString("Conectando no ECF...");
            Logo.getInstance().getBarraProgresso().setString("Ativando o ECF...");
            ecfAtivo = ControllerPaf.getInstance().ativarEcf();
            Logo.getInstance().getBarraProgresso().setValue(50);
        } catch (Exception ex) {
            //log.error("Problemas ao conectar no ECF", ex);
            ExibirParaUsuario.getInstance().erro(Logo.getInstance(), ex, "Problemas ao conectar ou ativar o ECF.", false);
        }
        return ecfAtivo;
    }

    private void validaArquivoAuxiliar() throws Exception {
        CriptografiaUtil.getInstance().descriptografar("conf/auxiliar.txt", AUXILIAR);
        CriptografiaUtil.getInstance().descriptografar("conf/config.txt", CONFIG);
    }

    private static void geraArquivoMd5() {
        try {
            if (VariaveisDoSistema.getEcfAtivo()) {
                try {
                    PAF.gerarArquivos();
                } catch (Exception ex) {
                    ExibirParaUsuario.getInstance().erro(Logo.getInstance(), ex, "Problemas ao gerar o arquivoMD5.txt", true);
                }
            }
        } catch (Exception ex) {
            ExibirParaUsuario.getInstance().erro(Logo.getInstance(), ex, ex.getMessage(), true);
        }
    }

    private static void validaTef() {
        // validacao do TEF
        if (Boolean.valueOf(Util.getConfig().get("pag.cartao"))) {
            TEF.setTEF(Util.getConfig());
            Logo.getInstance().getBarraProgresso().setString("Validando o TEF...");
            Logo.getInstance().getBarraProgresso().setValue(60);
            while (!TEF.gpAtivo()) {
                ExibirParaUsuario.getInstance().atencao(Logo.getInstance(), "Gerenciador Padrão não está ativo!\nPor favor ative-o para continuar.");
            }
            Logo.getInstance().setAlwaysOnTop(false);
            if (Boolean.valueOf(Util.getConfig().get("pag.cartao"))) {
                try {
                    String arquivo = TEF.lerArquivo(TEF.getRespIntPos001(), 0);
                    boolean pendente = false;
                    if (arquivo != null) {
                        Map<String, String> mapa = TEF.iniToMap(arquivo);
                        pendente = !(mapa.get("000-000").equals("ATV") || mapa.get("000-000").equals("ADM"));
                    }
                    if (pendente || TEF.getPathTmp().listFiles(TEF.getFiltro()).length > 0) {

                        if (ExibirParaUsuario.getInstance().confirmacao(Logo.getInstance(), "O sistema identificou problemas no último TEF!\nO sistema irá cancelar a última operação!\nDeseja realizar este procedimento?")) {
                            // cancela somente o TEF
                            Logo.getInstance().getBarraProgresso().setString("Cancelando os TEF pendentes...");
                            TEF.cancelarPendentes(true);
                        }
                    }
                } catch (Exception ex) {
                    log.error("Problemas ao cancelar pendentes do TEF", ex);
                    ExibirParaUsuario.getInstance().erro(Logo.getInstance(), ex, "Problemas ao cancelar pendentes do TEF.\nCaso precise, estorne os cartões pelo ADM.", true);

                } finally {
                    TEF.deletarArquivo(TEF.getRespIntPos001());
                    for (File arquivo : TEF.getPathTmp().listFiles(TEF.getFiltro())) {
                        arquivo.delete();
                    }
                }
            }
            Logo.getInstance().setAlwaysOnTop(true);
        }
    }

    private Ecf recuperaEcfAtiva(String serie) throws IbsException {
        HibernateUtil hu = HibernateUtil.getInstance();
        Ecf ecf = null;
        try {
            ecf = (Ecf) hu.getSession().createCriteria(Ecf.class).add(Restrictions.eq("serie", serie)).uniqueResult();
        } catch (HibernateException ex) {
            throw new IbsException("Erro ao localizar ecf no banco de dados!", ex);
        }
        return ecf;
    }

    public void carregaAuxiliar() {
        //Carrega arquivo auxiliar
        if(VariaveisDoSistema.RECRIAR_ARQUIVO_AUXILIAR){
            //criaArquivoAuxiliar();
        }
        //Valida arquivo auxiliar
        Logo.getInstance().getBarraProgresso().setString("Lendo arquivo auxiliar...");
        try {
            validaArquivoAuxiliar();
        } catch (Exception ex) {
            ExibirParaUsuario.getInstance().erro(Logo.getInstance(), ex, "Erro ao validar o arquivo auxiliar.", true);
        }
    }

}
