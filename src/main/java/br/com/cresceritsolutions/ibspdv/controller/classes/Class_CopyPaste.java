/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.controller.classes;

import java.awt.Component;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.FocusAdapter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gutemberg
 */
public class Class_CopyPaste {

    public static void copia(String copia) {
        Clipboard board = Toolkit.getDefaultToolkit().getSystemClipboard();
        ClipboardOwner selection = new StringSelection(copia);
        board.setContents((Transferable) selection, selection);
    }

    public static String cola() throws UnsupportedFlavorException, IOException {
        Transferable t = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(new Object());
        return (String) t.getTransferData(DataFlavor.stringFlavor);
    }
}
