package br.com.cresceritsolutions.ibspdv.controller;

//Desenvolvedor : Gutemberg Lucas Vieira Lima
//Celular : (62)9112-0642
//Telefone : (62)3701-1466
//Email : ggutemberg@gmail.com/g_uto_lima@hotmail.com
import br.com.cresceritsolutions.ibspdv.controller.classes.Class_Pega_Separador_AlfaNumerico;
import br.com.cresceritsolutions.ibspdv.util.Util;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

/**
 *
 * @author Gutemberg
 */
public class ControllerCarregaXlsPrecosFranquia {

    private static ControllerCarregaXlsPrecosFranquia me;
    private Workbook wb;
    public HashMap titulos;

    public static ControllerCarregaXlsPrecosFranquia getInstance(Workbook wb) {
        if (me == null) {
            me = new ControllerCarregaXlsPrecosFranquia();
        }
        me.wb = wb;
        return me;
    }


    public void setWb(Workbook wb) {
        
    }

    public DefaultTableModel leTabelaPreco(String Sheet, String chave) {
        Sheet sheet = wb.getSheet(Sheet);
        int linha = Class_Pega_Separador_AlfaNumerico.getRetornaNumeroDaLinha(chave);
        int coluna = Class_Pega_Separador_AlfaNumerico.getRetornaNumeroDaColuna(chave);
        //Carrega titulo das colunas
        titulos = new HashMap();
        for (Row r : sheet) {
            for (Cell c : r) {
                if (c.getRowIndex() == linha) {
                    if (c.getColumnIndex() >= coluna) {
                        if (!sheet.isColumnHidden(c.getColumnIndex()) && sheet.getColumnWidth(c.getColumnIndex()) > 0) {
                            String celula = c.toString().trim().toUpperCase().replace("\n", " ").replace("\r", " ").replace("  ", "");
                            if (celula != null && !celula.equals("") && !titulos.containsValue(celula)) {
                                titulos.put(c.getColumnIndex(), celula);
                            }
                        }
                        if (c.equals("")) {
                            break;
                        }
                    }
                }
            }
        }
        //Acrescebta Sub coluna
        List<Object[]> l = new ArrayList<>();
        int linhasMerge = 0;
        DefaultTableModel dtm = new DefaultTableModel();
        for (Cell c : sheet.getRow(linha)) {
            if (c.getRowIndex() >= linha) {
                if (c.getColumnIndex() >= coluna) {
                    if (titulos.get(c.getColumnIndex()) != null) {
                        //Informações
                        if (linhasMerge == 0 && isRowMerge(sheet, c.getRowIndex())) {
                            linhasMerge = getNumLinhasMerge(sheet, c.getRowIndex(), coluna);
                        }
                        //Planos
                        if (isColumnMerge(sheet, c.getColumnIndex())) {

                            for (int j = 0; j <= getNumColunasMerge(sheet, c.getColumnIndex()); j++) {
                                dtm.addColumn(titulos.get(c.getColumnIndex()) + " " + sheet.getRow(linha + linhasMerge).getCell(c.getColumnIndex() + j).toString().toUpperCase());
                                l.add(new Object[]{c.getColumnIndex() + j, titulos.get(c.getColumnIndex()) + " " + sheet.getRow(linha + linhasMerge).getCell(c.getColumnIndex() + j).toString().toUpperCase()});
                            }
                        }
                    }
                }
            }
        }
        //Preenche modelo com preços
        int i = 0;
        int j = 0;
        for (Row r : sheet) {
            if (r.getRowNum() > linha + linhasMerge) {
                if (r.getCell(Integer.parseInt(l.get(0)[0].toString())) != null
                        && r.getCell(Integer.parseInt(l.get(1)[0].toString())) != null
                        && !r.getCell(Integer.parseInt(l.get(0)[0].toString())).toString().equals("")
                        && !r.getCell(Integer.parseInt(l.get(1)[0].toString())).toString().equals("")) {
                    dtm.addRow(new Object[]{});
                    for (Object[] o : l) {
                        int col = Integer.parseInt(o[0].toString());
                        Cell c = r.getCell(col);
                        c.setCellType(Cell.CELL_TYPE_STRING);
                        dtm.setValueAt(Util.getCelulaValida(c), i, j++);
                    }
                    i++;
                    j = 0;
                } else {
                    break;
                }
            }
        }
        return dtm;
    }

    public static void main(String args[]) throws IOException, InvalidFormatException{
        Workbook wb = WorkbookFactory.create(new File("C:\\Users\\Gutem\\Desktop\\book\\Tabelas_V02_26.02.2014.xls"));
        Sheet sheet = wb.getSheet("Aparelhos Consumo");
        int linha = Class_Pega_Separador_AlfaNumerico.getRetornaNumeroDaLinha("B8");
        int coluna = Class_Pega_Separador_AlfaNumerico.getRetornaNumeroDaColuna("B8");
        //Carrega titulo das colunas
        HashMap titulos = new HashMap();
        for (Row r : sheet) {
            for (Cell c : r) {
                if (c.getRowIndex() == linha) {
                    if (c.getColumnIndex() >= coluna) {
                        if (!sheet.isColumnHidden(c.getColumnIndex()) && sheet.getColumnWidth(c.getColumnIndex()) > 0) {
                            String celula = c.toString().trim().toUpperCase().replace("\n", " ").replace("\r", " ").replace("  ", "");
                            if (celula != null && !celula.equals("") && !titulos.containsValue(celula)) {
                                titulos.put(c.getColumnIndex(), celula);
                            }
                        }
                        if (c.equals("")) {
                            break;
                        }
                    }
                }
            }
        }
        //Acrescebta Sub coluna
        List<Object[]> l = new ArrayList<>();
        int linhasMerge = 0;
        DefaultTableModel dtm = new DefaultTableModel();
        for (Cell c : sheet.getRow(linha)) {
            if (c.getRowIndex() >= linha) {
                if (c.getColumnIndex() >= coluna) {
                    if (titulos.get(c.getColumnIndex()) != null) {
                        //Informações
                        if (linhasMerge == 0 && isRowMerge(sheet, c.getRowIndex())) {
                            linhasMerge = getNumLinhasMerge(sheet, c.getRowIndex(), coluna);
                        }
                        //Planos
                        if (isColumnMerge(sheet, c.getColumnIndex())) {

                            for (int j = 0; j <= getNumColunasMerge(sheet, c.getColumnIndex()); j++) {
                                dtm.addColumn(titulos.get(c.getColumnIndex()) + " " + sheet.getRow(linha + linhasMerge).getCell(c.getColumnIndex() + j).toString().toUpperCase());
                                l.add(new Object[]{c.getColumnIndex() + j, titulos.get(c.getColumnIndex()) + " " + sheet.getRow(linha + linhasMerge).getCell(c.getColumnIndex() + j).toString().toUpperCase()});
                            }
                        }
                    }
                }
            }
        }
        //Preenche modelo com preços
        int i = 0;
        int j = 0;
        for (Row r : sheet) {
            if (r.getRowNum() > linha + linhasMerge) {
                if (r.getCell(Integer.parseInt(l.get(0)[0].toString())) != null && !r.getCell(Integer.parseInt(l.get(0)[0].toString())).toString().equals("")) {
                    dtm.addRow(new Object[]{});
                    for (Object[] o : l) {
                        int col = Integer.parseInt(o[0].toString());

                        Cell c = r.getCell(col);
                        c.setCellType(Cell.CELL_TYPE_STRING);
                        System.out.println(c.getRichStringCellValue());
                        dtm.setValueAt(Util.getCelulaValida(c), i, j++);
                    }
                    i++;
                    j = 0;
                } else {
                    break;
                }
            }
        }
    }

    private static boolean isColumnMerge(Sheet s, int coluna) {
        for (int i = 0; i < s.getNumMergedRegions(); i++) {
            if (s.getMergedRegion(i).getFirstColumn() == coluna) {
                return true;
            }
        }
        return false;
    }

    private static boolean isRowMerge(Sheet s, int linha) {
        for (int i = 0; i < s.getNumMergedRegions(); i++) {
            if (s.getMergedRegion(i).getFirstRow() == linha) {
                return true;
            }
        }
        return false;
    }

    private static int getNumColunasMerge(Sheet s, int coluna) {
        for (int i = 0; i < s.getNumMergedRegions(); i++) {
            if (s.getMergedRegion(i).getFirstColumn() == coluna) {
                return s.getMergedRegion(i).getLastColumn() - s.getMergedRegion(i).getFirstColumn();
            }
        }
        return 0;
    }

    private static int getNumLinhasMerge(Sheet s, int linhas, int coluna) {
        for (int i = 0; i < s.getNumMergedRegions(); i++) {
            if (s.getMergedRegion(i).getFirstRow() == linhas && s.getMergedRegion(i).getFirstColumn() == coluna) {
                return s.getMergedRegion(i).getLastRow() - s.getMergedRegion(i).getFirstRow();
            }
        }
        return 0;
    }
}
