/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.controller;

import acbr.acbrmonitor.TEF;
import br.com.cresceritsolutions.ibspdv.models.hb.Formas;
import br.com.cresceritsolutions.ibspdv.models.hb.Pagamentos;
import br.com.cresceritsolutions.ibspdv.util.ExibirParaUsuario;
import br.com.cresceritsolutions.ibspdv.view.core.Aguarde;
import br.com.cresceritsolutions.ibspdv.view.venda.FinalizaVenda;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Gutem
 */
public class ControllerTef {

    private static ControllerTef me;
    private int limite;
    private DefaultTableModel dtmPag;

    public static ControllerTef getInstance() {
        if (me == null) {
            me = new ControllerTef();
        }
        return me;
    }

    public void iniciar(Double valor) {
        Double apagar = Double.parseDouble(FinalizaVenda.getInstance().lblTotal.getText().isEmpty() ? "0.00" : FinalizaVenda.getInstance().lblTotal.getText().replace(".", "").replace(",", "."));
        Pagamentos pagamentos = new Pagamentos();
        DefaultComboBoxModel<Pagamentos> boxModel = (DefaultComboBoxModel<Pagamentos>) FinalizaVenda.getInstance().lstFormaPagamento.getModel();
        Double pago = 0.0;
        for (int i = 0; i < boxModel.getSize(); i++) {
            Pagamentos pagamentos1 = boxModel.getElementAt(i);
            pago += pagamentos1.getValor();
        }

        String coo = FinalizaVenda.getInstance().txtCOO.getText(); // Util.formataNumero(Caixa.getInstancia().getVenda().getEcfVendaCoo(), 6, 0, false);
        try {
            String id = TEF.gerarId();
            TEF.realizarTransacao(id, coo, valor);
            TEF.focar(FinalizaVenda.getInstance().getTitle());

            Double valorCartao = Double.valueOf(TEF.getDados().get("003-000")) / 100;
            String rede = TEF.getDados().get("010-000");
            int trans = Integer.valueOf(TEF.getDados().get("011-000"));
            Date data = new SimpleDateFormat("ddMMyyyyHHmmss").parse(TEF.getDados().get("022-000") + TEF.getDados().get("023-000"));
            String nsu = TEF.getDados().get("012-000");
            String arquivo = TEF.getPathTmp().getAbsolutePath() + System.getProperty("file.separator") + "pendente" + id + ".txt";
            String msg = TEF.getDados().get("030-000");
            String parcelas = TEF.getDados().get("018-000");
            Double desconto = Double.valueOf(TEF.getDados().get("709-000")!=null?TEF.getDados().get("709-000"):"0") / 100;
            boolean confirmado = false;
            FinalizaVenda.getInstance().txtDesconto.setText(String.valueOf(desconto).replace(".", ","));
            FinalizaVenda.getInstance().atualizaTotais();

            // caso o pagamento seja menor que o total restante, podendo ter outro cartao
            if (valorCartao.compareTo(apagar - pago) < 0) {
                // confirma e gera o backup
                FileUtils.copyFile(new File(arquivo), new File(arquivo.replace("pendente", "backup")));
                arquivo = arquivo.replace("pendente", "backup");
                TEF.confirmarTransacao(id, true);
                FinalizaVenda.getInstance().lblTef.setText("TEF [" + msg + "]");
                confirmado = true;
                Aguarde.getInstance().setVisible(false);
            } else {
                Aguarde.getInstance().getLblInfo().setText(msg);
            }
            FinalizaVenda.getInstance().TEFS++;
            pagamentos.setFormas((Formas) FinalizaVenda.getInstance().cboFormaPagamento.getSelectedItem());
            pagamentos.setValor(valorCartao);
            pagamentos.setNsu(nsu);
            pagamentos.setRede(rede);
            pagamentos.setData(data);
            pagamentos.setTrans(String.valueOf(trans));
            pagamentos.setParcelas(Integer.parseInt(parcelas == null ? "0" : parcelas));
            pagamentos.setVenda(FinalizaVenda.getInstance().venda);
            pagamentos.setArquivo(arquivo);
            pagamentos.setConfirmado(confirmado);
            boxModel.addElement(pagamentos);
            FinalizaVenda.getInstance().atualizaPagamentos(boxModel);
        } catch (Exception ex) {
            Aguarde.getInstance().setVisible(false);
            if (ex.getMessage() != null) {
                //O pai da mensagem será a janela intFinalizarVenda
                ExibirParaUsuario.getInstance().erro(FinalizaVenda.getInstance(), ex, ex.getMessage(), false);
            }

        }

    }

}
