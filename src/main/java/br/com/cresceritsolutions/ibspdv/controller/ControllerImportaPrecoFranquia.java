/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.controller;

import br.com.cresceritsolutions.ibspdv.models.enuns.OpcoesTituloPrecoFranquia;
import br.com.cresceritsolutions.ibspdv.models.hb.ItensPrecoFranquia;
import br.com.cresceritsolutions.ibspdv.models.hb.PlanoPrecoFranquia;
import br.com.cresceritsolutions.ibspdv.models.hb.PrecoFranquia;
import br.com.cresceritsolutions.ibspdv.models.hb.ProdutoPrecoFranquia;
import br.com.cresceritsolutions.ibspdv.models.hb.TipoPagamento;
import br.com.cresceritsolutions.ibspdv.models.hb.TipoPrecoFranquia;
import br.com.cresceritsolutions.ibspdv.util.HibernateUtil;
import br.com.cresceritsolutions.ibspdv.util.IbsException;
import br.com.cresceritsolutions.ibspdv.util.Util;
import br.com.cresceritsolutions.ibspdv.view.tabelapreco.operadora.CadPrecoFranquia;
import br.com.cresceritsolutions.ibspdv.view.tabelapreco.operadora.ConfiguracaoPrecoFranquia;
import br.com.cresceritsolutions.ibspdv.view.tabelapreco.operadora.ConfiguracaoValorPrecoFranquia;
import br.com.cresceritsolutions.ibspdv.view.tabelapreco.operadora.ItensTabbetNovo;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author Gutem
 */
public class ControllerImportaPrecoFranquia {

    private static ControllerImportaPrecoFranquia me;

    public static ControllerImportaPrecoFranquia getInstance() {
        if (me == null) {
            me = new ControllerImportaPrecoFranquia();
        }
        return me;
    }

    public void iniciar() throws IbsException, ConstraintViolationException {
        //salva book de preÇos
        HibernateUtil hu = HibernateUtil.getInstance();
        try {

            TipoPagamento vista = (TipoPagamento) hu.getSession().createCriteria(TipoPagamento.class).add(Restrictions.eq("descricao", "À VISTA")).uniqueResult();
            TipoPagamento prazo = (TipoPagamento) hu.getSession().createCriteria(TipoPagamento.class).add(Restrictions.eq("descricao", "À PRAZO")).uniqueResult();

            PrecoFranquia precoFranquia = new PrecoFranquia();
            precoFranquia.setDescricao(CadPrecoFranquia.getInstance().txtDescricao.getText());
            precoFranquia.setDataVigencia(Util.getData(CadPrecoFranquia.getInstance().cboData.getSelectedItem().toString()));
            precoFranquia.setDataAlteracao(new Date());
            precoFranquia.setDataInclusao(new Date());
            precoFranquia.setAtivo(true);

            CadPrecoFranquia temp = CadPrecoFranquia.getInstance();
            ItensTabbetNovo[] itns = temp.ts;
            List<TipoPrecoFranquia> lstTipoFranquia = new ArrayList<>();
            for (ItensTabbetNovo itn : itns) {
                TipoPrecoFranquia tipoPrecoFranquia = new TipoPrecoFranquia();
                tipoPrecoFranquia.setPrecoFranquia(precoFranquia);
                tipoPrecoFranquia.setDescricao(itn.getName());
                DefaultTableModel dtm = (DefaultTableModel) itn.tbInfo.getModel();
                int colunaFabricante = getModelo(OpcoesTituloPrecoFranquia.FABRICANTE, dtm);
                int colunaModelo = getModelo(OpcoesTituloPrecoFranquia.MODELO, dtm);
                int colunaCodigoSap = getModelo(OpcoesTituloPrecoFranquia.CODIGO_SAP, dtm);
                int colunaGama = getModelo(OpcoesTituloPrecoFranquia.GAMA, dtm);

                for (int i = 0; i < dtm.getRowCount(); i++) {
                    String descricaoFabricante = null;
                    String descricaoModelo = null;
                    String descricaoCodigoSap = null;
                    String descricaoGama = null;
                    for (int j = 0; j < dtm.getColumnCount(); j++) {
                        if (colunaFabricante == j) {
                            descricaoFabricante = dtm.getValueAt(i, colunaFabricante).toString();
                        }
                        if (colunaModelo == j) {
                            descricaoModelo = dtm.getValueAt(i, colunaModelo).toString();
                        }
                        if (colunaCodigoSap == j) {
                            descricaoCodigoSap = dtm.getValueAt(i, colunaCodigoSap).toString();
                        }
                        if (colunaGama == j) {
                            descricaoGama = dtm.getValueAt(i, colunaGama).toString();
                        }

                        String descricaoPlano = getPlano(dtm.getColumnName(j));
                        if (descricaoPlano != null
                                && descricaoFabricante != null && !descricaoFabricante.isEmpty()
                                && descricaoModelo != null && !descricaoModelo.isEmpty()
                                && descricaoCodigoSap != null && !descricaoCodigoSap.isEmpty()
                                && descricaoGama != null && !descricaoGama.isEmpty()) {

                            ItensPrecoFranquia itensPrecoFranquia = new ItensPrecoFranquia();
                            itensPrecoFranquia.setTipoPrecoFranquia(tipoPrecoFranquia);

                            ProdutoPrecoFranquia produtoPrecoFranquia = (ProdutoPrecoFranquia) hu.getSession().createCriteria(ProdutoPrecoFranquia.class)
                                    .add(Restrictions.eq("fabricante", descricaoFabricante))
                                    .add(Restrictions.eq("modelo", descricaoModelo))
                                    .add(Restrictions.eq("codigoSap", descricaoCodigoSap))
                                    .add(Restrictions.eq("gama", descricaoGama)).uniqueResult();
                            if (produtoPrecoFranquia == null) {
                                produtoPrecoFranquia = new ProdutoPrecoFranquia();
                                produtoPrecoFranquia.setFabricante(descricaoFabricante);
                                produtoPrecoFranquia.setModelo(descricaoModelo);
                                produtoPrecoFranquia.setCodigoSap(descricaoCodigoSap);
                                produtoPrecoFranquia.setGama(descricaoGama);
                                hu.getSession().save(produtoPrecoFranquia);
                            }

                            PlanoPrecoFranquia planoPrecoFranquia = (PlanoPrecoFranquia) hu.getSession().createCriteria(PlanoPrecoFranquia.class)
                                    .add(Restrictions.eq("descricao", descricaoPlano)).uniqueResult();
                            if (planoPrecoFranquia == null) {
                                planoPrecoFranquia = new PlanoPrecoFranquia();
                                planoPrecoFranquia.setDescricao(descricaoPlano);
                                hu.getSession().save(planoPrecoFranquia);
                            }

                            itensPrecoFranquia.setProdutoPrecoFranquia(produtoPrecoFranquia);
                            itensPrecoFranquia.setPlanoPrecoFranquia(planoPrecoFranquia);
                            String valor = getValor(dtm.getValueAt(i, j).toString());

                            TipoPagamento tpValor = vista;
                            if (dtm.getColumnName(j).toUpperCase().contains("PRAZO")) {
                                tpValor = prazo;
                            }

                            itensPrecoFranquia.setTipoPagamento(tpValor);
                            itensPrecoFranquia.setValor(valor);
                            tipoPrecoFranquia.getItensPrecoFranquias().add(itensPrecoFranquia);
                        }
                    }
                }
                lstTipoFranquia.add(tipoPrecoFranquia);
            }
            hu.beginTransaction();
            for (TipoPrecoFranquia tpf : lstTipoFranquia) {
                hu.getSession().saveOrUpdate(tpf);
                for (Object o : tpf.getItensPrecoFranquias()) {
                    hu.getSession().saveOrUpdate(o);
                }
            }
            hu.getSession().saveOrUpdate(precoFranquia);
            hu.commit();
        } catch (HibernateException ex) {
            hu.rollback();
            throw new IbsException("Não foi possível salvar! Verifique se já não há uma tabela com a mesma descrição!", ex);
        }

    }

    private String getPlano(String coluna) {
        for (int i = 0; i < ConfiguracaoPrecoFranquia.getInstance().tbConf.getRowCount(); i++) {
            if (OpcoesTituloPrecoFranquia.PLANO.toString().equalsIgnoreCase(ConfiguracaoPrecoFranquia.getInstance().tbConf.getValueAt(i, 1).toString())
                    && coluna.contains(ConfiguracaoPrecoFranquia.getInstance().tbConf.getValueAt(i, 0).toString())) {
                return ConfiguracaoPrecoFranquia.getInstance().tbConf.getValueAt(i, 0).toString();
            }
        }
        return null;
    }

    private String getValor(String valor) {
        for (int i = 0; i < ConfiguracaoValorPrecoFranquia.getInstance().tbConf.getRowCount(); i++) {
            if (ConfiguracaoValorPrecoFranquia.getInstance().tbConf.getValueAt(i, 0).toString().equalsIgnoreCase(valor)) {
                return ConfiguracaoValorPrecoFranquia.getInstance().tbConf.getValueAt(i, 1).toString();
            }
        }
        return valor;
    }

    private int getModelo(OpcoesTituloPrecoFranquia opcoesTituloPrecoFranquia, DefaultTableModel dtm) {
        String descricaoColuna = "";
        for (int i = 0; i < ConfiguracaoPrecoFranquia.getInstance().tbConf.getRowCount(); i++) {
            if (ConfiguracaoPrecoFranquia.getInstance().tbConf.getValueAt(i, 1).toString().toUpperCase()
                    .equals(opcoesTituloPrecoFranquia.toString())) {
                descricaoColuna = ConfiguracaoPrecoFranquia.getInstance().tbConf.getValueAt(i, 0).toString();
            }
        }

        for (int j = 0; j < dtm.getRowCount(); j++) {
            if (descricaoColuna.trim().equalsIgnoreCase(dtm.getColumnName(j).trim())) {
                return j;
            }
        }
        return -1;
    }

}
