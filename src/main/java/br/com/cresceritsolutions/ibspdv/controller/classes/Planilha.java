/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.controller.classes;

public class Planilha{

	private String data_venda;
	private String ntc;
	private String iccid;
	private String imei;
	private String aparelho;
	private String tipo_venda;
	private String plano;
	private String margem_venda;
	private String valor_compra;
	private String valor_venda;
	private String valor;
	private String tipo_up;
	private String tipo_mig;
	private String tipo_desconto_up;
	private String valor_desconto_up;
	private String protocolo_ged;
	private String status_contrato;
	private String cpf;
	private String data_rec_contrato;
	private String data_dig;
	private String data_batimento;
	private String data_envio_inspecao;
	private String data_alteracao_status;
	private String origem_desc_up;
	private String valor_desc_up;
	private String valido;
        private String obs;
        private String cod_revenda;
        private String plataforma;
        private String ddd;
        private String simcard;
        private String statusLinha;
        private String cod_vendedor;
        private String vendedor;
        private String primario;
        private String secundario;
        private String acao_mark;
        private String regional;
        private String estado;
        private String cod_pdv;
        private String usuario;
        private String irregular;
        private int tipo_planilha;
        
	public String getProtocolo_ged() {
		return protocolo_ged;
	}

	public void setProtocolo_ged(String protocolo_ged) {
		this.protocolo_ged = protocolo_ged;
	}

	public String getOrigem_desc_up() {
		return origem_desc_up;
	}

	public void setOrigem_desc_up(String origem_desc_up) {
		this.origem_desc_up = origem_desc_up;
	}

	public String getValor_desc_up() {
		return valor_desc_up;
	}

	public void setValor_desc_up(String valor_desc_up) {
		this.valor_desc_up = valor_desc_up;
	}

	public String getProtocolo_desc_up() {
		return protocolo_desc_up;
	}

	public void setProtocolo_desc_up(String protocolo_desc_up) {
		this.protocolo_desc_up = protocolo_desc_up;
	}

	private String protocolo_desc_up;
	
	public String getIccid() {
		return Class_Formato.getRetornaApenasNumero(iccid);
	}

	public void setIccid(String iccid) {
		this.iccid = iccid;
	}

	public String getTipo_up() {
		return tipo_up;
	}

	public void setTipo_up(String tipo_up) {
		this.tipo_up = tipo_up;
	}

	public String getTipo_mig() {
		return tipo_mig;
	}

	public void setTipo_mig(String tipo_mig) {
		this.tipo_mig = tipo_mig;
	}

	public String getTipo_desconto_up() {
		return tipo_desconto_up;
	}

	public void setTipo_desconto_up(String tipo_desconto_up) {
		this.tipo_desconto_up = tipo_desconto_up;
	}

	public String getValor_desconto_up() {
		return valor_desconto_up;
	}

	public void setValor_desconto_up(String valor_desconto_up) {
		this.valor_desconto_up = valor_desconto_up;
	}

	
	public String getStatus_contrato() {
		return status_contrato;
	}

	public void setStatus_contrato(String status_contrato) {
		this.status_contrato = status_contrato;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getData_rec_contrato() {
		return data_rec_contrato;
	}

	public void setData_rec_contrato(String data_rec_contrato) {
		this.data_rec_contrato = data_rec_contrato;
	}

	public String getData_dig() {
		return data_dig;
	}

	public void setData_dig(String data_dig) {
		this.data_dig = data_dig;
	}

	public String getData_batimento() {
		return data_batimento;
	}

	public void setData_batimento(String data_batimento) {
		this.data_batimento = data_batimento;
	}

	public String getData_envio_inspecao() {
		return data_envio_inspecao;
	}

	public void setData_envio_inspecao(String data_envio_inspecao) {
		this.data_envio_inspecao = data_envio_inspecao;
	}

	public String getData_alteracao_status() {
		return data_alteracao_status;
	}

	public void setData_alteracao_status(String data_alteracao_status) {
		this.data_alteracao_status = data_alteracao_status;
	}

	public String getData_venda() {
		return data_venda;
	}

	public void setData_venda(String data_venda) {
		this.data_venda = data_venda;
	}

	public String getNtc() {
		return Class_Formato.getRetornaApenasNumero(ntc);
	}

	public void setNtc(String ntc) {
		this.ntc = ntc;
	}

	public String getImei() {
		return Class_Formato.getRetornaApenasNumero(imei);
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getAparelho() {
		return aparelho;
	}

	public void setAparelho(String aparelho) {
		this.aparelho = aparelho;
	}

	public String getTipo_venda() {
		return tipo_venda;
	}

	public void setTipo_venda(String tipo_venda) {
		this.tipo_venda = tipo_venda;
	}

	public String getPlano() {
		return plano;
	}

	public void setPlano(String plano) {
		this.plano = plano;
	}

	public String getMargem_venda() {
		return margem_venda;
	}

	public void setMargem_venda(String margem_venda) {
		this.margem_venda = margem_venda;
	}

	public String getValor_compra() {
		return valor_compra;
	}

	public void setValor_compra(String valor_compra) {
		this.valor_compra = valor_compra;
	}

	public String getValor_venda() {
		return valor_venda;
	}

	public void setValor_venda(String valor_venda) {
		this.valor_venda = valor_venda;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

    /**
     * @return the valido
     */
    public String getValido() {
        return valido;
    }

    /**
     * @param valido the valido to set
     */
    public void setValido(String valido) {
        this.valido = valido;
    }

    /**
     * @return the obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * @param obs the obs to set
     */
    public void setObs(String obs) {
        this.obs = obs;
    }

    /**
     * @return the cod_revenda
     */
    public String getCod_revenda() {
        return cod_revenda;
    }

    /**
     * @param cod_revenda the cod_revenda to set
     */
    public void setCod_revenda(String cod_revenda) {
        this.cod_revenda = cod_revenda;
    }

    /**
     * @return the plataforma
     */
    public String getPlataforma() {
        return plataforma;
    }

    /**
     * @param plataforma the plataforma to set
     */
    public void setPlataforma(String plataforma) {
        this.plataforma = plataforma;
    }

    /**
     * @return the ddd
     */
    public String getDdd() {
        return ddd;
    }

    /**
     * @param ddd the ddd to set
     */
    public void setDdd(String ddd) {
        this.ddd = ddd;
    }

    /**
     * @return the simcard
     */
    public String getSimcard() {
        return simcard;
    }

    /**
     * @param simcard the simcard to set
     */
    public void setSimcard(String simcard) {
        this.simcard = simcard;
    }

    /**
     * @return the statusLinha
     */
    public String getStatusLinha() {
        return statusLinha;
    }

    /**
     * @param statusLinha the statusLinha to set
     */
    public void setStatusLinha(String statusLinha) {
        this.statusLinha = statusLinha;
    }

    /**
     * @return the cod_vendedor
     */
    public String getCod_vendedor() {
        return cod_vendedor;
    }

    /**
     * @param cod_vendedor the cod_vendedor to set
     */
    public void setCod_vendedor(String cod_vendedor) {
        this.cod_vendedor = cod_vendedor;
    }

    /**
     * @return the vendedor
     */
    public String getVendedor() {
        return vendedor;
    }

    /**
     * @param vendedor the vendedor to set
     */
    public void setVendedor(String vendedor) {
        this.vendedor = vendedor;
    }

    /**
     * @return the primario
     */
    public String getPrimario() {
        return primario;
    }

    /**
     * @param primario the primario to set
     */
    public void setPrimario(String primario) {
        this.primario = primario;
    }

    /**
     * @return the secundario
     */
    public String getSecundario() {
        return secundario;
    }

    /**
     * @param secundario the secundario to set
     */
    public void setSecundario(String secundario) {
        this.secundario = secundario;
    }

    /**
     * @return the acao_mark
     */
    public String getAcao_mark() {
        return acao_mark;
    }

    /**
     * @param acao_mark the acao_mark to set
     */
    public void setAcao_mark(String acao_mark) {
        this.acao_mark = acao_mark;
    }

    /**
     * @return the regional
     */
    public String getRegional() {
        return regional;
    }

    /**
     * @param regional the regional to set
     */
    public void setRegional(String regional) {
        this.regional = regional;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the cod_pdv
     */
    public String getCod_pdv() {
        return cod_pdv;
    }

    /**
     * @param cod_pdv the cod_pdv to set
     */
    public void setCod_pdv(String cod_pdv) {
        this.cod_pdv = cod_pdv;
    }

    /**
     * @return the usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the irregular
     */
    public String getIrregular() {
        return irregular;
    }

    /**
     * @param irregular the irregular to set
     */
    public void setIrregular(String irregular) {
        this.irregular = irregular;
    }

    /**
     * @return the tipo_planilha
     */
    public int getTipo_planilha() {
        return tipo_planilha;
    }

    /**
     * @param tipo_planilha the tipo_planilha to set
     */
    public void setTipo_planilha(int tipo_planilha) {
        this.tipo_planilha = tipo_planilha;
    }

}
