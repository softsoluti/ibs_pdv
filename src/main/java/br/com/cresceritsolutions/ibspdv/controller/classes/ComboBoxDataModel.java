package br.com.cresceritsolutions.ibspdv.controller.classes;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import javax.swing.JCheckBox;
/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */

public class ComboBoxDataModel extends AbstractListModel implements ComboBoxModel {

    JCheckBox[] choices;
    Object selectedItem;

    public ComboBoxDataModel(JCheckBox[] choices) {
        this.choices = choices;
    }

    @Override
    public Object getSelectedItem() {
        return this.selectedItem;
    }

    @Override
    public void setSelectedItem(Object anItem) {
        this.selectedItem = anItem;
    }

    @Override
    public Object getElementAt(int index) {
        return (JCheckBox) choices[index];
    }

    @Override
    public int getSize() {
        return choices.length;
    }
}
