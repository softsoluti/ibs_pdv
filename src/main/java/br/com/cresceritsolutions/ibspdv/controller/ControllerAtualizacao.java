/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.controller;

import br.com.cresceritsolutions.ibspdv.models.validacao.Atualizacao;
import br.com.cresceritsolutions.ibspdv.util.CriptografiaUtil;
import br.com.cresceritsolutions.ibspdv.util.Util;
import br.com.cresceritsolutions.ibspdv.util.VariaveisDoSistema;
import static br.com.cresceritsolutions.ibspdv.util.VariaveisDoSistema.AUXILIAR;
import java.util.List;
import java.util.Properties;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Gutem
 */
public class ControllerAtualizacao {

    public static ControllerAtualizacao me;

    public boolean executar() throws Exception {
        Properties c = new Properties();
        c.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
        c.setProperty("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
        c.setProperty("hibernate.connection.url", "jdbc:mysql://dbmy0023.whservidor.com:3306/gutem");
        c.setProperty("hibernate.connection.username", "gutem");
        c.setProperty("hibernate.connection.password", CriptografiaUtil.SBV());
        c.setProperty("hibernate.connection.autoReconnect", "true");

        c.setProperty("connection.provider_class", "org.hibernate.connection.C3P0ConnectionProvider");
        c.setProperty("c3p0.min_size", "5");
        c.setProperty("c3p0.max_size", "20");
        c.setProperty("c3p0.timeout", "1800");
        c.setProperty("c3p0.max_statements", "100");
        c.setProperty("hibernate.c3p0.testConnectionOnCheckout", "true");
        c.setProperty("hibernate.current_session_context_class", "thread");
        SessionFactory sf = new Configuration()
                .setProperties(c)
                .addResource("br/com/cresceritsolutions/ibspdv/models/validacao/CadastroIbs.hbm.xml")
                .addResource("br/com/cresceritsolutions/ibspdv/models/validacao/HistoricoAtualizacao.hbm.xml")
                .addResource("br/com/cresceritsolutions/ibspdv/models/validacao/AtualizacaoBd.hbm.xml")
                .addResource("br/com/cresceritsolutions/ibspdv/models/validacao/Atualizacao.hbm.xml")
                .buildSessionFactory();
        Session session = sf.openSession();
        List<Atualizacao> lstAtualizacao = session.createCriteria(Atualizacao.class)
                .add(Restrictions.eq("descricao", VariaveisDoSistema.getNomeSistema()))
                .addOrder(Order.desc("data"))
                .list();
        Atualizacao atualizacao;
        if (lstAtualizacao.size() > 0) {
            atualizacao = lstAtualizacao.get(0);

            VariaveisDoSistema.AUXILIAR.setProperty("out.atualizacao_corrente", Util.getData(atualizacao.getData()));
            VariaveisDoSistema.AUXILIAR.setProperty("out.versao_corrente", atualizacao.getVersao());
            VariaveisDoSistema.AUXILIAR.setProperty("out.link_atualizacao", atualizacao.getLink());
            CriptografiaUtil.getInstance().criptografar("conf/auxiliar.txt", AUXILIAR);
            return true;
        } else {
            return false;
        }

    }

    public static ControllerAtualizacao getInstance() {
        if (me == null) {
            me = new ControllerAtualizacao();
        }
        return me;
    }
}
