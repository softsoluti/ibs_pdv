/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.controller;

import acbr.acbrmonitor.TEF;
import br.com.cresceritsolutions.ibspdv.models.core.IbsPdvException;
import br.com.cresceritsolutions.ibspdv.models.enuns.SituacaoItemCompraVenda;
import br.com.cresceritsolutions.ibspdv.models.enuns.SituacaoPagamento;
import br.com.cresceritsolutions.ibspdv.models.hb.ItemCompra;
import br.com.cresceritsolutions.ibspdv.models.hb.ItemVenda;
import br.com.cresceritsolutions.ibspdv.models.hb.Pagamentos;
import br.com.cresceritsolutions.ibspdv.models.hb.Parcela;
import br.com.cresceritsolutions.ibspdv.models.hb.ServicoPacote;
import br.com.cresceritsolutions.ibspdv.models.hb.TipoDeDocumento;
import br.com.cresceritsolutions.ibspdv.models.hb.Venda;
import br.com.cresceritsolutions.ibspdv.util.HibernateUtil;
import br.com.cresceritsolutions.ibspdv.util.IbsException;
import br.com.cresceritsolutions.ibspdv.util.Util;
import br.com.cresceritsolutions.ibspdv.util.VariaveisDoSistema;
import br.com.cresceritsolutions.ibspdv.view.core.Aguarde;
import br.com.cresceritsolutions.ibspdv.view.venda.FinalizaVenda;
import br.com.cresceritsolutions.ibspdv.view.venda.MensagemVendaFinalizada;
import jACBrFramework.ACBrException;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Gutem
 */
public class ControllerFecharVenda {

    private Logger log;
    private List<Pagamentos> pagamentos;
    private double bruto;
    private double acres_desc;
    private double troco;
    private String obs;
    private Venda venda;

    public ControllerFecharVenda(List<Pagamentos> pagamentos, double bruto, Double acres_desc, double troco, String obs) {
        this.pagamentos = pagamentos;
        this.bruto = bruto;
        this.acres_desc = acres_desc;
        this.troco = troco;
        this.obs = obs;
    }

    public void executar() throws IbsException {
        venda = FinalizaVenda.getInstance().venda;
        HibernateUtil hu = HibernateUtil.getInstance();
        try {
            TEF.bloquear(true);
            // fecha a venda no cupom
            //fecharVendaECF();
            if (VariaveisDoSistema.getEcfAtivo()) {
                ControllerPaf.getInstance().finalizaCupom(pagamentos, venda);
            }
            // salva no bd
            //fecharVendaBanco();

            hu.beginTransaction();
            fechaVendaBanco(hu.getSession());
            // salva o documento para relatorio
            if (VariaveisDoSistema.getEcfAtivo()) {
                ControllerPaf.getInstance().comandoSalvarDocumento("RV", venda, hu.getSession());
            }// salva pagamento e imprime os cartoes se tiver
            //ControllerPaf.getInstance().comandoImprimirCartao(pagamentos, troco);
            removePagamentos(hu.getSession());
            salvaPagamentos(hu.getSession());
            hu.commit();
            // coloca na tela
            //fecharVendaTela();
            TEF.bloquear(false);
            MensagemVendaFinalizada.newInstance(String.valueOf(venda.getId()), venda.getCliente()!=null && venda.getCliente().getPessoaFisica() != null ? venda.getCliente().getPessoaFisica().getNome() : (venda.getCliente()!=null && venda.getCliente().getPessoaJuridica()!=null?venda.getCliente().getPessoaJuridica().getRazao():""), "").setVisible(true);
            FinalizaVenda.getInstance().setVisible(false);

        } catch (IbsException ex) {
            TEF.bloquear(false);
            throw ex;
        } catch (HibernateException ex) {
            hu.rollback();
            throw new IbsException("Erro ao fechar a venda!", ex);
        } finally {
            Aguarde.getInstance().setVisible(false);
        }
    }

    private void salvaPagamentos(Session session) throws IbsException {
        List<File> impressos = new ArrayList<>();
        // soma os valores de todos os cartoes, para colocar no CCD e o total da venda
        double valCard = 0.00;
        double total = 0.00;
        for (Pagamentos pag : pagamentos) {
            if (pag.getFormas().isTef()) {
                valCard += pag.getValor();
            }
            total += pag.getValor();
        }

        // percorre os pagamentos para inserir no banco e imprimir cartoes
        ComandoImprimirCartao cmdCartao = new ComandoImprimirCartao();
        Iterator<Pagamentos> ip = pagamentos.iterator();
        while (ip.hasNext()) {
            Pagamentos pag = ip.next();
            if (pag.getFormas().isTef() && Boolean.valueOf(Util.getConfig().get("pag.cartao"))) {
                try {
                    cmdCartao.abrir(pag, total, session);
                } catch (ACBrException ex) {
                    throw new IbsException("Erro ao abril modulo TEF.", ex);
                }
                try {
                    cmdCartao.executar();
                } catch (ACBrException | IbsPdvException ex) {
                    throw new IbsException("Erro ao exectar modulo TEF.", ex);
                }
                impressos.add(new File(pag.getArquivo()));
            } else if (pag.getParcelas() == null) {
                Parcela parcela = new Parcela();
                parcela.setData(pag.getData());
                parcela.setValor(pag.getValor());
                parcela.setNsu(pag.getNsu());
                parcela.setPagamentos(pag);
                session.save(parcela);
            }
            // salva o pagamento
            pag.setVenda(venda);
            pag.setSituacao(SituacaoPagamento.RECEBIDO.getValor());
            //Cupom fiscal
            TipoDeDocumento tdd = (TipoDeDocumento) session.createCriteria(TipoDeDocumento.class).add(Restrictions.eq("codigo", 1)).uniqueResult();
            pag.setTipoDeDocumento(tdd);
            session.save(pag);
            Iterator<Parcela> i = pag.getParcelas_1().iterator();
            while (i.hasNext()) {
                Parcela p = i.next();
                session.save(p);
            }
        }
        try {
            cmdCartao.fechar();
        } catch (ACBrException ex) {
            throw new IbsException("Erro ao fechar modulo TEF.", ex);
        }

        // caso todos os cartoes tenham todas as vias impressas, deleta os arquivos pendentes dos mesmos
        for (File file : impressos) {
            file.delete();
        }
    }

    public void fechaVendaBanco(Session session) {
        Iterator<ItemVenda> i = venda.getItemVendas().iterator();
        while (i.hasNext()) {
            ItemVenda itemVenda = i.next();
            if (itemVenda.getPlanoDeVenda() != null) {
                session.saveOrUpdate(itemVenda.getPlanoDeVenda());
            }
            Iterator<ServicoPacote> i1 = itemVenda.getServicoPacotes().iterator();
            while (i1.hasNext()) {
                ServicoPacote servicoPacote = i1.next();
                session.saveOrUpdate(servicoPacote.getPlanoDeVenda());
                servicoPacote.setItemVenda(itemVenda);
                session.saveOrUpdate(servicoPacote);
            }
            //Muda situacao item compra chip
            if (itemVenda.getItemCompra().getChip() != null) {
                session.refresh(itemVenda.getItemCompra());
                itemVenda.getItemCompra().setSituacao(SituacaoItemCompraVenda.VENDIDO.getId());
                session.saveOrUpdate(itemVenda.getItemCompra());
            }
            //Muda situacao item compra produto
            session.refresh(itemVenda.getItemCompra());
            itemVenda.getItemCompra().setSituacao(SituacaoItemCompraVenda.VENDIDO.getId());
            Iterator<ItemCompra> i2 = itemVenda.getItemComprasForItemVendaId().iterator();
            while (i2.hasNext()) {
                ItemCompra ic = i2.next();
                session.refresh(ic);
                ic.setSituacao(SituacaoItemCompraVenda.VENDIDO.getId());
                ic.setItemVendaByItemVendaId(itemVenda);
                session.saveOrUpdate(ic);
            }
            if (itemVenda.getDescontoFranquia() != null) {
                session.saveOrUpdate(itemVenda.getDescontoFranquia());
            }
            session.saveOrUpdate(itemVenda.getItemCompra());
            session.saveOrUpdate(itemVenda);
        }
        session.saveOrUpdate(venda);
        if (venda.getNumNotaFiscal() == null) {
            venda.setNumNotaFiscal(venda.getId());
        }
        session.saveOrUpdate(venda);
    }

    private void removePagamentos(Session s) throws IbsException {
        Iterator<Pagamentos> i = venda.getPagamentoses().iterator();
        while (i.hasNext()) {
            Pagamentos p = i.next();
            s.delete(p);
        }
    }

}
