package br.com.cresceritsolutions.ibspdv.controller;

import br.com.cresceritsolutions.ibspdv.util.IbsException;


/**
 * Interface dos comandos usados pelos controladores.
 *
 * @author Pedro H. Lira
 */
public interface IComando {

    /**
     * Metodo que executa a acao.
     *
     * @exception IbsPdvException dispara caso nao consiga executar.
     * @throws Exception 
     */
    public void executar() throws IbsException, Exception;

    /**
     * Metodo que desfaz o comando executado.
     *
     * @exception IbsPdvException dispara caso nao consiga executar.
     */
    public void desfazer() throws IbsException;

}
