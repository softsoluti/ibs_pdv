package br.com.cresceritsolutions.ibspdv.controller;

import acbr.acbrmonitor.TEF;
import br.com.cresceritsolutions.ibspdv.models.core.IbsPdvException;
import br.com.cresceritsolutions.ibspdv.models.hb.Pagamentos;
import br.com.cresceritsolutions.ibspdv.models.hb.Parcela;
import br.com.cresceritsolutions.ibspdv.util.Util;
import br.com.cresceritsolutions.ibspdv.view.venda.FinalizaVenda;
import jACBrFramework.ACBrException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.hibernate.Session;

/**
 * Classe que realiza a acao de imprimir e confirmar as operacoes de Cartao.
 *
 * @author Pedro H. Lira
 */
public class ComandoImprimirCartao implements IComando {

    private Pagamentos pag;
    private String comando = null;
    private Session session;

    /**
     * Construtor padrao.
     */
    public ComandoImprimirCartao() {

    }

    /**
     * Metodo que abre as operacoes com cartao
     *
     * @param pag o pagamento em cartao.
     * @param total o total da venda.
     * @throws jACBrFramework.ACBrException
     */
    public void abrir(Pagamentos pag, double total, Session session) throws ACBrException {
        this.session = session;
        this.pag = pag;
        if (comando == null) {

            try {
                // abre o relatorio vinculado
                String coo = pag.getVenda().getCoo() + "";
                String codigo = pag.getFormas().getCodigo();
                //String sCard = Util.formataNumero(vCard, 1, 2, false).replace(",", ".");
                Double sTotal = Double.parseDouble(Util.formataNumero(total, 1, 2, false).replace(",", "."));
                ControllerPaf.getInstance().abreCupomVinculado(coo, codigo, sTotal);
                comando = "ECF_LinhaCupomVinculado";
            } catch (ACBrException | NumberFormatException ex) {
                // abre o relatorio gerencial
                ControllerPaf.getInstance().abreRelatorioGerencial(0);
                comando = "ECF_LinhaRelatorioGerencial";
            }
        }
    }

    @Override
    public void executar() throws ACBrException, IbsPdvException {
        // imprime as vias
        try {
            String arq = null;
            if (pag.getArquivo().contains("pendente")) {
                arq = TEF.lerArquivo(TEF.getRespIntPos001(), 0);
            }
            if (arq == null) {
                arq = TEF.lerArquivo(pag.getArquivo(), 0);
            }
            Map<String, String> dados = TEF.iniToMap(arq);
            TEF.imprimirVias(dados, comando.equals("ECF_LinhaCupomVinculado"));
            // se for o pendente, precisa confirmar no GP e sera o ultimo cartao
            if (pag.getArquivo().contains("pendente")) {
                String id = pag.getArquivo().replaceAll("\\D", "");
                TEF.confirmarTransacao(id, true);
                // pega o numero GNF da impressao do cartao
                pag.setGnf(Integer.parseInt(ControllerPaf.getInstance().getNumGNF()));
                // salva o documento para relatorio
                ControllerPaf.getInstance().comandoSalvarDocumento(comando.equals("ECF_LinhaCupomVinculado") ? "CC" : "RG", FinalizaVenda.getInstance().venda, session);
            }
            // gerar as parcelas
            List<Parcela> parcelas = gerarParcela(dados, pag);
            pag.setParcelas_1(new HashSet(parcelas));
        } catch (Exception ex) {
            fechar();
            throw new IbsPdvException(ex);
        }
    }

    /**
     * Metodo que fecha as operacoes com cartao
     *
     * @throws jACBrFramework.ACBrException
     */
    public void fechar() throws ACBrException {
        if (comando != null) {
            comando = null;
            ControllerPaf.getInstance().fechaRelatorio();
        }
    }

    @Override
    public void desfazer() {
        // metodo nao aplicavel.
    }

    /**
     * Metodo que adiciona a lista de parcelas as parcelas correspondente do
     * pagamento informado.
     *
     * @param dados o mapa de dados lidos do arquivo.
     * @param pagamento o objeto de pagamento a ser considerado.
     * @return uma lista de parcelas efetuadas pelo cartao.
     */
    private List<Parcela> gerarParcela(Map<String, String> dados, Pagamentos pagamento) {
        // recupera as parcelas
        List<Parcela> parcelas = new ArrayList<>();

        if (dados.get("018-000") == null) {
            Parcela parcela = new Parcela();
            parcela.setData(pagamento.getData());
            parcela.setValor(pagamento.getValor());
            parcela.setNsu(pagamento.getNsu());
            parcela.setPagamentos(pagamento);
            parcelas.add(parcela);
        } else {
            int faturas = Integer.valueOf(dados.get("018-000"));
            for (int fat = 1; fat <= faturas; fat++) {
                Date dt;
                Double vl;
                String nsu;
                String chave = "019-" + Util.formataNumero(fat, 3, 0, false);

                if (dados.get(chave) != null) {
                    // data
                    try {
                        dt = new SimpleDateFormat("ddMMyyyy").parse(dados.get(chave));
                    } catch (ParseException ex) {
                        dt = pagamento.getData();
                    }
                    // valor
                    vl = Double.valueOf(dados.get(chave.replace("019", "020"))) / 100;
                    // nsu
                    nsu = dados.get(chave.replace("019", "021"));
                } else {
                    // data
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(pagamento.getData());
                    cal.add(Calendar.MONTH, fat);
                    dt = cal.getTime();
                    // valor
                    vl = pagamento.getValor() / faturas;
                    // nsu
                    nsu = pagamento.getNsu();
                }
                // adicionando
                Parcela parcela = new Parcela();
                parcela.setData(dt);
                parcela.setValor(vl);
                parcela.setNsu(nsu);
                parcela.setPagamentos(pagamento);
                parcelas.add(parcela);
            }
        }
        return parcelas;
    }
}
