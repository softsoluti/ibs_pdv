/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.controller;

import br.com.cresceritsolutions.ibspdv.dao.LogaNoSistemaDao;
import br.com.cresceritsolutions.ibspdv.util.IbsException;
import java.awt.Component;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author walsirene
 */
public class ControllerLogaNoSistema {

    private static ControllerLogaNoSistema me;

    public static ControllerLogaNoSistema getInstance() {
        if (me == null) {
            me = new ControllerLogaNoSistema();
        }
        return me;
    }

    public boolean executar(Component c, String usuario, String senha) throws IbsException, NoSuchAlgorithmException {
        if (!usuario.equals("")) {
            if (!senha.equals("")) {
                return LogaNoSistemaDao.getInstance().executar(c, usuario, senha);
            } else {
                throw new IbsException("Digite a senha!");
            }
        } else {
            throw new IbsException("Digite o usuário!");

        }
    }

}
