/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.controller.classes;

import br.com.cresceritsolutions.ibspdv.util.VariaveisDoSistema;

/**
 *
 * @author Gutemberg
 */
public class Class_Nome_Sistema {

    public static String getNomeVersao() {

        return getNome() +" "+ getVersao();

    }

    public static String getNome() {
        return VariaveisDoSistema.getNomeSistema();
    }

    public static String getNomeCompleto() {
        return "<html><center>Integrated Business System - Professional " + getVersao() + " (Sistema Comercial Integrado)</center></html>";
    }

    public static String getNomeLogo() {

        return "<html>" + getNome() + "<br><font font-size=15 color='000000' >Professional </font><font size=5>" + getVersao() + "</font></font></html>\"</html>";

    }

    public static String getVersao() {
        return VariaveisDoSistema.AUXILIAR.getProperty("paf.versao");
    }
}
