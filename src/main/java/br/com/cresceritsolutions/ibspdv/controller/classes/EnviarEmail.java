/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.controller.classes;

import br.com.cresceritsolutions.ibspdv.util.CriptografiaUtil;
import br.com.cresceritsolutions.ibspdv.util.ExibirParaUsuario;
import br.com.cresceritsolutions.ibspdv.util.VariaveisDoSistema;
import br.com.cresceritsolutions.ibspdv.view.core.PrincipalPDV;
import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;

/**
 *
 * @author Walsirene
 */
public class EnviarEmail {

    public static void enviar(String texto) throws EmailException {
        // Caminho do arquivo a ser enviado  
        // File f = new File("xls\\erro.png");

//        EmailAttachment attachment = new EmailAttachment();
//        attachment.setPath(f.getPath()); // Obtem o caminho do arquivo  
//        attachment.setDisposition(EmailAttachment.ATTACHMENT);
//        attachment.setDescription("File");
//        attachment.setName(f.getName()); // Obtem o nome do arquivo  
        // Create the email message  
        MultiPartEmail email = new MultiPartEmail();
        email.setDebug(true);
        email.setHostName("smtp.gmail.com");
        email.setAuthentication("ibsautomatico", CriptografiaUtil.SE());
        email.setSSL(true);
        email.addTo("ggutemberg@gmail.com"); //pode ser qualquer um email  
        email.setFrom("ibsautomatico@gmail.com"); //aqui necessita ser o email que voce fara a autenticacao  
        email.setSubject("Mensagem Usuário(" + VariaveisDoSistema.USUARIO.getLogin() + ") - EMPRESA(" + VariaveisDoSistema.AUXILIAR.getProperty("out.empresa") + ")/CHAVE(" + VariaveisDoSistema.AUXILIAR.getProperty("out.chave") + ")");
        email.setMsg(texto);

        // add the attachment  
//            email.attach(attachment);
        // send the email  
        email.send();
        ExibirParaUsuario.getInstance().mensagem(PrincipalPDV.getInstance(), "Mensagem enviada com sucesso!\nObrigado por nos contatar, trataremos o seu problema o mais rápido possível!", true);

    }

    public static void enviarErro(String texto) throws EmailException {
        takeAPrint();
        // Caminho do arquivo a ser enviado  
        File f = new File("./arquivos/erro.png");
        EmailAttachment attachment = new EmailAttachment();
        attachment.setPath(f.getPath()); // Obtem o caminho do arquivo  
        attachment.setDisposition(EmailAttachment.ATTACHMENT);
        attachment.setDescription("File");
        attachment.setName(f.getName()); // Obtem o nome do arquivo  
        // Create the email message  
        MultiPartEmail email = new MultiPartEmail();
        email.setDebug(true);
        email.setHostName("smtp.gmail.com");
        email.setAuthentication("ibsautomatico", CriptografiaUtil.SE());
        email.setSSL(true);
        email.addTo("ggutemberg@gmail.com"); //pode ser qualquer um email  
        email.setFrom("ibsautomatico@gmail.com"); //aqui necessita ser o email que voce fara a autenticacao  
        email.setSubject("Report automático Usuário(" + VariaveisDoSistema.USUARIO.getLogin() + ") - EMPRESA(" + VariaveisDoSistema.AUXILIAR.getProperty("out.empresa") + ")/CHAVE(" + VariaveisDoSistema.AUXILIAR.getProperty("out.chave") + ")");
        email.setMsg(texto);
        // add the attachment  
        if (f.exists()) {
            email.attach(attachment);
        }
        // send the email  
        email.send();
        f.delete();
    }

    public static void takeAPrint() {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        //Definindo a dimensão que quer capturar
        //pode ser definido o tamanho que desejar
        Dimension screenSize = toolkit.getScreenSize();
        Rectangle screenRect = new Rectangle(screenSize);
        // criando o print screen
        Robot robot = null;
        try {
            try {
                robot = new Robot();
            } catch (AWTException ex) {
                Logger.getLogger(EnviarEmail.class.getName()).log(Level.SEVERE, null, ex);
            }
            BufferedImage screenCapturedImage = robot.createScreenCapture(screenRect);
            //depois disso é só procurar a imagem no local indicado abaixo, no meu caso em:
            // /Users/rodrigogomes/printScreen.png
            //Aqui você pode alterar o formato da imagem para, por exemplo, JPG
            //É só mudar o ?png? para ?jpg? e pronto
            ImageIO.write(screenCapturedImage, "png", new File("./arquivos/erro.png"));
        } catch (IOException ex) {
            Logger.getLogger(EnviarEmail.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
