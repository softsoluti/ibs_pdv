//Desenvolvedor : Gutemberg Lucas Vieira Lima
//Celular : (62)9112-0642
//Telefone : (62)3701-1466
//Email : ggutemberg@gmail.com/g_uto_lima@hotmail.com
package br.com.cresceritsolutions.ibspdv.controller.classes;

import javax.swing.SwingWorker;

public class ExecutaBarraProgresso extends SwingWorker<Void, Void> {

    public Void doInBackground() {

        int progress = 0;
        setProgress(0);
        for (int i = 0; i < 100; i++) {
            progress++;
            setProgress(progress);
        }
        return null;
    }

    @Override
    public void done() {
    }
}
