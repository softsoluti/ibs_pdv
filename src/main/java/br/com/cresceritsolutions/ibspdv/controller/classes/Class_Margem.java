/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.controller.classes;

public class Class_Margem {

    public static float getMargemMercantil(float Valor) {
        if (Valor <= 149.0F) {
            return 25.0F;
        }
        if (Valor <= 249.0F) {
            return 40.0F;
        }
        if (Valor <= 449.0F) {
            return 65.0F;
        }
        if (Valor <= 649.0F) {
            return 110.0F;
        }
        if (Valor <= 849.0F) {
            return 155.0F;
        }
        if (Valor <= 1099.0F) {
            return 200.0F;
        }
        if (Valor <= 1299.0F) {
            return 255.0F;
        }
        return 300.0F;
    }
}