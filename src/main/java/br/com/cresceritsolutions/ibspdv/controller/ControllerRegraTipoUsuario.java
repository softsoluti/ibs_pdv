/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.controller;

import br.com.cresceritsolutions.ibspdv.models.hb.RegraTipoUsuario;
import br.com.cresceritsolutions.ibspdv.models.hb.TipoUsuario;
import br.com.cresceritsolutions.ibspdv.util.ExibirParaUsuario;
import br.com.cresceritsolutions.ibspdv.util.HibernateUtil;
import java.util.Iterator;
import java.util.List;
import org.hibernate.HibernateException;

/**
 *
 * @author gutemberg
 */
public class ControllerRegraTipoUsuario {

    private static ControllerRegraTipoUsuario me;
    private static List<RegraTipoUsuario> regraTipoUsuario;

    public static ControllerRegraTipoUsuario getInstance() {
        if (me == null) {
            me = new ControllerRegraTipoUsuario();
        }
        return me;
    }

    //contrutor
    private ControllerRegraTipoUsuario() {

    }

    //carrega lista inteira de regras
    public void carregaTbComponenteVisivel() {
        HibernateUtil hu = HibernateUtil.getInstance();
        try {
            regraTipoUsuario = hu.getSession().createQuery("FROM RegraTipoUsuario").list();
        }catch(HibernateException ex){
            ExibirParaUsuario.getInstance().erro(null, ex, "Erro ao carregar perfis de usuário!", true);
        }
    }

    //verifica se o componente na lista salva pode ser visivel
    public boolean getVisivel(TipoUsuario tipoUsuario, String nomeComponente) {
        Iterator<RegraTipoUsuario> o = regraTipoUsuario.iterator();
        while (o.hasNext()) {
            RegraTipoUsuario r = o.next();
            if (tipoUsuario!=null && tipoUsuario.getNome().equals(r.getTipoUsuario().getNome())
                    && nomeComponente != null) {
                if (nomeComponente.equals(r.getComponente())) {
                    return r.isVisivel();
                }
            }
        }
        return true;
    }

}
