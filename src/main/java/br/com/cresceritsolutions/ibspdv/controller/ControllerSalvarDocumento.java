package br.com.cresceritsolutions.ibspdv.controller;

import acbr.acbrmonitor.AcbrMonitor;
import acbr.acbrmonitor.EComandoECF;
import br.com.cresceritsolutions.ibspdv.models.ecf.EcfDocumento;
import br.com.cresceritsolutions.ibspdv.util.ExibirParaUsuario;
import br.com.cresceritsolutions.ibspdv.util.IbsException;
import br.com.cresceritsolutions.ibspdv.util.VariaveisDoSistema;
import br.com.cresceritsolutions.ibspdv.view.core.PrincipalPDV;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;




/**
 * Classe responsavel por salvar os documentos
 *
 * @author Pedro H. Lira
 */
public class ControllerSalvarDocumento implements IComando {

    private String tipo;
    private String nPedido = "0";
    public static int ID_DOCUMENTO_SALVO;

    /**
     * Costrutor padrao passando o tipo de documento a ser salvo.
     *
     * @param tipo o tipo de documento [CM, RV, CC, CN, NC, RG]
     * @param jTextField
     */
    public ControllerSalvarDocumento(String tipo, String nPedido) {
        this.tipo = tipo;
        this.nPedido = nPedido;
    }

    @Override
    public void executar() throws IbsException {
        // seta os dados do documento.
        EcfDocumento doc = new EcfDocumento();
        doc.setEcfImpressora(VariaveisDoSistema.getImpressora());
        doc.setEcfDocumentoUsuario(VariaveisDoSistema.USUARIO.getId());
        String[] resp = AcbrMonitor.enviar(EComandoECF.ECF_NumCupom);
        doc.setEcfDocumentoCoo(Integer.valueOf(resp[1]));
        if (!tipo.equals("RV")) {//Se não for Relatório válido
            resp = AcbrMonitor.enviar(EComandoECF.ECF_NumGNF);//Geral de operações não fiscais
            doc.setEcfDocumentoGnf(Integer.valueOf(resp[1]));
        }

        if (tipo.equals("RG")) {//Relatório Geral
            resp = AcbrMonitor.enviar(EComandoECF.ECF_NumGRG);//Geral relatório Gerencial
            doc.setEcfDocumentoGrg(Integer.valueOf(resp[1]));
        } else if (tipo.equals("CC")) {//Comprovante cartão
            resp = AcbrMonitor.enviar(EComandoECF.ECF_NumCDC);//Comprovante crédito ou débito
            doc.setEcfDocumentoCdc(Integer.valueOf(resp[1]));
        }

        doc.setEcfDocumentoTipo(tipo);
        resp = AcbrMonitor.enviar(EComandoECF.ECF_DataHora);
        Date data;
        try {
            data = new SimpleDateFormat("dd/MM/yy HH:mm:ss").parse(resp[1]);
        } catch (ParseException ex) {
            data = new Date();
        }
        doc.setEcfDocumentoData(data);
        // salva
        try {
            ID_DOCUMENTO_SALVO = EcfDocumento.salvar(doc, nPedido);
        } catch (SQLException e) {
            ExibirParaUsuario.getInstance().erro(PrincipalPDV.getInstance(), e, "Erro ao salvar o domento do ecf!", false);
        }
    }

    @Override
    public void desfazer() throws IbsException {
        // comando nao aplicavel.
    }
}
