/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.controller;

import br.com.cresceritsolutions.ibspdv.util.Util;
import br.com.cresceritsolutions.ibspdv.util.VariaveisDoSistema;
import br.com.cresceritsolutions.ibspdv.view.core.Aguarde;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.html.HtmlDivision;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlFileInput;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlOption;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSelect;
import com.gargoylesoftware.htmlunit.html.HtmlTextArea;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
import com.gargoylesoftware.htmlunit.util.NameValuePair;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.OfficeXmlFileException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Gutemberg
 */
public class ControllerRoboIW {

    String login;
    String senha;
    public String somaTotal = "";
    public boolean isOK = false;
    private String PROTOCOLO;
    private String ANEXO;
    private String ASSUNTO;
    private String CORPO;
    private String DESTINATARIO;

    public ControllerRoboIW() {
        isOK = carregaCredenciais();
    }

    private boolean carregaCredenciais() {
                login = VariaveisDoSistema.EMPRESA.getFranquia().getIwClaro().getUsuario();
                senha = VariaveisDoSistema.EMPRESA.getFranquia().getIwClaro().getSenha();
        return login != null && !login.equals("");
    }

    public int anexar(String tipo, String vmes, String ano, String cat, int quantLin, String arquivo, String protocoloAnterior,String valorReceber) {
        int retorno = 0;
        if (isOK) {
            String descricaoDif = tipo + " " + vmes.toUpperCase() + " " + cat + " " + ano;
            String codMobile = login.substring(1, login.length());
            String quantidadelinhas = String.valueOf(quantLin);
            int mes = Util.getIdMes(vmes); // numero do m?s
            String irregularidade = "2"; //sempre 2
            String recontestacao = "1";//1=sim, 2 = n?o
            if (tipo.contains("DIFERIDO")) {
                recontestacao = "2";
            }
            int tpDif = 0;//1=com,2=serv,3=reb
            if (descricaoDif.contains("ATIVACAO")) {
                tpDif = 1;
            }
            if (descricaoDif.contains("REBATE")) {
                tpDif = 2;
            }
            if (descricaoDif.contains("SERVICOS")) {
                tpDif = 3;
            }
            int fila = 0;//60=comissao,66=rebate,61=servicos
            int idTipoAnexo = 0;
            int idForm = 0;
            switch (tpDif) {
                case 1://ativacao
                    fila = 60;
                    idTipoAnexo = 131;
                    idForm = 230;
                    break;
                case 2://rebate
                    fila = 66;
                    idTipoAnexo = 134;
                    idForm = 228;
                    break;
                case 3://servicos
                    fila = 61;
                    idTipoAnexo = 135;
                    idForm = 229;
                    break;
            }
            try {
                WebClient webClient = null;
                if (!Aguarde.getInstance().isCancelado()) {
                    webClient = new WebClient(BrowserVersion.INTERNET_EXPLORER_9);
                    //webClient.setThrowExceptionOnScriptError(false);
                    WebRequest requestSettings = new WebRequest(new URL("https://iw.claro.com.br/v2/login/autenticar"), HttpMethod.POST);
                    List<NameValuePair> nameValuePairs = new ArrayList<>();
                    nameValuePairs.add(new NameValuePair("login", login));
                    nameValuePairs.add(new NameValuePair("senha", senha));
                    requestSettings.setRequestParameters(nameValuePairs);
                    if (!Aguarde.getInstance().isCancelado()) {
                        Aguarde.getInstance().getBarraProgresso().setString("Autenticando...");
                        HtmlPage currentPage = webClient.getPage(requestSettings);
                        System.out.println(currentPage.asText());
                        currentPage = webClient.getPage("https://iw.claro.com.br/v2/wkf/chamado/demanda/index");
                        HtmlElement link = (HtmlElement) currentPage.getByXPath("//td[@onclick='WKF_SelecionaFila(" + fila + ")']").get(0);
                        if (!Aguarde.getInstance().isCancelado()) {
                            Aguarde.getInstance().getBarraProgresso().setString("Selecionando fila de " + cat + "...");
                            currentPage = link.click();//click
                            Thread.sleep(1000);//Tempo para retorno do click a aba anexo
                            //Primeira Pagina
                            if (!Aguarde.getInstance().isCancelado()) {
                                Aguarde.getInstance().getBarraProgresso().setString("Preenchendo 1? formul?rio...");
                                Aguarde.getInstance().getBarraProgresso().setValue(20);
                                HtmlTextInput demanda = (HtmlTextInput) currentPage.getElementById("NOM_TAREFA");//Campo demanda
                                HtmlTextArea descricao = (HtmlTextArea) currentPage.getElementById("DSC_TAREFA");//Campo descric?o
                                demanda.setText(descricaoDif);
                                descricao.setText(descricaoDif);
                                //Segunda pagina
                                if (!Aguarde.getInstance().isCancelado()) {
                                    Aguarde.getInstance().getBarraProgresso().setString("Preenchendo 2? formul?rio e anexando arquivo...");
                                    Aguarde.getInstance().getBarraProgresso().setValue(40);
                                    HtmlPage IFRAME_ANEXOS = (HtmlPage) currentPage.getFrameByName("iframeAnexoUpload").getEnclosedPage();
                                    HtmlSelect selectTipo = (HtmlSelect) IFRAME_ANEXOS.getElementById("COD_TIPO_ANEXO");
                                    HtmlOption opcoesTipo = selectTipo.getOptionByValue(String.valueOf(idTipoAnexo));
                                    HtmlTextInput nomeArquivo = (HtmlTextInput) IFRAME_ANEXOS.getElementById("NOM_ANEXO");
                                    HtmlTextArea descricaoArquivo = (HtmlTextArea) IFRAME_ANEXOS.getElementById("DSC_ANEXO");
                                    HtmlFileInput anexaArquivo = (HtmlFileInput) IFRAME_ANEXOS.getElementById("ANEXO");
                                    HtmlInput anexar = (HtmlInput) IFRAME_ANEXOS.getElementById("submit");
                                    //SETA
                                    selectTipo.setSelectedAttribute(opcoesTipo, true);
                                    nomeArquivo.setText(selectTipo.asText());//Seta texto com a sele??o do combobox tipo
                                    descricaoArquivo.setText(descricaoDif);
                                    anexaArquivo.setAttribute("value", arquivo);
                                    anexar.click();
                                    Thread.sleep(1000);
                                    //TERCEIRA PAGINA
                                    if (!Aguarde.getInstance().isCancelado()) {
                                        Aguarde.getInstance().getBarraProgresso().setString("Preenchendo 3? formul?rio...");
                                        Aguarde.getInstance().getBarraProgresso().setValue(60);
                                        HtmlDivision division = (HtmlDivision) currentPage.getElementById("divFormulario_" + idForm);
                                        HtmlPage formulario = division.click();
                                        Thread.sleep(1000);
                                        //PREENCHIMENTO DO FORMULARIO
                                        HtmlTextInput txtMobile = (HtmlTextInput) formulario.getElementById("NUM_COD_MOBILE_1");
                                        HtmlTextInput nApDiv = (HtmlTextInput) formulario.getElementById("NUM_APURACAO_DIVERGENTE_1");
                                        HtmlSelect selectMes = (HtmlSelect) formulario.getElementById("COD_MES_CONFERENCIA_1");
                                        HtmlSelect selectTpContestacao = (HtmlSelect) formulario.getElementById("COD_TIPO_CONTESTACAO_1");
                                        HtmlSelect selectRecontestacao = (HtmlSelect) formulario.getElementById("IND_RECONTESTACAO_1");
                                        HtmlTextInput txtProtocoloAnt = (HtmlTextInput) formulario.getElementById("NUM_CHAMADO_1");
                                        HtmlTextInput txtValorAreceber = (HtmlTextInput) formulario.getElementById("VLR_RECEBER_1");
                                        HtmlElement btnEnviar = (HtmlElement) formulario.getElementById("btn_form_enviar");
                                        txtMobile.setText(codMobile);
                                        nApDiv.setText(quantidadelinhas);
                                        if (recontestacao.equals("1")) {
                                            txtProtocoloAnt.setText(protocoloAnterior);
                                        }
                                        txtValorAreceber.setText(valorReceber);
                                        HtmlOption opcoesMes = selectMes.getOptionByValue(String.valueOf(mes));
                                        HtmlOption opcoesTpContestacao = selectTpContestacao.getOptionByValue(irregularidade);
                                        HtmlOption opcoesRecontestacao = selectRecontestacao.getOptionByValue(recontestacao);

                                        selectMes.setSelectedAttribute(opcoesMes, true);
                                        selectTpContestacao.setSelectedAttribute(opcoesTpContestacao, true);
                                        selectRecontestacao.setSelectedAttribute(opcoesRecontestacao, true);
                                        if (!Aguarde.getInstance().isCancelado()) {
                                            Aguarde.getInstance().getBarraProgresso().setString("Salvando formul?rio...");
                                            Aguarde.getInstance().getBarraProgresso().setValue(80);
                                            btnEnviar.click();//Salva formulario
                                            Thread.sleep(1000);
                                            if (!Aguarde.getInstance().isCancelado()) {
                                                Aguarde.getInstance().getBarraProgresso().setString("Gerando protocolo, aguarde por favor...");
                                                Aguarde.getInstance().getBarraProgresso().setValue(90);
                                                HtmlElement btnConcluir = (HtmlElement) currentPage.getElementById("btn_concluir");
                                                currentPage = btnConcluir.click();//Conclui
                                                Thread.sleep(1000);
                                                HtmlElement protocolo = (HtmlElement) currentPage.getElementById("ID_TAREFA");
                                                retorno = Integer.parseInt(Util.getRetornaApenasNumero(protocolo.getTextContent()));
                                                webClient.closeAllWindows();//FECHA TODAS AS PAGINAS
                                                Aguarde.getInstance().getBarraProgresso().setValue(100);
                                            } else {
                                                webClient.closeAllWindows();//FECHA TODAS AS PAGINAS
                                                JOptionPane.showMessageDialog(null, "Processo cancelado!", "Mensagem", JOptionPane.INFORMATION_MESSAGE);
                                            }
                                        } else {
                                            webClient.closeAllWindows();//FECHA TODAS AS PAGINAS
                                            JOptionPane.showMessageDialog(null, "Processo cancelado!", "Mensagem", JOptionPane.INFORMATION_MESSAGE);
                                        }
                                    } else {
                                        webClient.closeAllWindows();//FECHA TODAS AS PAGINAS
                                        JOptionPane.showMessageDialog(null, "Processo cancelado!", "Mensagem", JOptionPane.INFORMATION_MESSAGE);
                                    }
                                } else {
                                    webClient.closeAllWindows();//FECHA TODAS AS PAGINAS
                                    JOptionPane.showMessageDialog(null, "Processo cancelado!", "Mensagem", JOptionPane.INFORMATION_MESSAGE);
                                }
                            } else {
                                webClient.closeAllWindows();//FECHA TODAS AS PAGINAS
                                JOptionPane.showMessageDialog(null, "Processo cancelado!", "Mensagem", JOptionPane.INFORMATION_MESSAGE);
                            }
                        } else {
                            webClient.closeAllWindows();//FECHA TODAS AS PAGINAS
                            JOptionPane.showMessageDialog(null, "Processo cancelado!", "Mensagem", JOptionPane.INFORMATION_MESSAGE);
                        }
                    } else {
                        webClient.closeAllWindows();//FECHA TODAS AS PAGINAS
                        JOptionPane.showMessageDialog(null, "Processo cancelado!", "Mensagem", JOptionPane.INFORMATION_MESSAGE);
                    }
                } else {
                    webClient.closeAllWindows();//FECHA TODAS AS PAGINAS
                    JOptionPane.showMessageDialog(null, "Processo cancelado!", "Mensagem", JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (InterruptedException | IOException | FailingHttpStatusCodeException ex) {
                Logger.getLogger(ControllerRoboIW.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

    private void salvar(HtmlPage currentPage, String caminho) {
        try {
            File f = new File(caminho);
            f.delete();
            currentPage.save(f);
        } catch (IOException ex) {
            Logger.getLogger(ControllerRoboIW.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean acessar(String login, String senha, String protocolo, String salvarEm, int tempo, int tipo) {
        try {
            WebClient webClient = new WebClient(BrowserVersion.FIREFOX_17);
            //webClient.setThrowExceptionOnScriptError(false);
            WebRequest requestSettings = new WebRequest(new URL("https://iw.claro.com.br/v2/login/autenticar"), HttpMethod.POST);
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new NameValuePair("login", login));
            nameValuePairs.add(new NameValuePair("senha", senha));
            requestSettings.setRequestParameters(nameValuePairs);
            HtmlPage currentPage = webClient.getPage(requestSettings);
            currentPage = webClient.getPage("https://iw.claro.com.br/v2/wkw/tarefa/consulta/detalhe?COD_PASTA=3&COD_TAREFA=" + protocolo);
            HtmlDivision div = (HtmlDivision) currentPage.getByXPath("//div[@aba='aba_anexos']").get(0);
            currentPage = div.click();
            Thread.sleep(tempo);//Tempo para retorno do click a aba anexo
            HtmlElement arquivoDownload = (HtmlElement) currentPage.getByXPath("//td[@class='GRID_TD_R']").get(2);
            //File f = new File("C://teste.html");
            //f.delete();
            //currentPage.save(f);
            if (arquivoDownload.asText().contains(protocolo)) {
                salvaArquivo(arquivoDownload.click().getWebResponse().getContentAsStream(), salvarEm, true, tipo);
                return true;
            }
        } catch (InterruptedException | IOException | FailingHttpStatusCodeException ex) {
            Logger.getLogger(ControllerRoboIW.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    private boolean salvaArquivo(InputStream is, String salvarEm, boolean descompactar, int tipo) {
        try {
            File f = new File(salvarEm);
            OutputStream out = new FileOutputStream(f);
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = is.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            is.close();
            out.flush();
            out.close();
            File f1 = new File(salvarEm.replace(".zip", ""));
            f1.mkdir();
            if (descompactar) {
                ControllerZip.descompactar(salvarEm, f1.getPath(), descompactar);
            }
            int colunaNtc = 0;
            int colunaValor = 0;
            for (int i = 0; i < f1.listFiles().length; i++) {
                switch (tipo) {
                    case 1://ativacao
                        colunaNtc = 4;
                        colunaValor = 8;
                        converterXLS(f1.listFiles()[i].getPath(), colunaNtc, colunaValor, f1 + "/" + f1.getName() + ".xls", true, tipo, true);
                        break;
                    case 2://rebate
                        colunaNtc = 3;
                        colunaValor = 44;
                        converterXLS(f1.listFiles()[i].getPath(), colunaNtc, colunaValor, f1 + "/" + f1.getName() + ".xls", true, tipo, false);
                        break;
                    case 3://servicos
                        colunaNtc = 4;
                        colunaValor = 9;
                        converterXLS(f1.listFiles()[i].getPath(), colunaNtc, colunaValor, f1 + "/" + f1.getName() + ".xls", true, tipo, true);
                }
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public float converterXLS(String file, int colunantc, int colunaSoma, String salvarEm, boolean RmOrigen, int tipo, boolean criaColunaValidoComissao) {
        Workbook wb = null;
        FileInputStream origem = null;
        try {
            origem = new FileInputStream(file);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ControllerRoboIW.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            wb = new HSSFWorkbook(origem);
        } catch (OfficeXmlFileException | IOException ex) {
            try {
                wb = new XSSFWorkbook(new FileInputStream(file));
            } catch (IOException ex1) {
                Logger.getLogger(ControllerRoboIW.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        HSSFWorkbook nwb = new HSSFWorkbook();
        Float soma = 0f;
        boolean possuiValor = false;
        
        
        
        for (int s = 0; s < wb.getNumberOfSheets(); s++) {//Sheet
            nwb.createSheet();//cria sheet
            for (int r = 0; r < wb.getSheetAt(s).getLastRowNum() + 1; r++) {
                nwb.getSheetAt(s).createRow(r);//cria linha
                try {
                    cel:
                    for (int c = 0; c < wb.getSheetAt(s).getRow(r).getLastCellNum(); c++) {
                        if (wb.getSheetAt(s).getRow(r) != null) {
                            nwb.getSheetAt(s).getRow(r).createCell(c);//cria celula
                            Cell celula = wb.getSheetAt(s).getRow(r).getCell(c);
                            String saida;
                            saida = Util.getCelulaValida(celula);
                            if (tipo == 2 && (saida == null || saida.equals(""))) {
                                saida = "0";
                            }
                            if (c == colunantc) {
                                if (!saida.equals("") && saida != null) {
                                    nwb.getSheetAt(s).getRow(r).getCell(c).setCellValue(saida);//set celula
                                    if (r > 0 && c == colunaSoma) {
                                        soma += Float.parseFloat(saida);
                                        if (Float.parseFloat(saida) != 0) {
                                            possuiValor = true;
                                        }
                                    }
                                } else {
                                    nwb.getSheetAt(s).removeRow(nwb.getSheetAt(s).getRow(r));
                                    break cel;
                                }
                            } else {
                                nwb.getSheetAt(s).getRow(r).getCell(c).setCellValue(saida);//set celula
                                if (r > 0 && c == colunaSoma) {
                                    soma += Float.parseFloat(saida);
                                    if (Float.parseFloat(saida) != 0) {
                                        possuiValor = true;
                                    }
                                }
                            }
                        }
                        if (criaColunaValidoComissao && c == wb.getSheetAt(s).getRow(r).getLastCellNum() - 1) {
                            if (nwb.getSheetAt(s).getRow(r).getCell(c).getRowIndex() == 0) {
                                nwb.getSheetAt(s).getRow(r).createCell(c + 1);
                                nwb.getSheetAt(s).getRow(r).getCell(c + 1).setCellValue("VALIDO COMISSAO");
                            } else {
                                if (possuiValor) {
                                    nwb.getSheetAt(s).getRow(r).createCell(c + 1);
                                    nwb.getSheetAt(s).getRow(r).getCell(c + 1).setCellValue("t");
                                } else {
                                    nwb.getSheetAt(s).getRow(r).createCell(c + 1);
                                    nwb.getSheetAt(s).getRow(r).getCell(c + 1).setCellValue("f");
                                }
                                possuiValor = false;
                            }
                        }
                    }
                } catch (NullPointerException ex) {
                    //ex.printStackTrace();
                }
            }
        }
        try {
            origem.close();
        } catch (IOException ex) {
            Logger.getLogger(ControllerRoboIW.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (RmOrigen) {
            ControllerZip.deleteDir(new File(file));
        }
        String filename = salvarEm;
        try {
            FileOutputStream out = new FileOutputStream(filename);
            nwb.write(out);
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(ControllerRoboIW.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.somaTotal = String.valueOf(soma);
        return soma;
    }

    public void executaDownload(String DESTINATARIO) {
        this.DESTINATARIO = DESTINATARIO;
        //p.setCredentials("pdv", loginIw.toUpperCase());
        String sql = " SELECT RETORNA_DATA_PT(envio_diferido.DATA_ENVIO) AS DATA,"
                + "CONCAT('RESPOSTA ', controle_analise_diferido.DESCRICAO,' ',envio_diferido.DESCRICAO,' ',RETORNA_DESCRICAO_MES(controle_analise_diferido.MES),'/',ANO) AS DESCRICAO,"
                + " IF(TO_DAYS(SYSDATE())-TO_DAYS(envio_diferido.DATA_ENVIO)>(SELECT VALOR FROM configuracao_sistema WHERE PROPRIEDADE='PRAZO_RESPOSTA_OPERADORA'),'SIM','N?O') AS RESPOSTA,"
                + "envio_diferido.N_PROTOCOLO AS PROTOCOLO,"
                + "envio_diferido.VALOR,"
                + "envio_diferido.N_LINHAS "
                + "FROM  controle_analise_diferido INNER JOIN envio_diferido ON envio_diferido.ID_CONTROLE_DIFERIDO= controle_analise_diferido.ID  WHERE  CONCAT('RESPOSTA ', controle_analise_diferido.DESCRICAO,' ',envio_diferido.DESCRICAO,' ',controle_analise_diferido.MES,'/',ANO) NOT IN(SELECT CONCAT(tipos_planilhas_xls.DESCRICAO,' ',controle_planilha_xls.MES,'/',controle_planilha_xls.ANO) AS DESCRICAO FROM controle_planilha_xls  INNER JOIN tipos_planilhas_xls ON tipos_planilhas_xls.ID = controle_planilha_xls.ID_TIPO  WHERE tipos_planilhas_xls.DESCRICAO LIKE '%RESPOSTA%') AND controle_analise_diferido.ID_ENTERPRISE='" + VariaveisDoSistema.USUARIO.getLogin() + "'";
        /*Class_Conexao_mysql cc = new Class_Conexao_mysql();
        cc.set_Conexao_Ibs_Base();
        cc.Conectar();
        try {
            cc.executaSqlSelect("SELECT ID,USUARIOIW,SENHAIW FROM empresa WHERE ID='" + Class_Usuario_Logado.getIdEmpresa() + "' ORDER BY USUARIOIW");

            String loginIw = "";
            String senhaIw = "";
            if (cc.getRs().first()) {
                loginIw = cc.getRs().getString("USUARIOIW");
                senhaIw = cc.getRs().getString("SENHAIW");
            }
            cc.executaSqlSelect(sql);
            String conteudoBd;
            while (cc.getRs().next()) {
                PROTOCOLO = cc.getRs().getString("PROTOCOLO");
                String protocolo = cc.getRs().getString("PROTOCOLO");
                String salvarEm = "./xls/respostasIw/" + loginIw.toUpperCase() + "-" + cc.getRs().getString("DESCRICAO").replace("/", "-") + " " + protocolo + ".zip";
                ANEXO = salvarEm.replace(".zip", "");
                int tempoCarregaLinkAnexo = 10000;
                boolean arquivoBaixado = false;
                if (cc.getRs().getString("DESCRICAO").contains("ATIVACAO")) {
                    arquivoBaixado = acessar(loginIw, senhaIw, protocolo, salvarEm, tempoCarregaLinkAnexo, 1);
                }
                if (cc.getRs().getString("DESCRICAO").contains("REBATE")) {
                    arquivoBaixado = acessar(loginIw, senhaIw, protocolo, salvarEm, tempoCarregaLinkAnexo, 2);
                }
                if (cc.getRs().getString("DESCRICAO").contains("SERVICOS")) {
                    arquivoBaixado = acessar(loginIw, senhaIw, protocolo, salvarEm, tempoCarregaLinkAnexo, 3);
                }
                conteudoBd = "DATA ABERTURA : " + cc.getRs().getString("DATA") + "\r\nDESCRICAO : " + cc.getRs().getString("DESCRICAO") + "\r\nPROTOCOLO : " + cc.getRs().getString("PROTOCOLO") + "\r\nVALOR CONTESTADO : R$ " + cc.getRs().getString("VALOR").replace(".", ",") + "\r\nN? CONTESTACOES : " + cc.getRs().getString("N_LINHAS") + "\r\nCONCORDOU EM PAGAR : " + " R$ " + somaTotal.replace(".", ",");
                ASSUNTO = "IBS: IW CLARO " + loginIw.toUpperCase() + " P:" + PROTOCOLO + "" + " R$ " + somaTotal.replace(".", ",");
                CORPO = conteudoBd;
                java.awt.Toolkit.getDefaultToolkit().beep();
                if (arquivoBaixado) {
                    enviar();
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ControllerRoboIW.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            cc.Desconectar();
        }*/
    }

    public void enviar() {
        //takeAPrint();
        // Caminho do arquivo a ser enviado  
        //ArquivoPreferencias ap = new ArquivoPreferencias();
        ControllerZip.compactar(ANEXO, ANEXO.replace(new File(ANEXO).getName(), ""), true);
        File f = new File(ANEXO + ".zip");
        EmailAttachment attachment = new EmailAttachment();
        attachment.setPath(f.getPath()); // Obtem o caminho do arquivo  
        attachment.setDisposition(EmailAttachment.ATTACHMENT);
        attachment.setDescription("File");
        attachment.setName(f.getName()); // Obtem o nome do arquivo  
        try {
            // Create the email message  
            MultiPartEmail email = new MultiPartEmail();
            email.setDebug(true);
           /* if (ap.getVariavel("E-SMTP") != null && !ap.getVariavel("E-SMTP").equals("")) {
                email.setHostName(ap.getVariavel("E-SMTP"));
                email.setAuthentication(ap.getVariavel("E-CONTA"), ap.getVariavel("E-SENHA"));
                email.setSSL(true);
                email.setFrom(ap.getVariavel("E-MAIL")); //aqui necessita ser o email que voce fara a autenticacao  
            } else {
                email.setHostName("smtp.gmail.com");
                email.setAuthentication("ibsautomatico", Class_Criptografia.SE());
                email.setSSL(true);
                email.setFrom("ibsautomatico@gmail.com"); //aqui necessita ser o email que voce fara a autenticacao  
            }*/
            for (String s : DESTINATARIO.split(";")) {
                email.addTo(s); //pode ser qualquer um email  
            }
            email.setSubject(ASSUNTO);
            email.setMsg(CORPO);
            // add the attachment  
            email.attach(attachment);
            // send the email  
            email.send();
            ControllerZip.deleteDir(f);
        } catch (EmailException ex) {
            ex.printStackTrace();
        }
    }
}
