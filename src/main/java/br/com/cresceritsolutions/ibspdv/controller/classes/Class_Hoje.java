/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.controller.classes;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Gutemberg
 */
public class Class_Hoje {
    public static String Hoje() {
        Date data = new Date();
        SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
        return formatador.format(data);
    }
}
