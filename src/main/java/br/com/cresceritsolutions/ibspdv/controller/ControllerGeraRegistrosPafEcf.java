/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.controller;

import acbr.ACBrEAD;
import static br.com.cresceritsolutions.ibspdv.controller.PAF.getPathArquivos;
import br.com.cresceritsolutions.ibspdv.models.anexo1.v02_02.iv.A2;
import br.com.cresceritsolutions.ibspdv.models.anexo1.v02_02.iv.AnexoIV;
import br.com.cresceritsolutions.ibspdv.models.anexo1.v02_02.iv.E2;
import br.com.cresceritsolutions.ibspdv.models.anexo1.v02_02.iv.E3;
import br.com.cresceritsolutions.ibspdv.models.anexo1.v02_02.iv.P2;
import br.com.cresceritsolutions.ibspdv.models.anexo1.v02_02.iv.R01;
import br.com.cresceritsolutions.ibspdv.models.anexo1.v02_02.iv.R02;
import br.com.cresceritsolutions.ibspdv.models.anexo1.v02_02.iv.R03;
import br.com.cresceritsolutions.ibspdv.models.anexo1.v02_02.iv.R04;
import br.com.cresceritsolutions.ibspdv.models.anexo1.v02_02.iv.R05;
import br.com.cresceritsolutions.ibspdv.models.anexo1.v02_02.iv.U1;
import br.com.cresceritsolutions.ibspdv.models.enuns.SituacaoVenda;
import br.com.cresceritsolutions.ibspdv.models.hb.Ecf;
import br.com.cresceritsolutions.ibspdv.models.hb.ItensPrecoFranquia;
import br.com.cresceritsolutions.ibspdv.models.hb.Pagamentos;
import br.com.cresceritsolutions.ibspdv.models.hb.Produto;
import br.com.cresceritsolutions.ibspdv.models.hb.ReducaoZ;
import br.com.cresceritsolutions.ibspdv.models.hb.ReducaoZTotais;
import br.com.cresceritsolutions.ibspdv.models.hb.Venda;
import br.com.cresceritsolutions.ibspdv.util.HibernateUtil;
import br.com.cresceritsolutions.ibspdv.util.IbsException;
import br.com.cresceritsolutions.ibspdv.util.Util;
import br.com.cresceritsolutions.ibspdv.util.VariaveisDoSistema;
import static br.com.cresceritsolutions.ibspdv.util.VariaveisDoSistema.AUXILIAR;
import jACBrFramework.ACBrException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.beanio.BeanIOConfigurationException;
import org.beanio.BeanWriter;
import org.beanio.StreamFactory;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Gutem
 */
public class ControllerGeraRegistrosPafEcf {

    private static ControllerGeraRegistrosPafEcf me;

    public static ControllerGeraRegistrosPafEcf getInstance() {
        if (me == null) {
            me = new ControllerGeraRegistrosPafEcf();
        }
        return me;
    }

    /**
     * Metodo que emite o Registros do PAF-ECF.
     *
     * @param dataFinal
     * @param dataInicial
     * @param pesquisa
     * @param tp
     * @return
     * @exception Exception dispara uma excecao caso nao consiga.
     */
    public String gerar(int tp, String pesquisa, String dataInicial, String dataFinal) throws IbsException {
        //CARREGA ANEXOIV
        //U1
        //IDENTIFICAÇÃO  DO  ESTABELECIMENTO  USUÁRIO  DO  PAFECF
        String retorno = "";
        U1 u1 = new U1();
        u1.setCnpj(AUXILIAR.getProperty("cli.cnpj"));
        u1.setIe(AUXILIAR.getProperty("cli.ie"));
        u1.setIm(AUXILIAR.getProperty("cli.im"));
        u1.setRazao(AUXILIAR.getProperty("cli.razao"));
        AnexoIV anexoIV = new AnexoIV();

        anexoIV.setU1(u1);
        anexoIV.setListaA2(getListaA2(dataInicial, dataFinal));//OK
        anexoIV.setListaP2(getListaP2(dataInicial, dataFinal, tp, pesquisa));//OK
        anexoIV.setListaE2(getListaE2(dataInicial, dataFinal, tp, pesquisa));//OK
        anexoIV.setE3(getE3(dataInicial, dataFinal, pesquisa));//OK
        anexoIV.setR01(getR01(dataInicial, dataFinal, tp, pesquisa));//OK

        anexoIV.setListaR02(getListaR02(dataInicial, dataFinal, tp, pesquisa));
        anexoIV.setListaR03(getListaR03(dataInicial, dataFinal, tp, pesquisa));
        anexoIV.setListaR04(getListaR04(dataInicial, dataFinal, tp, pesquisa));
       // anexoIV.setListaR05(getListaR05(dataInicial, dataFinal, tp, pesquisa));
        //anexoIV.setListaR06(getListaR06(dataInicial, dataFinal, tp, pesquisa));
        //anexoIV.setListaR07(getListaR07(dataInicial, dataFinal, tp, pesquisa));
        retorno = gerarRegistrosPafEcf(anexoIV);
        return retorno;
    }

    //Informações sobre as formas de pagamento
    private static List<A2> getListaA2(String dataInicial, String dataFinal) {
        //Formas de pagamento
        HibernateUtil hu = HibernateUtil.getInstance();
        List<A2> a2s = new ArrayList<>();
        Iterator<Pagamentos> pagamentos = hu.getSession()
                .createQuery("select distinct pagamento from Pagamentos pagamento "
                        + "left join venda venda "
                        + "left join movimento movimento "
                        + "where caixa.dataAbertura between '" + dataInicial + " 00:00:00' and '" + dataFinal + " 23:59:59' group by pagamento.data, pagamento.formas.descricao,pagamento.tipoDeDocumento.codigo")
                .list().iterator();

        while (pagamentos.hasNext()) {
            Pagamentos pagamento = pagamentos.next();
            A2 temp = new A2();
            temp.setData(pagamento.getData());//DATA
            temp.setMeioPagamento(pagamento.getFormas().getDescricao());//MEIO PAGAMENTO
            temp.setCodigoTpDoc(String.valueOf(pagamento.getTipoDeDocumento().getCodigo()));
            temp.setValor(pagamento.getValor());
            a2s.add(temp);
        }
        /*
         cc.executaSqlSelect("SELECT "
         + "DATE_FORMAT(pedido.DATA_VENDA,'%Y-%m-%d') AS DATA,"
         + "formas_pg_validas.DESCRICAO AS DESCRICAO,"
         + "paf_tp_doc.CODIGO AS COD_DOC,"
         + "SUM(formas_de_pagamento.VALOR) AS VALOR "
         + "FROM caixa "
         + "INNER JOIN pedido ON pedido.ID_CAIXA = caixa.ID "
         + "INNER JOIN formas_de_pagamento ON formas_de_pagamento.ID_PEDIDO = pedido.ID "
         + "INNER JOIN formas_pg_validas ON formas_pg_validas.ID = formas_de_pagamento.ID_FORMAS_PG_VALIDAS"
         + " INNER JOIN paf_tp_doc ON paf_tp_doc.ID = formas_de_pagamento.ID_PAF_TP_DOC"
         + " WHERE caixa.DATA_ABERTURA>='" + dataInicial + " 00:00:00'"
         + " AND caixa.DATA_ABERTURA<='" + dataFinal + " 23:59:99'"
         + " GROUP BY DATA, DESCRICAO, COD_DOC");
         */
        return a2s;
    }

    //Informações de preços de mercadorias
    private static List<P2> getListaP2(String dataInicial, String dataFinal, int tp, String pesquisa) throws IbsException {
        List<P2> p2s = new ArrayList<>();
        HibernateUtil hu = HibernateUtil.getInstance();
        Iterator<Produto> produtos = hu.getSession()
                .createQuery("from Produto p where p.ativo=true").list().iterator();
        try {
            while (produtos.hasNext()) {
                Produto produto = produtos.next();
                Iterator<ItensPrecoFranquia> i = produto.getProdutoPrecoFranquia().getItensPrecoFranquias().iterator();
                while (i.hasNext()) {
                    ItensPrecoFranquia itensPrecoFranquia = i.next();
                    if (Util.isParcebleDouble(itensPrecoFranquia.getValor())) {
                        P2 p2 = new P2();
                        p2.setCnpj(VariaveisDoSistema.EMPRESA.getPessoaJuridica().getCnpj());
                        p2.setCodigo(produto.getReferencia());
                        p2.setDescricao(produto.getMarca().getDescricao() + " " + produto.getDescricao());
                        p2.setUnidade(produto.getUnidadeMedida().getUm());
                        p2.setIat(String.valueOf(produto.getIat().getCodigo()));
                        p2.setIppt(String.valueOf(produto.getIppt().getCodigo()));
                        p2.setSituacaoTributaria(String.valueOf(produto.getSituacaoTributaria().getCodigo()));
                        p2.setAliquota(produto.getImposto().getAliquotaIcms());
                        p2.setValorUnitario(Double.parseDouble(itensPrecoFranquia.getValor()));

                        p2s.add(p2);
                    }
                }
            }
        } catch (HibernateException ex) {
            throw new IbsException("Erro ao carregar registro tipo P2!", ex);
        }
        return p2s;
    }

    //Informações do estoque
    private static List<E2> getListaE2(String dataInicial, String dataFinal, int tp, String pesquisa) throws IbsException {
        List<E2> e2s = new ArrayList<E2>();
        HibernateUtil hu = HibernateUtil.getInstance();
        Iterator<Produto> produtos = hu.getSession()
                .createCriteria(Produto.class)
                .add(Restrictions.eq("ativo", true)).list().iterator();
        try {
            while (produtos.hasNext()) {
                Produto produto = produtos.next();
                E2 temp = new E2();
                temp.setCnpj(AUXILIAR.getProperty("cli.cnpj"));//
                temp.setCodigo(produto.getReferencia());//
                temp.setDescricao(produto.getMarca().getDescricao() + " " + produto.getDescricao());
                temp.setUnidade(produto.getUnidadeMedida().getUm());
                temp.setEstoque(produto.getItemCompras().size());
                e2s.add(temp);
            }
        } catch (HibernateException ex) {
            throw new IbsException("Erro ao carregar registro tipo E2!", ex);
        }

        return e2s;
    }

    //Informações do ecf base que atualizou o estoque
    private static E3 getE3(String dataInicial, String dataFinal, String pesquisa) {
        E3 e3 = new E3();
        Ecf ecf = VariaveisDoSistema.V_ECF;
        e3.setNumeroFabricacao(ecf.getSerie());//
        e3.setMfAdicional(ecf.getMfadicional());//
        e3.setTpEcf(ecf.getTipo());//
        e3.setMarcaEcf(ecf.getMarca());//
        e3.setModeloEcf(ecf.getModelo());//
        e3.setDataEstoque(VariaveisDoSistema.CAIXA.getDataAbertura());//
        e3.setHoraEstoque(VariaveisDoSistema.CAIXA.getDataAbertura());//
        return e3;
    }

    //6.21. REGISTRO TIPO R01 - IDENTIFICAÇÃO DO ECF, DO USUÁRIO, DO PAF-ECF E DA EMPRESA DESENVOLVEDORA
    private static R01 getR01(String dataInicial, String dataFinal, int tp, String pesquisa) throws IbsException {
        R01 temp = new R01();
        Ecf ecf = VariaveisDoSistema.V_ECF;
        temp.setSerie(ecf.getSerie());//
        temp.setMfAdicional(ecf.getMfadicional());//
        temp.setTipoECF(ecf.getTipo());
        temp.setMarcaECF(ecf.getMarca());
        temp.setModeloECF(ecf.getModelo());
        try {
            temp.setVersaoSB(ControllerPaf.getInstance().getNumVersaoSB());
        } catch (ACBrException ex) {
            throw new IbsException("Erro ao extrair a versão do SB!", ex);
        }
        try {
            temp.setDataSB(ControllerPaf.getInstance().getDataSB());
        } catch (ACBrException ex) {
            throw new IbsException("Erro ao extrair a data do SB!", ex);
        }
        //temp.setDataSB(new SimpleDateFormat("dd/MM/yy HH:mm:ss").parse(resp[1]));
        temp.setNumeroECF(Integer.parseInt(ecf.getIdentificacao()));
        temp.setEmpresaCNPJ(AUXILIAR.getProperty("cli.cnpj"));
        temp.setEmpresaIE(AUXILIAR.getProperty("cli.ie"));
        temp.setShCNPJ(AUXILIAR.getProperty("sh.cnpj"));
        temp.setShIE(AUXILIAR.getProperty("sh.ie"));
        temp.setShIM(AUXILIAR.getProperty("sh.im") != null ? Integer.parseInt(AUXILIAR.getProperty("sh.im")) : 0);
        temp.setShRazao(AUXILIAR.getProperty("sh.razao"));

        temp.setPafNome(AUXILIAR.getProperty("paf.nome"));
        temp.setPafVersao(AUXILIAR.getProperty("paf.versao"));

        StringBuilder arquivoMD5 = new StringBuilder(System.getProperty("user.dir"));
        arquivoMD5.append(System.getProperty("file.separator"));
        arquivoMD5.append("arquivos");
        arquivoMD5.append(System.getProperty("file.separator"));
        arquivoMD5.append("arquivoMD5.txt");
        try {
            temp.setPafMD5(PAF.gerarMD5(arquivoMD5.toString()));
        } catch (Exception ex) {
            throw new IbsException("Erro ao gerar arquivo MD5!", ex);
        }
        try {
            temp.setInicio(new SimpleDateFormat("yyyy-MM-dd").parse(dataInicial));
        } catch (ParseException ex) {
            throw new IbsException("Erro ao formatar data inicial!", ex);
        }
        try {
            temp.setFim(new SimpleDateFormat("yyyy-MM-dd").parse(dataFinal));
        } catch (ParseException ex) {
            throw new IbsException("Erro ao formatar data final!", ex);
        }
        temp.setPafER(AUXILIAR.getProperty("paf.er").replace(".", ""));

        return temp;
    }

    //REGISTRO TIPO R03 - DETALHE DA REDUÇÃO Z
    private static List<R02> getListaR02(String dataInicial, String dataFinal, int tp, String pesquisa) throws IbsException {
        List<R02> l = new ArrayList<R02>();
        HibernateUtil hu = HibernateUtil.getInstance();
        try {
            Iterator<ReducaoZ> i = hu.getSession().createQuery("from ReducaoZ").list().iterator();
            while (i.hasNext()) {
                ReducaoZ reducaoZ = i.next();
                R02 temp = new R02();
                temp.setSerie(reducaoZ.getEcf().getSerie());
                temp.setMfAdicional(reducaoZ.getEcf().getMfadicional());
                temp.setModeloECF(reducaoZ.getEcf().getModelo());
                temp.setUsuario(reducaoZ.getEcf().getUsuario());
                temp.setCrz(reducaoZ.getCrz());
                temp.setCoo(reducaoZ.getCooFin());
                temp.setCro(reducaoZ.getCro());
                temp.setMovimento(reducaoZ.getMovimento());
                temp.setBruto(reducaoZ.getBruto());
                temp.setIssqn(reducaoZ.getIssqn() ? 'S' : 'N');

                l.add(temp);
            }
        } catch (HibernateException ex) {
            throw new IbsException("Erro ao carregar reduções z!", ex);
        }
        return l;
    }

    //REGISTRO TIPO R03 - DETALHE DA REDUÇÃO Z
    //6.23.1. Observações:
    //6.23.1.1. Deve ser criado um registro tipo R03 para cada totalizador parcial constante na 
    //Redução Z emitida pelo ECF no período informado no arquivo, observando-se o disposto no item 2 do 
    //requisito XXVI.
    private static List<R03> getListaR03(String dataInicial, String dataFinal, int tp, String pesquisa) throws IbsException {
        List<R03> l = new ArrayList<R03>();
        HibernateUtil hu = HibernateUtil.getInstance();
        try {
            Iterator<ReducaoZTotais> i = hu.getSession().createQuery("from ReducaoZTotais").list().iterator();
            while (i.hasNext()) {
                ReducaoZTotais reducaoZTotais = i.next();
                R03 temp = new R03();
                temp.setSerie(reducaoZTotais.getReducaoZ().getEcf().getSerie());
                temp.setMfAdicional(reducaoZTotais.getReducaoZ().getEcf().getMfadicional());
                temp.setModeloECF(reducaoZTotais.getReducaoZ().getEcf().getModelo());
                temp.setUsuario(reducaoZTotais.getReducaoZ().getEcf().getUsuario());
                temp.setCrz(reducaoZTotais.getReducaoZ().getCrz());
                temp.setTotalizador(reducaoZTotais.getCodigo());
                temp.setValor(reducaoZTotais.getValor());
                l.add(temp);
            }
        } catch (HibernateException ex) {
            throw new IbsException("Erro ao carregar reduções z!", ex);
        }
        return l;
    }

    //6.24.  REGISTRO  TIPO  R04  -  CUPOM  FISCAL,  NOTA  FISCAL  DE  VENDA  A CONSUMIDOR E BILHETE DE PASSAGEM
    //    6.24.1. Observações:
    //    6.24.1.1. Deve ser criado um registro tipo R04 para cada Cupom Fiscal, Nota Fiscal de 
    //    Venda  a  Consumidor  ou  Bilhete  de  Passagem  emitido  pelo  ECF  no  período  informado  no  arquivo, 
    //    observando-se o disposto no item 3 do requisito XXVI.
    //    6.24.1.2.  Não  deve  ser  criado  registro  relativo  a  documento  para  cancelamento  de 
    //    documento anterior (vide item 6.24.1.5).
    //    6.24.1.3. Campo 09: Não informar este campo caso ocorra o cancelamento do Cupom 
    //    Fiscal em emissão antes da impressão da totalização do documento.
    //    6.24.1.4. Campo 14: Não informar este campo caso ocorra o cancelamento do Cupom 
    //    Fiscal em emissão antes da impressão da totalização do documento.
    //    6.24.1.5.  Campo  15:  Caso  tenha  ocorrido  o  cancelamento  do  documento  durante  sua 
    //    emissão  ou  imediatamente  após  por  meio  da  emissão  de  documento  para  cancelamento  de 
    //    documento anterior, informar "S", caso contrário, informar "N".
    //    6.24.1.6.  Campo  19:  Informar  somente  os  caracteres  relativos  aos  dígitos  do  número, 
    //    sem máscaras de edição.
    private static List<R04> getListaR04(String dataInicial, String dataFinal, int tp, String pesquisa) throws IbsException {
        List<R04> l = new ArrayList<R04>();
        HibernateUtil hu = HibernateUtil.getInstance();
        try {
            Iterator<Venda> i = hu.getSession().createQuery("from ReducaoZTotais").list().iterator();
            while (i.hasNext()) {
                Venda venda = i.next();
                R04 temp = new R04();
                temp.setSerie(venda.getCaixa().getEcf().getSerie());
                temp.setMfAdicional(venda.getCaixa().getEcf().getMfadicional());
                temp.setModeloECF(venda.getCaixa().getEcf().getModelo());
                temp.setUsuario(venda.getCaixa().getEcf().getUsuario());
                temp.setDocumento(Integer.parseInt(venda.getCcf()));
                temp.setCoo(Integer.parseInt(venda.getCoo()));
                temp.setData(venda.getDataInicioEmissaoDoc());
                temp.setBruto(venda.getBruto());
                temp.setDesconto(venda.getDesconto());
                temp.setTipoDesconto('V');
                temp.setAcrescimo(venda.getAcrescimoDesconto());
                temp.setLiquido(venda.getTotal());
                temp.setCancelado(venda.getSituacao() == SituacaoVenda.CANCELADO.getId() ? 'S' : 'N');
                temp.setCanceladoAcrescimo(venda.getAcrescimoDesconto());
                temp.setOrdemAcresDesc('A');
                temp.setClienteNome(venda.getCliente().getPessoaFisica() != null ? venda.getCliente().getPessoaFisica().getNome() : venda.getCliente().getPessoaJuridica().getRazao());
                temp.setClienteCPF(venda.getCliente().getPessoaFisica() != null ? venda.getCliente().getPessoaFisica().getCpf() : venda.getCliente().getPessoaJuridica().getCnpj());
                l.add(temp);
            }
        } catch (HibernateException ex) {
            throw new IbsException("Erro ao carregar reduções z!", ex);
        }
        return l;
    }

    //6.25. REGISTRO TIPO R05  -  DETALHE DO CUPOM FISCAL, DA NOTA FISCAL DE VENDA A CONSUMIDOR OU DO BILHETE DE PASSAGEM
    //6.25.1. Observações:
    //6.25.1.1.  Deve  ser  criado  um  registro  tipo  R05  para  cada  item  (produto  ou  serviço) 
    //registrado  no  documento  emitido  pelo  ECF  e  informado  no  registro  tipo  R04,  observando -se  o 
    //disposto no item 3 do requisito XXVI.
    //6.25.1.2. Campo 10  -  Deve conter os primeiros cem caracteres da descrição do produto 
    //ou serviço constante no documento.
    //6.25.1.3. Campo 17 - Vide tabela do subitem 6.23.1.2.
    //6.25.1.4.  Campo  19  -  Informar  a  quantidade  cancelada  somente  quando  ocorrer  o 
    //cancelamento parcial do item.
    //6.25.1.5.  Campo  20  -  Informar  o  valor  cancelado  somente  quando  ocorrer  o 
    //cancelamento parcial do item.
    //6.25.1.6.  Campo  24:  Informar  o  número  de  casas  decimais  da  quantidade 
    //comercializada.
    //6.25.1.7. Campo 25: Informar o número de casas decimais do valor unitário do produto 
    //ou serviço.

    /*private static List<R05> getListaR05(String dataInicial, String dataFinal, int tp, String pesquisa) throws IbsException {
        List<R05> l = new ArrayList<R05>();
        HibernateUtil hu = HibernateUtil.getInstance();
        try {
            Iterator<ItemVenda> i = hu.getSession().createQuery("from item_venda").list().iterator();
            while (i.hasNext()) {
                ItemVenda itemVenda = i.next();
                R05 temp = new R05();

                temp.setSerie(itemVenda.getVenda().getCaixa().getEcf().getSerie());
                temp.setMfAdicional(itemVenda.getVenda().getCaixa().getEcf().getMfadicional());
                temp.setModeloECF(itemVenda.getVenda().getCaixa().getEcf().getModelo());
                temp.setUsuario(Integer.parseInt(itemVenda.getVenda().getCaixa().getEcf().getCodigo()));
                temp.setDocumento(Integer.parseInt(itemVenda.getVenda().getCcf()));
                temp.setCoo(Integer.parseInt(itemVenda.getVenda().getCoo()));
                temp.setItem(itemVenda.getNumeroItem());//Número do item registrado no documento
                temp.setCodigo(itemVenda.getItemCompra().getProduto().getReferencia());
                temp.setDescricao(itemVenda.getItemCompra().getProduto().getMarca().getDescricao() + " " + itemVenda.getItemCompra().getProduto().getDescricao());
                temp.setQuantidade(Double.parseDouble(String.valueOf(itemVenda.getQtd())));
                temp.setUnidade(itemVenda.getItemCompra().getProduto().getUnidadeMedida().getUm());
                temp.setBruto(itemVenda.getBruto());
                temp.setDesconto(itemVenda.getDesconto());
                temp.setAcrescimo(0.00);

                temp.setTotal(itemVenda.getLiquido());
                r05.setTotalizador(cc.getRs().getString("TL_PRCL") + "1");
                              
                
                if (cc.getRs().getString("TL_PRCL").equals("T") || cc.getRs().getString("TL_PRCL").equals("S")) {
                    String sufixo = 
                            cc.getRs().getString("TL_PRCL") + 
                            Util.formataNumero(cc.getRs().getString("VLR_TBT"), 2, 2, false).replace(",", "");
                    
                    for (int i = 0; i < totaisCodigo.getSize(); i++) {
                        if (totaisCodigo.getElementAt(i).endsWith(sufixo)) {
                            r05.setTotalizador(totaisCodigo.getElementAt(i));
                            break;
                        }
                    }
                } else {
                    r05.setTotalizador(cc.getRs().getString("TL_PRCL") + "1");
                }
                
                temp.setTotalizador(dataFinal);

                temp.setCancelado(Objects.equals(itemVenda.getSituacao(), SituacaoItemCompraVenda.CANCELADO.getId()) ? 'S' : 'N');

                temp.setCanceladoQtd(0.0);
                temp.setCanceladoValor(0.00);
                temp.setCanceladoAcrescimo(0.00);
                temp.setIat(itemVenda.getItemCompra().getProduto().getIat().getCodigo());
                temp.setIppt(itemVenda.getItemCompra().getProduto().getIppt().getCodigo());
                temp.setDecimalQuantidade(2);
                temp.setDecimalValor(2);
                l.add(temp);
            }
        } catch (HibernateException ex) {
            throw new IbsException("Erro ao carregar reduções z!", ex);
        }
        return l;

    }

    private static List<R06> getListaR06(String dataInicial, String dataFinal,
            int tp, String pesquisa) throws SQLException {
        // r06
        List<R06> l = new ArrayList<R06>();

        //ATENÇÃO CONTINUAR DIA 3/10/2013COLOCAR FILTRO DE DATA
        cc.executaSqlSelect("CALL SELECIONA_TIPO_R06('" + dataInicial + "','" + dataFinal + " 23:59:59','" + Comandos_Iniciais.getImpressora().getId() + "','" + Class_Usuario_Logado.getIdEmpresa() + "')");
        try {
            while (cc.getRs().next()) {
                R06 r06 = new R06();
                r06.setSerie(cc.getRs().getString("SERIE"));
                r06.setMfAdicional(cc.getRs().getString("MFADICIONAL"));
                r06.setModeloECF(cc.getRs().getString("MODELO"));
                r06.setUsuario(cc.getRs().getInt("NUMERO_USUARIO"));
                r06.setCoo(cc.getRs().getInt("COO"));
                r06.setGnf(cc.getRs().getInt("GNF"));
                r06.setGrg(cc.getRs().getInt("GRG"));
                r06.setCdc(cc.getRs().getInt("CDC"));
                r06.setTipo(cc.getRs().getString("TIPO"));
                r06.setData(cc.getRs().getDate("DATA"));
                l.add(r06);
            }
        } finally {
            cc.Desconectar();
        }

        return l;
    }

    private static List<R07> getListaR07(String dataInicial, String dataFinal,
            int tp, String pesquisa) throws SQLException {
        // r07
        List<R07> l = new ArrayList<R07>();

        //ATENÇÃO CONTINUAR DIA 3/10/2013COLOCAR FILTRO DE DATA
        cc.executaSqlSelect("CALL SELECIONA_TIPO_R07('" + dataInicial + "','" + dataFinal + " 23:59:59','" + Comandos_Iniciais.getImpressora().getId() + "','" + Class_Usuario_Logado.getIdEmpresa() + "')");
        try {
            while (cc.getRs().next()) {
                R07 r07 = new R07();
                r07.setSerie(cc.getRs().getString("SERIE"));
                r07.setMfAdicional(cc.getRs().getString("MFADICIONAL"));
                r07.setModeloECF(cc.getRs().getString("MODELO"));
                r07.setUsuario(cc.getRs().getInt("NUMERO_USUARIO"));
                r07.setCoo(cc.getRs().getInt("COO"));
                r07.setCcf(cc.getRs().getInt("CCF"));
                r07.setGnf(cc.getRs().getInt("GNF"));
                r07.setMeioPagamento(cc.getRs().getString("DESCRICAO"));
                r07.setValor(cc.getRs().getDouble("VALOR_PAGO"));
                r07.setEstorno(cc.getRs().getString("INDICADOR_ESTORNO").charAt(0));
                r07.setValorEstorno(cc.getRs().getDouble("VALOR_ESTORNADO"));
                l.add(r07);
            }
        } finally {
            cc.Desconectar();
        }
        return l;
    }*/

    private static String gerarRegistrosPafEcf(AnexoIV anexoIV) throws IbsException {
        // gerar o arquivo
        StringBuilder sb = new StringBuilder(getPathArquivos());
        sb.append("RegistrosPafEcf_").append(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())).append(".txt");
        FileWriter fw;
        try {
            fw = new FileWriter(sb.toString());
        } catch (IOException ex) {
            throw new IbsException("Erro ao escrever arquivo!", ex);
        }
        // compila no formato
        StreamFactory factory = StreamFactory.newInstance();
        try {
            factory.load(PAF.class.getClass().getResourceAsStream("/br/com/cresceritsolutions/ibspdv/models/anexo1/v02_02/iv/AnexoIV.xml"));
        } catch (BeanIOConfigurationException | IOException ex) {
            throw new IbsException("Erro ao carregar o modelo AnexoIV!", ex);
        }
        BeanWriter bw = factory.createWriter("AnexoIV", fw);

        // escevendo no arquivo
        //U1
        bw.write(anexoIV.getU1());
        //A2
        for (A2 a2 : anexoIV.getListaA2()) {
            bw.write(a2);
            bw.flush();
        }
        //P2
        for (P2 p2 : anexoIV.getListaP2()) {
            bw.write(p2);
            bw.flush();
        }
        //E2
        for (E2 e2 : anexoIV.getListaE2()) {
            bw.write(e2);
            bw.flush();
        }
        //E3
        bw.write(anexoIV.getE3());

        //R01
        bw.write(anexoIV.getR01());

        //R02
        for (R02 r02 : anexoIV.getListaR02()) {
            bw.write(r02);
            bw.flush();
        }

        //R03
        for (R03 r03 : anexoIV.getListaR03()) {
            bw.write(r03);
            bw.flush();
        }

        //R04
        for (R04 r04 : anexoIV.getListaR04()) {
            bw.write(r04);
            bw.flush();
        }

        //R05
        for (R05 r05 : anexoIV.getListaR05()) {
            bw.write(r05);
            bw.flush();
        }

       /* //R06
        for (R06 r06 : anexoIV.getListaR06()) {
            bw.write(r06);
            bw.flush();
        }

        //R07
        for (R07 r07 : anexoIV.getListaR07()) {
            bw.write(r07);
            bw.flush();
        }*/

        bw.flush();
        bw.close();

        try {
            // assinando o arquivo
            //assinarArquivoEAD(path);
            // assinando o arquivo
            /* BemaString cEad = new BemaString();
             cEad.buffer = new String(
             "                                                        ");
             */
            ACBrEAD acbead = new ACBrEAD();
            //acbead.gerarChaves();
            acbead.assinarArquivoComEAD(sb.toString(), false);
            //assinarArquivoEAD(path);
        } catch (Exception ex) {
            Logger.getLogger(PAF.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sb.toString();
    }
}
