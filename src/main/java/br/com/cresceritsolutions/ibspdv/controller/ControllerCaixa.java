/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.controller;

import br.com.cresceritsolutions.ibspdv.models.hb.Caixa;
import br.com.cresceritsolutions.ibspdv.models.hb.Formas;
import br.com.cresceritsolutions.ibspdv.models.hb.Movimento;
import br.com.cresceritsolutions.ibspdv.models.hb.Pagamentos;
import br.com.cresceritsolutions.ibspdv.models.hb.TipoDeDocumento;
import br.com.cresceritsolutions.ibspdv.util.HibernateUtil;
import br.com.cresceritsolutions.ibspdv.util.IbsException;
import br.com.cresceritsolutions.ibspdv.util.Util;
import br.com.cresceritsolutions.ibspdv.util.VariaveisDoSistema;
import jACBrFramework.ACBrException;
import java.util.Date;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author gutemberg
 */
public class ControllerCaixa {

    private static ControllerCaixa me;

    private ControllerCaixa() {
    }

    public static ControllerCaixa getInstance() {
        if (me == null) {
            me = new ControllerCaixa();
        }
        return me;
    }

    //Abre caixa
    public boolean abrirCaixa(Double valor) throws IbsException, ACBrException {
        HibernateUtil hu = HibernateUtil.getInstance();

        try {
            hu.beginTransaction();
            Caixa caixa = new Caixa();
            caixa.setEmpresa(VariaveisDoSistema.EMPRESA);
            caixa.setOperadorByOperadorId(VariaveisDoSistema.OPERADOR);
            caixa.setAberto(true);
            caixa.setDataAbertura(new Date());
            caixa.setValorAbertura(valor);
            caixa.setNumeroCaixa(Integer.parseInt(Util.getConfig().get("ibspdv.caixa")));
            caixa.setEcf(VariaveisDoSistema.V_ECF);

            //Salva caixa
            hu.getSession().save(caixa.getOperadorByOperadorId());
            caixa.setEcf(VariaveisDoSistema.V_ECF);
            hu.getSession().save(caixa);

            Formas formas = (Formas) hu.getSession().createCriteria(Formas.class).add(Restrictions.like("descricao", "dinheiro")).uniqueResult();
            TipoDeDocumento tdd = (TipoDeDocumento) hu.getSession().createCriteria(TipoDeDocumento.class).add(Restrictions.eq("codigo", 2)).uniqueResult();
            
            if (formas == null) {
                Formas f = new Formas();
                f.setAtivo(true);
                f.setDataInclusao(new Date());
                f.setDataInclusao(new Date());
                f.setDescricao("Dinheiro");
                f.setTef(false);
                formas = f;
            }
            Pagamentos pagamentos = new Pagamentos();
            pagamentos.setFormas(formas);
            pagamentos.setTipoDeDocumento(tdd);
            pagamentos.setParcelas(0);
            pagamentos.setValor(valor);
            pagamentos.setVencimento(null);
            pagamentos.setVenda(null);

            Movimento movimento = new Movimento(caixa, null, pagamentos, valor, false, "Abertura do caixa");

            hu.getSession().saveOrUpdate(formas);
            hu.getSession().saveOrUpdate(pagamentos);
            hu.getSession().saveOrUpdate(movimento);

            if (VariaveisDoSistema.getEcfAtivo()) {
                ControllerPaf.getInstance().corrigeEstadoErro(false);
                ControllerPaf.getInstance().leituraX();
                ControllerPaf.getInstance().aberturaCaixa(valor);
            }
            hu.commit();

            VariaveisDoSistema.CAIXA = caixa;
            return true;
        } catch (HibernateException ex) {
            try{
                hu.rollback();
                throw new IbsException("Erro ao abrir o caixa!\nErro Técnico: "+ex.getMessage(), ex);
            }catch(HibernateException ex2){
                throw new IbsException("Erro ao abrir o caixa!\nErro Técnico: "+ex2.getMessage(), ex2);
            }
        }

    }

}
