/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.controller;

import br.com.cresceritsolutions.ibspdv.util.HibernateUtil;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 *
 * @author Gutem
 */
public class ControllerGerarTabelaIndiceTecnico {

    private static ControllerGerarTabelaIndiceTecnico me;

    public static ControllerGerarTabelaIndiceTecnico newInstance() {
        me = new ControllerGerarTabelaIndiceTecnico();
        return me;
    }

    public static ControllerGerarTabelaIndiceTecnico getInstance() {
        if (me == null) {
            me = new ControllerGerarTabelaIndiceTecnico();
        }
        return me;
    }

    public void executar(String DIRETORIO) {
        HibernateUtil hu = HibernateUtil.getInstance();
        
        
        String ID_BOOK = "(SELECT book_descricao.ID FROM book_descricao WHERE book_descricao.VIGENTE=1)";
/*
        // CARREGA PLANOS
        DefaultComboBoxModel<String> planos = new DefaultComboBoxModel<String>();
        cc.executaSqlSelect("SELECT DESCRICAO FROM item_planos WHERE ATIVO=1");
        try {
            while (cc.getRs().next()) {// CARREGA PLANOS EXISTENTES
                planos.addElement(cc.getRs().getString("DESCRICAO"));
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // CARREGA PRODUTOS
        DefaultComboBoxModel<String> produtos = new DefaultComboBoxModel<String>();
        cc.executaSqlSelect("SELECT ID FROM produtos WHERE ATIVO=1 AND TIPO_PRECO=1");
        try {
            while (cc.getRs().next()) {// CARREGA PRODUTOS EXISTENTES
                produtos.addElement(cc.getRs().getString("ID"));
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // CARREGA TABELA DE INDICE TÉCNICO
        StringBuilder textoArquivo = new StringBuilder();
        for (int i = 0; i < produtos.getSize(); i++) {// PRODUTO
            String pr = produtos.getElementAt(i);
            for (int j = 0; j < planos.getSize(); j++) {// PLANOS
                String pl = planos.getElementAt(j);
                try {
                    cc.executaSqlSelectTratarErro("SELECT "
                            + "CONCAT(produtos.id,(SELECT item_planos.ID FROM item_planos WHERE item_planos.DESCRICAO='" + pl + "')) AS COD_FINAL,"
                            + "produtos.id AS COD_PRODUTO,"
                            + "(SELECT ID FROM item_planos WHERE DESCRICAO='" + pl + "') AS COD_PLANO,"
                            + "TRIM(CONCAT(marca_produtos.DESCRICAO,' ',produtos.DESCRICAO,' ',produtos.DETALHES)) AS PRODUTO "
                            + "FROM precos_do_sistema "
                            + "INNER JOIN produtos ON produtos.ID=precos_do_sistema.APARELHO "
                            + "INNER JOIN marca_produtos ON marca_produtos.ID = produtos.ID_MARCA "
                            + "INNER JOIN book_abas ON book_abas.ID = precos_do_sistema.ID_CATEGORIA "
                            + "WHERE " + "precos_do_sistema.ID_BOOK=" + ID_BOOK
                            + " " + "AND precos_do_sistema.aparelho=" + pr
                            + " " + "AND precos_do_sistema."
                            + Class_Formato.getCodSimbol(pl.replace(" ", "_"))
                            + "_VISTA!='null'");
                    while (cc.getRs().next()) {
                        if (!textoArquivo.toString().contains("PRODUTO FINAL : " + cc.getRs().getString("PRODUTO").toUpperCase() + " - " + pl.toUpperCase() + "   COD : " + cc.getRs().getString("COD_FINAL"))) {
                            textoArquivo.append("PRODUTO FINAL : " + cc.getRs().getString("PRODUTO").toUpperCase() + " - " + pl.toUpperCase() + "   COD : " + cc.getRs().getString("COD_FINAL"));// PRODUTO
                            textoArquivo.append("\r\nPRODUTO INSUMO : " + cc.getRs().getString("PRODUTO").toUpperCase() + "   COD : " + cc.getRs().getString("COD_PRODUTO") + " QUANTIDADE : 1");// APARELHO
                            textoArquivo.append("\r\nPRODUTO INSUMO : " + pl.toUpperCase() + "   COD : " + cc.getRs().getString("COD_PLANO") + " QUANTIDADE : 1");// PLANO
                            textoArquivo.append("\r\n\r\n");// CATEGORIA
                        }
                    }
                } catch (SQLException e) {
                    if (e.getErrorCode() == 1054) {

                    } else {
                        e.printStackTrace();
                    }
                }
            }
        }
        FileWriter f;
        try {
            File arquivo = new File((DIRETORIO + ".txt").replace(".txt.txt", ".txt"));
            f = new FileWriter(arquivo);
            f.write(textoArquivo.toString());
            f.flush();
            f.close();
            BemaString cEad = new BemaString();
            cEad.buffer = new String(
                    "                                                        ");
            assinarArquivoEAD(DIRETORIO);
            //generateEAD(DIRETORIO,					Chaves.PUBLICA, Chaves.PRIVADA,					cEad, 1);

            JOptionPane.showMessageDialog(null, "Arquivo gerado com sucesso!");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
*/
    }
}
