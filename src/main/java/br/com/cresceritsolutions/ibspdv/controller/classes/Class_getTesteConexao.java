/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.controller.classes;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Gutemberg
 */
public class Class_getTesteConexao {

    public static String ip;
    public static int porta;

    public static boolean teste(String ip, int porta) {
        try {
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(ip, porta));
            socket.close();
            return true;
        } catch (UnknownHostException ex) {
            return false;
        } catch (IOException ex) {
            return false;
        }
    }

    public static boolean teste2(String banco, String ip, int porta, String usuario, String senha) {
        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            Connection c = (DriverManager.getConnection("jdbc:jtds:sqlserver://" + ip + ":" + porta + "/" + banco, usuario, senha));
            c.close();
            return true;
        } catch (SQLException | ClassNotFoundException ex) {
            return false;
        }
    }
}
