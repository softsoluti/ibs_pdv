/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.view.core;

import br.com.cresceritsolutions.ibspdv.models.UI.ComboBoxIBS;
import br.com.cresceritsolutions.ibspdv.models.UI.DialogIBS;
import br.com.cresceritsolutions.ibspdv.models.enuns.OpcoesTipoValorPrecoFranquia;
import br.com.cresceritsolutions.ibspdv.models.enuns.SituacaoItemCompraVenda;
import br.com.cresceritsolutions.ibspdv.models.hb.ItemCompra;
import br.com.cresceritsolutions.ibspdv.models.hb.ItensPrecoFranquia;
import br.com.cresceritsolutions.ibspdv.models.hb.PrecoFranquia;
import br.com.cresceritsolutions.ibspdv.models.hb.Produto;
import br.com.cresceritsolutions.ibspdv.models.hb.ProdutoPrecoFranquia;
import br.com.cresceritsolutions.ibspdv.models.hb.TipoPrecoFranquia;
import br.com.cresceritsolutions.ibspdv.renders.RenderVisualizaPrecoFranquia;
import br.com.cresceritsolutions.ibspdv.util.DimencionaTB;
import br.com.cresceritsolutions.ibspdv.util.ExibirParaUsuario;
import br.com.cresceritsolutions.ibspdv.util.HibernateUtil;
import br.com.cresceritsolutions.ibspdv.util.IbsException;
import br.com.cresceritsolutions.ibspdv.util.Util;
import br.com.cresceritsolutions.ibspdv.view.venda.NovaVendaConcomitante;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Order;

/**
 *
 * @author Gutem
 */
public class PrecosFranquia extends DialogIBS {

    private static PrecosFranquia me;

    public static PrecosFranquia newInstance(boolean concomitante) {
        me = new PrecosFranquia();
        me.concomitante = concomitante;
        try {
            me.carregaVigente();
        } catch (IbsException ex) {
            ExibirParaUsuario.getInstance().erro(me, ex, ex.getMessage(), false);
        }
        return me;
    }

    public static PrecosFranquia getInstance() throws IbsException {
        if (me == null) {
            me = new PrecosFranquia();
        }
        me.carregaVigente();
        return me;
    }
    public boolean concomitante;

    /**
     * Creates new form PrecosFranquia
     */
    public PrecosFranquia() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtPesquisa = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jToolBar1 = new javax.swing.JToolBar();
        jButton1 = new javax.swing.JButton();
        tbtPrecos = new javax.swing.JTabbedPane();
        jToolBar2 = new javax.swing.JToolBar();
        jButton2 = new javax.swing.JButton();
        cboTabelaVigente = new ComboBoxIBS();
        jLabel3 = new javax.swing.JLabel();
        chkEstoque = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Preços Franquia");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        txtPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPesquisaActionPerformed(evt);
            }
        });

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Tabela de preços Franquia");
        jLabel1.setOpaque(true);

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/search_24x24.png"))); // NOI18N
        jButton1.setFocusable(false);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        tbtPrecos.setBackground(new java.awt.Color(204, 204, 204));
        tbtPrecos.setForeground(new java.awt.Color(0, 51, 204));
        tbtPrecos.setTabLayoutPolicy(javax.swing.JTabbedPane.SCROLL_TAB_LAYOUT);
        tbtPrecos.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jToolBar2.setFloatable(false);
        jToolBar2.setRollover(true);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/close_24x24.png"))); // NOI18N
        jButton2.setFocusable(false);
        jButton2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jToolBar2.add(jButton2);

        cboTabelaVigente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboTabelaVigenteActionPerformed(evt);
            }
        });

        jLabel3.setText("Tabela Vigente");

        chkEstoque.setSelected(true);
        chkEstoque.setText("Somente Estoque");
        chkEstoque.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkEstoqueActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtPesquisa)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cboTabelaVigente, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(chkEstoque)
                                .addGap(0, 430, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jToolBar2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)))
                .addContainerGap())
            .addComponent(tbtPrecos)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cboTabelaVigente, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)
                            .addComponent(chkEstoque))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tbtPrecos, javax.swing.GroupLayout.DEFAULT_SIZE, 394, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jToolBar2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (!cboTabelaVigente.getSelectedItem().equals("")) {
            try {
                pesquisar();
            } catch (IbsException ex) {
                ExibirParaUsuario.getInstance().erro(me, ex, ex.getMessage(), false);
            }
        } else {
            ExibirParaUsuario.getInstance().atencao(me, "Selecione a tabela de preços vigente!");
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
    }//GEN-LAST:event_formWindowClosing

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        dispose();
        NovaVendaConcomitante.getInstance().setModal(true);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void txtPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPesquisaActionPerformed
        if (!cboTabelaVigente.getSelectedItem().equals("")) {
            try {
                pesquisar();
            } catch (IbsException ex) {
                ExibirParaUsuario.getInstance().erro(me, ex, ex.getMessage(), false);
            }
        } else {
            ExibirParaUsuario.getInstance().atencao(me, "Selecione a tabela de preços vigente!");
        }
    }//GEN-LAST:event_txtPesquisaActionPerformed

    private void cboTabelaVigenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboTabelaVigenteActionPerformed
        if (!cboTabelaVigente.getSelectedItem().equals("")) {
            try {
                pesquisar();
            } catch (IbsException ex) {
                ExibirParaUsuario.getInstance().erro(me, ex, ex.getMessage(), false);
            }
        }
    }//GEN-LAST:event_cboTabelaVigenteActionPerformed

    private void chkEstoqueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkEstoqueActionPerformed
        if (!cboTabelaVigente.getSelectedItem().equals("")) {
            try {
                pesquisar();
            } catch (IbsException ex) {
                ExibirParaUsuario.getInstance().erro(me, ex, ex.getMessage(), false);
            }
        }
    }//GEN-LAST:event_chkEstoqueActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JComboBox cboTabelaVigente;
    private javax.swing.JCheckBox chkEstoque;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JTabbedPane tbtPrecos;
    private javax.swing.JTextField txtPesquisa;
    // End of variables declaration//GEN-END:variables

    private void pesquisar() throws IbsException {
        new Thread(new Runnable() {

            @Override
            public void run() {
                HibernateUtil hu = HibernateUtil.getInstance();
                try {
                    hu.beginTransaction();
                    PrecoFranquia precoFranquia = (PrecoFranquia) cboTabelaVigente.getSelectedItem();
                    hu.getSession().refresh(precoFranquia);
                    List<TipoPrecoFranquia> tipoPrecoFranquias = new ArrayList<TipoPrecoFranquia>(precoFranquia.getTipoPrecoFranquias());
                    Collections.sort(tipoPrecoFranquias);
                    Iterator<TipoPrecoFranquia> i = tipoPrecoFranquias.iterator();
                    tbtPrecos.removeAll();
                    while (i.hasNext()) {
                        TipoPrecoFranquia tipoPrecoFranquia = i.next();
                        PainelTiposPrecoFranquia ptpf = getPainel(tipoPrecoFranquia);
                        if (ptpf.getTbItens().getRowCount() > 0) {
                            tbtPrecos.add(tipoPrecoFranquia.getDescricao(), ptpf);
                        }
                    }
                    hu.commit();
                } catch (HibernateException ex) {
                    hu.rollback();
                    ExibirParaUsuario.getInstance().erro(me, ex, "Erro ao carregar pesquisa!", false);
                }
                Aguarde.getInstance().setVisible(false);
            }
        }).start();
        Aguarde.getInstance().setVisible(true);
    }

    private void carregaVigente() throws IbsException {
        DefaultComboBoxModel cbm = new DefaultComboBoxModel();
        HibernateUtil hu = HibernateUtil.getInstance();
        try {

            List<PrecoFranquia> lst = hu.getSession().createCriteria(PrecoFranquia.class).addOrder(Order.desc("dataVigencia")).list();
            Iterator<PrecoFranquia> it = lst.iterator();
            Date d = null;
            DefaultComboBoxModel boxModel = new DefaultComboBoxModel();
            while (it.hasNext()) {
                PrecoFranquia precoFranquia = it.next();
                if (d == null) {
                    d = precoFranquia.getDataVigencia();
                }
                if (precoFranquia.getDataVigencia().equals(d)) {
                    boxModel.addElement(precoFranquia);
                }
            }
            if (boxModel.getSize() > 1) {
                cbm.addElement("");
            }

            for (int i = 0; i < boxModel.getSize(); i++) {
                cbm.addElement(boxModel.getElementAt(i));
            }
            cboTabelaVigente.setModel(cbm);
            if (cboTabelaVigente.getSelectedItem() != null && !cboTabelaVigente.getSelectedItem().equals("")) {
                pesquisar();
            }
        } catch (HibernateException ex) {
            throw new IbsException("Erro ao carregar vigente!", ex);
        }
    }

    private PainelTiposPrecoFranquia getPainel(TipoPrecoFranquia tipoPrecoFranquia) {
        PainelTiposPrecoFranquia ptpf = new PainelTiposPrecoFranquia();
        List<ProdutoPrecoFranquia> produtos = new ArrayList<>();
        DefaultTableModel dtm = new DefaultTableModel() {
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return false;
            }
        };
        dtm.addColumn("Estoque");
        dtm.addColumn("Produto");
        dtm.addColumn("Gama");
        List<ItensPrecoFranquia> itensPrecoFranquias = new ArrayList<ItensPrecoFranquia>(tipoPrecoFranquia.getItensPrecoFranquias());

        DefaultComboBoxModel<ProdutoPrecoFranquia> modelProduto = new DefaultComboBoxModel();

        Iterator<ItensPrecoFranquia> itpf = itensPrecoFranquias.iterator();
        DefaultComboBoxModel<String> colunaPlanoUnica = new DefaultComboBoxModel();
        while (itpf.hasNext()) {
            ItensPrecoFranquia itensPrecoFranquia = itpf.next();
            if (colunaPlanoUnica.getIndexOf(itensPrecoFranquia.getPlanoPrecoFranquia().getDescricao()) == -1) {
                dtm.addColumn(itensPrecoFranquia.getPlanoPrecoFranquia().getDescricao().toUpperCase() + " À VISTA");
                dtm.addColumn(itensPrecoFranquia.getPlanoPrecoFranquia().getDescricao().toUpperCase() + " À PRAZO");
                colunaPlanoUnica.addElement(itensPrecoFranquia.getPlanoPrecoFranquia().getDescricao());
            }
            if (modelProduto.getIndexOf(itensPrecoFranquia.getProdutoPrecoFranquia()) == -1) {
                modelProduto.addElement(itensPrecoFranquia.getProdutoPrecoFranquia());
            }
        }
        int i = 0;
        for (int p = 0; p < modelProduto.getSize(); p++) {
            ProdutoPrecoFranquia produto = modelProduto.getElementAt(p);
            String produtoDescricao = produto.getFabricante() + " " + produto.getModelo();
            String gamaDescricao = produto.getGama();
            if (produtoDescricao.toUpperCase().contains(txtPesquisa.getText().toUpperCase())) {
                int estoque = 0;
                if (produto.getProdutos().size() > 0) {
                    Iterator<ItemCompra> iItemCompra = new ArrayList<Produto>(produto.getProdutos()).get(0).getItemCompras().iterator();
                    while (iItemCompra.hasNext()) {
                        ItemCompra itemCompra = iItemCompra.next();
                        if (itemCompra.getSituacao() == SituacaoItemCompraVenda.DISPONIVEL.getId()) {
                            estoque++;
                        }
                    }
                }

                //Tras somente o que tiver estoque
                if (chkEstoque.isSelected()) {
                    if (estoque > 0) {
                        dtm.addRow(new Object[]{});
                        dtm.setValueAt(estoque, i, 0);
                        dtm.setValueAt(produtoDescricao, i, 1);
                        dtm.setValueAt(gamaDescricao, i, 2);
                        for (int col = 3; col < dtm.getColumnCount(); col++) {
                            for (ItensPrecoFranquia itensPrecoFranquia : itensPrecoFranquias) {
                                String descricao = itensPrecoFranquia.getProdutoPrecoFranquia().getFabricante() + " " + itensPrecoFranquia.getProdutoPrecoFranquia().getModelo();
                                if (dtm.getValueAt(i, 1).equals(descricao)
                                        && dtm.getColumnName(col).equalsIgnoreCase(itensPrecoFranquia.getPlanoPrecoFranquia().getDescricao() + " " + OpcoesTipoValorPrecoFranquia.A_VISTA.toString())
                                        && itensPrecoFranquia.getTipoPagamento().getDescricao().equals(OpcoesTipoValorPrecoFranquia.A_VISTA.toString())) {
                                    dtm.setValueAt(Util.getFormatoMoeda(itensPrecoFranquia.getValor()), i, col);
                                }
                                if (dtm.getValueAt(i, 1).equals(descricao)
                                        && dtm.getColumnName(col).equalsIgnoreCase(itensPrecoFranquia.getPlanoPrecoFranquia().getDescricao() + " " + OpcoesTipoValorPrecoFranquia.A_PRAZO.toString())
                                        && itensPrecoFranquia.getTipoPagamento().getDescricao().equals(OpcoesTipoValorPrecoFranquia.A_PRAZO.toString())) {
                                    dtm.setValueAt(Util.getFormatoMoeda(itensPrecoFranquia.getValor()), i, col);
                                }
                            }
                        }
                        produtos.add(produto);
                        i++;
                    }
                } else {
                    dtm.addRow(new Object[]{});
                    dtm.setValueAt(estoque, i, 0);
                    dtm.setValueAt(produtoDescricao, i, 1);
                    dtm.setValueAt(gamaDescricao, i, 2);
                    for (int col = 3; col < dtm.getColumnCount(); col++) {
                        for (ItensPrecoFranquia itensPrecoFranquia : itensPrecoFranquias) {
                            String descricao = itensPrecoFranquia.getProdutoPrecoFranquia().getFabricante() + " " + itensPrecoFranquia.getProdutoPrecoFranquia().getModelo();
                            if (dtm.getValueAt(i, 1).equals(descricao)
                                    && dtm.getColumnName(col).equalsIgnoreCase(itensPrecoFranquia.getPlanoPrecoFranquia().getDescricao() + " " + OpcoesTipoValorPrecoFranquia.A_VISTA.toString())
                                    && itensPrecoFranquia.getTipoPagamento().getDescricao().equals(OpcoesTipoValorPrecoFranquia.A_VISTA.toString())) {
                                dtm.setValueAt(Util.getFormatoMoeda(itensPrecoFranquia.getValor()), i, col);
                            }
                            if (dtm.getValueAt(i, 1).equals(descricao)
                                    && dtm.getColumnName(col).equalsIgnoreCase(itensPrecoFranquia.getPlanoPrecoFranquia().getDescricao() + " " + OpcoesTipoValorPrecoFranquia.A_PRAZO.toString())
                                    && itensPrecoFranquia.getTipoPagamento().getDescricao().equals(OpcoesTipoValorPrecoFranquia.A_PRAZO.toString())) {
                                dtm.setValueAt(Util.getFormatoMoeda(itensPrecoFranquia.getValor()), i, col);
                            }
                        }
                    }
                    produtos.add(produto);
                    i++;
                }
            }
        }

        ptpf.getTbItens().setModel(dtm);
        ptpf.lblLinhas.setText("Linhas : " + dtm.getRowCount());
        ptpf.getTbItens().setDefaultRenderer(Object.class, new RenderVisualizaPrecoFranquia());
        ptpf.produtos = produtos;
        DimencionaTB.executar(ptpf.getTbItens());
        return ptpf;
    }
}
