/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.view.tabelapreco.operadora;

import br.com.cresceritsolutions.ibspdv.util.ImagesUtil;
import java.awt.Component;
import java.awt.Point;
import java.awt.Toolkit;
import javax.swing.JDialog;
import javax.swing.JInternalFrame;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

/**
 *
 * @author walsirene
 */
public class Class_Dialog extends JDialog {

    public Class_Dialog(Component c, String titulo) {
        super.setUndecorated(true);
        super.setModal(true);
        super.setSize(c.getPreferredSize().width, c.getPreferredSize().height + 30);
        super.setLocationRelativeTo(null);
        JInternalFrame frame = new JInternalFrame();
        frame.setFrameIcon(ImagesUtil.getInstance().getMarcaIcone());
        frame.setTitle(titulo);
        frame.add(c);
        frame.setClosable(true);
        frame.addInternalFrameListener(new InternalFrameListener() {
            @Override
            public void internalFrameClosing(InternalFrameEvent e) {
                dispose();
            }

            @Override
            public void internalFrameOpened(InternalFrameEvent e) {
            }

            @Override
            public void internalFrameClosed(InternalFrameEvent e) {
            }

            @Override
            public void internalFrameIconified(InternalFrameEvent e) {
            }

            @Override
            public void internalFrameDeiconified(InternalFrameEvent e) {
            }

            @Override
            public void internalFrameActivated(InternalFrameEvent e) {
            }

            @Override
            public void internalFrameDeactivated(InternalFrameEvent e) {
            }
        });
        setContentPane(frame);
        frame.setVisible(true);
    }

    public Class_Dialog(Component c, boolean unDecoreted, boolean modal) {
        super.setUndecorated(unDecoreted);
        super.setModal(modal);
        super.setIconImage(ImagesUtil.getInstance().getMarcaIcone().getImage());
        super.setSize(c.getPreferredSize().width, c.getPreferredSize().height + 30);
        super.add(c);
    }

    @Override
    public void setLocation(Point p) {
        if (p.getX() + getWidth() > Toolkit.getDefaultToolkit().getScreenSize().getWidth()) {
            double x = Toolkit.getDefaultToolkit().getScreenSize().getWidth() - getWidth();
            p.setLocation(x, p.getY());
        }
        if (p.getY() + getHeight() > Toolkit.getDefaultToolkit().getScreenSize().getHeight()) {
            double y = Toolkit.getDefaultToolkit().getScreenSize().getHeight() - getHeight();
            p.setLocation(p.getX(), y);
        }
        super.setLocation(p);
    }
}
