/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.view.localizadores;

import br.com.cresceritsolutions.ibspdv.models.UI.DialogIBS;
import br.com.cresceritsolutions.ibspdv.models.UI.JTextIBS;
import br.com.cresceritsolutions.ibspdv.models.enuns.EnumTipoVenda;
import br.com.cresceritsolutions.ibspdv.models.enuns.SituacaoItemCompraVenda;
import br.com.cresceritsolutions.ibspdv.models.enuns.TipoPadrao;
import br.com.cresceritsolutions.ibspdv.models.hb.ItemCompra;
import br.com.cresceritsolutions.ibspdv.models.hb.Produto;
import br.com.cresceritsolutions.ibspdv.nfe.EmissaoNFE;
import br.com.cresceritsolutions.ibspdv.renders.RenderPadrao;
import br.com.cresceritsolutions.ibspdv.util.DimencionaTB;
import br.com.cresceritsolutions.ibspdv.util.ExibirParaUsuario;
import br.com.cresceritsolutions.ibspdv.util.HibernateUtil;
import br.com.cresceritsolutions.ibspdv.util.IbsException;
import br.com.cresceritsolutions.ibspdv.view.cadastro.CadProduto;
import br.com.cresceritsolutions.ibspdv.view.core.PrincipalPDV;
import br.com.cresceritsolutions.ibspdv.view.venda.ConfiguraItemVendaDiversos;
import br.com.cresceritsolutions.ibspdv.view.venda.FinalizaVenda;
import br.com.cresceritsolutions.ibspdv.view.venda.PreVenda;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.swing.table.DefaultTableModel;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Gutem
 */
public class LocalizaItemVenda extends DialogIBS {

    private static LocalizaItemVenda me;

    public static LocalizaItemVenda getInstance() {
        if (me == null) {
            me = new LocalizaItemVenda();
        }
        return me;
    }
    private List<Object> dados;

    /**
     * Creates new form LocalizaItemVenda
     */
    private LocalizaItemVenda() {
        initComponents();
        tbResultado.setDefaultRenderer(Object.class, RenderPadrao.getInstance());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tbResultado = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        lblLinhas = new javax.swing.JLabel();
        jToolBar3 = new javax.swing.JToolBar();
        btnPesquisar = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        txtPesquisa = new JTextIBS(100)
        ;
        jToolBar1 = new javax.swing.JToolBar();
        btnOK = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        tbResultado.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        tbResultado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Referência", "Descrição", "QTD"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbResultado.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tbResultado.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tbResultado.getTableHeader().setResizingAllowed(false);
        tbResultado.getTableHeader().setReorderingAllowed(false);
        tbResultado.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbResultadoMouseClicked(evt);
            }
        });
        tbResultado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbResultadoKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tbResultado);
        if (tbResultado.getColumnModel().getColumnCount() > 0) {
            tbResultado.getColumnModel().getColumn(0).setResizable(false);
            tbResultado.getColumnModel().getColumn(1).setResizable(false);
            tbResultado.getColumnModel().getColumn(2).setResizable(false);
            tbResultado.getColumnModel().getColumn(2).setPreferredWidth(60);
        }

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/product.png"))); // NOI18N
        jLabel1.setText("Selecione um produto");
        jLabel1.setOpaque(true);

        lblLinhas.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        lblLinhas.setText("Linhas : 0");

        jToolBar3.setFloatable(false);
        jToolBar3.setRollover(true);

        btnPesquisar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/search_24x24.png"))); // NOI18N
        btnPesquisar.setToolTipText("Pesquisar");
        btnPesquisar.setFocusable(false);
        btnPesquisar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnPesquisar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisarActionPerformed(evt);
            }
        });
        jToolBar3.add(btnPesquisar);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/add-file_24x24.png"))); // NOI18N
        jButton2.setToolTipText("Novo Produto | F1");
        jButton2.setFocusable(false);
        jButton2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jToolBar3.add(jButton2);

        txtPesquisa.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        txtPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPesquisaActionPerformed(evt);
            }
        });
        txtPesquisa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPesquisaKeyPressed(evt);
            }
        });

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        btnOK.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/checkmark_24x24.png"))); // NOI18N
        btnOK.setToolTipText("Selecionar");
        btnOK.setFocusable(false);
        btnOK.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnOK.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOKActionPerformed(evt);
            }
        });
        jToolBar1.add(btnOK);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/close_24x24.png"))); // NOI18N
        jButton1.setToolTipText("Cancelar");
        jButton1.setFocusable(false);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 456, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblLinhas, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txtPesquisa)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jToolBar3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPesquisa, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jToolBar3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblLinhas)
                    .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisarActionPerformed
        try {
            pesquisar();
        } catch (IbsException ex) {
            ExibirParaUsuario.getInstance().erro(PrincipalPDV.getInstance(), ex, ex.getMessage(), false);
        }
    }//GEN-LAST:event_btnPesquisarActionPerformed

    private void txtPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPesquisaActionPerformed
        try {
            pesquisar();
        } catch (IbsException ex) {
            ExibirParaUsuario.getInstance().erro(PrincipalPDV.getInstance(), ex, ex.getMessage(), false);
        }
    }//GEN-LAST:event_txtPesquisaActionPerformed

    private void btnOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOKActionPerformed
        if (tbResultado.getSelectedRow() != -1) {
            try {
                selecionar((Produto) dados.get(tbResultado.getSelectedRow()));
            } catch (IbsException ex) {
                ExibirParaUsuario.getInstance().erro(PrincipalPDV.getInstance(), ex, ex.getMessage(), false);
            }
        }
    }//GEN-LAST:event_btnOKActionPerformed

    private void tbResultadoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbResultadoMouseClicked
        if (tbResultado.getSelectedRow() != -1) {
            permiteAlterar(true);
            if (evt.getClickCount() == 2) {
                try {
                    Produto produto = (Produto) dados.get(tbResultado.getSelectedRow());
                    if (produto.getMarca() != null) {
                        selecionar(produto);
                    }else{
                        ExibirParaUsuario.getInstance().atencao(me, "Este produto não está com o cadastro finalizado!\nSelecione a marca desse produto!");
                    }

                } catch (IbsException ex) {
                    ExibirParaUsuario.getInstance().erro(PrincipalPDV.getInstance(), ex, ex.getMessage(), false);
                }
            }
        }
    }//GEN-LAST:event_tbResultadoMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        setVisible(false);
        if (PrincipalPDV.getInstance().getConcomitante().equals(EnumTipoVenda.CONCOMITANTE)) {
            FinalizaVenda.getInstance().setVisible(true);
        } else if (PrincipalPDV.getInstance().getConcomitante().equals(EnumTipoVenda.PRE_VENDA)) {
            PreVenda.getInstance().setVisible(true);
        } else if (PrincipalPDV.getInstance().getConcomitante().equals(EnumTipoVenda.NFE)) {
            EmissaoNFE.getInstance().setVisible(true);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        try {
            pesquisar();
        } catch (IbsException ex) {
            ExibirParaUsuario.getInstance().erro(PrincipalPDV.getInstance(), ex, ex.getMessage(), false);
        }
    }//GEN-LAST:event_formWindowOpened

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        setVisible(false);
        if (PrincipalPDV.getInstance().getConcomitante().equals(EnumTipoVenda.CONCOMITANTE)) {
            FinalizaVenda.getInstance().setVisible(true);
        } else if (PrincipalPDV.getInstance().getConcomitante().equals(EnumTipoVenda.PRE_VENDA)) {
            PreVenda.getInstance().setVisible(true);
        } else if (PrincipalPDV.getInstance().getConcomitante().equals(EnumTipoVenda.NFE)) {
            EmissaoNFE.getInstance().setVisible(true);
        }
    }//GEN-LAST:event_formWindowClosing

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        CadProduto.newInstance().novo().setVisible(true);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void txtPesquisaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPesquisaKeyPressed
        //F1
        if (evt.getKeyCode() == 112) {
            CadProduto.newInstance().novo().setVisible(true);
        }
    }//GEN-LAST:event_txtPesquisaKeyPressed

    private void tbResultadoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbResultadoKeyPressed
        if (tbResultado.getSelectedRow() != -1) {
            permiteAlterar(true);
            if (evt.getKeyCode() == 10) {
                try {
                    selecionar((Produto) dados.get(tbResultado.getSelectedRow()));
                } catch (IbsException ex) {
                    ExibirParaUsuario.getInstance().erro(PrincipalPDV.getInstance(), ex, ex.getMessage(), false);
                }
            }
        }
    }//GEN-LAST:event_tbResultadoKeyPressed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnOK;
    private static javax.swing.JButton btnPesquisar;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JToolBar jToolBar3;
    private javax.swing.JLabel lblLinhas;
    private javax.swing.JTable tbResultado;
    public javax.swing.JTextField txtPesquisa;
    // End of variables declaration//GEN-END:variables

    private void selecionar(Produto o) throws IbsException {

        setVisible(false);
        Produto produto = o;
        HibernateUtil hu = HibernateUtil.getInstance();
        String subTipo = null;
        try {
            hu.getSession().refresh(produto);
            subTipo = produto.getMarca().getTipoProduto().getTipoPadrao();
        } catch (HibernateException ex) {
            ExibirParaUsuario.getInstance().erro(me, ex, "Erro de conexão com a base de dados!", false);
        }
        if (subTipo != null && subTipo.equals(TipoPadrao.CELULAR.toString())) {
            LocalizaImei.newInstance().seleciona(produto).setVisible(true);
        } else if (subTipo != null && subTipo.equals(TipoPadrao.CHIP_NP.toString())) {
            LocalizaIccid.getInstance().seleciona(produto).setVisible(true);
        } else if (subTipo != null && subTipo.equals(TipoPadrao.CHIP_PRE_ATIVO.toString())) {
            LocalizaIccid.getInstance().seleciona(produto).setVisible(true);
        } else if (subTipo != null && subTipo.equals(TipoPadrao.DIVERSO.toString())) {
            ConfiguraItemVendaDiversos.newInstance().seleciona(produto).setVisible(true);
        }

    }

    public void pesquisar() throws IbsException {
        DefaultTableModel model = (DefaultTableModel) tbResultado.getModel();
        model.setRowCount(0);
        HibernateUtil hu = HibernateUtil.getInstance();
        try {
            dados = hu.getSession().createCriteria(Produto.class, "produto").setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                    .add(Restrictions.or(
                                    Restrictions.like("produto.referencia", "%" + txtPesquisa.getText() + "%"),
                                    Restrictions.like("produto.descricao", "%" + txtPesquisa.getText() + "%")))
                    //.createCriteria("itemCompras", "itemCompra").add(Restrictions.eq("itemCompra.situacao", SituacaoItemCompraVenda.DISPONIVEL.getId()))
                    .list();
            Iterator<Object> produtos = dados.iterator();
            while (produtos.hasNext()) {
                Produto produto = (Produto) produtos.next();
                model.addRow(new Object[]{produto.getReferencia(), produto.getDescricao(), getProdutosDisponiveis(produto.getItemCompras())});
            }
            lblLinhas.setText("Linhas : " + model.getRowCount());
            tbResultado.setModel(model);
            tbResultado.setDefaultRenderer(Object.class, new RenderPadrao());
            DimencionaTB.executar(tbResultado);
            permiteAlterar(false);
        } catch (HibernateException ex) {
            throw new IbsException("Erro ao carregar pesquisa!", ex);
        }

    }

    private void permiteAlterar(boolean b) {
        btnOK.setEnabled(b);
    }

    private int getProdutosDisponiveis(Set itemCompras) {
        Iterator<ItemCompra> i = itemCompras.iterator();
        int total = 0;
        while (i.hasNext()) {
            ItemCompra compra = i.next();
            if (compra.getSituacao() == SituacaoItemCompraVenda.DISPONIVEL.getId()) {
                total++;
            }
        }
        return total;
    }

    public LocalizaItemVenda newInstance() {
        me = new LocalizaItemVenda();
        return me;
    }
}
