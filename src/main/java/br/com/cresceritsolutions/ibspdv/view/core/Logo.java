/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.view.core;

import br.com.cresceritsolutions.ibspdv.controller.ControllerAtualizacao;
import br.com.cresceritsolutions.ibspdv.controller.ControllerIniciaSistema;
import br.com.cresceritsolutions.ibspdv.controller.ControllerValidaSistema;
import br.com.cresceritsolutions.ibspdv.controller.classes.Class_Nome_Sistema;
import br.com.cresceritsolutions.ibspdv.models.UI.FrameIBS;
import br.com.cresceritsolutions.ibspdv.models.enuns.Ramo;
import br.com.cresceritsolutions.ibspdv.models.hb.Usuario;
import br.com.cresceritsolutions.ibspdv.util.CriptografiaUtil;
import br.com.cresceritsolutions.ibspdv.util.ExibirParaUsuario;
import br.com.cresceritsolutions.ibspdv.util.HibernateUtil;
import br.com.cresceritsolutions.ibspdv.util.IbsException;
import br.com.cresceritsolutions.ibspdv.util.Util;
import br.com.cresceritsolutions.ibspdv.util.VariaveisDoSistema;
import static br.com.cresceritsolutions.ibspdv.util.VariaveisDoSistema.AUXILIAR;
import jACBrFramework.ACBrException;
import java.awt.Desktop;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URI;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;

/**
 *
 * @author gutemberg
 */
public class Logo extends FrameIBS {

    static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(Logo.class);

    private static boolean valida() {
        try {
            SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");//se quiser apenas a hora apague o formato da data  
            String ntpServer = "a.st1.ntp.br";//servidor de horario brasileiro  
            NTPUDPClient timeClient = new NTPUDPClient();
            InetAddress inetAddress = InetAddress.getByName(ntpServer);
            TimeInfo timeInfo = timeClient.getTime(inetAddress);
            long returnTime = timeInfo.getReturnTime();
            Date time = new Date(returnTime);
            VariaveisDoSistema.AUXILIAR.setProperty("out.data_atual", sdf1.format(time));
            CriptografiaUtil.getInstance().criptografar("conf/auxiliar.txt", AUXILIAR);
        } catch (UnknownHostException ex) {
        } catch (IOException ex) {
        } catch (Exception ex) {
        }
        Date data = Util.getData(VariaveisDoSistema.AUXILIAR.getProperty("out.validade"));
        return data.after(Util.getData(VariaveisDoSistema.AUXILIAR.getProperty("out.data_atual")));
    }

    private static void addAcesso() {
        int acessos = Integer.parseInt(VariaveisDoSistema.AUXILIAR.getProperty("out.contador_acessos_apos_queda"));
        VariaveisDoSistema.AUXILIAR.setProperty("out.contador_acessos_apos_queda", String.valueOf(acessos++));
        try {
            CriptografiaUtil.getInstance().criptografar("conf/auxiliar.txt", AUXILIAR);
        } catch (Exception ex) {
            Logger.getLogger(Logo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void resetaContadorAcessos() {
        if (Integer.parseInt(VariaveisDoSistema.AUXILIAR.getProperty("out.contador_acessos_apos_queda")) > 0) {
            VariaveisDoSistema.AUXILIAR.setProperty("out.contador_acessos_apos_queda", "0");
            try {
                CriptografiaUtil.getInstance().criptografar("conf/auxiliar.txt", AUXILIAR);
            } catch (Exception ex) {
                Logger.getLogger(Logo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private static void modoFiscal() {
        //Abre janela
        Logo.getInstance().setVisible(true);
        //Carrega configurações iniciais ex(valida tef,ecf, le arquivo auxiliar...)
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ControllerIniciaSistema.getInstance().validaSistema();
                    getInstance().getBarraProgresso().setValue(100);
                    getInstance().setVisible(false);
                    VariaveisDoSistema.INICIALIZA_MODO_FISCAL = true;
                    Login.getInstance().setVisible(true);
                } catch (ACBrException | IbsException ex) {
                    ExibirParaUsuario.getInstance().erro(me, ex, ex.getMessage(), true);
                }
            }

        }).start();
    }

    private static void modoNormal() {
        //Abre janela
        Logo.getInstance().setVisible(true);
        //Carrega configurações iniciais ex(valida tef,ecf, le arquivo auxiliar...)
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ControllerIniciaSistema.getInstance().validaSistema();
                    getInstance().getBarraProgresso().setValue(100);
                    getInstance().setVisible(false);
                    VariaveisDoSistema.RAMO = Ramo.get(Integer.parseInt(VariaveisDoSistema.AUXILIAR.getProperty("out.ramo")));
                    VariaveisDoSistema.INICIALIZA_MODO_FISCAL = false;
                    Login.getInstance().setVisible(true);
                } catch (ACBrException | IbsException ex) {
                    ExibirParaUsuario.getInstance().erro(me, ex, ex.getMessage(), true);
                }
            }

        }).start();
    }

    public static void acessaNormal() throws Exception {
        VariaveisDoSistema.AUXILIAR.setProperty("out.ultima_atualizacao", "11/02/2015");
        VariaveisDoSistema.AUXILIAR.setProperty("paf.versao", "1.3.1");
        CriptografiaUtil.getInstance().criptografar("conf/auxiliar.txt", AUXILIAR);

        //Atualiza arquivo auxiliar com informações de atualização
        ControllerAtualizacao.getInstance().executar();
        Date ultimaAtualizacao = Util.getData(VariaveisDoSistema.AUXILIAR.getProperty("out.ultima_atualizacao"));
        Date atualizacaoCorrente = Util.getData(VariaveisDoSistema.AUXILIAR.getProperty("out.atualizacao_corrente"));
        if (ultimaAtualizacao.before(atualizacaoCorrente)) {
            if (ExibirParaUsuario.getInstance().confirmacao(me, "Há uma nova atualização para o sistema!\nDeseja atualizar o sistema agora!")) {
                Desktop.getDesktop().browse(new URI(VariaveisDoSistema.AUXILIAR.getProperty("out.link_atualizacao")));
                System.exit(0);
            }
        }
        //Validaçao online
        ControllerValidaSistema.getInstance().executar();
        Boolean valido = valida();
        if (valido) {
            resetaContadorAcessos();
            modoNormal();
        } else {
            ExibirParaUsuario.getInstance().atencao(me, "O sistema está com a data vencida!\nContate o administrador do sistema!");
        }
    }

    /**
     * Creates new form Logo
     */
    public Logo() {
        initComponents();
        lblLogo.setText(Class_Nome_Sistema.getNomeLogo());
        setResizable(false);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblLogo = new javax.swing.JLabel();
        barraProgresso = new javax.swing.JProgressBar();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);
        setResizable(false);

        lblLogo.setBackground(java.awt.Color.white);
        lblLogo.setFont(new java.awt.Font("Times New Roman", 3, 60)); // NOI18N
        lblLogo.setForeground(new java.awt.Color(1, 6, 135));
        lblLogo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblLogo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Logo 128.png"))); // NOI18N
        lblLogo.setText("<html>IBS-PDV<br><font font-size=15 color='000000' >Professional</font></html>");
        lblLogo.setOpaque(true);

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Integrated Business System - Professional");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(barraProgresso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(lblLogo, javax.swing.GroupLayout.DEFAULT_SIZE, 515, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(lblLogo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(barraProgresso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JProgressBar barraProgresso;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel lblLogo;
    // End of variables declaration//GEN-END:variables
    private static Logo me;

    //Nova instancia
    public static Logo getInstance() {
        if (me == null) {
            me = new Logo();
        }
        return me;
    }

    //Metodo que inicia o sitema
    public static void main(String[] args) {

        VariaveisDoSistema.RECRIAR_ARQUIVO_AUXILIAR = true;
        try {

            //Carrega configurações pre setadas do sistema ex(Modelo swing UI,empresa,validação...)
            ControllerIniciaSistema.getInstance().getLoad();
            ControllerIniciaSistema.getInstance().carregaAuxiliar();
            HibernateUtil hu = HibernateUtil.getInstance();
            try {

                List<Usuario> usuarios = hu.getSession().createQuery("from Usuario").list();
                if (usuarios.isEmpty()) {
                    PrimeiroAcesso.newInstance().setVisible(true);
                } else {
                    acessaNormal();
                }
            } catch (HibernateException ex) {
                throw new IbsException("Erro ao verificar identidade na base de dados!", ex);
            }
        } catch (Exception ex) {
            ExibirParaUsuario.getInstance().atencao(getInstance(), "Verifique sua conexão!\nO sistema trabalhará em modo off-line!");
            if (Integer.parseInt(VariaveisDoSistema.AUXILIAR.getProperty("out.contador_acessos_apos_queda"))
                    <= Integer.parseInt(VariaveisDoSistema.AUXILIAR.getProperty("out.acessos_apos_queda_internet"))) {
                addAcesso();
                modoNormal();
            } else {
                ExibirParaUsuario.getInstance().atencao(me, "O sistema inicializará apenas com as opções fiscais!");
                modoFiscal();
            }
        }

    }

    /**
     * @return the barraProgresso
     */
    public javax.swing.JProgressBar getBarraProgresso() {
        return barraProgresso;
    }

    /**
     * @param barraProgresso the barraProgresso to set
     */
    public void setBarraProgresso(javax.swing.JProgressBar barraProgresso) {
        this.barraProgresso = barraProgresso;
    }

}
