/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * int_Cad_Produtos.java
 *
 * Created on 19/04/2011, 17:05:36
 */
package br.com.cresceritsolutions.ibspdv.view.cadastro;

import br.com.cresceritsolutions.ibspdv.models.UI.ComboBoxIBS;
import br.com.cresceritsolutions.ibspdv.models.UI.DialogIBS;
import br.com.cresceritsolutions.ibspdv.models.UI.JCheckListIBS;
import br.com.cresceritsolutions.ibspdv.models.UI.JTextAreaIBS;
import br.com.cresceritsolutions.ibspdv.models.UI.JTextIBS;
import br.com.cresceritsolutions.ibspdv.models.hb.Plano;
import br.com.cresceritsolutions.ibspdv.models.hb.PlanoPrecoFranquia;
import br.com.cresceritsolutions.ibspdv.models.hb.TipoVenda;
import br.com.cresceritsolutions.ibspdv.util.ExibirParaUsuario;
import br.com.cresceritsolutions.ibspdv.util.HibernateUtil;
import br.com.cresceritsolutions.ibspdv.util.IbsException;
import br.com.cresceritsolutions.ibspdv.util.Util;
import static br.com.cresceritsolutions.ibspdv.view.cadastro.CadPlano.getInstance;
import br.com.cresceritsolutions.ibspdv.view.core.PrincipalPDV;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Gutemberg
 */
public final class CadPlano extends DialogIBS {

    private static final long serialVersionUID = 1L;
    private static CadPlano me;
    private Plano plano = new Plano();

    public static CadPlano newInstance() {
        me = new CadPlano();
        return me;
    }

    public static CadPlano getInstance() {
        if (me == null) {
            me = new CadPlano();
        }
        return me;
    }

    /**
     * Creates new form int_Cad_Produtos
     */
    public CadPlano() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnAddMarca = new javax.swing.JButton();
        jToolBar1 = new javax.swing.JToolBar();
        btnSalvar = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        lblGrupo = new javax.swing.JLabel();
        txtDescricao = new JTextIBS(45);
        lblInfoProduto = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtObservacoes = new JTextAreaIBS(100);
        txtValor = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtDescricao1 = new JTextIBS(45);
        jPanel2 = new javax.swing.JPanel();
        scrollPaneTipoVenda = new javax.swing.JScrollPane();
        jCheckBox1 = new javax.swing.JCheckBox();
        jLabel1 = new javax.swing.JLabel();
        cboPlanoTabelaPreco = new ComboBoxIBS();
        chkAtivo = new javax.swing.JCheckBox();
        chkServicoPacote = new javax.swing.JCheckBox();
        chkComissaoAdicional = new javax.swing.JCheckBox();
        lblTitulo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        btnAddMarca.setText("...");
        btnAddMarca.setToolTipText("Adicionar Marca");

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        btnSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/save_24x24.png"))); // NOI18N
        btnSalvar.setToolTipText("Salvar");
        btnSalvar.setFocusable(false);
        btnSalvar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSalvar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });
        jToolBar1.add(btnSalvar);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/close_24x24.png"))); // NOI18N
        jButton1.setToolTipText("Sair");
        jButton1.setFocusable(false);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lblGrupo.setText("Descrição Cupom");

        txtDescricao.setBackground(new java.awt.Color(255, 255, 204));
        txtDescricao.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtDescricaoKeyReleased(evt);
            }
        });

        lblInfoProduto.setText("Observação");

        txtObservacoes.setColumns(20);
        txtObservacoes.setRows(5);
        jScrollPane1.setViewportView(txtObservacoes);

        txtValor.setText("0,00");
        txtValor.setToolTipText("Valor mensal do plano");
        txtValor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtValorKeyReleased(evt);
            }
        });

        jLabel2.setText("Valor Mensal");

        jLabel3.setText("Descrição");

        txtDescricao1.setBackground(new java.awt.Color(255, 255, 204));
        txtDescricao1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtDescricao1KeyReleased(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Tipos de venda vinculado"));

        jCheckBox1.setText("Seleciona todos");
        jCheckBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scrollPaneTipoVenda)
                    .addComponent(jCheckBox1))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrollPaneTipoVenda, javax.swing.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBox1))
        );

        jLabel1.setText("Plano Tabela de Preços");

        chkAtivo.setSelected(true);
        chkAtivo.setText("Ativo");
        chkAtivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkAtivoActionPerformed(evt);
            }
        });

        chkServicoPacote.setText("Serviço/Pacote");
        chkServicoPacote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkServicoPacoteActionPerformed(evt);
            }
        });

        chkComissaoAdicional.setText("Comissão adicional");
        chkComissaoAdicional.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkComissaoAdicionalActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(chkAtivo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(chkServicoPacote)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(chkComissaoAdicional))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(cboPlanoTabelaPreco, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtValor, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel2))))
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblInfoProduto)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(txtDescricao))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblGrupo)
                                    .addComponent(txtDescricao1))))
                        .addGap(10, 10, 10))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chkAtivo)
                    .addComponent(chkServicoPacote)
                    .addComponent(chkComissaoAdicional))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(lblGrupo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDescricao1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cboPlanoTabelaPreco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtValor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblInfoProduto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
                .addContainerGap())
        );

        lblTitulo.setBackground(new java.awt.Color(255, 255, 255));
        lblTitulo.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        lblTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitulo.setText("CADASTRO DE PLANO/SERVIÇOS/PACOTE");
        lblTitulo.setAutoscrolls(true);
        lblTitulo.setOpaque(true);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAddMarca, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(lblTitulo)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(149, 149, 149)
                        .addComponent(btnAddMarca)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void chkAtivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkAtivoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chkAtivoActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        try {
            if (valida()) {
                try {
                    salvar();
                    ExibirParaUsuario.getInstance().mensagem(PrincipalPDV.getInstance(), "Registro salvo com sucesso!", true);
                    PesquisaCadPlano.getInstance().pesquisar();
                    getInstance().dispose();
                } catch (IbsException ex) {
                    ExibirParaUsuario.getInstance().erro(PrincipalPDV.getInstance(), ex, ex.getMessage(), false);
                }
            }
        } catch (IbsException ex) {
            ExibirParaUsuario.getInstance().erro(PrincipalPDV.getInstance(), ex, ex.getMessage(), false);
        }
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void formInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosing

    }//GEN-LAST:event_formInternalFrameClosing

    private void txtDescricaoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDescricaoKeyReleased
        txtDescricao1.setText(txtDescricao.getText());
    }//GEN-LAST:event_txtDescricaoKeyReleased

    private void formInternalFrameActivated(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameActivated

    }//GEN-LAST:event_formInternalFrameActivated

    private void txtValorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtValorKeyReleased
        txtValor.setText(Util.colocaVirgulaNaSegundaCasa(txtValor.getText()));
    }//GEN-LAST:event_txtValorKeyReleased

    private void txtDescricao1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDescricao1KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDescricao1KeyReleased

    private void jCheckBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox1ActionPerformed
        checkListTipoVenda.selectAll(jCheckBox1.isSelected());
    }//GEN-LAST:event_jCheckBox1ActionPerformed

    private void chkComissaoAdicionalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkComissaoAdicionalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chkComissaoAdicionalActionPerformed

    private void chkServicoPacoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkServicoPacoteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chkServicoPacoteActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddMarca;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JComboBox cboPlanoTabelaPreco;
    private javax.swing.JCheckBox chkAtivo;
    private javax.swing.JCheckBox chkComissaoAdicional;
    private javax.swing.JCheckBox chkServicoPacote;
    private javax.swing.JButton jButton1;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JLabel lblGrupo;
    private javax.swing.JLabel lblInfoProduto;
    public javax.swing.JLabel lblTitulo;
    private javax.swing.JScrollPane scrollPaneTipoVenda;
    public static javax.swing.JTextField txtDescricao;
    public static javax.swing.JTextField txtDescricao1;
    private javax.swing.JTextArea txtObservacoes;
    private javax.swing.JTextField txtValor;
    // End of variables declaration//GEN-END:variables
    private JCheckListIBS checkListTipoVenda;

    public CadPlano novo() throws IbsException {
        plano = new Plano();
        plano.setAtivo(true);
        setTela();
        return me;
    }

    public CadPlano editar(Object o) throws IbsException {
        plano = (Plano) o;
        setTela();
        return me;
    }

    private void salvar() throws IbsException {
        setObjeto();
        HibernateUtil hu = HibernateUtil.getInstance();
        try {
            hu.beginTransaction();
            hu.getSession().saveOrUpdate(plano);
            hu.commit();
        } catch (HibernateException ex) {
            hu.rollback();
            throw new IbsException("Erro inesperado!", ex);
        }
    }

    private void setTela() throws IbsException {
        carregaListaDeTipos();
        carregaPlanosTabelaPrecos();
        chkAtivo.setSelected(plano.isAtivo());
        chkServicoPacote.setSelected(plano.isServicoPacote());
        chkComissaoAdicional.setSelected(plano.isComissaoAdicional());
        txtDescricao.setText(plano.getDescricao());
        txtDescricao1.setText(plano.getDescricaoCupom());
        cboPlanoTabelaPreco.setSelectedItem(plano.getPlanoPrecoFranquia());
        txtValor.setText(Util.getFormatoMoeda(plano.getValorMensal()).replace("R$ ", ""));
        txtObservacoes.setText(plano.getObservacao());
        DefaultComboBoxModel boxModel = new DefaultComboBoxModel(plano.getTipoVendas().toArray());
        for (int i = 0; i < checkListTipoVenda.getListDescription().getModel().getSize(); i++) {
            if (boxModel.getIndexOf(checkListTipoVenda.getListDescription().getModel().getElementAt(i)) != -1) {
                checkListTipoVenda.setSelect(true, i);
            }
        }
        txtDescricao.grabFocus();
    }

    private void setObjeto() {
        plano.setAtivo(chkAtivo.isSelected());
        plano.setServicoPacote(chkServicoPacote.isSelected());
        plano.setDescricao(txtDescricao.getText());
        plano.setDescricaoCupom(txtDescricao1.getText());
        plano.setPlanoPrecoFranquia((PlanoPrecoFranquia) cboPlanoTabelaPreco.getSelectedItem());
        if(txtValor.getText().isEmpty()){
            txtValor.setText("0,00");
        }
        plano.setValorMensal(Double.parseDouble(txtValor.getText().replace(",", ".")));
        plano.setComissaoAdicional(chkComissaoAdicional.isSelected());
        plano.setObservacao(txtObservacoes.getText());
        plano.setTipoVendas(new HashSet(checkListTipoVenda.getListSelects()));
    }

    private void carregaListaDeTipos() throws IbsException {
        HibernateUtil hu = HibernateUtil.getInstance();
        try {
            
            DefaultComboBoxModel<TipoVenda> boxModel = new DefaultComboBoxModel(hu.getSession().createCriteria(TipoVenda.class).list().toArray());
            List<TipoVenda> lstObjetos = new ArrayList<>();
            List<Boolean> lstValores = new ArrayList<>();
            for (int i = 0; i < boxModel.getSize(); i++) {
                lstValores.add(false);
                lstObjetos.add(boxModel.getElementAt(i));
            }
            checkListTipoVenda = new JCheckListIBS();
            checkListTipoVenda.criaCheckList(scrollPaneTipoVenda, lstObjetos.toArray(), lstValores.toArray());
        } catch (HibernateException ex) {
            throw new IbsException("Erro ao carregar lista de tipos!", ex);
        }
    }

    private boolean valida() throws IbsException {
        if (!txtDescricao.getText().isEmpty()) {
            if (!existePlanoComDescricao(txtDescricao.getText())) {
                if (!txtDescricao1.getText().isEmpty()) {
                    return true;
                } else {
                    ExibirParaUsuario.getInstance().atencao(me, "Informe a descrição que sairá no cupom fiscal");
                }
            } else {
                ExibirParaUsuario.getInstance().atencao(me, "Já possui um plano com essa descrição!");
            }
        } else {
            ExibirParaUsuario.getInstance().atencao(me, "Já possui um plano com essa descrição!");
        }
        return false;
    }

    private boolean existePlanoComDescricao(String text) throws IbsException {
        if (plano.getId() != null) {
            return false;
        }
        HibernateUtil hu = HibernateUtil.getInstance();

        try {
            
            return hu.getSession().createCriteria(Plano.class
            ).add(Restrictions.eq("descricao", text)).uniqueResult() != null;
        } catch (HibernateException ex) {
            throw new IbsException("Erro ao localizar plano!", ex);
        }
    }
    
    private void carregaPlanosTabelaPrecos() throws IbsException {
        HibernateUtil hu = HibernateUtil.getInstance();
        try {
            List<PlanoPrecoFranquia> l = hu.getSession().createCriteria(PlanoPrecoFranquia.class).list();
            Collections.sort(l);
            cboPlanoTabelaPreco.setModel(new DefaultComboBoxModel(l.toArray()));
            cboPlanoTabelaPreco.setSelectedItem(null);
        } catch (HibernateException ex) {
            throw new IbsException("Erro ao carregar produtos da tabela de preços!", ex);
        }
    }
}
