/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.view.core;

import acbr.acbrmonitor.EComandoECF;
import br.com.cresceritsolutions.ibspdv.controller.ControllerPaf;
import br.com.cresceritsolutions.ibspdv.controller.ControllerRegraTipoUsuario;
import br.com.cresceritsolutions.ibspdv.controller.ControllerSalvarDocumento;
import br.com.cresceritsolutions.ibspdv.controller.ControllerValidaSistema;
import br.com.cresceritsolutions.ibspdv.controller.PAF;
import br.com.cresceritsolutions.ibspdv.controller.classes.Class_Imagens;
import br.com.cresceritsolutions.ibspdv.models.UI.FrameIBS;
import br.com.cresceritsolutions.ibspdv.models.enuns.EnumTipoVenda;
import br.com.cresceritsolutions.ibspdv.models.hb.Cliente;
import br.com.cresceritsolutions.ibspdv.models.hb.Colaborador;
import br.com.cresceritsolutions.ibspdv.models.hb.TipoUsuario;
import br.com.cresceritsolutions.ibspdv.nfe.ConsultaVendaEmitirNfe;
import br.com.cresceritsolutions.ibspdv.nfe.NovaNotaFiscalEletronica;
import br.com.cresceritsolutions.ibspdv.nfe.PesquisaNfe;
import br.com.cresceritsolutions.ibspdv.util.CriptografiaUtil;
import br.com.cresceritsolutions.ibspdv.util.ExibirParaUsuario;
import br.com.cresceritsolutions.ibspdv.util.HibernateUtil;
import br.com.cresceritsolutions.ibspdv.util.IbsException;
import br.com.cresceritsolutions.ibspdv.util.Util;
import br.com.cresceritsolutions.ibspdv.util.VariaveisDoSistema;
import br.com.cresceritsolutions.ibspdv.view.adm.AlteraSenha;
import br.com.cresceritsolutions.ibspdv.view.ajuda.NotificarErroFabricante;
import br.com.cresceritsolutions.ibspdv.view.ajuda.Sobre;
import br.com.cresceritsolutions.ibspdv.view.cadastro.PesquisaCadBalanco;
import br.com.cresceritsolutions.ibspdv.view.cadastro.PesquisaCadCanalVenda;
import br.com.cresceritsolutions.ibspdv.view.cadastro.PesquisaCadCargo;
import br.com.cresceritsolutions.ibspdv.view.cadastro.PesquisaCadClassificacao;
import br.com.cresceritsolutions.ibspdv.view.cadastro.PesquisaCadClientes;
import br.com.cresceritsolutions.ibspdv.view.cadastro.PesquisaCadColaborador;
import br.com.cresceritsolutions.ibspdv.view.cadastro.PesquisaCadDepartamento;
import br.com.cresceritsolutions.ibspdv.view.cadastro.PesquisaCadEmpresa;
import br.com.cresceritsolutions.ibspdv.view.cadastro.PesquisaCadFormaPagamento;
import br.com.cresceritsolutions.ibspdv.view.cadastro.PesquisaCadFornecedor;
import br.com.cresceritsolutions.ibspdv.view.cadastro.PesquisaCadImposto;
import br.com.cresceritsolutions.ibspdv.view.cadastro.PesquisaCadMarca;
import br.com.cresceritsolutions.ibspdv.view.cadastro.PesquisaCadOferta;
import br.com.cresceritsolutions.ibspdv.view.cadastro.PesquisaCadPlano;
import br.com.cresceritsolutions.ibspdv.view.cadastro.PesquisaCadProduto;
import br.com.cresceritsolutions.ibspdv.view.cadastro.PesquisaCadTipoProduto;
import br.com.cresceritsolutions.ibspdv.view.cadastro.PesquisaCadTipoUsuario;
import br.com.cresceritsolutions.ibspdv.view.cadastro.PesquisaCadTipoVenda;
import br.com.cresceritsolutions.ibspdv.view.cadastro.PesquisaCadTransportadora;
import br.com.cresceritsolutions.ibspdv.view.cadastro.PesquisaCadUnidadeMedida;
import br.com.cresceritsolutions.ibspdv.view.cadastro.PesquisaCadUsuario;
import br.com.cresceritsolutions.ibspdv.view.cadastro.PesquisaTabelaRemuneracaoAgenteAutorizado;
import br.com.cresceritsolutions.ibspdv.view.caixa.AbrirCaixa;
import br.com.cresceritsolutions.ibspdv.view.compra.PesquisaCadCompra;
import br.com.cresceritsolutions.ibspdv.view.compra.PesquisaCadTransferencia;
import br.com.cresceritsolutions.ibspdv.view.fiscal.PAF_MF;
import br.com.cresceritsolutions.ibspdv.view.fiscal.PafRegistrosPafEcf;
import br.com.cresceritsolutions.ibspdv.view.fiscal.VendasPeriodo;
import br.com.cresceritsolutions.ibspdv.view.paf.Sangria;
import br.com.cresceritsolutions.ibspdv.view.paf.Suprimento;
import br.com.cresceritsolutions.ibspdv.view.relatorios.ConsultaVendas;
import br.com.cresceritsolutions.ibspdv.view.relatorios.LocalizaProduto;
import br.com.cresceritsolutions.ibspdv.view.relatorios.MinhasVendas;
import br.com.cresceritsolutions.ibspdv.view.relatorios.RelatorioCaixas;
import br.com.cresceritsolutions.ibspdv.view.relatorios.RelatorioComissaoColaborador;
import br.com.cresceritsolutions.ibspdv.view.relatorios.RelatorioParaContestacao;
import br.com.cresceritsolutions.ibspdv.view.tabelapreco.operadora.PesquisaCadPrecoFranquia;
import br.com.cresceritsolutions.ibspdv.view.tabelapreco.operadora.PesquisaCadPrecoLoja;
import br.com.cresceritsolutions.ibspdv.view.venda.CancelarVenda;
import br.com.cresceritsolutions.ibspdv.view.venda.EmAberto;
import br.com.cresceritsolutions.ibspdv.view.venda.NovaPreVenda;
import br.com.cresceritsolutions.ibspdv.view.venda.NovaVendaConcomitante;
import br.com.cresceritsolutions.ibspdv.view.venda.NovaVendaServico;
import br.com.cresceritsolutions.ibspdv.view.venda.Pendentes;
import jACBrFramework.ACBrException;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

/**
 *
 * @author gutemberg
 */
public class PrincipalPDV extends FrameIBS {

    private static Logger log;
    private EnumTipoVenda concomitante;

    /**
     * Creates new form np
     */
    public PrincipalPDV() {
        log = Logger.getLogger(PrincipalPDV.class);
        initComponents();
        jLabel1.setIcon(new ImageIcon("./arquivos/fundo.jpg"));
        carregaComponentesVisiveis();
        if (VariaveisDoSistema.USUARIO != null) {
            lblUserLog.setText("Usuario : " + VariaveisDoSistema.USUARIO.getLogin());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jToolBar1 = new javax.swing.JToolBar();
        btnConsulta = new javax.swing.JButton();
        separadorConsulta = new javax.swing.JToolBar.Separator();
        btnClientes = new javax.swing.JButton();
        separadorClientes = new javax.swing.JToolBar.Separator();
        btnEntrada = new javax.swing.JButton();
        separadorEntrada = new javax.swing.JToolBar.Separator();
        btnVenda = new javax.swing.JButton();
        separadorVenda = new javax.swing.JToolBar.Separator();
        btnPreVenda = new javax.swing.JButton();
        separadorPreVenda = new javax.swing.JToolBar.Separator();
        btnTabelaPreco = new javax.swing.JButton();
        separadorTabelaPreco = new javax.swing.JToolBar.Separator();
        btnFechar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        lblUserLog = new javax.swing.JLabel();
        lblInfo = new javax.swing.JLabel();
        lblAlerta = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        BarraDeMenus = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItem44 = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        jMenuItem34 = new javax.swing.JMenuItem();
        menuCaixa = new javax.swing.JMenu();
        itemSangria = new javax.swing.JMenuItem();
        itemSuprimento = new javax.swing.JMenuItem();
        itemReducaoZ = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        itemFilaPendentes = new javax.swing.JMenuItem();
        itemAbrirCaixa = new javax.swing.JMenuItem();
        itemCancelarVenda = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        itemMenuTEF = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem13 = new javax.swing.JMenuItem();
        jMenu7 = new javax.swing.JMenu();
        itemMenuPreVenda = new javax.swing.JMenuItem();
        itemVenda = new javax.swing.JMenuItem();
        itemMenuServico = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        jMenuItem9 = new javax.swing.JMenuItem();
        menuFiscal = new javax.swing.JMenu();
        jMenuItem15 = new javax.swing.JMenuItem();
        jMenuItem16 = new javax.swing.JMenuItem();
        jMenuItem17 = new javax.swing.JMenuItem();
        jMenuItem18 = new javax.swing.JMenuItem();
        jMenuItem19 = new javax.swing.JMenuItem();
        jMenuItem20 = new javax.swing.JMenuItem();
        jMenuItem21 = new javax.swing.JMenuItem();
        jMenuItem22 = new javax.swing.JMenuItem();
        jMenuItem23 = new javax.swing.JMenuItem();
        jMenu11 = new javax.swing.JMenu();
        jMenu13 = new javax.swing.JMenu();
        jMenuItem56 = new javax.swing.JMenuItem();
        jMenuItem54 = new javax.swing.JMenuItem();
        jMenuItem55 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem60 = new javax.swing.JMenuItem();
        jMenuItem59 = new javax.swing.JMenuItem();
        jMenuItem61 = new javax.swing.JMenuItem();
        jMenuItem12 = new javax.swing.JMenuItem();
        jMenu14 = new javax.swing.JMenu();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem53 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem69 = new javax.swing.JMenuItem();
        jMenuItem58 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem68 = new javax.swing.JMenuItem();
        jMenuItem65 = new javax.swing.JMenuItem();
        jMenuItem62 = new javax.swing.JMenuItem();
        jMenu12 = new javax.swing.JMenu();
        jMenuItem66 = new javax.swing.JMenuItem();
        jMenuItem67 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem64 = new javax.swing.JMenuItem();
        jMenuItem57 = new javax.swing.JMenuItem();
        jMenuItem63 = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        jMenuItem25 = new javax.swing.JMenuItem();
        itemMenuEmitirNFE = new javax.swing.JMenuItem();
        itemMenuEmitirNfeVinculada = new javax.swing.JMenuItem();
        jMenu6 = new javax.swing.JMenu();
        itemMenuRelatorioCaixa = new javax.swing.JMenuItem();
        jMenuItem28 = new javax.swing.JMenuItem();
        jMenuItem29 = new javax.swing.JMenuItem();
        jMenuItem30 = new javax.swing.JMenuItem();
        jMenuItem31 = new javax.swing.JMenuItem();
        jMenuItem32 = new javax.swing.JMenuItem();
        jMenuItem33 = new javax.swing.JMenuItem();
        jMenuItem40 = new javax.swing.JMenuItem();
        jMenuItem37 = new javax.swing.JMenuItem();
        jMenuItem14 = new javax.swing.JMenuItem();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenu8 = new javax.swing.JMenu();
        jMenuItem45 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenu9 = new javax.swing.JMenu();
        jMenuItem51 = new javax.swing.JMenuItem();
        jMenuItem52 = new javax.swing.JMenuItem();
        jMenuItem10 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jPanel3.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        btnConsulta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/search 32.png"))); // NOI18N
        btnConsulta.setToolTipText("Consulta");
        btnConsulta.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnConsulta.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnConsulta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultaActionPerformed(evt);
            }
        });
        btnConsulta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnConsultaKeyPressed(evt);
            }
        });
        jToolBar1.add(btnConsulta);
        jToolBar1.add(separadorConsulta);

        btnClientes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Office-Client-Male-Light-icon.png"))); // NOI18N
        btnClientes.setToolTipText("Clientes");
        btnClientes.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnClientes.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClientesActionPerformed(evt);
            }
        });
        btnClientes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnClientesKeyPressed(evt);
            }
        });
        jToolBar1.add(btnClientes);
        jToolBar1.add(separadorClientes);

        btnEntrada.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/entrada 32.png"))); // NOI18N
        btnEntrada.setToolTipText("Entrada de Mercadoria");
        btnEntrada.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEntrada.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnEntrada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEntradaActionPerformed(evt);
            }
        });
        btnEntrada.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnEntradaKeyPressed(evt);
            }
        });
        jToolBar1.add(btnEntrada);
        jToolBar1.add(separadorEntrada);

        btnVenda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/venda.png"))); // NOI18N
        btnVenda.setToolTipText("Nova venda");
        btnVenda.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnVenda.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnVenda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVendaActionPerformed(evt);
            }
        });
        btnVenda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnVendaKeyPressed(evt);
            }
        });
        jToolBar1.add(btnVenda);
        jToolBar1.add(separadorVenda);

        btnPreVenda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/carrinho de compras 32.png"))); // NOI18N
        btnPreVenda.setToolTipText("Nova Pré-Venda");
        btnPreVenda.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnPreVenda.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnPreVenda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPreVendaActionPerformed(evt);
            }
        });
        btnPreVenda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnPreVendaKeyPressed(evt);
            }
        });
        jToolBar1.add(btnPreVenda);
        jToolBar1.add(separadorPreVenda);

        btnTabelaPreco.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/consulta preco 32.png"))); // NOI18N
        btnTabelaPreco.setToolTipText("Tabela de Precos Operadora");
        btnTabelaPreco.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnTabelaPreco.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnTabelaPreco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTabelaPrecoActionPerformed(evt);
            }
        });
        btnTabelaPreco.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnTabelaPrecoKeyPressed(evt);
            }
        });
        jToolBar1.add(btnTabelaPreco);
        jToolBar1.add(separadorTabelaPreco);

        btnFechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/close_32x32.png"))); // NOI18N
        btnFechar.setToolTipText("Sair");
        btnFechar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnFechar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });
        btnFechar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnFecharKeyPressed(evt);
            }
        });
        jToolBar1.add(btnFechar);

        jPanel2.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jPanel2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        lblUserLog.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblUserLog.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        lblInfo.setFont(new java.awt.Font("Arial", 0, 10)); // NOI18N
        lblInfo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblInfo.setText("Sistema Comercial Integrado / Suporte : suporte@cresceritsolutions.com.br");
        lblInfo.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        lblInfo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblInfoMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblInfoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblInfoMouseExited(evt);
            }
        });

        lblAlerta.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        lblAlerta.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblAlertaMouseClicked(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Arial", 1, 11)); // NOI18N
        jLabel2.setText(" F7 - NOVA VENDA | F8 - NOVO PEDIDO");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblInfo, javax.swing.GroupLayout.DEFAULT_SIZE, 640, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblAlerta, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblUserLog, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblUserLog, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblAlerta, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setOpaque(true);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 0, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 331, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        BarraDeMenus.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jMenu1.setText("Acesso");
        jMenu1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jMenuItem1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem1.setText("Log-Off");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);
        jMenu1.add(jSeparator1);

        jMenuItem44.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem44.setText("Alterar senha de acesso");
        jMenuItem44.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem44ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem44);
        jMenu1.add(jSeparator4);

        jMenuItem34.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        jMenuItem34.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem34.setText("Sair");
        jMenuItem34.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem34ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem34);

        BarraDeMenus.add(jMenu1);

        menuCaixa.setText("Caixa");
        menuCaixa.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        itemSangria.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        itemSangria.setText("Sangria");
        itemSangria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemSangriaActionPerformed(evt);
            }
        });
        menuCaixa.add(itemSangria);

        itemSuprimento.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        itemSuprimento.setText("Suprimento");
        itemSuprimento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemSuprimentoActionPerformed(evt);
            }
        });
        menuCaixa.add(itemSuprimento);

        itemReducaoZ.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        itemReducaoZ.setText("*Redução Z");
        itemReducaoZ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemReducaoZActionPerformed(evt);
            }
        });
        menuCaixa.add(itemReducaoZ);
        menuCaixa.add(jSeparator2);

        itemFilaPendentes.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        itemFilaPendentes.setText("Pendentes");
        itemFilaPendentes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemFilaPendentesActionPerformed(evt);
            }
        });
        menuCaixa.add(itemFilaPendentes);

        itemAbrirCaixa.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        itemAbrirCaixa.setText("Abrir caixa");
        itemAbrirCaixa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemAbrirCaixaActionPerformed(evt);
            }
        });
        menuCaixa.add(itemAbrirCaixa);

        itemCancelarVenda.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        itemCancelarVenda.setText("Cancelamento / Devolução");
        itemCancelarVenda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemCancelarVendaActionPerformed(evt);
            }
        });
        menuCaixa.add(itemCancelarVenda);
        menuCaixa.add(jSeparator3);

        itemMenuTEF.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        itemMenuTEF.setText("TEF");
        itemMenuTEF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemMenuTEFActionPerformed(evt);
            }
        });
        menuCaixa.add(itemMenuTEF);

        BarraDeMenus.add(menuCaixa);

        jMenu3.setText("Comercial");
        jMenu3.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jMenuItem13.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem13.setText("Entradas");
        jMenuItem13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem13ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem13);

        jMenu7.setText("Saidas");
        jMenu7.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenu7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu7ActionPerformed(evt);
            }
        });

        itemMenuPreVenda.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F8, 0));
        itemMenuPreVenda.setText("Pré-Venda");
        itemMenuPreVenda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemMenuPreVendaActionPerformed(evt);
            }
        });
        jMenu7.add(itemMenuPreVenda);

        itemVenda.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F7, 0));
        itemVenda.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        itemVenda.setText("Venda");
        itemVenda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemVendaActionPerformed(evt);
            }
        });
        jMenu7.add(itemVenda);

        itemMenuServico.setText("Serviço");
        itemMenuServico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemMenuServicoActionPerformed(evt);
            }
        });
        jMenu7.add(itemMenuServico);

        jMenuItem11.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem11.setText("Em aberto");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem11);

        jMenu3.add(jMenu7);

        jMenuItem9.setText("Transferência");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem9);

        BarraDeMenus.add(jMenu3);

        menuFiscal.setText("Menu Fiscal");
        menuFiscal.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jMenuItem15.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem15.setText("LX");
        jMenuItem15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem15ActionPerformed(evt);
            }
        });
        menuFiscal.add(jMenuItem15);

        jMenuItem16.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem16.setText("LMF");
        jMenuItem16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem16ActionPerformed(evt);
            }
        });
        menuFiscal.add(jMenuItem16);

        jMenuItem17.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem17.setText("Arq. MF");
        jMenuItem17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem17ActionPerformed(evt);
            }
        });
        menuFiscal.add(jMenuItem17);

        jMenuItem18.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem18.setText("Arq. MFD");
        jMenuItem18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem18ActionPerformed(evt);
            }
        });
        menuFiscal.add(jMenuItem18);

        jMenuItem19.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem19.setText("Identificação do PAF-ECF");
        jMenuItem19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem19ActionPerformed(evt);
            }
        });
        menuFiscal.add(jMenuItem19);

        jMenuItem20.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem20.setText("*Vendas do período");
        jMenuItem20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem20ActionPerformed(evt);
            }
        });
        menuFiscal.add(jMenuItem20);

        jMenuItem21.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem21.setText("Tab. Índice Técnico Produção");
        jMenuItem21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem21ActionPerformed(evt);
            }
        });
        menuFiscal.add(jMenuItem21);

        jMenuItem22.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem22.setText("Parâmetros de Configuração");
        jMenuItem22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem22ActionPerformed(evt);
            }
        });
        menuFiscal.add(jMenuItem22);

        jMenuItem23.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem23.setText("*Registros do PAF-ECF");
        jMenuItem23.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem23ActionPerformed(evt);
            }
        });
        menuFiscal.add(jMenuItem23);

        BarraDeMenus.add(menuFiscal);

        jMenu11.setText("Cadastro");
        jMenu11.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jMenu13.setText("Gerencial");

        jMenuItem56.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem56.setText("Cargo");
        jMenuItem56.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem56ActionPerformed(evt);
            }
        });
        jMenu13.add(jMenuItem56);

        jMenuItem54.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem54.setText("Classificação");
        jMenuItem54.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem54ActionPerformed(evt);
            }
        });
        jMenu13.add(jMenuItem54);

        jMenuItem55.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem55.setText("Colaborador");
        jMenuItem55.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem55ActionPerformed(evt);
            }
        });
        jMenu13.add(jMenuItem55);

        jMenuItem4.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem4.setText("Departamento");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu13.add(jMenuItem4);

        jMenuItem60.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem60.setText("Empresa");
        jMenuItem60.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem60ActionPerformed(evt);
            }
        });
        jMenu13.add(jMenuItem60);

        jMenuItem59.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem59.setText("Tipo de Usuário");
        jMenuItem59.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem59ActionPerformed(evt);
            }
        });
        jMenu13.add(jMenuItem59);

        jMenuItem61.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem61.setText("Usuário");
        jMenuItem61.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem61ActionPerformed(evt);
            }
        });
        jMenu13.add(jMenuItem61);

        jMenuItem12.setText("Remuneração do Agente");
        jMenuItem12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem12ActionPerformed(evt);
            }
        });
        jMenu13.add(jMenuItem12);

        jMenu11.add(jMenu13);

        jMenu14.setText("Comercial");

        jMenuItem6.setText("Canal de Vendas");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu14.add(jMenuItem6);

        jMenuItem53.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem53.setText("Cliente");
        jMenuItem53.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem53ActionPerformed(evt);
            }
        });
        jMenu14.add(jMenuItem53);

        jMenuItem7.setText("Imposto");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu14.add(jMenuItem7);

        jMenuItem69.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem69.setText("Formas de pagamento");
        jMenuItem69.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem69ActionPerformed(evt);
            }
        });
        jMenu14.add(jMenuItem69);

        jMenuItem58.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem58.setText("Fornecedor");
        jMenuItem58.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem58ActionPerformed(evt);
            }
        });
        jMenu14.add(jMenuItem58);

        jMenuItem2.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem2.setText("Marca");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu14.add(jMenuItem2);

        jMenuItem68.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem68.setText("Oferta");
        jMenuItem68.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem68ActionPerformed(evt);
            }
        });
        jMenu14.add(jMenuItem68);

        jMenuItem65.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem65.setText("Plano");
        jMenuItem65.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem65ActionPerformed(evt);
            }
        });
        jMenu14.add(jMenuItem65);

        jMenuItem62.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem62.setText("Produto");
        jMenuItem62.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem62ActionPerformed(evt);
            }
        });
        jMenu14.add(jMenuItem62);

        jMenu12.setText("Tabelas de preços");
        jMenu12.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jMenuItem66.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem66.setText("Claro");
        jMenuItem66.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem66ActionPerformed(evt);
            }
        });
        jMenu12.add(jMenuItem66);

        jMenuItem67.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem67.setText("Loja");
        jMenuItem67.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem67ActionPerformed(evt);
            }
        });
        jMenu12.add(jMenuItem67);

        jMenu14.add(jMenu12);

        jMenuItem3.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem3.setText("Tipo de Produto");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu14.add(jMenuItem3);

        jMenuItem64.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem64.setText("Tipo de Venda");
        jMenuItem64.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem64ActionPerformed(evt);
            }
        });
        jMenu14.add(jMenuItem64);

        jMenuItem57.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem57.setText("Transportadora");
        jMenuItem57.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem57ActionPerformed(evt);
            }
        });
        jMenu14.add(jMenuItem57);

        jMenuItem63.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem63.setText("Unidade de Medida");
        jMenuItem63.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem63ActionPerformed(evt);
            }
        });
        jMenu14.add(jMenuItem63);

        jMenu11.add(jMenu14);

        BarraDeMenus.add(jMenu11);

        jMenu5.setText("Nota Fiscal");
        jMenu5.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jMenuItem25.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem25.setText("* Consulta NF-e");
        jMenuItem25.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem25ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem25);

        itemMenuEmitirNFE.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        itemMenuEmitirNFE.setText("* Emitir NF-e");
        itemMenuEmitirNFE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemMenuEmitirNFEActionPerformed(evt);
            }
        });
        jMenu5.add(itemMenuEmitirNFE);

        itemMenuEmitirNfeVinculada.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        itemMenuEmitirNfeVinculada.setText("* Emitir NF-e Vinculada");
        itemMenuEmitirNfeVinculada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemMenuEmitirNfeVinculadaActionPerformed(evt);
            }
        });
        jMenu5.add(itemMenuEmitirNfeVinculada);

        BarraDeMenus.add(jMenu5);

        jMenu6.setText("Relatório");
        jMenu6.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        itemMenuRelatorioCaixa.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        itemMenuRelatorioCaixa.setText("Caixa");
        itemMenuRelatorioCaixa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemMenuRelatorioCaixaActionPerformed(evt);
            }
        });
        jMenu6.add(itemMenuRelatorioCaixa);

        jMenuItem28.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem28.setText("* Inventário do estoque");
        jMenuItem28.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem28ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem28);

        jMenuItem29.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem29.setText("* Produtos disponíveis");
        jMenuItem29.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem29ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem29);

        jMenuItem30.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem30.setText("* Estornos vendedor - xls");
        jMenuItem30.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem30ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem30);

        jMenuItem31.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem31.setText("Histórico produto");
        jMenuItem31.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem31ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem31);

        jMenuItem32.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem32.setText("* Mapa de vendas - xls");
        jMenuItem32.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem32ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem32);

        jMenuItem33.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem33.setText("Minhas comissões");
        jMenuItem33.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem33ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem33);

        jMenuItem40.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem40.setText("* Minhas Vendas");
        jMenuItem40.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem40ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem40);

        jMenuItem37.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem37.setText("Consulta Vendas");
        jMenuItem37.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem37ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem37);

        jMenuItem14.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem14.setText("Produtors disponíveis");
        jMenuItem14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem14ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem14);

        jMenuItem8.setText("Relatório para contestação");
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem8);

        BarraDeMenus.add(jMenu6);

        jMenu8.setText("Utilitários");
        jMenu8.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jMenuItem45.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F3, 0));
        jMenuItem45.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem45.setText("Calculadora");
        jMenuItem45.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem45ActionPerformed(evt);
            }
        });
        jMenu8.add(jMenuItem45);

        jMenuItem5.setText("* Balanço");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu8.add(jMenuItem5);

        BarraDeMenus.add(jMenu8);

        jMenu9.setText("Ajuda");
        jMenu9.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jMenuItem51.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem51.setText("Notificar erro ao fabricante");
        jMenuItem51.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem51ActionPerformed(evt);
            }
        });
        jMenu9.add(jMenuItem51);

        jMenuItem52.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMenuItem52.setText("Sobre");
        jMenuItem52.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem52ActionPerformed(evt);
            }
        });
        jMenu9.add(jMenuItem52);

        jMenuItem10.setText("Validar");
        jMenuItem10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem10ActionPerformed(evt);
            }
        });
        jMenu9.add(jMenuItem10);

        BarraDeMenus.add(jMenu9);

        setJMenuBar(BarraDeMenus);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        logoff();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void btnConsultaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultaActionPerformed
        ProdutosDisponiveis.newInstante().setVisible(true);
    }//GEN-LAST:event_btnConsultaActionPerformed

    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        if (ExibirParaUsuario.getInstance().confirmacao(me, "Deseja Realmente Sair do Sistema?")) {
            System.exit(0);
        }
    }//GEN-LAST:event_btnFecharActionPerformed

    private void lblInfoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblInfoMouseClicked

    }//GEN-LAST:event_lblInfoMouseClicked

    private void lblInfoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblInfoMouseEntered

    }//GEN-LAST:event_lblInfoMouseEntered

    private void lblInfoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblInfoMouseExited

    }//GEN-LAST:event_lblInfoMouseExited

    private void lblAlertaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblAlertaMouseClicked

    }//GEN-LAST:event_lblAlertaMouseClicked

    private void jMenuItem34ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem34ActionPerformed
        if (ExibirParaUsuario.getInstance().confirmacao(me, "Deseja Realmente Sair do Sistema?")) {
            System.exit(0);
        }
    }//GEN-LAST:event_jMenuItem34ActionPerformed

    private void itemSangriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemSangriaActionPerformed
        if (VariaveisDoSistema.USUARIO.getColaborador() != null) {

            if (!Util.getData(Util.getData(VariaveisDoSistema.CAIXA.getDataAbertura())).before(Util.getData(Util.getData(new Date())))) {
                Sangria.newInstance().setVisible(true);
            } else {
                ExibirParaUsuario.getInstance().atencao(me, "Aguardando Redução Z!");
            }
        } else {
            ExibirParaUsuario.getInstance().atencao(me, "Você não está vinculado a nenhum colaborador.\nPortando não pode efetuar operações comerciais.\nPara configurar contate o administrador do sistema!");
        }
    }//GEN-LAST:event_itemSangriaActionPerformed

    private void itemSuprimentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemSuprimentoActionPerformed
        if (VariaveisDoSistema.USUARIO.getColaborador() != null) {

            if (!Util.getData(Util.getData(VariaveisDoSistema.CAIXA.getDataAbertura())).before(Util.getData(Util.getData(new Date())))) {
                Suprimento.newInstance().setVisible(true);
            } else {
                ExibirParaUsuario.getInstance().atencao(me, "Aguardando Redução Z!");
            }
        } else {
            ExibirParaUsuario.getInstance().atencao(me, "Você não está vinculado a nenhum colaborador.\nPortando não pode efetuar operações comerciais.\nPara configurar contate o administrador do sistema!");
        }
    }//GEN-LAST:event_itemSuprimentoActionPerformed

    private void itemReducaoZActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemReducaoZActionPerformed
        if (VariaveisDoSistema.USUARIO.getColaborador() != null) {
            if (!Util.getData(Util.getData(VariaveisDoSistema.CAIXA.getDataAbertura())).before(Util.getData(Util.getData(new Date())))) {
                ExibirParaUsuario.getInstance().atencao(me, "Se fechar o caixa não será mais possível abri-lo hoje!");
            }
            if (VariaveisDoSistema.getEcfAtivo()) {
                if (ExibirParaUsuario.getInstance().confirmacao(getInstance(), "Você tem certeza que quer fechar o caixa?")) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                ControllerPaf.getInstance().reducaoZ();
                            } catch (IbsException ex) {
                                ExibirParaUsuario.getInstance().erro(me, ex, ex.getMessage(), false);
                            }
                            Aguarde.getInstance().setVisible(false);
                        }
                    }).start();
                    Aguarde.getInstance().setVisible(true);
                }
            } else {
                if (ExibirParaUsuario.getInstance().confirmacao(getInstance(), "Você tem certeza que quer fechar o caixa?")) {
                    HibernateUtil hu = HibernateUtil.getInstance();
                    try {
                        hu.beginTransaction();
                        hu.getSession().refresh(VariaveisDoSistema.CAIXA);
                        VariaveisDoSistema.CAIXA.setAberto(false);
                        VariaveisDoSistema.CAIXA.setDataFechamento(new Date());
                        hu.getSession().save(VariaveisDoSistema.CAIXA);
                        hu.commit();
                        atualizaEstadoCaixa();
                        ExibirParaUsuario.getInstance().mensagem(me, "Caixa fechado com sucesso!", true);
                    } catch (HibernateException ex) {
                        hu.rollback();
                        ExibirParaUsuario.getInstance().erro(me, ex, ex.getMessage(), false);
                    }

                }
            }

        } else {
            ExibirParaUsuario.getInstance().atencao(me, "Você não está vinculado a nenhum colaborador.\nPortando não pode efetuar operações comerciais.\nPara configurar contate o administrador do sistema!");
        }
    }//GEN-LAST:event_itemReducaoZActionPerformed

    private void itemFilaPendentesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemFilaPendentesActionPerformed
        if (VariaveisDoSistema.USUARIO.getColaborador() != null) {
            setComcomitante(EnumTipoVenda.PRE_VENDA);
            Pendentes.newInstance().setVisible(true);
        } else {
            ExibirParaUsuario.getInstance().atencao(me, "Você não está vinculado a nenhum colaborador.\nPortando não pode efetuar operações comerciais.\nPara configurar contate o administrador do sistema!");
        }
    }//GEN-LAST:event_itemFilaPendentesActionPerformed

    private void itemMenuTEFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemMenuTEFActionPerformed
        if (VariaveisDoSistema.USUARIO.getColaborador() != null) {
            if (VariaveisDoSistema.getEcfAtivo()) {
                ControllerPaf.getInstance().tef();
            } else {
                ExibirParaUsuario.getInstance().atencao(me, "Essa versão não possui funções de TEF!");
            }
        } else {
            ExibirParaUsuario.getInstance().atencao(me, "Você não está vinculado a nenhum colaborador.\nPortando não pode efetuar operações comerciais.\nPara configurar contate o administrador do sistema!");
        }
    }//GEN-LAST:event_itemMenuTEFActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        PesquisaCadMarca.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        PesquisaCadDepartamento.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem53ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem53ActionPerformed
        PesquisaCadClientes.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem53ActionPerformed

    private void jMenuItem54ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem54ActionPerformed
        PesquisaCadClassificacao.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem54ActionPerformed

    private void jMenuItem55ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem55ActionPerformed
        PesquisaCadColaborador.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem55ActionPerformed

    private void jMenuItem56ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem56ActionPerformed
        PesquisaCadCargo.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem56ActionPerformed

    private void jMenuItem57ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem57ActionPerformed
        PesquisaCadTransportadora.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem57ActionPerformed

    private void jMenuItem58ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem58ActionPerformed
        PesquisaCadFornecedor.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem58ActionPerformed

    private void jMenuItem59ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem59ActionPerformed
        PesquisaCadTipoUsuario.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem59ActionPerformed

    private void jMenuItem60ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem60ActionPerformed
        PesquisaCadEmpresa.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem60ActionPerformed

    private void jMenuItem61ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem61ActionPerformed
        PesquisaCadUsuario.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem61ActionPerformed

    private void jMenuItem62ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem62ActionPerformed
        PesquisaCadProduto.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem62ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        PesquisaCadTipoProduto.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem63ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem63ActionPerformed
        PesquisaCadUnidadeMedida.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem63ActionPerformed

    private void jMenuItem13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem13ActionPerformed
        PesquisaCadCompra.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem13ActionPerformed

    private void btnVendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVendaActionPerformed
        novaVenda(null);
    }//GEN-LAST:event_btnVendaActionPerformed

    private void btnClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClientesActionPerformed
        PesquisaCadClientes.newInstance().setVisible(true);
    }//GEN-LAST:event_btnClientesActionPerformed

    private void jMenuItem64ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem64ActionPerformed
        PesquisaCadTipoVenda.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem64ActionPerformed

    private void jMenuItem65ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem65ActionPerformed
        PesquisaCadPlano.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem65ActionPerformed

    private void jMenuItem66ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem66ActionPerformed
        PesquisaCadPrecoFranquia.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem66ActionPerformed

    private void jMenuItem68ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem68ActionPerformed
        PesquisaCadOferta.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem68ActionPerformed

    private void jMenuItem67ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem67ActionPerformed
        PesquisaCadPrecoLoja.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem67ActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        if (!ExibirParaUsuario.getInstance().confirmacao(me, "Deseja Realmente Sair do Sistema?")) {
            setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        } else {
            setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        }
    }//GEN-LAST:event_formWindowClosing

    private void jMenuItem69ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem69ActionPerformed
        PesquisaCadFormaPagamento.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem69ActionPerformed

    private void itemAbrirCaixaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemAbrirCaixaActionPerformed
        if (VariaveisDoSistema.USUARIO.getColaborador() != null) {
            AbrirCaixa.newInstance().setVisible(true);
        } else {
            ExibirParaUsuario.getInstance().atencao(me, "Você não está vinculado a nenhum colaborador.\nPortando não pode efetuar operações comerciais.\nPara configurar contate o administrador do sistema!");
        }
    }//GEN-LAST:event_itemAbrirCaixaActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        atualizaEstadoCaixa();
    }//GEN-LAST:event_formWindowOpened

    private void itemVendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemVendaActionPerformed
        novaVenda(null);
    }//GEN-LAST:event_itemVendaActionPerformed

    private void jMenuItem15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem15ActionPerformed
        leituraX();
    }//GEN-LAST:event_jMenuItem15ActionPerformed

    private void jMenuItem16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem16ActionPerformed
        lmf();
    }//GEN-LAST:event_jMenuItem16ActionPerformed

    private void jMenuItem17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem17ActionPerformed
        arquivoMf();
    }//GEN-LAST:event_jMenuItem17ActionPerformed

    private void jMenuItem18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem18ActionPerformed
        arquivoMfd();
    }//GEN-LAST:event_jMenuItem18ActionPerformed

    private void jMenuItem19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem19ActionPerformed
        identificacaoPafEcf();
    }//GEN-LAST:event_jMenuItem19ActionPerformed

    private void btnTabelaPrecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTabelaPrecoActionPerformed
        PrecosFranquia.newInstance(false).setVisible(true);
    }//GEN-LAST:event_btnTabelaPrecoActionPerformed

    private void jMenuItem14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem14ActionPerformed
        ProdutosDisponiveis.newInstante().setVisible(true);
    }//GEN-LAST:event_jMenuItem14ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        PesquisaCadCanalVenda.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void itemMenuRelatorioCaixaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemMenuRelatorioCaixaActionPerformed
        RelatorioCaixas.newInstance().setVisible(true);
    }//GEN-LAST:event_itemMenuRelatorioCaixaActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        PesquisaCadBalanco.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem31ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem31ActionPerformed
        try {
            LocalizaProduto.newInstance().setVisible(true);
        } catch (IbsException ex) {
            ExibirParaUsuario.getInstance().erro(me, ex, ex.getMessage(), false);
        }
    }//GEN-LAST:event_jMenuItem31ActionPerformed

    private void jMenuItem37ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem37ActionPerformed
        try {
            if (VariaveisDoSistema.USUARIO.getColaborador() != null && VariaveisDoSistema.USUARIO.getColaborador().isVendedor()) {
                ConsultaVendas.newInstance(VariaveisDoSistema.USUARIO.getColaborador()).setVisible(true);
            } else {
                ConsultaVendas.newInstance().setVisible(true);
            }
        } catch (IbsException ex) {
            ExibirParaUsuario.getInstance().erro(me, ex, ex.getMessage(), false);
        }
    }//GEN-LAST:event_jMenuItem37ActionPerformed

    private void jMenuItem40ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem40ActionPerformed
        try {
            MinhasVendas.newInstance().setVisible(true);
        } catch (IbsException ex) {
            ExibirParaUsuario.getInstance().erro(me, ex, ex.getMessage(), false);
        }
    }//GEN-LAST:event_jMenuItem40ActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        PesquisaCadImposto.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void itemCancelarVendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemCancelarVendaActionPerformed
        if (VariaveisDoSistema.USUARIO.getColaborador() != null) {
            CancelarVenda.newInstance().setVisible(true);
        } else {
            ExibirParaUsuario.getInstance().atencao(me, "Você não está vinculado a nenhum colaborador.\nPortando não pode efetuar operações comerciais.\nPara configurar contate o administrador do sistema!");
        }
    }//GEN-LAST:event_itemCancelarVendaActionPerformed

    private void jMenu7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu7ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenu7ActionPerformed

    private void jMenuItem20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem20ActionPerformed
        VendasPeriodo.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem20ActionPerformed

    private void jMenuItem21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem21ActionPerformed

        if (VariaveisDoSistema.getEcfAtivo()) {
            final JDialog dialog = new JDialog();
            dialog.setModal(true);
            dialog.setIconImage(new Class_Imagens().getMarcaIcone()
                    .getImage());
            dialog.setTitle("Tabela de indice técnico");
            JPanel jPanel = new JPanel();
            final JFileChooser chooser = new JFileChooser();
            chooser.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent arg0) {
                    if (VariaveisDoSistema.getEcfAtivo()) {
                        if (arg0.getActionCommand().equals(
                                JFileChooser.APPROVE_SELECTION)) {
                            setCursor(new Cursor(Cursor.WAIT_CURSOR));
                            //ECF.gerarArquivoIndiceTecnic(chooser.getSelectedFile().toString());
                            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                            dialog.dispose();
                        } else {
                            dialog.dispose();
                        }
                    } else {
                        JOptionPane.showMessageDialog(me,
                                "Essa versão não possui funções da impressora!",
                                "Atenção", JOptionPane.WARNING_MESSAGE);
                    }
                }
            });
            chooser.setDialogType(JFileChooser.SAVE_DIALOG);
            jPanel.setSize(640, 460);
            dialog.setSize(640, 460);
            jPanel.add(chooser);
            dialog.getContentPane().add(jPanel);
            dialog.setVisible(true);

            JOptionPane.showMessageDialog(me, "Este  PAF-ECF não executa funções de baixa  de estoque com base"
                    + " em índices técnicos de produção,\n não podendo ser utilizando por estabelecimento que necessite "
                    + "deste recurso", "Atenção", JOptionPane.WARNING_MESSAGE);

        } else {
            JOptionPane.showMessageDialog(me, "Essa versão não possui funções da impressora!", "Atenção", JOptionPane.WARNING_MESSAGE);
        }

    }//GEN-LAST:event_jMenuItem21ActionPerformed

    private void jMenuItem22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem22ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem22ActionPerformed

    private void jMenuItem23ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem23ActionPerformed
        PafRegistrosPafEcf.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem23ActionPerformed

    private void jMenuItem25ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem25ActionPerformed
        try {
            PesquisaNfe.newInstance().setVisible(true);
        } catch (IbsException ex) {
            ExibirParaUsuario.getInstance().erro(me, ex, ex.getMessage(), false);
        }
    }//GEN-LAST:event_jMenuItem25ActionPerformed

    private void jMenuItem28ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem28ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem28ActionPerformed

    private void jMenuItem29ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem29ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem29ActionPerformed

    private void jMenuItem30ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem30ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem30ActionPerformed

    private void jMenuItem32ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem32ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem32ActionPerformed

    private void jMenuItem33ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem33ActionPerformed
        RelatorioComissaoColaborador.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem33ActionPerformed

    private void jMenuItem44ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem44ActionPerformed
        AlteraSenha.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem44ActionPerformed

    private void jMenuItem45ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem45ActionPerformed
        try {
            Runtime.getRuntime().exec("calc");
        } catch (IOException ex) {
            ExibirParaUsuario.getInstance().erro(me, ex, "Erro ao abrir a calculadora!", false);
        }
    }//GEN-LAST:event_jMenuItem45ActionPerformed

    private void jMenuItem51ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem51ActionPerformed
        NotificarErroFabricante.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem51ActionPerformed

    private void jMenuItem52ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem52ActionPerformed
        Sobre.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem52ActionPerformed

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed
        if (VariaveisDoSistema.USUARIO.getColaborador() != null) {
            PesquisaCadTransferencia.newInstance().setVisible(true);
        } else {
            ExibirParaUsuario.getInstance().atencao(me, "Você não está vinculado a nenhum colaborador.\nPortando não pode efetuar operações comerciais.\nPara configurar contate o administrador do sistema!");
        }
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    private void itemMenuPreVendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemMenuPreVendaActionPerformed
        novaPreVenda(null);
    }//GEN-LAST:event_itemMenuPreVendaActionPerformed

    private void btnPreVendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPreVendaActionPerformed
        novaPreVenda(null);
    }//GEN-LAST:event_btnPreVendaActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        if (VariaveisDoSistema.USUARIO.getColaborador() != null) {
            setComcomitante(EnumTipoVenda.PRE_VENDA);
            EmAberto.newInstance().setVisible(true);
        } else {
            ExibirParaUsuario.getInstance().atencao(me, "Você não está vinculado a nenhum colaborador.\nPortando não pode efetuar operações comerciais.\nPara configurar contate o administrador do sistema!");
        }
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void itemMenuEmitirNFEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemMenuEmitirNFEActionPerformed

        novaNfe(null);

    }//GEN-LAST:event_itemMenuEmitirNFEActionPerformed

    private void itemMenuEmitirNfeVinculadaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemMenuEmitirNfeVinculadaActionPerformed
        ConsultaVendaEmitirNfe.newInstance().setVisible(true);
    }//GEN-LAST:event_itemMenuEmitirNfeVinculadaActionPerformed

    private void itemMenuServicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemMenuServicoActionPerformed
        if (VariaveisDoSistema.USUARIO.getColaborador() != null) {
            try {
                NovaVendaServico.newInstance().setVisible(true);
            } catch (IbsException ex) {
                ExibirParaUsuario.getInstance().erro(me, ex, ex.getMessage(), false);
            }
        } else {
            ExibirParaUsuario.getInstance().atencao(me, "Você não está vinculado a nenhum colaborador.\nPortando não pode efetuar operações comerciais.\nPara configurar contate o administrador do sistema!");
        }
    }//GEN-LAST:event_itemMenuServicoActionPerformed

    private void btnEntradaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEntradaActionPerformed
        PesquisaCadCompra.newInstance().setVisible(true);
    }//GEN-LAST:event_btnEntradaActionPerformed

    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem8ActionPerformed
        RelatorioParaContestacao.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem8ActionPerformed

    private void jMenuItem10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem10ActionPerformed
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    ControllerValidaSistema.getInstance().executar();
                    Aguarde.getInstance().setVisible(false);
                    ExibirParaUsuario.getInstance().mensagem(me, "Validação checada com sucesso!"
                            + "\nReinicie o sistema para atualizar as definições!", false);
                } catch (Exception ex) {
                    ExibirParaUsuario.getInstance().erro(me, ex, "Erro ao checar validade do sistema!", false);
                }

            }
        }).start();
        Aguarde.getInstance().setVisible(true);

    }//GEN-LAST:event_jMenuItem10ActionPerformed

    private void jMenuItem12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem12ActionPerformed
        PesquisaTabelaRemuneracaoAgenteAutorizado.newInstance().setVisible(true);
    }//GEN-LAST:event_jMenuItem12ActionPerformed

    private void btnConsultaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnConsultaKeyPressed
        System.out.println(evt.getKeyCode());
        if (evt.getKeyCode() == 10) {
            ProdutosDisponiveis.newInstante().setVisible(true);
        }
    }//GEN-LAST:event_btnConsultaKeyPressed

    private void btnClientesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnClientesKeyPressed
        if (evt.getKeyCode() == 10) {
            PesquisaCadClientes.newInstance().setVisible(true);
        }
    }//GEN-LAST:event_btnClientesKeyPressed

    private void btnEntradaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnEntradaKeyPressed
        if (evt.getKeyCode() == 10) {
            PesquisaCadCompra.newInstance().setVisible(true);
        }
    }//GEN-LAST:event_btnEntradaKeyPressed

    private void btnVendaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnVendaKeyPressed
        if (evt.getKeyCode() == 10) {
            novaVenda(null);
        }
    }//GEN-LAST:event_btnVendaKeyPressed

    private void btnPreVendaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnPreVendaKeyPressed
        if (evt.getKeyCode() == 10) {
            novaPreVenda(null);
        }
    }//GEN-LAST:event_btnPreVendaKeyPressed

    private void btnTabelaPrecoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnTabelaPrecoKeyPressed
        if (evt.getKeyCode() == 10) {
            PrecosFranquia.newInstance(false).setVisible(true);
        }
    }//GEN-LAST:event_btnTabelaPrecoKeyPressed

    private void btnFecharKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnFecharKeyPressed
        if (evt.getKeyCode() == 10) {
            if (ExibirParaUsuario.getInstance().confirmacao(me, "Deseja Realmente Sair do Sistema?")) {
                System.exit(0);
            }
        }
    }//GEN-LAST:event_btnFecharKeyPressed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JMenuBar BarraDeMenus;
    private javax.swing.JButton btnClientes;
    private javax.swing.JButton btnConsulta;
    private javax.swing.JButton btnEntrada;
    private javax.swing.JButton btnFechar;
    private javax.swing.JButton btnPreVenda;
    private javax.swing.JButton btnTabelaPreco;
    private javax.swing.JButton btnVenda;
    private javax.swing.JMenuItem itemAbrirCaixa;
    private javax.swing.JMenuItem itemCancelarVenda;
    private javax.swing.JMenuItem itemFilaPendentes;
    private javax.swing.JMenuItem itemMenuEmitirNFE;
    private javax.swing.JMenuItem itemMenuEmitirNfeVinculada;
    private javax.swing.JMenuItem itemMenuPreVenda;
    private javax.swing.JMenuItem itemMenuRelatorioCaixa;
    private javax.swing.JMenuItem itemMenuServico;
    private javax.swing.JMenuItem itemMenuTEF;
    private javax.swing.JMenuItem itemReducaoZ;
    private javax.swing.JMenuItem itemSangria;
    private javax.swing.JMenuItem itemSuprimento;
    private javax.swing.JMenuItem itemVenda;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu11;
    private javax.swing.JMenu jMenu12;
    private javax.swing.JMenu jMenu13;
    private javax.swing.JMenu jMenu14;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenu jMenu8;
    private javax.swing.JMenu jMenu9;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JMenuItem jMenuItem13;
    private javax.swing.JMenuItem jMenuItem14;
    private javax.swing.JMenuItem jMenuItem15;
    private javax.swing.JMenuItem jMenuItem16;
    private javax.swing.JMenuItem jMenuItem17;
    private javax.swing.JMenuItem jMenuItem18;
    private javax.swing.JMenuItem jMenuItem19;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem20;
    private javax.swing.JMenuItem jMenuItem21;
    private javax.swing.JMenuItem jMenuItem22;
    private javax.swing.JMenuItem jMenuItem23;
    private javax.swing.JMenuItem jMenuItem25;
    private javax.swing.JMenuItem jMenuItem28;
    private javax.swing.JMenuItem jMenuItem29;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem30;
    private javax.swing.JMenuItem jMenuItem31;
    private javax.swing.JMenuItem jMenuItem32;
    private javax.swing.JMenuItem jMenuItem33;
    private javax.swing.JMenuItem jMenuItem34;
    private javax.swing.JMenuItem jMenuItem37;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem40;
    private javax.swing.JMenuItem jMenuItem44;
    private javax.swing.JMenuItem jMenuItem45;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem51;
    private javax.swing.JMenuItem jMenuItem52;
    private javax.swing.JMenuItem jMenuItem53;
    private javax.swing.JMenuItem jMenuItem54;
    private javax.swing.JMenuItem jMenuItem55;
    private javax.swing.JMenuItem jMenuItem56;
    private javax.swing.JMenuItem jMenuItem57;
    private javax.swing.JMenuItem jMenuItem58;
    private javax.swing.JMenuItem jMenuItem59;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem60;
    private javax.swing.JMenuItem jMenuItem61;
    private javax.swing.JMenuItem jMenuItem62;
    private javax.swing.JMenuItem jMenuItem63;
    private javax.swing.JMenuItem jMenuItem64;
    private javax.swing.JMenuItem jMenuItem65;
    private javax.swing.JMenuItem jMenuItem66;
    private javax.swing.JMenuItem jMenuItem67;
    private javax.swing.JMenuItem jMenuItem68;
    private javax.swing.JMenuItem jMenuItem69;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JLabel lblAlerta;
    private javax.swing.JLabel lblInfo;
    protected static javax.swing.JLabel lblUserLog;
    private javax.swing.JMenu menuCaixa;
    private javax.swing.JMenu menuFiscal;
    private javax.swing.JToolBar.Separator separadorClientes;
    private javax.swing.JToolBar.Separator separadorConsulta;
    private javax.swing.JToolBar.Separator separadorEntrada;
    private javax.swing.JToolBar.Separator separadorPreVenda;
    private javax.swing.JToolBar.Separator separadorTabelaPreco;
    private javax.swing.JToolBar.Separator separadorVenda;
    // End of variables declaration//GEN-END:variables
    public static PrincipalPDV me;

    public static PrincipalPDV newInstance() {
        me = new PrincipalPDV();
        return me;
    }

    public static PrincipalPDV getInstance() {
        if (me == null) {
            me = new PrincipalPDV();
        }
        return me;
    }

    //Configura os componentes conforme o seu tipo de usuário
    private void carregaComponentesVisiveis() {
        if (!VariaveisDoSistema.INICIALIZA_MODO_FISCAL) {
            ControllerRegraTipoUsuario.getInstance().carregaTbComponenteVisivel();
            TipoUsuario tipoUsuario = VariaveisDoSistema.TIPO_USUARIO;

            btnConsulta.setVisible(ControllerRegraTipoUsuario.getInstance().getVisivel(tipoUsuario, "Consulta"));
            separadorConsulta.setVisible(ControllerRegraTipoUsuario.getInstance().getVisivel(tipoUsuario, "Consulta"));

            btnClientes.setVisible(ControllerRegraTipoUsuario.getInstance().getVisivel(tipoUsuario, "Clientes"));//Atalhos
            separadorClientes.setVisible(ControllerRegraTipoUsuario.getInstance().getVisivel(tipoUsuario, "Clientes"));//Atalhos

            btnEntrada.setVisible(ControllerRegraTipoUsuario.getInstance().getVisivel(tipoUsuario, "Entradas"));
            separadorEntrada.setVisible(ControllerRegraTipoUsuario.getInstance().getVisivel(tipoUsuario, "Entradas"));

            btnVenda.setVisible(ControllerRegraTipoUsuario.getInstance().getVisivel(tipoUsuario, "Venda"));
            separadorVenda.setVisible(ControllerRegraTipoUsuario.getInstance().getVisivel(tipoUsuario, "Venda"));

            btnPreVenda.setVisible(ControllerRegraTipoUsuario.getInstance().getVisivel(tipoUsuario, "Pré-Venda"));
            separadorPreVenda.setVisible(ControllerRegraTipoUsuario.getInstance().getVisivel(tipoUsuario, "Pré-Venda"));

            for (int i = 0; i < BarraDeMenus.getComponentCount(); i++) {
                if (BarraDeMenus.getComponent(i) instanceof JMenu) {
                    JMenu menu = (JMenu) BarraDeMenus.getComponent(i);
                    menu.setVisible(ControllerRegraTipoUsuario.getInstance().getVisivel(tipoUsuario, menu.getText()));//seta visibilidate menu
                    for (int imenu = 0; imenu < menu.getPopupMenu().getComponentCount(); imenu++) {
                        if (menu.getPopupMenu().getComponent(imenu) instanceof JMenuItem) {
                            JMenuItem item = (JMenuItem) menu.getPopupMenu().getComponent(imenu);
                            item.setVisible(ControllerRegraTipoUsuario.getInstance().getVisivel(tipoUsuario, item.getText()));//Seta visibilitade itemmenu
                            if (menu.getPopupMenu().getComponent(imenu) instanceof JMenu) {//Verifica se o sub menu é menu
                                JMenu subMenu1 = (JMenu) menu.getPopupMenu().getComponent(imenu);//Cria sub menu 1
                                for (int iSubMenu2 = 0; iSubMenu2 < subMenu1.getPopupMenu().getComponentCount(); iSubMenu2++) {//Sub menu2
                                    JMenuItem subMenu2 = (JMenuItem) subMenu1.getPopupMenu().getComponent(iSubMenu2);
                                    subMenu2.setVisible(ControllerRegraTipoUsuario.getInstance().getVisivel(tipoUsuario, subMenu2.getText()));
                                }

                            }
                        }
                    }
                }
            }
            jMenu1.setVisible(ControllerRegraTipoUsuario.getInstance().getVisivel(tipoUsuario, jMenu1.getName()));
        } else {
            jToolBar1.setVisible(false);
            for (int i = 0; i < BarraDeMenus.getComponentCount(); i++) {
                JMenu menu = (JMenu) BarraDeMenus.getComponent(i);
                if (!menu.getText().equals("Menu Fiscal") && !menu.getText().equals("Acesso")) {
                    menu.setVisible(false);
                }

            }
        }
        configuraSistemaParaVersao();
    }

    private void logoff() {
        setVisible(false);
        Login.newInstance().setVisible(true);
    }

    public void atualizaEstadoCaixa() {
        boolean b = VariaveisDoSistema.CAIXA == null ? false : VariaveisDoSistema.CAIXA.isAberto();
        itemAbrirCaixa.setEnabled(!b);
        itemCancelarVenda.setEnabled(b);
        itemReducaoZ.setEnabled(b);
        itemSangria.setEnabled(b);
        itemSuprimento.setEnabled(b);
        itemFilaPendentes.setEnabled(b);
        btnVenda.setEnabled(b);
        itemVenda.setEnabled(b);

        itemMenuTEF.setEnabled(b);
        btnPreVenda.setEnabled(b);
        itemMenuPreVenda.setEnabled(b);
        itemMenuServico.setEnabled(b);
        itemMenuEmitirNFE.setEnabled(b);
        itemMenuEmitirNfeVinculada.setEnabled(b);

        itemMenuTEF.setEnabled(b);
        btnPreVenda.setEnabled(b);
        itemMenuPreVenda.setEnabled(b);
        itemMenuServico.setEnabled(b);
        itemMenuEmitirNFE.setEnabled(b);
        itemMenuEmitirNfeVinculada.setEnabled(b);

    }

    public void novaVenda(final Cliente cliente) {
        if (VariaveisDoSistema.USUARIO.getColaborador() != null) {
            HibernateUtil.getInstance().refresh(VariaveisDoSistema.USUARIO);
            if (!Util.getData(Util.getData(VariaveisDoSistema.CAIXA.getDataAbertura())).before(Util.getData(Util.getData(new Date())))) {
                setComcomitante(EnumTipoVenda.CONCOMITANTE);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        HibernateUtil hu = HibernateUtil.getInstance();
                        try {
                            hu.getSession().refresh(VariaveisDoSistema.USUARIO);
                            if (VariaveisDoSistema.getEcfAtivo()) {
                                // usa calendar para subtrair data  
                                Calendar calendarData = Calendar.getInstance();
                                calendarData.setTime(VariaveisDoSistema.CAIXA.getDataAbertura());
                                int numeroDiasParaSubtrair = 1;
                                calendarData.add(Calendar.DATE, numeroDiasParaSubtrair);
                                Date dataCaixa = calendarData.getTime();
                                if (VariaveisDoSistema.CAIXA.isAberto() && dataCaixa.after(new Date())) {
                                    boolean permite = true;
                                    StringBuilder erro = new StringBuilder();
                                    // valida o serial do ECF
                                    try {
                                        ControllerPaf.getInstance().validarSerial(VariaveisDoSistema.AUXILIAR.getProperty("ecf.serie").split(";")[0]);
                                    } catch (ACBrException ex) {
                                        permite = false;
                                        log.error("Problemas ao validar o serial.", ex);
                                        erro.append(ex.getMessage()).append("\n");
                                    }

                                    // valida o GT do ECF
                                    try {
                                        double gt = Double.valueOf(VariaveisDoSistema.AUXILIAR.getProperty("ecf.gt").replace(",", "."));
                                        double novoGT = ControllerPaf.getInstance().validarGT(gt);
                                        if (novoGT > 0.00) {
                                            VariaveisDoSistema.AUXILIAR.setProperty("ecf.gt", Util.formataNumero(novoGT, 1, 2, false));
                                            CriptografiaUtil.getInstance().criptografar("conf/auxiliar.txt", VariaveisDoSistema.AUXILIAR);
                                        }
                                    } catch (Exception ex) {
                                        permite = false;
                                        log.error("Problemas ao validar o GT.", ex);
                                        erro.append(ex.getMessage()).append("\n");
                                    }
                                    if (!erro.toString().isEmpty()) {
                                        ExibirParaUsuario.getInstance().atencao(me, erro.toString());
                                    }
                                    if (permite) {
                                        if (VariaveisDoSistema.USUARIO.getColaborador() != null && VariaveisDoSistema.USUARIO.getColaborador().isVendedor()) {
                                            NovaVendaConcomitante.newInstance(cliente, (Colaborador) VariaveisDoSistema.USUARIO.getColaborador()).setVisible(true);
                                        } else {
                                            NovaVendaConcomitante.newInstance(cliente).setVisible(true);
                                        }
                                    }
                                } else {
                                    ExibirParaUsuario.getInstance().atencao(me, "Caixa aguardando redução Z.\nFeche o caixa, emita redução Z e abra novamente para finalizar os pedidos.");
                                }
                            } else {
                                if (VariaveisDoSistema.USUARIO.getColaborador() != null && VariaveisDoSistema.USUARIO.getColaborador().isVendedor()) {
                                    NovaVendaConcomitante.newInstance(cliente, (Colaborador) VariaveisDoSistema.USUARIO.getColaborador()).setVisible(true);
                                } else {
                                    NovaVendaConcomitante.newInstance(cliente).setVisible(true);
                                }
                            }
                        } catch (HibernateException ex) {
                            ExibirParaUsuario.getInstance().erro(me, ex, "Erro ao abrir nova venda!", false);
                            Aguarde.getInstance().setVisible(false);
                        }
                    }
                }).start();
                Aguarde.getInstance().getLblInfo().setText("Aguarde o processamento...");
                Aguarde.getInstance().setVisible(true);
            } else {
                ExibirParaUsuario.getInstance().atencao(me, "Aguardando Redução Z!");
            }
        } else {
            ExibirParaUsuario.getInstance().atencao(me, "Você não está vinculado a nenhum colaborador.\nPortando não pode efetuar operações comerciais.\nPara configurar contate o administrador do sistema!");
        }
    }

    private void leituraX() {
        if (VariaveisDoSistema.getEcfAtivo()) {
            Boolean escolha = ExibirParaUsuario.getInstance().confirmacao(me, "Deseja emitir a LeituraX?");
            if (escolha) {
                try {
                    ControllerPaf.getInstance().corrigeEstadoErro(true);
                    ControllerPaf.getInstance().leituraX();
                } catch (ACBrException ex) {
                    log.error("Não foi possivel emitir a LeituraX! -> ", ex);
                    ExibirParaUsuario.getInstance().erro(me, ex, "Não foi possível emitir a LeituraX!", false);
                }
            }
        } else {
            ExibirParaUsuario.getInstance().atencao(me, "Essa versão não possui funções da impressora!");
        }
    }

    private void lmf() {
        if (VariaveisDoSistema.getEcfAtivo()) {
            PAF_MF.newInstance().setComando(EComandoECF.ECF_IE);
            PAF_MF.getInstance().limpar();
            PAF_MF.getInstance().lblTitulo.setText("LMF");
            PAF_MF.getInstance().setVisible(true);
        } else {
            ExibirParaUsuario.getInstance().atencao(me, "Essa versão não possui funções da impressora!");
        }
    }

    private void arquivoMf() {
        if (VariaveisDoSistema.getEcfAtivo()) {
            PAF_MF.newInstance().setComando(EComandoECF.ECF_PafMf_Mfd_Espelho);
            PAF_MF.getInstance().limpar();
            PAF_MF.getInstance().lblTitulo.setText("ARQUIVO MF");
            PAF_MF.getInstance().setVisible(true);
        } else {
            ExibirParaUsuario.getInstance().atencao(me, "Essa versão não possui funções da impressora!");
        }
    }

    private void arquivoMfd() {
        if (VariaveisDoSistema.getEcfAtivo()) {
            PAF_MF.newInstance().setComando(EComandoECF.ECF_PafMf_Mfd_Cotepe1704);
            PAF_MF.getInstance().limpar();
            PAF_MF.getInstance().lblTitulo.setText("ARQUIVO MFD");
            PAF_MF.getInstance().setVisible(true);
        } else {
            ExibirParaUsuario.getInstance().atencao(me, "Essa versão não possui funções da impressora!");
        }
    }

    private void identificacaoPafEcf() {
        if (VariaveisDoSistema.getEcfAtivo()) {
            int escolha = JOptionPane.showOptionDialog(this, "Deseja emitir o relatório de identificação do PAF-ECF?", "Menu Fiscal",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, Util.OPCOES, JOptionPane.YES_OPTION);
            if (escolha == JOptionPane.YES_OPTION) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            PAF.emitirIdentificaoPAF(Util.getConfig().get("ecf.relpaf"));
                            new ControllerSalvarDocumento("RG", "0").executar();
                            Aguarde.getInstance().setVisible(false);
                        } catch (Exception ex) {
                            Aguarde.getInstance().setVisible(false);
                            log.error("Nao foi possivel emitir o relatorio de identificacao do paf! -> ", ex);
                            ExibirParaUsuario.getInstance().erro(me, ex, "Não foi possível emitir o Relatório!\nVerifique se o ECF está ativa e livre.", false);
                        }
                    }
                }).start();

                Aguarde.getInstance().setVisible(true);
            }
        } else {
            ExibirParaUsuario.getInstance().atencao(me, "Essa versão não possui funções da impressora!");
        }
    }

    private void novaNfe(final Cliente cliente) {
        if (VariaveisDoSistema.USUARIO.getColaborador() != null) {
            if (!Util.getData(Util.getData(VariaveisDoSistema.CAIXA.getDataAbertura())).before(Util.getData(Util.getData(new Date())))) {
                setComcomitante(EnumTipoVenda.NFE);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        HibernateUtil hu = HibernateUtil.getInstance();
                        try {
                            hu.getSession().refresh(VariaveisDoSistema.USUARIO);
                            if (VariaveisDoSistema.USUARIO.getColaborador() != null && VariaveisDoSistema.USUARIO.getColaborador().isVendedor()) {
                                NovaNotaFiscalEletronica.newInstance(cliente, (Colaborador) VariaveisDoSistema.USUARIO.getColaborador()).setVisible(true);
                            } else {
                                NovaNotaFiscalEletronica.newInstance(cliente).setVisible(true);
                            }
                        } catch (HibernateException ex) {
                            ExibirParaUsuario.getInstance().erro(me, ex, ex.getMessage(), false);
                        }
                    }
                }).start();
                Aguarde.getInstance().getLblInfo().setText("Aguarde o processamento...");
                Aguarde.getInstance().setVisible(true);
            } else {
                ExibirParaUsuario.getInstance().atencao(me, "Aguardando Redução Z!");
            }
        } else {
            ExibirParaUsuario.getInstance().atencao(me, "Você não está vinculado a nenhum colaborador.\nPortando não pode efetuar operações comerciais.\nPara configurar contate o administrador do sistema!");
        }
    }

    private void novaPreVenda(final Cliente cliente) {
        if (VariaveisDoSistema.USUARIO.getColaborador() != null) {
            if (!Util.getData(Util.getData(VariaveisDoSistema.CAIXA.getDataAbertura())).before(Util.getData(Util.getData(new Date())))) {
                setComcomitante(EnumTipoVenda.PRE_VENDA);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        HibernateUtil hu = HibernateUtil.getInstance();
                        try {
                            hu.getSession().refresh(VariaveisDoSistema.USUARIO);
                            if (VariaveisDoSistema.USUARIO.getColaborador() != null && VariaveisDoSistema.USUARIO.getColaborador().isVendedor()) {
                                NovaPreVenda.newInstance(cliente, (Colaborador) VariaveisDoSistema.USUARIO.getColaborador()).setVisible(true);
                            } else {
                                NovaPreVenda.newInstance(cliente).setVisible(true);
                            }
                        } catch (HibernateException ex) {
                            ExibirParaUsuario.getInstance().erro(me, ex, "Erro de conexão com a base de dados!", false);
                        }
                    }
                }).start();
                Aguarde.getInstance().getLblInfo().setText("Aguarde o processamento...");
                Aguarde.getInstance().setVisible(true);
            } else {
                ExibirParaUsuario.getInstance().atencao(me, "Aguardando Redução Z!");
            }
        } else {
            ExibirParaUsuario.getInstance().atencao(me, "Você não está vinculado a nenhum colaborador.\nPortando não pode efetuar operações comerciais.\nPara configurar contate o administrador do sistema!");
        }
    }

    public EnumTipoVenda getConcomitante() {
        return me.concomitante;
    }

    public void setComcomitante(EnumTipoVenda concomitante) {
        me.concomitante = concomitante;
    }

    private void configuraSistemaParaVersao() {
        //Se o sistema for de gestão
        if (!VariaveisDoSistema.getSistemaPdv()) {
            menuCaixa.setVisible(false);
            menuFiscal.setVisible(false);
            itemMenuRelatorioCaixa.setVisible(false);
            btnVenda.setVisible(false);
            itemVenda.setVisible(false);
        }
    }
}
