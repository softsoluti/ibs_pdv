/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.view.cadastro;

import br.com.cresceritsolutions.ibspdv.models.UI.DialogIBS;
import br.com.cresceritsolutions.ibspdv.util.CriptografiaUtil;
import br.com.cresceritsolutions.ibspdv.util.ExibirParaUsuario;
import br.com.cresceritsolutions.ibspdv.util.IbsException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author walsirene
 */
public class CadUsuarioAlteraSenha extends DialogIBS {

    private static CadUsuarioAlteraSenha me;

    public static CadUsuarioAlteraSenha getInstance() {
        if (me == null) {
            me = new CadUsuarioAlteraSenha();
        }
        return me;
    }

    public static CadUsuarioAlteraSenha newInstance() {
        me = new CadUsuarioAlteraSenha();
        return me;
    }

    private boolean alterado;

    /**
     * Creates new form int_AlteraSenha
     */
    public CadUsuarioAlteraSenha() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtSenhaAtual = new javax.swing.JPasswordField();
        txtNovaSenha = new javax.swing.JPasswordField();
        txtConfNovaSenha = new javax.swing.JPasswordField();
        lblNot = new javax.swing.JLabel();
        lblTitulo = new javax.swing.JLabel();
        jToolBar1 = new javax.swing.JToolBar();
        btnOk = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jLabel1.setText("Senha Atual");

        jLabel2.setText("Nova senha");

        jLabel3.setText("Confirma nova senha");

        txtSenhaAtual.setNextFocusableComponent(txtNovaSenha);
        txtSenhaAtual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSenhaAtualActionPerformed(evt);
            }
        });
        txtSenhaAtual.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtSenhaAtualFocusGained(evt);
            }
        });

        txtNovaSenha.setNextFocusableComponent(txtConfNovaSenha);
        txtNovaSenha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNovaSenhaActionPerformed(evt);
            }
        });
        txtNovaSenha.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtNovaSenhaFocusGained(evt);
            }
        });
        txtNovaSenha.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNovaSenhaKeyReleased(evt);
            }
        });

        txtConfNovaSenha.setNextFocusableComponent(btnOk);
        txtConfNovaSenha.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtConfNovaSenhaFocusGained(evt);
            }
        });
        txtConfNovaSenha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtConfNovaSenhaActionPerformed(evt);
            }
        });
        txtConfNovaSenha.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtConfNovaSenhaKeyReleased(evt);
            }
        });

        lblNot.setFont(new java.awt.Font("Tahoma", 2, 11)); // NOI18N
        lblNot.setForeground(new java.awt.Color(255, 0, 0));

        lblTitulo.setBackground(new java.awt.Color(255, 255, 255));
        lblTitulo.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        lblTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitulo.setText("ALTERAR SENHA");
        lblTitulo.setAutoscrolls(true);
        lblTitulo.setOpaque(true);

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        btnOk.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/checkmark_24x24.png"))); // NOI18N
        btnOk.setEnabled(false);
        btnOk.setFocusable(false);
        btnOk.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnOk.setNextFocusableComponent(txtSenhaAtual);
        btnOk.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOkActionPerformed(evt);
            }
        });
        jToolBar1.add(btnOk);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/close_24x24.png"))); // NOI18N
        jButton1.setToolTipText("Cancelar");
        jButton1.setFocusable(false);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtSenhaAtual)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(lblNot, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(83, 83, 83)
                                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtNovaSenha)
                            .addComponent(txtConfNovaSenha))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1))
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addComponent(lblTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, 256, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(lblTitulo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtSenhaAtual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtNovaSenha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtConfNovaSenha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblNot, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOkActionPerformed
        try {
            if (CadUsuario.getInstance().usuario.getSenha().equals(CriptografiaUtil.getInstance().encryptMD5(CadUsuario.getInstance().usuario.getLogin(), String.valueOf(txtSenhaAtual.getPassword())))) {
                if (!String.valueOf(txtNovaSenha.getPassword()).equals("")) {
                    if (Arrays.equals(txtNovaSenha.getPassword(), txtConfNovaSenha.getPassword())) {
                        me.alterado = true;
                        setVisible(false);
                    } else {
                        ExibirParaUsuario.getInstance().atencao(me, "A confirmação da senha está incorreta!");
                    }
                } else {
                    ExibirParaUsuario.getInstance().atencao(me, "Digite a nova senha!");
                }
            } else {
                ExibirParaUsuario.getInstance().atencao(me, "A senha atual não confere!");
            }
        } catch (IbsException | NoSuchAlgorithmException ex) {
            Logger.getLogger(CadUsuarioAlteraSenha.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnOkActionPerformed

    private void txtConfNovaSenhaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtConfNovaSenhaKeyReleased
        if (Arrays.equals(txtNovaSenha.getPassword(), txtConfNovaSenha.getPassword())) {
            btnOk.setEnabled(true);
        } else {
            btnOk.setEnabled(false);
        }
    }//GEN-LAST:event_txtConfNovaSenhaKeyReleased

    private void txtSenhaAtualFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtSenhaAtualFocusGained
        txtSenhaAtual.selectAll();
    }//GEN-LAST:event_txtSenhaAtualFocusGained

    private void txtNovaSenhaFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNovaSenhaFocusGained
        txtNovaSenha.selectAll();
    }//GEN-LAST:event_txtNovaSenhaFocusGained

    private void txtConfNovaSenhaFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtConfNovaSenhaFocusGained
        txtConfNovaSenha.selectAll();
    }//GEN-LAST:event_txtConfNovaSenhaFocusGained

    private void txtSenhaAtualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSenhaAtualActionPerformed
        txtNovaSenha.grabFocus();
    }//GEN-LAST:event_txtSenhaAtualActionPerformed

    private void txtNovaSenhaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNovaSenhaActionPerformed
        txtConfNovaSenha.grabFocus();
    }//GEN-LAST:event_txtNovaSenhaActionPerformed

    private void txtConfNovaSenhaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtConfNovaSenhaActionPerformed
        try {
            if (CadUsuario.getInstance().usuario.getSenha().equals(CriptografiaUtil.getInstance().encryptMD5(CadUsuario.getInstance().usuario.getLogin(), String.valueOf(txtSenhaAtual.getPassword())))) {
                if (!String.valueOf(txtNovaSenha.getPassword()).equals("")) {
                    if (Arrays.equals(txtNovaSenha.getPassword(), txtConfNovaSenha.getPassword())) {
                        me.alterado = true;
                        setVisible(false);
                    } else {
                        ExibirParaUsuario.getInstance().atencao(me, "A confirmação da senha está incorreta!");
                    }
                } else {
                    ExibirParaUsuario.getInstance().atencao(me, "Digite a nova senha!");
                }
            } else {
                ExibirParaUsuario.getInstance().atencao(me, "A senha atual não confere!");
            }
        } catch (IbsException | NoSuchAlgorithmException ex) {
            Logger.getLogger(CadUsuarioAlteraSenha.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_txtConfNovaSenhaActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        me.alterado = false;

    }//GEN-LAST:event_formWindowClosing

    private void txtNovaSenhaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNovaSenhaKeyReleased
        if (Arrays.equals(txtNovaSenha.getPassword(), txtConfNovaSenha.getPassword())) {
            btnOk.setEnabled(true);
        } else {
            btnOk.setEnabled(false);
        }
    }//GEN-LAST:event_txtNovaSenhaKeyReleased

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnOk;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JLabel lblNot;
    public javax.swing.JLabel lblTitulo;
    private javax.swing.JPasswordField txtConfNovaSenha;
    public javax.swing.JPasswordField txtNovaSenha;
    private javax.swing.JPasswordField txtSenhaAtual;
    // End of variables declaration//GEN-END:variables

    boolean alterado() {
        return this.alterado;
    }

}
