/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.view.relatorios;

import br.com.cresceritsolutions.ibspdv.controller.classes.Class_Imagens;
import br.com.cresceritsolutions.ibspdv.controller.classes.Class_Nome_Sistema;
import br.com.cresceritsolutions.ibspdv.models.UI.DialogIBS;
import br.com.cresceritsolutions.ibspdv.models.hb.Colaborador;
import br.com.cresceritsolutions.ibspdv.util.ExibirParaUsuario;
import br.com.cresceritsolutions.ibspdv.util.HibernateUtil;
import br.com.cresceritsolutions.ibspdv.util.IbsException;
import br.com.cresceritsolutions.ibspdv.util.Util;
import br.com.cresceritsolutions.ibspdv.util.VariaveisDoSistema;
import br.com.cresceritsolutions.ibspdv.view.localizadores.LocalizaVendedor;
import java.awt.Desktop;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JFrame;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRRuntimeException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.hibernate.HibernateException;

/**
 *
 * @author Gutem
 */
public class RelatorioComissaoColaborador extends DialogIBS {

    private static RelatorioComissaoColaborador me;

    public static RelatorioComissaoColaborador getInstance() {
        if (me == null) {
            me = new RelatorioComissaoColaborador();
        }
        return me;
    }

    public static RelatorioComissaoColaborador newInstance() {
        me = new RelatorioComissaoColaborador();
        return me;
    }

    /**
     * Creates new form RelatorioComissaoColaborador
     *
     */
    public RelatorioComissaoColaborador() {
        super();
        initComponents();
        cboDtIni.setSelectedItem(Util.getData(new Date()));
        cboDtFin.setSelectedItem(Util.getData(new Date()));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        txtVendedor = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jToolBar2 = new javax.swing.JToolBar();
        btnVendedor = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        cboDtFin = new br.com.cresceritsolutions.ibspdv.controller.classes.ComboBoxCalendar(true);
        cboDtIni = new br.com.cresceritsolutions.ibspdv.controller.classes.ComboBoxCalendar(true);
        jToolBar1 = new javax.swing.JToolBar();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        rdPadrao = new javax.swing.JRadioButton();
        rdHtml = new javax.swing.JRadioButton();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        txtVendedor.setEditable(false);
        txtVendedor.setFont(new java.awt.Font("Arial", 1, 11)); // NOI18N
        txtVendedor.setForeground(new java.awt.Color(0, 51, 204));
        txtVendedor.setBorder(null);
        txtVendedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtVendedorActionPerformed(evt);
            }
        });

        jLabel2.setText("Vendedor");

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Comissão Vendedor");
        jLabel1.setOpaque(true);

        jToolBar2.setFloatable(false);
        jToolBar2.setRollover(true);

        btnVendedor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/search_24x24.png"))); // NOI18N
        btnVendedor.setFocusable(false);
        btnVendedor.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnVendedor.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnVendedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVendedorActionPerformed(evt);
            }
        });
        jToolBar2.add(btnVendedor);

        jLabel4.setText("Data Final");

        jLabel3.setText("Data Inicial");

        cboDtFin.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00/00/0000" }));

        cboDtIni.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00/00/0000" }));
        cboDtIni.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboDtIniActionPerformed(evt);
            }
        });

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);
        jToolBar1.setToolTipText("Localizar");

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/search 32.png"))); // NOI18N
        jButton1.setFocusable(false);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/close_32x32.png"))); // NOI18N
        jButton2.setToolTipText("Cancelar");
        jButton2.setFocusable(false);
        jButton2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton2);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Formato do relatório"));

        buttonGroup2.add(rdPadrao);
        rdPadrao.setSelected(true);
        rdPadrao.setText("Padrão");

        buttonGroup2.add(rdHtml);
        rdHtml.setText("Html");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(rdPadrao)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rdHtml))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rdPadrao)
                    .addComponent(rdHtml)))
        );

        buttonGroup1.add(jRadioButton1);
        jRadioButton1.setSelected(true);
        jRadioButton1.setText("Sintético");

        buttonGroup1.add(jRadioButton2);
        jRadioButton2.setText("Analítico");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(txtVendedor, javax.swing.GroupLayout.DEFAULT_SIZE, 323, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jToolBar2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jRadioButton2)
                                .addGap(2, 2, 2))
                            .addComponent(jRadioButton1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cboDtIni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(cboDtFin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtVendedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jToolBar2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(cboDtIni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel4)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(cboDtFin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jToolBar1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jRadioButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jRadioButton2)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtVendedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtVendedorActionPerformed

    }//GEN-LAST:event_txtVendedorActionPerformed

    private void btnVendedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVendedorActionPerformed
        LocalizaVendedor.newInstance().setVisible(true);
        try {
            carregaVendedor(LocalizaVendedor.getInstance().colaborador);
        } catch (IbsException ex) {
            ExibirParaUsuario.getInstance().erro(me, ex, ex.getMessage(), false);
        }
    }//GEN-LAST:event_btnVendedorActionPerformed

    private void cboDtIniActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboDtIniActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cboDtIniActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (colaborador != null) {
            try {
                geraRelatorio();
            } catch (IbsException ex) {
                ExibirParaUsuario.getInstance().erro(me, ex, ex.getMessage(), false);
            }
        } else {
            ExibirParaUsuario.getInstance().atencao(me, "Selecione o vendedor!");
        }

    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnVendedor;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JComboBox cboDtFin;
    private javax.swing.JComboBox cboDtIni;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JRadioButton rdHtml;
    private javax.swing.JRadioButton rdPadrao;
    private javax.swing.JTextField txtVendedor;
    // End of variables declaration//GEN-END:variables
    private Colaborador colaborador;

    private void geraRelatorio() throws IbsException {
        try {
            // fill report
            Map<String, Object> map = new HashMap<>();
            map.put("NOME_SISTEMA", Class_Nome_Sistema.getNomeVersao());
            map.put("SUBREPORT_DIR", "./jasper/");
            map.put("DIR_LOGO", "./arquivos/logo_rel.jpg");
            map.put("DATA_INI", Util.getData(cboDtIni.getSelectedItem().toString()));
            map.put("DATA_FIN", Util.getData(cboDtFin.getSelectedItem().toString()));
            map.put("VENDEDOR", colaborador.getId());
            map.put("ID_EMPRESA", VariaveisDoSistema.EMPRESA.getId());
            JasperViewer jv;
            JasperPrint jp = null;
            InputStream fis = getClass().getClassLoader().getResourceAsStream("jasper/rel_ComissaoVendedor.jasper");

            BufferedInputStream bufferedInputStream = new BufferedInputStream(fis);
            // compile report
            JasperReport jasperReport = null;
            try {
                jasperReport = (JasperReport) JRLoader.loadObject(bufferedInputStream);
            } catch (JRException ex) {
                throw new IbsException("Erro ao carregar arquivo de relatório!", ex);
            }
            HibernateUtil hu = HibernateUtil.getInstance();

            try {
                jp = JasperFillManager.fillReport(jasperReport, map, hu.getConnection());
            } catch (JRException | SQLException ex) {
                throw new IbsException("Erro ao carregar arquivo de relatório!", ex);
            }

            // view report to UI
            // JasperViewer.viewReport(jp, false);
            if (!jp.getPages().isEmpty()) {
                if (rdHtml.isSelected()) {
                    String arquivo = "arquivos/Comissão Vendedor.html";
                    JasperExportManager.exportReportToHtmlFile(jp, arquivo);
                    Desktop.getDesktop().open(new File(arquivo));
                } else {
                    jv = new JasperViewer(jp, false);
                    jv.setExtendedState(JFrame.MAXIMIZED_BOTH);
                    jv.setIconImage(new Class_Imagens().getMarcaIcone().getImage());
                    jv.setTitle(Class_Nome_Sistema.getNomeVersao());
                    jv.setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
                    jv.setVisible(true);
                }
            } else {
                ExibirParaUsuario.getInstance().atencao(me, "Nenhum resultado encontrado com os parâmetros informados!");
            }
        } catch (JRRuntimeException | JRException ex) {
            throw new IbsException("Erro ao carregar relatório!", ex);
        } catch (IOException ex) {
            throw new IbsException("Erro ao carregar arquivo html!", ex);
        }
    }

    public void carregaVendedor(Colaborador colaborador) throws IbsException {
        HibernateUtil hu = HibernateUtil.getInstance();
        try {
            hu.refresh(colaborador);
            this.colaborador = colaborador;
            if (this.colaborador != null) {
                txtVendedor.setText(this.colaborador.getPessoaFisica().getNome());
            } else {
                txtVendedor.setText("");
            }
        } catch (HibernateException ex) {
            throw new IbsException("Erro ao carregar vendedor!", ex);
        }
    }
}
