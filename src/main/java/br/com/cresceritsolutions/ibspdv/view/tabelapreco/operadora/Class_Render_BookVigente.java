//Desenvolvedor : Gutemberg Lucas Vieira Lima
//Celular : (62)9112-0642
//Telefone : (62)3701-1466
//Email : ggutemberg@gmail.com/g_uto_lima@hotmail.com
package br.com.cresceritsolutions.ibspdv.view.tabelapreco.operadora;

import br.com.cresceritsolutions.ibspdv.util.CoresUtil;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * @author Ronald Tetsuo Miura
 */
public class Class_Render_BookVigente extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;
    /**
     *
     */
    private Font _fontePadrao = new Font("arial", Font.PLAIN, 12);
    private Font _fonteNegrito = new Font("Verdana", Font.BOLD, 12);

    /**
     * @see
     * javax.swing.table.TableCellRenderer#getTableCellRendererComponent(javax.swing.JTable,
     * java.lang.Object, boolean, boolean, int, int)
     */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
            boolean hasFocus, int row, int column) {

        Component c = super.getTableCellRendererComponent(
                table,
                value,
                isSelected,
                hasFocus,
                row,
                column);

        c.setFont(this._fontePadrao);

        if (table.getValueAt(row, 3).equals("1")) {
            c.setForeground(CoresUtil.getInstance().getBranco());
            c.setBackground(CoresUtil.getInstance().getVerdeMar());
        } else {
            c.setForeground(CoresUtil.getInstance().getPreto());
            c.setBackground(CoresUtil.getInstance().getBranco());
        }

        if (row == PesquisaCadPrecoFranquia.itsRow) {
            this.setForeground(CoresUtil.getInstance().getPreto());
            this.setBackground(CoresUtil.getInstance().getCinzaClaro());
        }

        if (isSelected) {
            if (column == 0) {
                c.setFont(_fonteNegrito);
                c.setForeground(CoresUtil.getInstance().getBranco());
                c.setBackground(CoresUtil.getInstance().getAzulClaro());
            } else {
                c.setFont(_fontePadrao);
                c.setForeground(CoresUtil.getInstance().getBranco());
                c.setBackground(CoresUtil.getInstance().getAzulClaro());
            }
        }
        return c;
    }
}
