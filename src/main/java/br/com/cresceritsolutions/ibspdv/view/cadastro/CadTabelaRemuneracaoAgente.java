/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.view.cadastro;

import br.com.cresceritsolutions.ibspdv.controller.classes.ComboBoxCalendar;
import br.com.cresceritsolutions.ibspdv.models.UI.DialogIBS;
import br.com.cresceritsolutions.ibspdv.models.UI.JTextIBS;
import br.com.cresceritsolutions.ibspdv.models.UI.JtextNumeroDouble;
import br.com.cresceritsolutions.ibspdv.models.hb.Classificacao;
import br.com.cresceritsolutions.ibspdv.models.hb.ItemTabelaRemuneracaoAgente;
import br.com.cresceritsolutions.ibspdv.models.hb.Plano;
import br.com.cresceritsolutions.ibspdv.models.hb.TabelaRemuneracaoAgente;
import br.com.cresceritsolutions.ibspdv.models.hb.TipoVenda;
import br.com.cresceritsolutions.ibspdv.renders.RenderPadraoRemuneracaoServico;
import br.com.cresceritsolutions.ibspdv.util.DimencionaTB;
import br.com.cresceritsolutions.ibspdv.util.ExibirParaUsuario;
import br.com.cresceritsolutions.ibspdv.util.HibernateUtil;
import br.com.cresceritsolutions.ibspdv.util.IbsException;
import br.com.cresceritsolutions.ibspdv.util.Util;
import br.com.cresceritsolutions.ibspdv.view.core.PrincipalPDV;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import javax.swing.DefaultCellEditor;
import javax.swing.table.DefaultTableModel;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Gutem
 */
public class CadTabelaRemuneracaoAgente extends DialogIBS {

    private static CadTabelaRemuneracaoAgente me;
    private TabelaRemuneracaoAgente tabelaRemuneracaoAgente = new TabelaRemuneracaoAgente();
    private List<EdicaoTabelaRemuneracaoAgente> lstEdicoes = new ArrayList<EdicaoTabelaRemuneracaoAgente>();

    public static CadTabelaRemuneracaoAgente getInstance() {
        if (me == null) {
            me = new CadTabelaRemuneracaoAgente();
        }
        return me;
    }

    public static CadTabelaRemuneracaoAgente newInstance() {
        me = new CadTabelaRemuneracaoAgente();
        return me;
    }
    private List<ItemTabelaRemuneracaoAgente> itens;

    /**
     * Creates new form ConfiguracaoRemuneracaoColaboradores
     */
    public CadTabelaRemuneracaoAgente() {
        initComponents();
        tbResultadoServico.setDefaultRenderer(Object.class, RenderPadraoRemuneracaoServico.getInstance());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbResultadoServico = new javax.swing.JTable();
        lblLinhas = new javax.swing.JLabel();
        jToolBar1 = new javax.swing.JToolBar();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jToolBar2 = new javax.swing.JToolBar();
        jLabel2 = new javax.swing.JLabel();
        txtPesquisa = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        txtDescricao = new JTextIBS(45);
        jLabel4 = new javax.swing.JLabel();
        cboDataVigencia = new ComboBoxCalendar(true);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Tabela de Remuneração do agente autorizado");
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel1.setOpaque(true);

        tbResultadoServico.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        tbResultadoServico.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Tipo", "Plano"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbResultadoServico.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tbResultadoServico.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbResultadoServicoMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbResultadoServico);
        if (tbResultadoServico.getColumnModel().getColumnCount() > 0) {
            tbResultadoServico.getColumnModel().getColumn(0).setResizable(false);
            tbResultadoServico.getColumnModel().getColumn(1).setResizable(false);
        }

        lblLinhas.setText("Linhas : 0");

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/save_24x24.png"))); // NOI18N
        jButton1.setToolTipText("Salvar alterações");
        jButton1.setFocusable(false);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/close_24x24.png"))); // NOI18N
        jButton2.setFocusable(false);
        jButton2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton2);

        jToolBar2.setBackground(new java.awt.Color(204, 204, 255));
        jToolBar2.setFloatable(false);
        jToolBar2.setRollover(true);

        jLabel2.setBackground(new java.awt.Color(204, 204, 255));
        jLabel2.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/servicoAgregado.png"))); // NOI18N
        jLabel2.setText("Serviços");
        jLabel2.setOpaque(true);
        jToolBar2.add(jLabel2);

        txtPesquisa.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        txtPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPesquisaActionPerformed(evt);
            }
        });
        txtPesquisa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPesquisaKeyReleased(evt);
            }
        });
        jToolBar2.add(txtPesquisa);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/search_24x24.png"))); // NOI18N
        jButton3.setToolTipText("Pesquisar");
        jButton3.setFocusable(false);
        jButton3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton3.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jToolBar2.add(jButton3);

        jLabel3.setText("Descrição");

        jLabel4.setText("Vigência");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 687, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jToolBar2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtDescricao)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(cboDataVigencia, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblLinhas, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboDataVigencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jToolBar2, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 345, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(lblLinhas)
                        .addGap(30, 30, 30))
                    .addComponent(jToolBar1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {
            if (valida()) {
                try {
                    salvar();
                    ExibirParaUsuario.getInstance().mensagem(PrincipalPDV.getInstance(), "Registro salvo com sucesso!", true);
                    PesquisaTabelaRemuneracaoAgenteAutorizado.getInstance().pesquisar();
                    getInstance().dispose();
                } catch (IbsException ex) {
                    ExibirParaUsuario.getInstance().erro(PrincipalPDV.getInstance(), ex, ex.getMessage(), false);
                }
            }
        } catch (IbsException ex) {
            ExibirParaUsuario.getInstance().erro(PrincipalPDV.getInstance(), ex, ex.getMessage(), false);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void tbResultadoServicoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbResultadoServicoMouseClicked

    }//GEN-LAST:event_tbResultadoServicoMouseClicked

    private void txtPesquisaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPesquisaKeyReleased

    }//GEN-LAST:event_txtPesquisaKeyReleased

    private void txtPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPesquisaActionPerformed
        loadTb();
        carregaDados();
        carregaEdicoes();
    }//GEN-LAST:event_txtPesquisaActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        loadTb();
        carregaDados();
        carregaEdicoes();
    }//GEN-LAST:event_jButton3ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox cboDataVigencia;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JLabel lblLinhas;
    private javax.swing.JTable tbResultadoServico;
    private javax.swing.JTextField txtDescricao;
    private javax.swing.JTextField txtPesquisa;
    // End of variables declaration//GEN-END:variables

    public CadTabelaRemuneracaoAgente novo() {
        tabelaRemuneracaoAgente = new TabelaRemuneracaoAgente();
        setTela();
        return me;
    }

    public CadTabelaRemuneracaoAgente editar(Object o) {
        HibernateUtil.getInstance().getSession().refresh(o);
        tabelaRemuneracaoAgente = (TabelaRemuneracaoAgente) o;
        setTela();
        return me;
    }

    private void setObjeto() {
        tabelaRemuneracaoAgente.setDescricao(txtDescricao.getText());
        tabelaRemuneracaoAgente.setVingencia(Util.getData(cboDataVigencia.getSelectedItem().toString()));
        txtPesquisa.setText("");
        loadTb();
        carregaDados();
        carregaEdicoes();
        HibernateUtil hu = HibernateUtil.getInstance();
        itens = new ArrayList<>();
        for (int j = 2; j < tbResultadoServico.getColumnCount(); j++) {
            Classificacao classificacao = (Classificacao) hu.getSession().createCriteria(Classificacao.class).add(Restrictions.eq("descricao", tbResultadoServico.getColumnName(j))).uniqueResult();
            for (int i = 0; i < tbResultadoServico.getRowCount(); i++) {
                ItemTabelaRemuneracaoAgente itra = new ItemTabelaRemuneracaoAgente();
                itra.setTabelaRemuneracaoAgente(tabelaRemuneracaoAgente);
                itra.setClassificacao(classificacao);
                itra.setTipoVenda((TipoVenda) tbResultadoServico.getValueAt(i, 0));
                itra.setPlano((Plano) tbResultadoServico.getValueAt(i, 1));
                itra.setValor(Double.parseDouble(tbResultadoServico.getValueAt(i, j).toString().replace(".", "").replace(",", ".")));
                itens.add(itra);
            }
        }
    }

    private void salvar() throws IbsException {
        setObjeto();
        HibernateUtil hu = HibernateUtil.getInstance();
        try {
            hu.beginTransaction();
            hu.getSession().saveOrUpdate(tabelaRemuneracaoAgente);
            Iterator<ItemTabelaRemuneracaoAgente> it = itens.iterator();
            while (it.hasNext()) {
                ItemTabelaRemuneracaoAgente iTemp = it.next();
                ItemTabelaRemuneracaoAgente itra = (ItemTabelaRemuneracaoAgente) hu.getSession().createCriteria(ItemTabelaRemuneracaoAgente.class)
                        .add(Restrictions.eq("tipoVenda", iTemp.getTipoVenda()))
                        .add(Restrictions.eq("plano", iTemp.getPlano()))
                        .add(Restrictions.eq("tabelaRemuneracaoAgente", iTemp.getTabelaRemuneracaoAgente()))
                        .add(Restrictions.eq("classificacao", iTemp.getClassificacao())).uniqueResult();
                if (itra == null) {
                    itra = iTemp;
                } else {
                    itra.setValor(iTemp.getValor());
                }
                hu.getSession().saveOrUpdate(itra);
            }
            hu.commit();
        } catch (HibernateException ex) {
            hu.rollback();
            throw new IbsException("Erro inesperado!", ex);
        }
    }

    private void setTela() {
        loadTb();
        carregaDados();
        carregaEdicoes();
        txtDescricao.grabFocus();
        txtDescricao.setText(tabelaRemuneracaoAgente.getDescricao());
        cboDataVigencia.setSelectedItem(Util.getData(tabelaRemuneracaoAgente.getVingencia()));
    }

    private boolean valida() throws IbsException {
        if (!txtDescricao.getText().isEmpty()) {
            if (!existeTabelaComDescricao(txtDescricao.getText())) {
                return true;
            } else {
                ExibirParaUsuario.getInstance().atencao(me, "Já possui uma tabela com essa descrição!");
            }
        } else {
            ExibirParaUsuario.getInstance().atencao(me, "Informe a descrição!");
        }

        return false;
    }

    private boolean existeTabelaComDescricao(String text) throws IbsException {
        if (tabelaRemuneracaoAgente.getId() != null) {
            return false;
        }
        HibernateUtil hu = HibernateUtil.getInstance();
        try {

            return hu.getSession().createCriteria(TabelaRemuneracaoAgente.class).add(Restrictions.eq("descricao", text)).uniqueResult() != null;
        } catch (HibernateException ex) {
            throw new IbsException("Erro ao localizar oferta!", ex);
        }
    }

    private void loadTb() {
        List<Object> descricaoColunas = new ArrayList<>();
        descricaoColunas.add("Tipo");
        descricaoColunas.add("Plano");
        HibernateUtil hu = HibernateUtil.getInstance();
        //Carrega Classificações
        List<Classificacao> lstClassificacao = hu.getSession().createCriteria(Classificacao.class).add(Restrictions.eq("ativo", true)).list();
        Collections.sort(lstClassificacao);
        Iterator<Classificacao> iClassificacao = lstClassificacao.iterator();
        while (iClassificacao.hasNext()) {
            Classificacao classificacao = iClassificacao.next();
            descricaoColunas.add(classificacao);
        }
        //Cria modelo da tabela
        DefaultTableModel dtm = new DefaultTableModel(new Object[][]{}, descricaoColunas.toArray()) {
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return columnIndex != 0 && columnIndex != 1;
            }
        };

        //Carreaga tipos e planos
        List<TipoVenda> lstTipoVenda = hu.getSession().createCriteria(TipoVenda.class).add(Restrictions.eq("ativo", true)).list();
        Collections.sort(lstTipoVenda);
        Iterator<TipoVenda> iTipoVenda = lstTipoVenda.iterator();
        while (iTipoVenda.hasNext()) {
            TipoVenda tipoVenda = iTipoVenda.next();
            List<Plano> lstPlano = new ArrayList<>(tipoVenda.getPlanos());
            Collections.sort(lstPlano);
            Iterator<Plano> iPlano = lstPlano.iterator();
            while (iPlano.hasNext()) {
                Plano plano = iPlano.next();
                //Filtro de pesquisa
                if (tipoVenda.toString().toUpperCase().contains(txtPesquisa.getText().toUpperCase()) || plano.toString().toUpperCase().contains(txtPesquisa.getText().toUpperCase())) {
                    Object[] objetos = new Object[dtm.getColumnCount()];
                    objetos[0] = tipoVenda;
                    objetos[1] = plano;
                    //Coloca 0,00 em todos os campos nulos
                    for (int i = 0; i < objetos.length; i++) {
                        if (i != 0 && i != 1) {
                            objetos[i] = "0,00";
                        }
                    }
                    dtm.addRow(objetos);
                }
            }
        }
        tbResultadoServico.setModel(dtm);
        //Aplica formato de texto nas celulas de edição
        final JtextNumeroDouble campoTexto = new JtextNumeroDouble();

        campoTexto.addKeyListener(new KeyAdapter() {

            @Override
            public void keyReleased(KeyEvent ke) {
                int coluna = tbResultadoServico.getSelectedColumn();
                int linha = tbResultadoServico.getSelectedRow();
                aplicaEdicao(campoTexto.getText(), linha, coluna);
            }

        });
//        campoTexto.addFocusListener(new FocusAdapter() {
//
//            @Override
//            public void focusLost(FocusEvent fe) {
//                int coluna = tbResultadoServico.getSelectedColumn();
//                int linha = tbResultadoServico.getSelectedRow();
//                aplicaEdicao(linha, coluna);
//            }
//
//        });
        for (int j = 2; j < tbResultadoServico.getColumnCount(); j++) {
            tbResultadoServico.getColumnModel().getColumn(j).setCellEditor(new DefaultCellEditor(campoTexto));
        }
        DimencionaTB.executar(tbResultadoServico);
        lblLinhas.setText("Linhas : " + dtm.getRowCount());

    }

    private void aplicaEdicao(String valor, int linha, int coluna) {
        HibernateUtil hu = HibernateUtil.getInstance();
        EdicaoTabelaRemuneracaoAgente etra = new EdicaoTabelaRemuneracaoAgente();

        etra.setClassificacao((Classificacao) hu.getSession().createCriteria(Classificacao.class).add(Restrictions.eq("descricao", tbResultadoServico.getColumnName(coluna))).uniqueResult());
        etra.setTipoVenda((TipoVenda) tbResultadoServico.getValueAt(linha, 0));
        etra.setPlano((Plano) tbResultadoServico.getValueAt(linha, 1));
        if (valor.isEmpty()) {
            valor = "0,00";
        }
        etra.setValor(Double.parseDouble(valor.replace(".", "").replace(",", ".")));

        Iterator<EdicaoTabelaRemuneracaoAgente> it = lstEdicoes.iterator();
        boolean contem = false;
        while (it.hasNext()) {
            EdicaoTabelaRemuneracaoAgente etra1 = it.next();
            if (etra1.getClassificacao().equals(etra.getClassificacao())
                    && etra1.getTipoVenda().equals(etra.tipoVenda)
                    && etra1.getPlano().equals(etra.plano)) {
                etra1.setValor(etra.getValor());
                contem = true;
                break;
            }
        }
        if (!contem) {
            lstEdicoes.add(etra);
        }

    }

    private void carregaDados() {
        HibernateUtil hu = HibernateUtil.getInstance();
        for (int j = 2; j < tbResultadoServico.getColumnCount(); j++) {
            Classificacao classificacao = (Classificacao) hu.getSession().createCriteria(Classificacao.class).add(Restrictions.eq("descricao", tbResultadoServico.getColumnName(j))).uniqueResult();
            for (int i = 0; i < tbResultadoServico.getRowCount(); i++) {
                TipoVenda tipoVenda = (TipoVenda) tbResultadoServico.getValueAt(i, 0);
                Plano plano = (Plano) tbResultadoServico.getValueAt(i, 1);
                tbResultadoServico.setValueAt(getDadoTb(tipoVenda, plano, classificacao), i, j);
            }
        }

    }

    private void carregaEdicoes() {
        HibernateUtil hu = HibernateUtil.getInstance();
        for (int j = 2; j < tbResultadoServico.getColumnCount(); j++) {
            Classificacao classificacao = (Classificacao) hu.getSession().createCriteria(Classificacao.class).add(Restrictions.eq("descricao", tbResultadoServico.getColumnName(j))).uniqueResult();
            for (int i = 0; i < tbResultadoServico.getRowCount(); i++) {
                TipoVenda tipoVenda = (TipoVenda) tbResultadoServico.getValueAt(i, 0);
                Plano plano = (Plano) tbResultadoServico.getValueAt(i, 1);
                String valor = getDadoEditados(tipoVenda, plano, classificacao);
                if (valor != null) {
                    tbResultadoServico.setValueAt(valor, i, j);
                }
            }
        }
    }

    private String getDadoTb(TipoVenda tipoVenda, Plano plano, Classificacao classificacao) {
        Iterator<ItemTabelaRemuneracaoAgente> it = tabelaRemuneracaoAgente.getItemTabelaRemuneracaoAgentes().iterator();
        while (it.hasNext()) {
            ItemTabelaRemuneracaoAgente itra = it.next();
            if (itra.getTipoVenda().equals(tipoVenda)
                    && itra.getPlano().equals(plano)
                    && itra.getClassificacao().equals(classificacao)) {
                return Util.getFormatoMoeda(itra.getValor()).replace("R$ ", "");
            }
        }
        return "0,00";
    }

    private String getDadoEditados(TipoVenda tipoVenda, Plano plano, Classificacao classificacao) {
        Iterator<EdicaoTabelaRemuneracaoAgente> it = lstEdicoes.iterator();
        while (it.hasNext()) {
            EdicaoTabelaRemuneracaoAgente itra = it.next();
            if (itra.getTipoVenda().equals(tipoVenda)
                    && itra.getPlano().equals(plano)
                    && itra.getClassificacao().equals(classificacao)) {
                return Util.getFormatoMoeda(itra.getValor()).replace("R$ ", "");
            }
        }
        return null;
    }

    private static class EdicaoTabelaRemuneracaoAgente {

        TipoVenda tipoVenda;
        Plano plano;
        Classificacao classificacao;
        Double valor;

        public Classificacao getClassificacao() {
            return classificacao;
        }

        public void setClassificacao(Classificacao classificacao) {
            this.classificacao = classificacao;
        }

        public Double getValor() {
            return valor;
        }

        public void setValor(Double valor) {
            this.valor = valor;
        }

        public EdicaoTabelaRemuneracaoAgente() {

        }

        public TipoVenda getTipoVenda() {
            return tipoVenda;
        }

        public void setTipoVenda(TipoVenda tipoVenda) {
            this.tipoVenda = tipoVenda;
        }

        public Plano getPlano() {
            return plano;
        }

        public void setPlano(Plano plano) {
            this.plano = plano;
        }

    }
}
