/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.renders;

import br.com.cresceritsolutions.ibspdv.models.UI.FonteIBS;
import br.com.cresceritsolutions.ibspdv.models.enuns.Fontes;
import br.com.cresceritsolutions.ibspdv.util.CoresUtil;
import br.com.cresceritsolutions.ibspdv.models.hb.ItemCompra;
import java.awt.Component;
import java.awt.Font;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Gutem
 */
public class RenderConfiguracaPrecoFranquia extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;
    private static RenderConfiguracaPrecoFranquia me;
    public List<ItemCompra> listaDeItens;

    public static RenderConfiguracaPrecoFranquia getInstance() {
        if (me == null) {
            me = new RenderConfiguracaPrecoFranquia();
        }
        return me;
    }

    /**
     *
     */
    private Font _fontePadrao = new FonteIBS();
    private Font _fonteNegrito = new Font(Fontes.VERDANA.toString(), Font.BOLD, 12);

    /**
     * @see
     * javax.swing.table.TableCellRenderer#getTableCellRendererComponent(javax.swing.JTable,
     * java.lang.Object, boolean, boolean, int, int)
     */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
            boolean hasFocus, int row, int column) {

        Component c = super.getTableCellRendererComponent(
                table,
                value,
                isSelected,
                hasFocus,
                row,
                column);

        c.setFont(this._fontePadrao);

        if (table.getValueAt(row, column).equals("")) {
                c.setForeground(CoresUtil.getInstance().getBranco());
                c.setBackground(CoresUtil.getInstance().getVermelhoClaro());
            } else {
                if (column == 0) {
                    c.setFont(_fonteNegrito);
                    c.setForeground(CoresUtil.getInstance().getPreto());
                    c.setBackground(CoresUtil.getInstance().getCinza());
                } else {
                    c.setForeground(CoresUtil.getInstance().getPreto());
                    c.setBackground(CoresUtil.getInstance().getBranco());
                }
            }


            if (isSelected) {
                if (column == 0) {
                    c.setFont(_fonteNegrito);
                    c.setForeground(CoresUtil.getInstance().getBranco());
                    c.setBackground(CoresUtil.getInstance().getAzulClaro());
                } else {
                    c.setFont(_fontePadrao);
                    c.setForeground(CoresUtil.getInstance().getBranco());
                    c.setBackground(CoresUtil.getInstance().getAzulClaro());
                }
            }

        return c;
    }
}
