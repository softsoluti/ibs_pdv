/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.renders;

import br.com.cresceritsolutions.ibspdv.util.CoresUtil;
import br.com.cresceritsolutions.ibspdv.view.localizadores.LocalizaImei;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Gutem
 */
public class RenderLocalizaImei extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;
    private static RenderLocalizaImei me;

    public static RenderLocalizaImei getInstance() {
        if (me == null) {
            me = new RenderLocalizaImei();
        }
        return me;
    }

    /**
     *
     */
    private Font _fontePadrao = new Font("arial", Font.PLAIN, 12);
    private Font _fonteNegrito = new Font("Verdana", Font.BOLD, 12);

    /**
     * @see
     * javax.swing.table.TableCellRenderer#getTableCellRendererComponent(javax.swing.JTable,
     * java.lang.Object, boolean, boolean, int, int)
     */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
            boolean hasFocus, int row, int column) {

        Component c = super.getTableCellRendererComponent(
                table,
                value,
                isSelected,
                hasFocus,
                row,
                column);

        c.setFont(this._fontePadrao);

        //altera a aparencia do componente baseado no valor da célula (value)   
        Color verdeClaro = new Color(0, 150, 0);
        Color preto = new Color(0, 0, 0);
        Color branco = new Color(255, 255, 255);
        Color vermelho = new Color(255, 0, 0);
        Color cinza = new Color(200, 200, 200);
        Color Azul = new Color(50, 50, 200);
        if (LocalizaImei.getInstance().maisAntigo == Integer.parseInt(table.getValueAt(row, 1).toString())) {
            c.setForeground(vermelho);
            if ((row % 2) == 0) {
                c.setBackground(CoresUtil.getInstance().getZebrado());
            } else {
                c.setBackground(CoresUtil.getInstance().getBranco());
            }
        } else {
            c.setForeground(preto);
            if ((row % 2) == 0) {
                c.setBackground(CoresUtil.getInstance().getZebrado());
            } else {
                c.setBackground(CoresUtil.getInstance().getBranco());
            }
        }

        if (isSelected) {
            c.setFont(_fontePadrao);
            c.setForeground(branco);
            c.setBackground(Azul);
        }

        return c;
    }
}
