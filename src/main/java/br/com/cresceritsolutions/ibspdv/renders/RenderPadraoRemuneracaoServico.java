/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.renders;

import br.com.cresceritsolutions.ibspdv.models.UI.FonteIBS;
import br.com.cresceritsolutions.ibspdv.models.enuns.Fontes;
import br.com.cresceritsolutions.ibspdv.util.CoresUtil;
import br.com.cresceritsolutions.ibspdv.models.hb.ItemCompra;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Gutem
 */
public class RenderPadraoRemuneracaoServico extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;
    private static RenderPadraoRemuneracaoServico me;
    public List<ItemCompra> listaDeItens;

    public static RenderPadraoRemuneracaoServico getInstance() {
        if (me == null) {
            me = new RenderPadraoRemuneracaoServico();
        }
        return me;
    }

    /**
     *
     */
    private final Font _fontePadrao = new FonteIBS();
    private final Font _fonteNegrito = new Font(Fontes.VERDANA.toString(), Font.BOLD, 12);

    /**
     * @see
     * javax.swing.table.TableCellRenderer#getTableCellRendererComponent(javax.swing.JTable,
     * java.lang.Object, boolean, boolean, int, int)
     */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
            boolean hasFocus, int row, int column) {

        Component c = super.getTableCellRendererComponent(
                table,
                value,
                isSelected,
                hasFocus,
                row,
                column);

        c.setFont(this._fontePadrao);

        //altera a aparência do componente baseado no valor da célula (value)   
        if ((row % 2) == 0) {
            c.setBackground(CoresUtil.getInstance().getZebrado());
        } else {
            c.setBackground(CoresUtil.getInstance().getBranco());
        }
        if (table.getValueAt(row, column).equals("0,00")||table.getValueAt(row, column).equals("R$ 0,00")) {
            c.setForeground(CoresUtil.getInstance().getVermelho());
        } else {
            c.setForeground(CoresUtil.getInstance().getPreto());
        }
        if (isSelected) {
            c.setFont(_fontePadrao);
            c.setForeground(CoresUtil.getInstance().getBranco());
            c.setBackground(CoresUtil.getInstance().getAzulClaro());
        }

        return c;
    }
}
