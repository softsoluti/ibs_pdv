/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cresceritsolutions.ibspdv.renders;

import br.com.cresceritsolutions.ibspdv.models.enuns.Fontes;
import br.com.cresceritsolutions.ibspdv.models.enuns.SituacaoVenda;
import br.com.cresceritsolutions.ibspdv.util.CoresUtil;
import br.com.cresceritsolutions.ibspdv.models.hb.ItemCompra;
import br.com.cresceritsolutions.ibspdv.models.hb.Venda;
import br.com.cresceritsolutions.ibspdv.models.hb.VendaServico;
import br.com.cresceritsolutions.ibspdv.view.caixa.CancelaVenda;
import br.com.cresceritsolutions.ibspdv.view.venda.CancelarVenda;
import java.awt.Component;
import java.awt.Font;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Gutem
 */
public class RenderPadraoCancelaVenda extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;
    private static RenderPadraoCancelaVenda me;
    public List<ItemCompra> listaDeItens;

    public static RenderPadraoCancelaVenda getInstance() {
        if (me == null) {
            me = new RenderPadraoCancelaVenda();
        }
        return me;
    }

    /**
     *
     */
    private final Font _fontePadrao = new Font(Fontes.ARIAL.toString(), Font.PLAIN, 12);
    private final Font _fonteNegrito = new Font(Fontes.ARIAL.toString(), Font.BOLD, 12);
    private final Font _fonteItalico = new Font(Fontes.ARIAL.toString(), Font.ITALIC, 12);

    /**
     * @see
     * javax.swing.table.TableCellRenderer#getTableCellRendererComponent(javax.swing.JTable,
     * java.lang.Object, boolean, boolean, int, int)
     */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
            boolean hasFocus, int row, int column) {

        Component c = super.getTableCellRendererComponent(
                table,
                value,
                isSelected,
                hasFocus,
                row,
                column);

        c.setFont(this._fontePadrao);

        //altera a aparência do componente baseado no valor da célula (value)   
        if ((row % 2) == 0) {
            c.setBackground(CoresUtil.getInstance().getZebrado());
        } else {
            c.setBackground(CoresUtil.getInstance().getBranco());
        }
        c.setForeground(CoresUtil.getInstance().getPreto());
        if (CancelarVenda.getInstance().dados.get(row) instanceof Venda) {
            Venda v = (Venda) CancelarVenda.getInstance().dados.get(row);
            if (v.getSituacao() != SituacaoVenda.VENDIDO.getId()) {
                c.setForeground(CoresUtil.getInstance().getVermelho());
                c.setFont(_fonteItalico);
            }
        } else {
            VendaServico v = (VendaServico) CancelarVenda.getInstance().dados.get(row);
            if (v.getSituacao() != SituacaoVenda.VENDIDO.getId()) {
                c.setForeground(CoresUtil.getInstance().getVermelho());
                c.setFont(_fonteItalico);
            }
        }

        if (isSelected) {
            c.setFont(_fontePadrao);
            c.setForeground(CoresUtil.getInstance().getBranco());
            c.setBackground(CoresUtil.getInstance().getAzulClaro());
        }

        return c;
    }
}
