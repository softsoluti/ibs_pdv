package jACBrFramework.serial.ecf;

public class Aliquota {
    //<editor-fold defaultstate="collapsed" desc="Fields">

    private String indice;
    private double aliquota;
    private boolean tipo;
    private double total;
    private boolean sequencia;

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Properties">
    public Aliquota(String indice, double aliquota, boolean tipo, double total, boolean sequencia) {
        this.indice = indice;
        this.aliquota = aliquota;
        this.tipo = tipo;
        this.total = total;
        this.sequencia = sequencia;
    }

    public String getIndice() {
        return indice;
    }

    public double getAliquota() {
        return aliquota;
    }

    public boolean getTipo() {
        return tipo;
    }


    public double getTotal() {
        return total;
    }

    public boolean getSequencia() {
        return sequencia;
    }


    //</editor-fold>
}
